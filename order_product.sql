-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2018 at 03:05 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lazania_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, 'ellelazena_x7Rbva', 1, 1, 1, '2018-01-01 17:33:18', '2018-01-01 17:33:18'),
(2, 'ellelazena_x7Rbva', 2, 1, 1, '2018-01-01 17:33:18', '2018-01-01 17:33:18'),
(3, 'ellelazena_5tFPtT', 1, 2, 1, '2018-01-01 18:25:47', '2018-01-01 18:25:47'),
(4, 'ellelazena_5tFPtT', 2, 2, 1, '2018-01-01 18:25:47', '2018-01-01 18:25:47'),
(5, 'ellelazena_U4R0eQ', 1, 4, 1, '2018-01-01 18:26:45', '2018-01-01 18:26:45'),
(6, 'ellelazena_U4R0eQ', 2, 4, 1, '2018-01-01 18:26:45', '2018-01-01 18:26:45'),
(7, 'ellelazena_781KBG', 5, 1, 1, '2018-01-01 22:03:57', '2018-01-01 22:03:57'),
(8, 'ellelazena_781KBG', 4, 1, 1, '2018-01-01 22:03:57', '2018-01-01 22:03:57'),
(9, 'ellelazena_781KBG', 3, 1, 1, '2018-01-01 22:03:57', '2018-01-01 22:03:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
