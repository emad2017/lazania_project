-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2018 at 03:05 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lazania_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `e_addresses`
--

CREATE TABLE `e_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `order_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gender` enum('male','female','','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `e_addresses`
--

INSERT INTO `e_addresses` (`id`, `customer_id`, `order_id`, `fname`, `lname`, `street`, `city`, `country`, `created_at`, `updated_at`, `gender`, `telephone`) VALUES
(1, 1, 'ellelazena_x7Rbva', 'emad', 'rashad', 'Ibrahim al moghazy KFS', NULL, 'SA', '2018-01-01 17:33:17', '2018-01-01 17:33:17', 'male', '010265903031'),
(2, 1, 'ellelazena_5tFPtT', 'emad', 'rashad', 'Ibrahim al moghazy KFS', NULL, 'SA', '2018-01-01 18:25:47', '2018-01-01 18:25:47', 'male', '010265903031'),
(3, 1, 'ellelazena_U4R0eQ', 'Mona', 'Ebrahim', 'King Abdul Aziz Road - Jasmine District', NULL, 'SA', '2018-01-01 18:26:45', '2018-01-01 18:26:45', 'male', '5552112254'),
(4, 1, 'ellelazena_781KBG', 'Ahmed', 'Abdlatif', 'Testing address', NULL, 'SA', '2018-01-01 22:03:57', '2018-01-01 22:03:57', 'male', '88888555666');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `e_addresses`
--
ALTER TABLE `e_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_addresses_customer_id_foreign` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `e_addresses`
--
ALTER TABLE `e_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
