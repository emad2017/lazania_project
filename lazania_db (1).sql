-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2017 at 10:17 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lazania_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `timestart` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeend` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `master_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kitchen_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `since_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tele` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `related_to` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `timestart`, `timeend`, `class_num`, `staff_num`, `master_num`, `kitchen_num`, `telephone1`, `info_img`, `desc_img`, `since_year`, `email`, `tele`, `facebook`, `twitter`, `google`, `status`, `related_to`, `created_at`, `updated_at`) VALUES
(1, '10 PM', '10 AM', '4', '6', '12', '5', '0554334390', NULL, NULL, '1995', 'contact@lazania.com', '011-4540547', 'https://www.facebook.com/ellelazena/', 'https://twitter.com/ellelazena', 'http://ellelazena.com/', 'active', 1, '2017-11-16 06:51:29', '2017-12-21 20:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `about_descriptions`
--

CREATE TABLE `about_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_descriptions`
--

INSERT INTO `about_descriptions` (`id`, `about_id`, `language_id`, `name`, `description`, `about`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'حلويات إللازينه', 'وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp; وصف لمحتوي الموقع&nbsp;', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.', 'طريق الملك عبد العزيز - حي الياسمين', '2017-11-16 06:51:29', '2017-12-21 16:09:48'),
(2, 1, 2, 'EllelaZena', 'website Description&nbsp; website Description&nbsp; website Description website Description website Description website Description', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. &nbsp;', 'King Abdul Aziz Road - Jasmine District', '2017-11-16 06:51:29', '2017-12-21 20:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_departments`
--

CREATE TABLE `blog_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_department_descriptions`
--

CREATE TABLE `blog_department_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_descriptions`
--

CREATE TABLE `blog_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_descriptions`
--

INSERT INTO `blog_descriptions` (`id`, `name`, `description`, `blog_id`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'ليبل', 'سبشيسش', 1, 1, '2017-11-21 04:18:05', '2017-11-21 04:18:05'),
(2, NULL, NULL, 1, 2, '2017-11-21 04:18:05', '2017-11-21 04:18:05'),
(3, 'سؤسيرب', 'بسيرشبسير', 2, 1, '2017-11-21 04:19:00', '2017-11-21 04:19:00'),
(4, 'zcvxdzc', 'fdsfds', 2, 2, '2017-11-21 04:19:00', '2017-11-21 04:19:00'),
(5, 'سيبسيب', 'سيشبيبشي', 3, 1, '2017-11-21 04:19:48', '2017-11-21 04:19:48'),
(6, NULL, NULL, 3, 2, '2017-11-21 04:19:48', '2017-11-21 04:19:48'),
(7, 'شيسبيسبيشس', 'شسيبيسش', 4, 1, '2017-11-21 04:20:31', '2017-11-21 04:20:31'),
(8, 'sdzfdsf', 'dsfsdf', 4, 2, '2017-11-21 04:20:31', '2017-11-21 04:20:31'),
(9, 'sdafdsfd', 'sdafsdf', 5, 1, '2017-11-21 04:20:52', '2017-11-21 04:20:52'),
(10, NULL, NULL, 5, 2, '2017-11-21 04:20:52', '2017-11-21 04:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `contact_with_u_s`
--

CREATE TABLE `contact_with_u_s` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_with_u_s`
--

INSERT INTO `contact_with_u_s` (`id`, `name`, `subject`, `email`, `msg`, `created_at`, `updated_at`) VALUES
(1, 'dsfgfsdf', 'fdgfdg', 'emadtab833@gmail.comeelancer2016', 'fdgfdgdfdgfgfd', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `desert_sub_departments`
--

CREATE TABLE `desert_sub_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `desert_sub_departments`
--

INSERT INTO `desert_sub_departments` (`id`, `image`, `status`, `parent`, `addby_id`, `created_at`, `updated_at`) VALUES
(1, 'OQMIVuRN1oK22NgS_lazania.jpg', 'active', 1, 1, '2017-12-23 13:08:12', '2017-12-23 13:08:12'),
(2, 'dTQ0HoHL2IeT0yrr_lazania.jpg', 'active', 1, 1, '2017-12-23 13:09:31', '2017-12-23 13:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `desert_sub_department_descriptions`
--

CREATE TABLE `desert_sub_department_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `subDeptId` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `desert_sub_department_descriptions`
--

INSERT INTO `desert_sub_department_descriptions` (`id`, `name`, `description`, `subDeptId`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'اختبار تصنيف فرعى', 'وصف&nbsp;', 1, 1, '2017-12-23 13:08:12', '2017-12-23 13:08:12'),
(2, 'Test Sub Department', 'this is a description', 1, 2, '2017-12-23 13:08:12', '2017-12-23 13:08:12'),
(3, 'تصنيف فرعى ثانى', 'وصف هذا التصنيف', 2, 1, '2017-12-23 13:09:31', '2017-12-23 13:09:31'),
(4, 'Test Sub department two', 'this is description', 2, 2, '2017-12-23 13:09:31', '2017-12-23 13:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `dessert_departments`
--

CREATE TABLE `dessert_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `img1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dessert_departments`
--

INSERT INTO `dessert_departments` (`id`, `img1`, `icon`, `status`, `addby_id`, `created_at`, `updated_at`) VALUES
(1, 'UdNACuOQQil4WkMX_lazania.jpeg', 'chocolate.png', 'active', 1, '2017-12-23 12:45:14', '2017-12-23 12:45:14'),
(2, 'KE4qc0rr9DVFjyQx_lazania.jpg', 'arabic-sweets.png', 'active', 1, '2017-12-23 12:51:07', '2017-12-23 12:51:07'),
(3, 'sbl1D4x20EMA1VlG_lazania.jpeg', 'cake.png', 'active', 1, '2017-12-23 12:52:19', '2017-12-23 12:52:19'),
(4, 'nHBZTMw466gYxO8R_lazania.jpg', 'kunafa-icon2.png', 'active', 1, '2017-12-23 12:55:58', '2017-12-23 12:55:58'),
(6, 'OOolhat2pdf7hi7q_lazania.jpg', 'ice-cream.png', 'active', 1, '2017-12-24 14:17:52', '2017-12-24 14:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `dessert_department_descriptions`
--

CREATE TABLE `dessert_department_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dessert_department_descriptions`
--

INSERT INTO `dessert_department_descriptions` (`id`, `name`, `description`, `department_id`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'الشيكولاته', 'وصف الشيكولاته', 1, 1, '2017-12-23 12:45:14', '2017-12-23 12:45:14'),
(2, 'Chocolate', 'chocolate description', 1, 2, '2017-12-23 12:45:14', '2017-12-23 12:45:14'),
(3, 'حلوى عربيه', 'وصف محتوى الحلويات العربيه', 2, 1, '2017-12-23 12:51:07', '2017-12-23 12:51:07'),
(4, 'Arabic Sweets', 'arabic sweets description', 2, 2, '2017-12-23 12:51:07', '2017-12-23 12:51:07'),
(5, 'الكيك', 'وصف محتوى الكيك', 3, 1, '2017-12-23 12:52:20', '2017-12-23 12:52:20'),
(6, 'Cake', 'cake department description', 3, 2, '2017-12-23 12:52:20', '2017-12-23 12:52:20'),
(7, 'الطعام العربى والسلطات', 'وصف محتوى هذا القسم', 4, 1, '2017-12-23 12:55:58', '2017-12-23 12:55:58'),
(8, 'Arabic food & salads', 'department description', 4, 2, '2017-12-23 12:55:58', '2017-12-23 12:55:58'),
(11, 'ايس كريم', 'وصف الايس كريم', 6, 1, '2017-12-24 14:17:52', '2017-12-24 14:17:52'),
(12, 'Ice Cream', 'Ice cream description', 6, 2, '2017-12-24 14:17:52', '2017-12-24 14:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `dessert_items`
--

CREATE TABLE `dessert_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `img1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `show_in_slider` tinyint(4) NOT NULL DEFAULT '0',
  `best_selling` tinyint(4) NOT NULL DEFAULT '0',
  `promoted` tinyint(4) NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED DEFAULT NULL,
  `recommended` tinyint(4) NOT NULL DEFAULT '0',
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `subDeptId` int(10) UNSIGNED DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dessert_items`
--

INSERT INTO `dessert_items` (`id`, `img1`, `img2`, `img3`, `rate`, `price`, `show_in_slider`, `best_selling`, `promoted`, `views`, `recommended`, `facebook`, `twitter`, `google`, `status`, `subDeptId`, `department_id`, `addby_id`, `created_at`, `updated_at`) VALUES
(1, 'pMDb4JZOd9RRML9r_lazania.jpg', NULL, NULL, 5, 210, 0, 1, 0, 104, 1, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:13:35', '2017-12-24 14:56:37'),
(2, 'Gxwkepxb6v6LJgfW_lazania.jpg', NULL, NULL, 4, 343, 0, 1, 0, 155, 1, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:19:07', '2017-12-24 14:21:02'),
(3, 'pxpjNxIB9q5bt0JR_lazania.jpg', NULL, NULL, 5, 234, 1, 0, 0, 2, 0, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:22:37', '2017-12-25 04:24:00'),
(4, 'IF8lHu4vcupOJ5ax_lazania.jpg', NULL, NULL, 5, 546, 1, 0, 0, 2, 0, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:23:44', '2017-12-24 14:40:17'),
(5, '4e5BrPyNm0EwUjRx_lazania.jpg', NULL, NULL, 5, 654, 1, 0, 0, 1, 0, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:24:35', '2017-12-25 04:24:52'),
(6, 'ZetRQFzrsx0t65Nk_lazania.jpeg', NULL, NULL, 5, 324, 0, 0, 1, 1, 0, NULL, NULL, NULL, 'active', 1, NULL, 1, '2017-12-23 13:47:10', '2017-12-25 04:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `dessert_item_descriptions`
--

CREATE TABLE `dessert_item_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dessert_item_descriptions`
--

INSERT INTO `dessert_item_descriptions` (`id`, `name`, `description`, `item_id`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'منتج رقم واحد شوكلاته', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 1, 1, '2017-12-23 13:13:35', '2017-12-23 13:13:35'),
(2, 'Product number once chocolate', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 1, 2, '2017-12-23 13:13:35', '2017-12-23 13:13:35'),
(3, 'منتج رقم اثنان  شوكلاته', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 2, 1, '2017-12-23 13:19:07', '2017-12-23 13:19:07'),
(4, 'Number two chocolate product', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 2, 2, '2017-12-23 13:19:07', '2017-12-23 13:19:07'),
(5, 'منتج سلايدر رقم واحد', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 3, 1, '2017-12-23 13:22:37', '2017-12-23 13:22:37'),
(6, 'Slider product number one', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 3, 2, '2017-12-23 13:22:37', '2017-12-23 13:22:37'),
(7, 'منتج سلايدر رقم اثنان', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 4, 1, '2017-12-23 13:23:44', '2017-12-23 13:23:44'),
(8, 'Slider product two', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 4, 2, '2017-12-23 13:23:44', '2017-12-23 13:23:44'),
(9, 'منتج سلايدر رقم ثلاث', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 5, 1, '2017-12-23 13:24:35', '2017-12-23 13:24:35'),
(10, 'Slider product three', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 5, 2, '2017-12-23 13:24:35', '2017-12-23 13:24:35'),
(11, 'اكثر منتج مشاهده ومتميز', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.', 6, 1, '2017-12-23 13:47:10', '2017-12-23 13:47:10'),
(12, 'Most Viewed product & promoted', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 6, 2, '2017-12-23 13:47:10', '2017-12-23 13:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `e_addresses`
--

CREATE TABLE `e_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_carts`
--

CREATE TABLE `e_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL,
  `product_quantity` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_customers`
--

CREATE TABLE `e_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','other') COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `e_customers`
--

INSERT INTO `e_customers` (`id`, `fname`, `lname`, `street`, `city`, `country`, `email`, `password`, `telephone`, `gender`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'emad', 'rashad', 'Ibrahim al moghazy', 'KFS', 'Egypt', 'emadtab97@gmail.com', '$1$WBvKg72g$.SrFk89cMAbulp9nd44BX0', '010265903031', 'male', NULL, NULL, NULL, '2017-12-24 16:58:07', '2017-12-24 16:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `e_delivery_methods`
--

CREATE TABLE `e_delivery_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_delivery_method_details`
--

CREATE TABLE `e_delivery_method_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_method_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_orders`
--

CREATE TABLE `e_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_ticket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `address_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_method_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `order_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_total` double(8,2) NOT NULL,
  `order_status` enum('pending','accepted','rejected') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_payment_attachments`
--

CREATE TABLE `e_payment_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_method_id` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_payment_methods`
--

CREATE TABLE `e_payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `e_wishlists`
--

CREATE TABLE `e_wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `label`, `status`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ar', '1', 'العربية', '2017-11-16 06:47:07', '2017-11-16 06:47:07'),
(2, 'en', '1', 'English', '2017-11-16 06:47:07', '2017-11-16 06:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_09_20_221854_create_languages_table', 1),
(4, '2017_10_10_042118_create_abouts_table', 1),
(5, '2017_10_10_045259_create_about_descriptions_table', 1),
(6, '2017_10_24_125718_create_dessert_departments_table', 1),
(7, '2017_10_24_125726_create_dessert_department_descriptions_table', 1),
(8, '2017_10_24_125736_create_dessert_items_table', 1),
(9, '2017_10_24_125741_create_dessert_item_descriptions_table', 1),
(10, '2017_10_24_125750_create_teams_table', 1),
(11, '2017_10_24_125779_create_blog_departments_table', 1),
(12, '2017_10_24_125780_create_blog_department_descriptions_table', 1),
(13, '2017_10_24_125834_create_blogs_table', 1),
(14, '2017_10_24_125841_create_blog_descriptions_table', 1),
(15, '2017_10_26_094049_create_special_events_table', 1),
(16, '2017_10_26_094102_create_special_event_descriptions_table', 1),
(17, '2017_10_26_094158_create_special_event_items_table', 1),
(18, '2017_10_26_094218_create_special_event_item_descriptions_table', 1),
(19, '2017_10_31_181427_create_contact_with_u_s_table', 1),
(20, '2017_11_06_211207_create_says_table', 1),
(21, '2017_11_06_211250_create_say_descriptions_table', 1),
(24, '2017_12_12_202037_create_desert_sub_departments_table', 2),
(25, '2017_12_12_202109_create_desert_sub_department_descriptions_table', 2),
(26, '2017_12_12_213616_add_sub_dep_id_column_to_deset_item_table', 3),
(29, '2017_12_14_185105_create_news_letters_table', 4),
(121, '2017_12_19_092137_create_ecommerce_payment_methods_table', 5),
(122, '2017_12_19_092222_create_ecommerce_delivery_methods_table', 5),
(124, '2017_12_20_091914_create_ecommerce_wishlists_table', 5),
(125, '2017_12_20_092112_create_ecommerce_carts_table', 5),
(126, '2017_12_20_092155_create_ecommerce_payment_attachments_table', 5),
(127, '2017_12_20_092257_create_addresses_table', 5),
(128, '2017_12_20_092338_create_orders_table', 5),
(129, '2017_12_20_092447_create_order_product_table', 5),
(130, '2017_12_21_092235_create_ecommerce_delivery_method_details_table', 5),
(132, '2017_12_21_064909_add_show_in_slider_column_to_desert_items', 7),
(133, '2017_12_21_085422_add_best_selling_column_to_deserts_items_table', 8),
(134, '2017_12_21_093742_add_column_promoted_best_selling_to_desert_item_table', 9),
(135, '2017_12_21_103559_add_most_viewed_column_to_desert_items_table', 10),
(136, '2017_12_21_111709_add_recommended_column_to_desert_table', 11),
(137, '2017_12_19_101711_create_customers_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_letters`
--

INSERT INTO `news_letters` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'hhhhh@ooooooo.ccccc', '2017-12-10 14:29:21', NULL),
(2, '55555@nnnnn.ccccc', '2017-12-20 08:00:00', NULL),
(3, 'emadtab97@gmail.com', '2017-12-21 21:54:57', '2017-12-21 21:54:57'),
(4, 'emad@testing.com', '2017-12-21 21:55:56', '2017-12-21 21:55:56'),
(5, 'tester@hello.com', '2017-12-21 21:57:16', '2017-12-21 21:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `says`
--

CREATE TABLE `says` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `says`
--

INSERT INTO `says` (`id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(4, '1513109818.jpg', 'active', '2017-11-16 23:18:54', '2017-12-13 04:16:58');

-- --------------------------------------------------------

--
-- Table structure for table `say_descriptions`
--

CREATE TABLE `say_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `sayAbout_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `say_descriptions`
--

INSERT INTO `say_descriptions` (`id`, `sayAbout_id`, `language_id`, `name`, `descriptions`, `created_at`, `updated_at`) VALUES
(7, 4, 1, 'محمود صدقي', 'موقع جميل جدا يعرض العديد من المحلويات الشهيه&nbsp;', '2017-11-16 23:18:54', '2017-11-16 23:18:54'),
(8, 4, 2, 'Mahmoud Sidky', 'i like this desserts in this website&nbsp;', '2017-11-16 23:18:54', '2017-11-16 23:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `special_events`
--

CREATE TABLE `special_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `special_events`
--

INSERT INTO `special_events` (`id`, `img`, `status`, `addby_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 'active', 1, '2017-12-23 18:41:26', '2017-12-23 18:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `special_event_descriptions`
--

CREATE TABLE `special_event_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `event_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `special_event_descriptions`
--

INSERT INTO `special_event_descriptions` (`id`, `name`, `description`, `event_id`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'الافراح والاعراس', ' <section class=\"services section-padding\">\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          \r\n          <div class=\"section-title\">\r\n            <h3>Wedding</h3>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12\">\r\n            <hr class=\"section\">\r\n          </div>\r\n\r\n          <div class=\"services-block\">\r\n            <p>When we take on a wedding, we make sure that it comes at its best  due to the amazing variety of products and touches that we offer.\r\n              <br><br>\r\n              Flowers are a language that all human beings on earth understand.\r\n              <br><br>\r\n              This language does not need translation. The saying goes \"By offering a flower, you create a smile\".\r\n              <br><br>\r\n              When lips stand unable to express feelings, flowers talk loud…with the best feeling.\r\n              <br><br>\r\n              At Saadeddine, we endeavour to offer the finest and the most beautiful of what you can choose to give as a present to whoever you love or with whomever you want to celebrate those special events of your life and the happy moments in order for these to reside firmly in the memory forever.<br><br>\r\n              Since we started our journey in the world of happy events, flowers and gifts, we have achieved the most beautiful dreams and turned them to unprecedented reality. We always pride ourselves on making the most beautiful part for your happiness every minute and every day.\r\n              <br><br>\r\n              In Saudi Arabia, Kuwait, Bahrain and most of Arab Gulf States, you will always find us with you making the happiest moments and drawing everlasting smiles on all faces.\r\n              <br><br>\r\n              Saadeddine is the top choice in parties, private events, formal events, weddings, and several royal ceremonies because, to put it in a nutshell, we excel in making happiness, and we offer the supreme and most beautiful services and unique, delicious products.\r\n            </p>\r\n          </div>\r\n\r\n          <blockquote class=\"mt-30\">\r\n            <h3>Newborn Section:</h3>\r\n            <p>Here we offer the best choices of gifts especially made for newborns –females and males- which are usually luxurious pieces of art, natural and artificial flowers, special souvenirs for the event, in addition to the most delicious kinds of Swiss, Belgian and French chocolate.</p>\r\n          </blockquote>\r\n\r\n          <blockquote class=\"mt-30\">\r\n            <h3>Newborn Section:</h3>\r\n            <p>Here we offer the best choices of gifts especially made for newborns –females and males- which are usually luxurious pieces of art, natural and artificial flowers, special souvenirs for the event, in addition to the most delicious kinds of Swiss, Belgian and French chocolate.</p>\r\n          </blockquote>\r\n        </div>\r\n      </div>\r\n    </section>', 1, 1, '2017-12-23 18:41:26', '2017-12-23 18:41:26'),
(2, 'Wedding', ' <section class=\"services section-padding\">\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          \r\n          <div class=\"section-title\">\r\n            <h3>Wedding</h3>\r\n          </div>\r\n\r\n          <div class=\"col-sm-12\">\r\n            <hr class=\"section\">\r\n          </div>\r\n\r\n          <div class=\"services-block\">\r\n            <p>When we take on a wedding, we make sure that it comes at its best  due to the amazing variety of products and touches that we offer.\r\n              <br><br>\r\n              Flowers are a language that all human beings on earth understand.\r\n              <br><br>\r\n              This language does not need translation. The saying goes \"By offering a flower, you create a smile\".\r\n              <br><br>\r\n              When lips stand unable to express feelings, flowers talk loud…with the best feeling.\r\n              <br><br>\r\n              At Saadeddine, we endeavour to offer the finest and the most beautiful of what you can choose to give as a present to whoever you love or with whomever you want to celebrate those special events of your life and the happy moments in order for these to reside firmly in the memory forever.<br><br>\r\n              Since we started our journey in the world of happy events, flowers and gifts, we have achieved the most beautiful dreams and turned them to unprecedented reality. We always pride ourselves on making the most beautiful part for your happiness every minute and every day.\r\n              <br><br>\r\n              In Saudi Arabia, Kuwait, Bahrain and most of Arab Gulf States, you will always find us with you making the happiest moments and drawing everlasting smiles on all faces.\r\n              <br><br>\r\n              Saadeddine is the top choice in parties, private events, formal events, weddings, and several royal ceremonies because, to put it in a nutshell, we excel in making happiness, and we offer the supreme and most beautiful services and unique, delicious products.\r\n            </p>\r\n          </div>\r\n\r\n          <blockquote class=\"mt-30\">\r\n            <h3>Newborn Section:</h3>\r\n            <p>Here we offer the best choices of gifts especially made for newborns –females and males- which are usually luxurious pieces of art, natural and artificial flowers, special souvenirs for the event, in addition to the most delicious kinds of Swiss, Belgian and French chocolate.</p>\r\n          </blockquote>\r\n\r\n          <blockquote class=\"mt-30\">\r\n            <h3>Newborn Section:</h3>\r\n            <p>Here we offer the best choices of gifts especially made for newborns –females and males- which are usually luxurious pieces of art, natural and artificial flowers, special souvenirs for the event, in addition to the most delicious kinds of Swiss, Belgian and French chocolate.</p>\r\n          </blockquote>\r\n        </div>\r\n      </div>\r\n    </section>', 1, 2, '2017-12-23 18:41:26', '2017-12-23 18:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `special_event_items`
--

CREATE TABLE `special_event_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `img1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_id` int(10) UNSIGNED DEFAULT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `special_event_item_descriptions`
--

CREATE TABLE `special_event_item_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL,
  `addby_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `img`, `name`, `description`, `jobtitle`, `facebook`, `twitter`, `google`, `status`, `addby_id`, `created_at`, `updated_at`) VALUES
(1, '1510832589.png', 'Mahmoud Sidky', 'this is important emploee', 'progammer', 'https://www.facebook.com/Sidky10', 'https://www.facebook.com/Sidky10', 'https://www.facebook.com/Sidky10', 'active', 1, '2017-11-16 18:43:09', '2017-11-16 18:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `propertyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `businessRecordNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branchName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headPersonName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctorName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proffestionPracticeNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctorHospitalRelatedTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctorSpecialization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobileNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userType` enum('user','doctor','pharmacy','hospital','medicalSupplies','pharmaCompany','adminstrator') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deviceToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActive` enum('active','not_active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_active',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `propertyName`, `businessRecordNumber`, `phoneNumber`, `branchName`, `email`, `website`, `headPersonName`, `doctorName`, `proffestionPracticeNumber`, `doctorHospitalRelatedTo`, `doctorSpecialization`, `mobileNumber`, `longitude`, `latitude`, `password`, `profileImage`, `userType`, `deviceToken`, `isActive`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, 'admin@tatweer.com', NULL, 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1$OZQeeMhH$SB7rzfSwXctzQKpkB27.t.', NULL, 'adminstrator', NULL, 'not_active', 'BKnICbK4p8Vd1BB71YdWynQLx7RuIEVjL953EndpysYn8SBdxIBBS8ExvOQ4', '2017-11-16 06:47:04', '2017-12-14 02:09:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abouts_related_to_foreign` (`related_to`);

--
-- Indexes for table `about_descriptions`
--
ALTER TABLE `about_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_descriptions_about_id_foreign` (`about_id`),
  ADD KEY `about_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_addby_id_foreign` (`addby_id`),
  ADD KEY `blogs_department_id_foreign` (`department_id`);

--
-- Indexes for table `blog_departments`
--
ALTER TABLE `blog_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_departments_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `blog_department_descriptions`
--
ALTER TABLE `blog_department_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_department_descriptions_department_id_foreign` (`department_id`),
  ADD KEY `blog_department_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `blog_descriptions`
--
ALTER TABLE `blog_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_descriptions_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `contact_with_u_s`
--
ALTER TABLE `contact_with_u_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desert_sub_departments`
--
ALTER TABLE `desert_sub_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `desert_sub_departments_parent_foreign` (`parent`),
  ADD KEY `desert_sub_departments_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `desert_sub_department_descriptions`
--
ALTER TABLE `desert_sub_department_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `desert_sub_department_descriptions_subdeptid_foreign` (`subDeptId`),
  ADD KEY `desert_sub_department_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `dessert_departments`
--
ALTER TABLE `dessert_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dessert_departments_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `dessert_department_descriptions`
--
ALTER TABLE `dessert_department_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dessert_department_descriptions_department_id_foreign` (`department_id`),
  ADD KEY `dessert_department_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `dessert_items`
--
ALTER TABLE `dessert_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dessert_items_department_id_foreign` (`department_id`),
  ADD KEY `dessert_items_addby_id_foreign` (`addby_id`),
  ADD KEY `dessert_items_subdeptid_foreign` (`subDeptId`);

--
-- Indexes for table `dessert_item_descriptions`
--
ALTER TABLE `dessert_item_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dessert_item_descriptions_item_id_foreign` (`item_id`),
  ADD KEY `dessert_item_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `e_addresses`
--
ALTER TABLE `e_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_addresses_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `e_carts`
--
ALTER TABLE `e_carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_carts_product_id_foreign` (`product_id`),
  ADD KEY `e_carts_customers_id_foreign` (`customers_id`);

--
-- Indexes for table `e_customers`
--
ALTER TABLE `e_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_delivery_methods`
--
ALTER TABLE `e_delivery_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_delivery_method_details`
--
ALTER TABLE `e_delivery_method_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_orders`
--
ALTER TABLE `e_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_orders_customer_id_foreign` (`customer_id`),
  ADD KEY `e_orders_address_id_foreign` (`address_id`),
  ADD KEY `e_orders_delivery_method_id_foreign` (`delivery_method_id`),
  ADD KEY `e_orders_payment_method_id_foreign` (`payment_method_id`);

--
-- Indexes for table `e_payment_attachments`
--
ALTER TABLE `e_payment_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_payment_attachments_payment_method_id_foreign` (`payment_method_id`);

--
-- Indexes for table `e_payment_methods`
--
ALTER TABLE `e_payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_wishlists`
--
ALTER TABLE `e_wishlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_wishlists_product_id_foreign` (`product_id`),
  ADD KEY `e_wishlists_customers_id_foreign` (`customers_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `says`
--
ALTER TABLE `says`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `say_descriptions`
--
ALTER TABLE `say_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `say_descriptions_sayabout_id_foreign` (`sayAbout_id`),
  ADD KEY `say_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `special_events`
--
ALTER TABLE `special_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `special_events_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `special_event_descriptions`
--
ALTER TABLE `special_event_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `special_event_descriptions_event_id_foreign` (`event_id`),
  ADD KEY `special_event_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `special_event_items`
--
ALTER TABLE `special_event_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `special_event_items_event_id_foreign` (`event_id`),
  ADD KEY `special_event_items_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `special_event_item_descriptions`
--
ALTER TABLE `special_event_item_descriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `special_event_item_descriptions_item_id_foreign` (`item_id`),
  ADD KEY `special_event_item_descriptions_language_id_foreign` (`language_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teams_addby_id_foreign` (`addby_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_descriptions`
--
ALTER TABLE `about_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_departments`
--
ALTER TABLE `blog_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_department_descriptions`
--
ALTER TABLE `blog_department_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_descriptions`
--
ALTER TABLE `blog_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `contact_with_u_s`
--
ALTER TABLE `contact_with_u_s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `desert_sub_departments`
--
ALTER TABLE `desert_sub_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `desert_sub_department_descriptions`
--
ALTER TABLE `desert_sub_department_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dessert_departments`
--
ALTER TABLE `dessert_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dessert_department_descriptions`
--
ALTER TABLE `dessert_department_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dessert_items`
--
ALTER TABLE `dessert_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dessert_item_descriptions`
--
ALTER TABLE `dessert_item_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `e_addresses`
--
ALTER TABLE `e_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_carts`
--
ALTER TABLE `e_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_customers`
--
ALTER TABLE `e_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `e_delivery_methods`
--
ALTER TABLE `e_delivery_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_delivery_method_details`
--
ALTER TABLE `e_delivery_method_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_orders`
--
ALTER TABLE `e_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_payment_attachments`
--
ALTER TABLE `e_payment_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_payment_methods`
--
ALTER TABLE `e_payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `e_wishlists`
--
ALTER TABLE `e_wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `news_letters`
--
ALTER TABLE `news_letters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `says`
--
ALTER TABLE `says`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `say_descriptions`
--
ALTER TABLE `say_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `special_events`
--
ALTER TABLE `special_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `special_event_descriptions`
--
ALTER TABLE `special_event_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `special_event_items`
--
ALTER TABLE `special_event_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `special_event_item_descriptions`
--
ALTER TABLE `special_event_item_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `abouts`
--
ALTER TABLE `abouts`
  ADD CONSTRAINT `abouts_related_to_foreign` FOREIGN KEY (`related_to`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `about_descriptions`
--
ALTER TABLE `about_descriptions`
  ADD CONSTRAINT `about_descriptions_about_id_foreign` FOREIGN KEY (`about_id`) REFERENCES `abouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `about_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_addby_id_foreign` FOREIGN KEY (`addby_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blogs_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `blog_departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_departments`
--
ALTER TABLE `blog_departments`
  ADD CONSTRAINT `blog_departments_addby_id_foreign` FOREIGN KEY (`addby_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_department_descriptions`
--
ALTER TABLE `blog_department_descriptions`
  ADD CONSTRAINT `blog_department_descriptions_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `blog_departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_department_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `desert_sub_departments`
--
ALTER TABLE `desert_sub_departments`
  ADD CONSTRAINT `desert_sub_departments_addby_id_foreign` FOREIGN KEY (`addby_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `desert_sub_departments_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `dessert_departments` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
