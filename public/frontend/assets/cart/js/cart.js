jQuery(document).ready(function($){

	var lang = $("#lang_header").val();

	 
	 // define lang variables for labels 
	 // 
	 if(lang == 'ar'){

	 	var price_label = "السعر" ; 
	 	var quantity_label = "الكمية"; 
	 	 
	 }else{

	 	var price_label = "Price" ; 
	 	var quantity_label = "Quantity"; 
	 }

	var cartWrapper = $('.cd-cart-container');
	 
	var productId = 0;

	if( cartWrapper.length > 0 ) {
		//store jQuery objects
		var cartBody = cartWrapper.find('.body')
		var cartList = cartBody.find('ul').eq(0);
		var cartTotal = cartWrapper.find('.checkout').find('span');
		var cartTrigger = cartWrapper.children('.cd-cart-trigger');
		var cartCount = cartTrigger.children('.count')
		var addToCartBtn = $('.cd-add-to-cart');
		var undo = cartWrapper.find('.undo');
		var undoTimeoutId;

		//add product to cart
		addToCartBtn.on('click', function(event){
			event.preventDefault();
			addToCart($(this));

		});

		//open/close cart
		cartTrigger.on('click', function(event){
			event.preventDefault();
			toggleCart();
		});

		//close cart when clicking on the .cd-cart-container::before (bg layer)
		cartWrapper.on('click', function(event){
			if( $(event.target).is($(this)) ) toggleCart(true);
		});

		//delete an item from the cart
		cartList.on('click', '.delete-item', function(event){
			event.preventDefault();

			var product_id =  $(this).attr('data-product-id') ;

			$(`a.to_be_hide_${product_id}`).removeClass('hidden');

			$(`a.item_${product_id}`).addClass('hidden');


			// so he decided to remove a product from the cart -_- 
			// i need to clean my session storage array by removing the entire object from it 
			// lets give it a shooooooot -_- 

			var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 

			if(parseJsonProducts != null ){

				parseJsonProducts.forEach(function(product , index){

						if(parseJsonProducts[index].product_id == product_id ){

							 
							parseJsonProducts.splice(index,1);

							var arrToJsonString = JSON.stringify(parseJsonProducts) ; 

							sessionStorage.setItem('products' , arrToJsonString ) ; 

        					return false;
						}
				});

			}

			removeProduct($(event.target).parents('.product'));
		});

		//update item quantity
		cartList.on('change', 'select', function(event){
			alert('change'); 
			quickUpdateCart();
		});

		// cartList.on('keydown' , '')

		//reinsert item deleted from the cart
		undo.on('click', 'a', function(event){
			clearInterval(undoTimeoutId);
			event.preventDefault();
			cartList.find('.deleted').addClass('undo-deleted').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
				$(this).off('webkitAnimationEnd oanimationend msAnimationEnd animationend').removeClass('deleted undo-deleted').removeAttr('style');
				quickUpdateCart();
			});
			undo.removeClass('visible');
		});
	}

	function toggleCart(bool) {
		var cartIsOpen = ( typeof bool === 'undefined' ) ? cartWrapper.hasClass('cart-open') : bool;
		
		if( cartIsOpen ) {
			cartWrapper.removeClass('cart-open');
			//reset undo
			clearInterval(undoTimeoutId);
			undo.removeClass('visible');
			cartList.find('.deleted').remove();

			setTimeout(function(){
				cartBody.scrollTop(0);
				//check if cart empty to hide it
				if( Number(cartCount.find('li').eq(0).text()) == 0) cartWrapper.addClass('empty');
			}, 500);
		} else {
			cartWrapper.addClass('cart-open');
		}
	}

	function addToCart(trigger) {
		var cartIsEmpty = cartWrapper.hasClass('empty');

		
		 

		// the trigger variable is my a tag clicked cloned object 
		// so i may provide data-attributes through this object to access them 
		var product_id = trigger.attr('product-id') ; 

		var product_name = trigger.attr('product-name') ; 

		var product_price = trigger.attr('product-price') ; 

		

		var product_quantity = trigger.attr('product-quantity') ; 

		var product_image = trigger.attr('product-image') ; 

		var selected_quantity = 1 ;

		$(`a.to_be_hide_${product_id}`).addClass('hidden');

		// trigger.addClass('hidden') ; 

		var class_to_show = ".item_" + product_id ; 
		$(class_to_show).removeClass('hidden');



		// first of all i need to check if there was existing entires first in session -_- 
		var existingEntries = JSON.parse(sessionStorage.getItem('products')); 

		if(existingEntries == null ) existingEntries = [] ; // then set it to empty array 

		// then fill my js object with info 
		var prodObj = {

			product_id : product_id , 
			product_name : product_name , 
			product_price : product_price , 
			product_image : product_image , 
			product_quantity : product_quantity , 
			selected_quantity : selected_quantity , 


		};

		// turn object into a json string 
		var obJsonString = JSON.stringify(prodObj);
 		
		// sessionStorage.setItem('product' , obJsonString ) ; 

		// push item to array 
		existingEntries.push(prodObj);

		// turn into json string for array 
		var arrToJsonString = JSON.stringify(existingEntries) ; 

		sessionStorage.setItem('products' , arrToJsonString ) ; 




		//update cart product list
		addProduct( product_id, product_name ,  product_price , product_quantity , product_image , selected_quantity );
		//update number of items 
		updateCartCount(cartIsEmpty);
		//update total price
		updateCartTotal( product_price  , true);
		//show cart
		cartWrapper.removeClass('empty');
	}

	function addProduct( product_id , product_name ,  product_price , product_quantity , product_image , selected_quantity ) {
		//this is just a product placeholder
		//you should insert an item with the selected product info
		//replace productId, productName, price and url with your real product info
		
	  	

		productId = product_id;
		var productAdded = $(`

			<li class="product">
				<div class="product-image">
					<a href="#0"><img src="${product_image}" alt="placeholder"></a>
				</div>
				<div class="product-details">
					<h3><a href="#0">${product_name}</a></h3>
					<p >${price_label}</p> 
					<span class="price">    ${product_price} </span>
					<br><br>
					<div class="actions">
						<a href="#0" class="delete-item" data-product-id="${productId}"><i class='fa fa-times'></i></a>
						<div class="quantity">
							<p  >${quantity_label}</p>
							<label for="cd-product-'+ ${productId} +'">     ${product_quantity}</label>
							
							 
									 
							</span>
						</div>
					</div>
				</div>
			</li> 
		

		`);
		cartList.prepend(productAdded);

		 
	}

	function removeProduct(product) {
		clearInterval(undoTimeoutId);
		cartList.find('.deleted').remove();


		   
		
		var topPosition = product.offset().top - cartBody.children('ul').offset().top ,
			// productQuantity = Number(product.find('.quantity').find('select').val()),
			productQuantity = Number(1),
			productTotPrice = Number(product.find('.price').text().replace('S.R', '')) * productQuantity;
		
		product.css('top', topPosition+'px').addClass('deleted');

		//update items count + total price
		updateCartTotal(productTotPrice, false);
		updateCartCount(true, -productQuantity);
		undo.addClass('visible');

		//wait 8sec before completely remove the item
		undoTimeoutId = setTimeout(function(){
			undo.removeClass('visible');
			cartList.find('.deleted').remove();
		}, 8000);
	}

	function quickUpdateCart() {
		var quantity = 0;
		var price = 0;
		
		cartList.children('li:not(.deleted)').each(function(){
			// var singleQuantity = Number($(this).find('select').val());
			var singleQuantity = Number(1);
			console.log(singleQuantity);
			quantity = quantity + singleQuantity;
			price = price + singleQuantity*Number($(this).find('.price').text().replace('S.R', ''));
		});

		cartTotal.text(price.toFixed(2));
		cartCount.find('li').eq(0).text(quantity);
		cartCount.find('li').eq(1).text(quantity+1);
	}

	function updateCartCount(emptyCart, quantity) {
		if( typeof quantity === 'undefined' ) {
			var actual = Number(cartCount.find('li').eq(0).text()) + 1;
			var next = actual + 1;
			
			if( emptyCart ) {
				cartCount.find('li').eq(0).text(actual);
				cartCount.find('li').eq(1).text(next);
			} else {
				cartCount.addClass('update-count');

				setTimeout(function() {
					cartCount.find('li').eq(0).text(actual);
				}, 150);

				setTimeout(function() {
					cartCount.removeClass('update-count');
				}, 200);

				setTimeout(function() {
					cartCount.find('li').eq(1).text(next);
				}, 230);
			}
		} else {
			var actual = Number(cartCount.find('li').eq(0).text()) + quantity;

			var next = actual + 1;
			
			cartCount.find('li').eq(0).text(actual);
			cartCount.find('li').eq(1).text(next);
		}
	}

	function updateCartTotal(price, bool) {
		bool ? cartTotal.text( (Number(cartTotal.text()) + Number(price)).toFixed(2) )  : cartTotal.text( (Number(cartTotal.text()) - Number(price)).toFixed(2) );
	}


	/**
	 * After adding objects to my array i need to make some majic here  
	 * need to check -_- if a product is already added in my sessionArray -_- 
	 * and if then i will hide the add to cart button and keep showing item added span -_- 
	 */
	

	

	// console.log(parseJsonProducts.length);

	if(  sessionStorage.getItem('products') !== null    ){
		
		var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 


		if(parseJsonProducts != 0 ){

		var cartIsEmpty = cartWrapper.hasClass('empty');	
		parseJsonProducts.forEach(function(product){

			 	// now i should have each product object on my little dirty hand  -_- 
			 	
			 	var product_id = product.product_id ; 
			 	var product_name = product.product_name ; 
			 	var product_price = product.product_price ; 
			 	var product_quantity = product.product_quantity ; 
			 	var product_image = product.product_image ; 
			 	var selected_quantity = product.selected_quantity ; 

			 	$(`a.to_be_hide_${product_id}`).addClass('hidden');

				// trigger.addClass('hidden') ; 

				var class_to_show = ".item_" + product_id ; 
				$(class_to_show).removeClass('hidden');

				// then call this function that adds the product -_- 
				addProduct( product_id , product_name ,  product_price , product_quantity , product_image , selected_quantity )
				updateCartCount(cartIsEmpty);
				//update total price
				updateCartTotal( product_price  , true);
		});

		cartWrapper.removeClass('empty');
	}else{
		cartWrapper.addClass('empty');
	}


		

	}else{

		
		cartWrapper.addClass('empty');
		
	}




});