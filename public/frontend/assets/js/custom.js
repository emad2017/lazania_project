$(function () {

  "use strict";

  var wind = $(window);

 

  /* navbar scrolling background
  -------------------------------------------------------*/
  wind.on("scroll",function () {

        var bodyScroll = wind.scrollTop(),
            navbar = $(".navbar-default");

        if(bodyScroll > 200){

            navbar.addClass("nav-scroll");

        }else{

            navbar.removeClass("nav-scroll");
        }

    });


  $('.header .owl-carousel').owlCarousel({
        items:1,
        autoplay:true,
        smartSpeed:500,
        dots:false,
        nav:true,
        navText:['<span> <i class="fa fa-chevron-left" aria-hidden="true"></i> </span>',
        '<span> <i class="fa fa-chevron-right" aria-hidden="true"></i> </span>']
    });


    $('.best-s-p .owl-carousel').owlCarousel({
        items:4,
        loop:true,
        autoplay:true,
        smartSpeed:500,
        responsiveClass:true,
        dots:false,
        nav:true,
        navText:['<span> <i class="fa fa-chevron-left" aria-hidden="true"></i> </span>',
        '<span> <i class="fa fa-chevron-right" aria-hidden="true"></i> </span>'],
        responsive:{
            1000:{
                items:4,
                nav:true,
                loop:false
            },
            767:{
                items:2,
                nav:true
            },
            0:{
                items:1,
                nav:true
            }
        }
    });


    $('.product .owl-carousel').owlCarousel({
        items:4,
        autoplay:true,
        smartSpeed:500,
        responsiveClass:true,
        dots:false,
        nav:true,
        navText:['<span> <i class="fa fa-chevron-left" aria-hidden="true"></i> </span>',
        '<span> <i class="fa fa-chevron-right" aria-hidden="true"></i> </span>'],
        responsive:{
            1000:{
                items:4,
                nav:true,
                loop:false
            },
            767:{
                items:2,
                nav:true
            },
            0:{
                items:1,
                nav:true
            }
        }
    });


    $('.r-products .owl-carousel').owlCarousel({
        items:4,
        loop:true,
        autoplay:true,
        smartSpeed:500,
        dots:false,
        nav:true,
        navText:['<span> <i class="fa fa-chevron-left" aria-hidden="true"></i> </span>',
        '<span> <i class="fa fa-chevron-right" aria-hidden="true"></i> </span>'],
        responsive:{
            1000:{
                items:4,
                nav:true,
                loop:false
            },
            767:{
                items:2,
                nav:true
            },
            0:{
                items:1,
                nav:true
            }
        }
    });




  /* side bar
    -------------------------------------------------------*/
    $(".block-content ").css("display","none");
    $(".block-category .block-title h3").on("click", function(){
        $(".block-content ").slideToggle(300);
    });
    $(".block-content .sub-product ").css("display","none");
   $(".side-bar .block-category .open-sub").on("click", function(){
        // $(".sub-product").slideToggle(500);
       $(this).next('ul').slideToggle(500);
    });






    // init the validator
    // validator files are included in the download package
    // otherwise download from http://1000hz.github.io/bootstrap-validator
    // contact form
    // contact form
    $('#contact-form').validator();

    $('#contact-form').on('submit', function (e) {
        // if (!e.isDefaultPrevented()) {
        //     var url = "{{ $ }}";

        //     $.ajax({
        //         type: "POST",
        //         url: url,
        //         data: $(this).serialize(),
        //         success: function (data)
        //         {
        //             var messageAlert = 'alert-' + data.type;
        //             var messageText = data.message;

        //             var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
        //             if (messageAlert && messageText) {
        //                 $('#contact-form').find('.messages').html(alertBox);
        //                 $('#contact-form')[0].reset();
        //             }
        //         }
        //     });
        //     return false;
        // }
    });


  /* Preloader
    -------------------------------------------------------*/
  $(window).on("load",function (){

    $("#preloader").fadeOut(500);

  });


});
