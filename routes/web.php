<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



####################################
#
#
# Security Routes like login - logout - Registration etc 
#
#
####################################




####################################
#
#
# Front End Routes that shows customer how beautiful are we  -_-
#
#
####################################



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [   'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' , 'localize' ]
    ],
    function()
    {

        Route::get('login' , function(){

           if (Auth::check()) {
                 return view('backend.dashboard.index');
           }else{
                return view('auth.login');
           }
        }); 

        Route::post('authenticate' , 'Auth\LoginController@authenticate');

        Route::post('logout' , 'Auth\LoginController@logout') ;
       
        Route::get('/','frontend\HomeController@getHomePage')->name('home_page');
        Route::get('/categories' , 'frontend\CategoriesController@index'); 



       
        Route::get('/special-occassion/{id}/{name}','frontend\SpeacialEventController@index');

        Route::get('contact','frontend\ContactUsController@contact');
        Route::post('post_contact' , 'frontend\ContactUsController@contactmsg')->name('contactmsg'); 


      
        Route::get('/{id}/sub-categories' , 'frontend\CategoriesController@indexSub');

        Route::get('/{id}/sub-categories/{sub_id}/products' , 'frontend\CategoriesController@indexProducts'); 


        Route::get('/{id}/sub-categories/{sub_id}/products/{product_id}' , 'frontend\CategoriesController@productDetails'); 


        Route::get('careers_list' , 'frontend\CareerController@index');   
        Route::get('/about-us', 'frontend\InformationalController@about' );   

  


     
        
        Route::post('subscribe' , 'frontend\HomeController@subscribe')->name('subscribe');



      
        Route::get('/customer-login', 'CustomerAuth\CustomerAuthController@showLoginForm')->name('customer_login');
        Route::post('/customer-login', 'CustomerAuth\CustomerAuthController@postLogin');


        Route::get('/customer-register' , 'CustomerAuth\CustomerRegistrationController@getRegistrationForm')->name('customer_register'); 
        Route::post('/customer-register' , 'CustomerAuth\CustomerRegistrationController@createAccount')->name('create_account'); 



        Route::get('customer-forget-password' , 'CustomerAuth\CustomerForgetPasswordController@showLinkRequestForm')->name('forgetPassword'); 
        Route::post('post-reset-email' , 'CustomerAuth\CustomerForgetPasswordController@sendResetLinkEmail')->name('sendResetLinkEmail'); 


        Route::get('customer-reset-password/{token}' , 'CustomerAuth\CustomerResetPasswordController@showResetForm'); 
        Route::post('reset-password' , 'CustomerAuth\CustomerResetPasswordController@reset');




        Route::group(['middleware' => ['customer'] , 'prefix'=>'customer' ], function () {
            
            Route::get('/dashboard' , 'frontend\CustomerController@dashboard')->name('customer_dashboard'); 
            Route::get('/account_information/edit' , 'frontend\CustomerController@accountInformation')->name('customer_information'); 
            Route::put('/account_information/{id}/update' , 'frontend\CustomerController@updateAccountInformation')->name('updateAccount'); 

            Route::put('/account_password/{id}/update' , 'frontend\CustomerController@updateAccountPassword')->name('updateAccountPassword');

            Route::get('/check_update_account' , 'frontend\CustomerController@checkUpdate' )->name('check_update');

            Route::post('/logout' , 'CustomerAuth\CustomerAuthController@logout')->name('logout_customer');
            // shopping cart page and logic  -_-    
            Route::get('/shopping-cart' , 'frontend\CustomerController@getCart')->name('shopping_cart'); 
            // whish list logic -_-
            Route::get('/add_product_to_whislist/{id}' , 'frontend\WishListController@addProduct' )->name('addProductToWishlist'); 
            Route::get('/wishlist' , 'frontend\WishListController@index')->name('whislist'); 
            Route::post('/wishlist/delete', 'frontend\WishListController@removeWishlistProduct' )->name('removeWishlistProduct');



            Route::get('checkout' , 'frontend\CheckoutController@getCheckoutBlade')->name('getCheckout'); 


            Route::get('my-orders' , 'frontend\OrderController@getMyOrders')->name('myOrders');
            Route::get('my-orders/{order_ticket}/details' , 'frontend\OrderController@orderDetails'); 
            Route::post('place-order' , 'frontend\OrderController@hackit')->name('order'); 



         
        });




 
        // search mechanism  -_- 
        Route::get('search_result' , 'frontend\SearchController@searchResutlt');
         




    });

 


####################################
#
#
# Backend Routes that describe how we do it  -_- 
#
#
####################################


Route::group(['prefix' => LaravelLocalization::setLocale() , 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' , 'auth' ]], function()
{
 
		Route::get('dashboard' , function(){ 
            return view('backend.dashboard.index');
		})->name('dashboard');
		Route::resource('language', 'Backend\LanguageController');
		Route::resource('user', 'Backend\UserController');
		Route::get('user/profile/{id}', 'Backend\UserController@profile')->name('user.profile');
        Route::resource('messages','Backend\messagesController');
        Route::resource('sayabout','Backend\SayAbout');
        Route::get('/about' , 'Backend\AboutController@index')->name('about') ;
        Route::put('/about/{id}' , 'Backend\AboutController@update')->name('about_update') ;
        Route::get('/about-photos/{related_to}' , 'Backend\AboutController@getImages')->name('about-photos') ;
        Route::delete('/about-photos/{id}', 'Backend\AboutController@destroyImage' )->name('destroy-image');
        Route::resource('dessertdepartment', 'Backend\DessertDepartmentController');
        Route::get('sub-depts/{id}' , 'Backend\DessertDepartmentController@getSubDepts')->name('sub_departments');
        Route::get('add-new-sub/{id}' , 'Backend\DessertDepartmentController@addNewSub')->name('add_new_sub');
        Route::post('post_sub' , 'Backend\DessertDepartmentController@postNewSub')->name('post_sub');
        Route::get('edit_sub/{id}' , 'Backend\DessertDepartmentController@editSubDepartment' )->name('sub_edit') ;
        Route::put('post_sub_update/{id}' , 'Backend\DessertDepartmentController@updateSubDepartment' )->name('update_sub_department');
        Route::delete('sub_department/{id}', 'Backend\DessertDepartmentController@deleteSubDepartment')->name('deleteSubDepartment');
        Route::get('items/{id}' , 'Backend\DessertDepartmentController@getDesertItemList')->name('items');
        Route::get('add-new-item/{id}' , 'Backend\DessertDepartmentController@addNewItem')->name('addNewItem');
        Route::post('post_item' , 'Backend\DessertDepartmentController@postNewItem')->name('postNewItem');
        Route::get('edit_item/{id}' , 'Backend\DessertDepartmentController@editItem' )->name('editItem') ;
        Route::put('update_item/{id}' , 'Backend\DessertDepartmentController@updateItem' )->name('updateItem');
        Route::delete('delete_item/{id}' , 'Backend\DessertDepartmentController@deleteItem' )->name('deleteItem');
        // Route::resource('blogdepartment', 'Backend\BlogDepartmentController');
        // Route::resource('careers', 'Backend\BlogController');
        Route::get('careers', 'Backend\BlogController@careers')->name('careers_backend');
        Route::put('careers_post/{id}', 'Backend\BlogController@careersPost')->name('post_career');
		Route::resource('team', 'Backend\TeamController');
        Route::resource('speacialevent', 'Backend\SpecialEventController');
        Route::resource('eventitem', 'Backend\SpeacialEventItemController');
        // Route::resource('social-media', 'Backend\SocialMediaController');
        Route::resource('say','Backend\SayController');
        Route::resource('visitorsMessages','Backend\visitorsMessagesController');
        Route::get('visitorsMessages/showReply/{id}','Backend\visitorsMessagesController@showReply');
        Route::post('visitorsMessages/reply', 'Backend\visitorsMessagesController@Reply');
        Route::get('newsletter' , 'Backend\NewsLetterController@index')->name('newsletter');
        Route::get('newsletter/send-email/{id}' , 'Backend\NewsLetterController@sendEmail')->name('sendEmail');
        Route::get('newsletter/send-bulck' , 'Backend\NewsLetterController@sendBulckEmail')->name('sendBulckEmail');
        Route::post('post_single_email/{id}' , 'Backend\NewsLetterController@postSingleEmail')->name('postSingleEmail');
        Route::post('post_bulck' , 'Backend\NewsLetterController@postBulck')->name('postBulck');



        Route::resource('customers' , 'Backend\CustomerController'); 
        Route::get('orders' , 'Backend\OrderController@index'); 
        Route::get('order/{order_ticket}/{notification?}/details' , 'Backend\OrderController@adminOrderDetails'); 

   


        // mark notification as read  -_- 
        
        Route::get('notification/{id}' , 'Backend\NotificationController@markAsRead');



        Route::resource('slider', 'Backend\SliderController');


});
