<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         *  جدول المستخدمين
         */
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');

            // اسم الجهه

            $table->string('propertyName')->nullable();

            // رقم السجل التجارى او الرخصه

            $table->string('businessRecordNumber')->nullable();

            // رقم الهاتف

            $table->string('phoneNumber')->nullable();

            // اسم الفرع

            $table->string('branchName')->nullable();

            // البريد الاليكترونى

            $table->string('email')->nullable();

            // الموقع الاليكترونى

            $table->string('website')->nullable();

            // اسم الشخص المسؤول       

            $table->string('headPersonName')->nullable();

            // اسم الطبيب

            $table->string('doctorName')->nullable();

            // رقم مزاوله المهنه

            $table->string('proffestionPracticeNumber')->nullable();

            // اسم المستشفى المنتمى اليها 

            $table->string('doctorHospitalRelatedTo')->nullable();

            // تخصص الطبيب

            $table->string('doctorSpecialization')->nullable();

            // رقم الجوال

            $table->string('mobileNumber')->nullable();

            // خط الطول

            $table->string('longitude')->nullable();

            // خط العرض

            $table->string('latitude')->nullable();

            $table->string('password')->nullable();

            // صوره البروفايل

            $table->string('profileImage')->nullable();

            // نوع المستخدم

            $table->enum('userType' , ['user','doctor','pharmacy','hospital','medicalSupplies','pharmaCompany','adminstrator'])->nullable();


            // رقم الجهاز المميز لاستخدامه لاحقا :-D 

            $table->string('deviceToken')->nullable();

            // حاله الحساب

            $table->enum('isActive', ['active','not_active'])->default('not_active');

            $table->rememberToken();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
