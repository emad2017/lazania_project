<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->enum('status',['active','not_active'])->nullable();


            // Make Forigen key of user
            $table->integer('addby_id')->unsigned()->nullable();

            $table->foreign('addby_id')->references('id')->on('users')->onDelete('cascade');


            // Make Forigen key
            $table->integer('department_id')->unsigned()->nullable();

            $table->foreign('department_id')->references('id')->on('blog_departments')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
