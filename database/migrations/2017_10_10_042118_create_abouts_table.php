<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * جدول من نحن
         */
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('timestart', 250)->nullable();
            $table->string('timeend', 250)->nullable();
            $table->string('class_num')->nullable();
            $table->string('staff_num')->nullable();
            $table->string('master_num')->nullable();
            $table->string('kitchen_num')->nullable();
            $table->string('telephone1',255)->nullable();
            $table->string('info_img')->nullable();
            $table->string('desc_img')->nullable();
            $table->string('since_year')->nullable();
            $table->string('email', 250)->nullable();
            $table->string('tele', 250)->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();

            $table->enum('status' , ['active' , 'not_active'])->nullable();

            $table->integer('related_to')->unsigned()->nullable();

            $table->foreign('related_to')->references('id')->on('users')->onDelete('cascade');


            // $table->foreign('related_to')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
