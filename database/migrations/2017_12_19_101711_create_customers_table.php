<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_customers', function (Blueprint $table) {

            $table->increments('id');
            $table->string('fname')->nullable(); 
            $table->string('lname')->nullable();
            $table->string('street')->nullable();  
            $table->string('city')->nullable();  
            $table->string('country')->nullable();  
            $table->string('email')->nullable();  
            $table->string('password')->nullable();  
            $table->string('telephone')->nullable();  
            $table->enum('gender', ['male','female' , 'other']);
            $table->string('provider')->nullable(); 
            $table->string('provider_id')->nullable(); 
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_customers');
    }
}
