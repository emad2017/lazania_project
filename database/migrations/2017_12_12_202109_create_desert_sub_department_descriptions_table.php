<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesertSubDepartmentDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desert_sub_department_descriptions', function (Blueprint $table) {
                
                $table->increments('id');

                $table->string('name')->nullable();
                
                $table->text('description')->nullable();

                $table->integer('subDeptId')->unsigned()->nullable();

                $table->foreign('subDeptId')->references('id')->on('desert_sub_departments')->onDelete('cascade');
            

                $table->integer('language_id')->unsigned()->nullable();

                $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desert_sub_department_descriptions');
    }
}
