<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * محتوى جدول من نحن
         */
        Schema::create('about_descriptions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('about_id')->unsigned()->nullable();

            $table->foreign('about_id')->references('id')->on('abouts')->onDelete('cascade');

            $table->integer('language_id')->unsigned()->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->string('name', 250)->nullable();
            $table->string('description',1000)->nullable();

            $table->string('about',1000)->nullable();
            $table->string('address', 250)->nullable();

            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_descriptions');
    }
}
