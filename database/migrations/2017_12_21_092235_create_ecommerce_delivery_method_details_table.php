<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceDeliveryMethodDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_delivery_method_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_method_id')->unsinged()->nullable(); 
            // $table->foreign('e_delivery_method_id')->references('id')->on('e_delivery_methods')->onDelete('cascade'); 
            $table->integer('price')->unsinged()->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_delivery_method_details');
    }
}
