<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMostViewedColumnToDesertItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->integer('views')->unsigned()->nullable()->after('promoted'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->dropColumn('views') ; 
        });
    }
}
