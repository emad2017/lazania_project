<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactWithUSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_with_u_s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable;
            $table->string('subject')->nullable;
            $table->string('email')->nullable;
            $table->string('msg',1000)->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_with_u_s');
    }
}
