<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_ticket')->nullable(); 
            $table->integer('customer_id')->unsigned()->nullable(); 
            $table->foreign('customer_id')->references('id')->on('e_customers')->onDelete('cascade');
            $table->integer('address_id')->unsigned()->nullable(); 
            $table->foreign('address_id')->references('id')->on('e_addresses')->onDelete('cascade');
            $table->integer('delivery_method_id')->unsigned()->nullable(); 
            $table->foreign('delivery_method_id')->references('id')->on('e_delivery_methods')->onDelete('cascade');
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->foreign('payment_method_id')->references('id')->on('e_payment_methods')->onDelete('cascade');
            $table->string('order_date')->nullable(); 
            $table->double('order_total' , 8 , 2 ) ; 
            $table->enum('order_status', ['pending', 'accepted' , 'rejected']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_orders');
    }
}
