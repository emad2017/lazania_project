<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowInSliderColumnToDesertItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->tinyInteger('show_in_slider')->default(0)->after('price'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->dropColumn('show_in_slider'); 
        });
    }
}
