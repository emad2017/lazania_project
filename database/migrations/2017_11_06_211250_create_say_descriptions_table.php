<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSayDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('say_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sayAbout_id')->unsigned()->nullable();
            $table->foreign('sayAbout_id')->references('id')->on('says')->onDelete('cascade');

            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->string('name',250)->nullable();
            $table->text('descriptions',1000)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('say_descriptions');
    }
}
