<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDessertDepartmentDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dessert_department_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description',1000)->nullable();

            // Make Forigen key !
            $table->integer('department_id')->unsigned()->nullable();

            $table->foreign('department_id')->references('id')->on('dessert_departments')->onDelete('cascade');
            // get foreign from languages

            $table->integer('language_id')->unsigned()->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dessert_department_descriptions');
    }
}
