<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceWishlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_wishlists', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable(); 
            $table->foreign('product_id')->references('id')->on('dessert_items')->onDelete('cascade'); 
            $table->integer('customers_id')->unsigned()->nullable(); 
            $table->foreign('customers_id')->references('id')->on('e_customers')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_wishlists');
    }
}
