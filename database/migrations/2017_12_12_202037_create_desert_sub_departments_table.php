<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesertSubDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desert_sub_departments', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('image')->nullable();
            
            $table->enum('status',['active','not_active']);


            $table->integer('parent')->unsigned()->nullable() ; 


            $table->foreign('parent')->references('id')->on('dessert_departments')->onDelete('cascade');
           
            $table->integer('addby_id')->unsigned()->nullable();

            $table->foreign('addby_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desert_sub_departments');
    }
}
