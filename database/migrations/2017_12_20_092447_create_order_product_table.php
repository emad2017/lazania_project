<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable(); 
            // $table->foreign('order_id')->references('id')->on('e_orders')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            // $table->foreign('product_id')->references('id')->on('dessert_items')->onDelete('cascade'); 
            $table->integer('quantity')->unsigned()->nullable(); 
            $table->integer('customer_id')->unsigned()->nullable();  
            // $table->foreign('customer_id')->references('id')->on('e_customers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
