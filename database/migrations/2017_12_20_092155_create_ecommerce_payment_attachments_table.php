<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommercePaymentAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_payment_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_method_id')->unsigned()->nullable(); 
            $table->foreign('payment_method_id')->references('id')->on('e_payment_methods')->onDelete('cascade'); 
            $table->string('image')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_payment_attachments');
    }
}
