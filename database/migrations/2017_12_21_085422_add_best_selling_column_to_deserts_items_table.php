<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBestSellingColumnToDesertsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
             $table->tinyInteger('best_selling')->default(0)->after('show_in_slider'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->dropColumn('show_in_slider');
        });
    }
}
