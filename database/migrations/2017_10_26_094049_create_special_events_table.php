<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialEventsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('special_events', function (Blueprint $table) {

            $table->increments('id');
            $table->string('img')->nullable();
            $table->enum('status',['active','not_active'])->nullable();



            // Make Forigen key of user
            $table->integer('addby_id')->unsigned()->nullable();

            $table->foreign('addby_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_events');
    }
}
