<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubDepIdColumnToDesetItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            
            $table->integer('subDeptId')->unsigned()->nullable()->after('status') ; 

            $table->foreign('subDeptId')->references('id')->on('desert_sub_departments')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dessert_items', function (Blueprint $table) {
            $table->dropColumn('subDeptId'); 
        });
    }
}
