<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialEventItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_event_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img1')->nullable();
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
            $table->integer('rate')->nullable();
            $table->integer('price')->nullable();
            $table->enum('status',['active','not_active']);


            // Make Forigen key of speacial event
            $table->integer('event_id')->unsigned()->nullable();

            $table->foreign('event_id')->references('id')->on('special_events')->onDelete('cascade');

            // Make Forigen key of user

            $table->integer('addby_id')->unsigned()->nullable();

            $table->foreign('addby_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_event_items');
    }
}
