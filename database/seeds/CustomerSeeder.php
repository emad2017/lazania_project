<?php

use Illuminate\Database\Seeder;

use App\Customer ; 

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = new Customer ; 

        $customer->fname = 'emad' ; 
        $customer->lname = 'rashad' ; 
        $customer->street = 'Ibrahim al moghazy' ; 
        $customer->city = 'KFS' ; 
        $customer->country = 'Egypt' ; 
        $customer->email = 'emadtab97@gmail.com' ; 
        $customer->password = crypt(123456 , '')  ; 
        $customer->telephone = '010265903031' ; 
        $customer->gender = 'male' ; 

        $customer->save(); 
    }
}
