<?php $__env->startSection('page_title' , trans('dessertdepartment.dessert_department_name')); ?>

<?php $__env->startSection('breadcrumb'); ?>

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('dessertdepartment.dessert_department_edit')); ?></h4>
    </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> <?php echo e(trans('dessertdepartment.dessert_department_edit')); ?> </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">



                            <nav>
                                <ul>
                                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="#<?php echo e($language->label); ?>"  ><span> &nbsp;<?php echo e(strtoupper($language->label)); ?></span></a></li>
                                        
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </ul>
                            </nav>





                       <?php echo Form::open(['route'=>['dessertdepartment.store'],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true ,
                       'id'=>'add_files']); ?>


                            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="content-wrap">

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="<?php echo e($loop->iteration == 1 ? 'active' : ''); ?>" id="<?php echo e($language->label); ?>">



                                        <!--  Name -->

                                        <div class="form-group<?php echo e($errors->has("dessert_department_name_$language->label") ? ' has-error' : ''); ?>">
                                            <label for=""><?php echo e(trans('dessertdepartment.dessert_department_name_')); ?></label>
                                            <div class="input-group">

                                                <input type="text" class="form-control" name="name_<?php echo e($language->label); ?>"
                                                       value=""
                                                       id="name_<?php echo e($language->label); ?>" placeholder="<?php echo e(trans('dessertdepartment.dessert_department_name_')); ?>">
                                                <?php if($errors->has("dessert_department_name_$language->label")): ?>
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li><?php echo e($errors->first("dessert_department_name_$language->label")); ?></li></ul>
                                                 </span>
                                                <?php endif; ?>

                                            </div>
                                        </div>


                                        <!--  Description -->

                                        <div class="form-group<?php echo e($errors->has("dessert_department_description_$language->label") ? ' has-error' : ''); ?>">
                                            <label for=""><?php echo e(trans('dessertdepartment.dessert_department_description_')); ?></label>
                                            <div class="input-group">

                                                <input type="text" class="form-control" name="description_<?php echo e($language->label); ?>"
                                                       value=""
                                                       id="description_<?php echo e($language->label); ?>" placeholder="<?php echo e(trans('dessertdepartment.dessert_department_description_')); ?>">
                                                <?php if($errors->has("dessert_department_description_$language->label")): ?>
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li><?php echo e($errors->first("dessert_department_description_$language->label")); ?></li></ul>
                                                 </span>
                                                <?php endif; ?>

                                            </div>
                                        </div>


                                    </section>


                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                


                                <!-- adding option to add icon to newely created main department --> 

                                 <div class="form-group ">

                                    <label for=" ">  &nbsp; <?php echo e(trans('backend.main_cat_icon')); ?></label>
                                    <div class="col-md-12">
                                        <div class="white-box">


                                            <input type="file" id="input-file-now" name="icon"  class="dropifyy" />
                                        </div>
                                    </div>


                                </div>




                                <div class="form-group ">

                                    <label for=" ">  &nbsp; <?php echo e(trans('dessertdepartment.dessert_department_img')); ?></label>
                                    <div class="col-md-12">
                                        <div class="white-box">


                                            <input type="file" id="input-file-now" name="file"  class="dropify" />
                                        </div>
                                    </div>


                                </div>

                                
                                <div class="form-group">
                                    <label for=" "> &nbsp; <?php echo e(trans('backend.status')); ?></label>

                                    <div class="col-md-12">
                                        &nbsp; &nbsp; &nbsp;
                                        <input type="checkbox" name="status" checked class="js-switch" data-color="#99d683" />
                                    </div>

                                </div>

                                

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10"><?php echo e(trans('backend.add')); ?></button>
                                    <button type="reset" value="Reset" class="btn btn-inverse waves-effect waves-light"><?php echo e(trans('backend.reset')); ?></button>

                                </div>
                            </div>






                            <?php echo Form::close(); ?>




                        </div>

                        <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








<?php $__env->stopSection(); ?>




<?php $__env->startSection('scripts'); ?>



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p><?php echo e(trans("department.select_images")); ?></p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"><?php echo e(trans("article.select_images")); ?></p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear"><?php echo e(trans("backend.remove")); ?></button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });

              $('.dropifyy').dropify({
                 
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>