<?php $__env->startSection('page_title' , trans('dessertdepartment.sub_departments')); ?>

<?php $__env->startSection('breadcrumb'); ?>


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('dessertdepartment.sub_departments')); ?></h4>
    </div>





<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">
        <div class="white-box">

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo e(trans('dessertdepartment.dessert_department_name_')); ?></th>
                        <th><?php echo e(trans('dessertdepartment.dessert_department_description_')); ?></th>

                        <th><?php echo e(trans('dessertdepartment.dessert_department_date')); ?></th>
                        <th><?php echo e(trans('dessertdepartment.dessert_department_status')); ?></th>




                        <th class="text-nowrap"><?php echo e(trans('backend.action')); ?></th>

                    </tr>
                    </thead>
                    <tbody>



                    <?php if(count($childrens)): ?>


                        <?php $__currentLoopData = $childrens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sun_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td>
                                    
                                        <?php echo e($sun_cat->name); ?> <br>
                                 
                                </td>





                                <td>
                                    <span class="">
                                         
                                            <?php echo e($sun_cat->description); ?> <br>
                                      

                                    </span>
                                </td>


                                <td><?php echo e(date( 'l jS \of F Y h:i:s A' , strtotime($sun_cat->created_at))); ?></td>



                                <td>
                                    <?php if($sun_cat->status == 'not_active'): ?>
                                        <span class="label label-danger"><?php echo e(trans('backend.not_active')); ?></span>
                                    <?php else: ?>
                                        <span class="label label-success"><?php echo e(trans('backend.active')); ?></span>
                                    <?php endif; ?>

                                </td>





                                <td class="text-nowrap text-center">



                                      <a  href="<?php echo e(route('items' , $sun_cat->subDeptId)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('dessertdepartment.show_related_items')); ?>">
                                          <i class="fa fa-align-justify text-inverse m-r-10"></i>
                                      </a>



                                      <a  href="<?php echo e(route('addNewItem' , $sun_cat->subDeptId )); ?>" data-toggle="tooltip"
                                     data-original-title="<?php echo e(trans('dessertdepartment.add_item')); ?>">
                                     <i class="fa fa-plus-circle text-inverse m-r-10"></i> </a>



                                        <a  href="<?php echo e(route('sub_edit' , $sun_cat->subDeptId)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.edit')); ?>">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                        <a  onclick="$('.dessertdepartment_<?php echo e($sun_cat->subDeptId); ?>').submit();"

                                       data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.delete')); ?>">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    <?php echo Form::open(['route'=>["deleteSubDepartment" , $sun_cat->subDeptId ] ,
                                    'class'=>"dessertdepartment_$sun_cat->id" ]); ?>


                                    <?php echo method_field('DELETE'); ?>




                                    <?php echo Form::close(); ?>


                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>

                        <tr>
                            <td colspan="7" class="text-center"><?php echo e(trans('dessertdepartment.sub_depts_count_zero')); ?></td>

                        </tr>

                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>