<?php $__env->startSection('page_title' , trans('dessertdepartment.main_departments')); ?>

<?php $__env->startSection('breadcrumb'); ?>


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('dessertdepartment.main_departments')); ?></h4>
    </div>





<?php $__env->stopSection(); ?>

<?php  
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
 ?>



<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">
        <div class="white-box">


            <a href="<?php echo e(route('dessertdepartment.create' )); ?>" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; ">Add new main category</a>

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo e(trans('dessertdepartment.dessert_department_name_')); ?></th>
                        

                        <th><?php echo e(trans('dessertdepartment.dessert_department_date')); ?></th>
                        <th><?php echo e(trans('dessertdepartment.dessert_department_status')); ?></th>




                        <th class="text-nowrap"><?php echo e(trans('backend.action')); ?></th>

                    </tr>
                    </thead>
                    <tbody>



                    <?php if($departments->count()): ?>


                        <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td>
                                    <?php $__currentLoopData = $department->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                            <?php if($description->language_id == $language_id ): ?>

                                            <?php echo e($description->name); ?>  

                                            <?php endif; ?> 
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </td>







                                <td><?php echo e(date( 'l jS \of F Y' , strtotime($department->created_at))); ?></td>



                                <td>
                                    <?php if($department->status == 'not_active'): ?>
                                        <span class="label label-danger"><?php echo e(trans('backend.not_active')); ?></span>
                                    <?php else: ?>
                                        <span class="label label-success"><?php echo e(trans('backend.active')); ?></span>
                                    <?php endif; ?>

                                </td>





                                <td class="text-nowrap ">





                                    <?php if(count($department->subDepts)): ?>
                                    <a  href="<?php echo e(route('sub_departments' , $department->id)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.show_sub_departments')); ?>">
                                        <i class="fa fa-align-justify text-inverse m-r-10"></i> </a>

                                    <?php endif; ?>

                                         <a  href="<?php echo e(route('add_new_sub' , $department->id )); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('dessertdepartment.add_sub_department')); ?>">
                                        <i class="fa fa-plus-circle text-inverse m-r-10"></i> </a>

                                        <a  href="<?php echo e(route('dessertdepartment.edit' , $department->id)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.edit')); ?>">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a  onclick="$('.dessertdepartment_<?php echo e($department->id); ?>').submit();"

                                       data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.delete')); ?>">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    <?php echo Form::open(['route'=>["dessertdepartment.destroy" , $department->id ] ,
                                    'class'=>"dessertdepartment_$department->id" ]); ?>


                                    <?php echo method_field('DELETE'); ?>




                                    <?php echo Form::close(); ?>


                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>

                        <tr>
                            <td colspan="7" class="text-center"><?php echo e(trans('dessertdepartment.dessert_department_zero')); ?></td>

                        </tr>

                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>







<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>