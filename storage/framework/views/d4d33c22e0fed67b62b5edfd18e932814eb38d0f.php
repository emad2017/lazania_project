        <?php  

           $lang = LaravelLocalization::getCurrentLocale();
           $language = \App\Language::where(['label'=>$lang])->first();
           $language_id = $language->id ;

         ?>

 


 <!-- ====== Navgition ======  -->
    <nav class="navbar navbar-default bootsnav navbar-mobile bootsnav">
    
      <div class="nav-header">
        <div class="container">
          <div class="col-md-6 col-md-offset-6">
              <p><span><i class="fa fa-phone-square fa-1x"></i> 0554334390</span> </p>
              <!-- lest -->
              <ul class="lest">

                <?php if(Auth::guard('customer')->check()): ?>
                  
                  <?php  
                    $customer = Auth::guard('customer')->user() ; 
                   ?>
                 


                   <li  class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" >  <?php echo e(trans('lazena.welcome')); ?> <?php echo e($customer->fname); ?></a>
                      <ul class="dropdown-menu">
                        
                        <li><a href="<?php echo e(url("$lang/customer/dashboard")); ?>"><?php echo e(trans('lazena.my_account')); ?></a></li>
                        
                        <li><a href="javascript:{}"  style="cursor: pointer;" onclick="document.getElementById('logout_form').submit();"><?php echo e(trans('lazena.log_out')); ?> </a> </li>

                        <?php echo Form::open(['url'=>"$lang/customer/logout" , 'id'=>'logout_form' , 'mehtod'=>'POST']); ?>

                              <!--   Logout Form Construction --> 
                        <?php echo Form::close(); ?>

                      </ul>
                    </li>


                    <li><a href="<?php echo e(url("$lang/customer/wishlist")); ?>"><?php echo e(trans('lazena.wishlist')); ?></a></li>

                  
                 <?php else: ?>
                  
                  <li>
                      <a href="<?php echo e(url("$lang/customer-login")); ?>"><?php echo e(trans('lazena.login')); ?></a>
                  </li>

                 <?php endif; ?>
                <li>
                    <a href="<?php echo e(url("$lang/contact")); ?>"><?php echo e(trans('lazena.find_us')); ?></a>
                </li>


                
              </ul>
              <!-- ./ lest -->

              <!-- lang -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><?php echo $lang == 'en' ? trans('lazena.english') : trans('lazena.arabic'); ?></a>
                <ul class="dropdown-menu">
                  <?php $__currentLoopData = LaravelLocalization::getSupportedLocales(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $localeCode => $properties): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><a rel="alternate" hreflang="<?php echo e($localeCode); ?>" href="<?php echo e(LaravelLocalization::getLocalizedURL($localeCode, null, [], true)); ?>"><?php echo $localeCode == 'en' ? trans('lazena.english') : trans('lazena.arabic'); ?></a></li>
         

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </li>
              <!-- ./ lang -->
          </div>
        </div>
      </div>
      
      <!-- Start Top Search -->
      <div class="top-search">
          <div class="container">
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input type="text" id="search_txt"   class="form-control" placeholder="<?php echo e(trans('lazena.search')); ?>">
                  <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
              </div>

              <div class="lest-searchable">
                <ul id="ul_searchable">



                 

                  
                </ul>
              </div>
          </div>

      </div>
      <!-- End Top Search -->
      
      <div class="container">            
          <!-- Start Atribute Navigation -->
          <div class="attr-nav">
              <ul>
                  
                  <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
              </ul>
          </div>
          <!-- End Atribute Navigation -->

          <!-- Start Header Navigation -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                  <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="<?php echo e(url("/$lang")); ?>">
                <img src="<?php echo e(asset('frontend/assets/img/logo.png')); ?>" class="logo" alt="">
               
              </a>
          </div>
          <!-- End Header Navigation -->

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar-menu">
              <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                

               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><?php echo e(trans('lazena.ellelazena')); ?></a>
                    <ul class="dropdown-menu">
                         <li><a href="<?php echo e(url("$lang/about-us")); ?>">
                          <img src="<?php echo e(asset('frontend/assets/img/icon/icon-about.png')); ?>" alt="">
                          <?php echo e(trans('lazena.about_us')); ?>

                        </a>
                </li>
                       
                       
                    </ul>
                </li>


                  <li><a href="<?php echo e(url("$lang/categories")); ?>"><?php echo e(trans('lazena.categories')); ?></a></li>

                  <!-- megamenu -->
                  <li class="dropdown megamenu-fw">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo e(trans('lazena.products')); ?></a>
                      <ul class="dropdown-menu megamenu-content" role="menu">
                          <li>
                              <div class="row">


                                <?php if(count($navigationCategoriesContent) > 0 ): ?>
                                <?php $__currentLoopData = $navigationCategoriesContent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <?php if($cat->language_id == $language_id): ?>
                                      <?php  
                                          $icon = $cat->dessertdepartments->icon ; 

                                          $clean_name = strtolower(str_replace(' ', '_', $cat->name)) ;
                                       ?>
                                      <div class="col-menu col-md-2">
                                      
                                              <ul class="menu-col">
                                                  <li><a href="<?php echo e(url("$lang/$cat->department_id/sub-categories")); ?>">
                                                    <img src="<?php echo e(asset("uploads/dessertdepartment/icons/$icon")); ?>" alt="">
                                                    <span><?php echo e($cat->name); ?></span></a>
                                                  </li>
                                              </ul>
                                           
                                      </div> 
                                      <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                   

                                 
 
                              </div><!-- end row -->
                          </li>
                      </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo e(trans('lazena.services')); ?></a>
                      <ul class="dropdown-menu">
                          
                          <?php if(count($specialOccasionsContent)>0): ?>
                              <?php $__currentLoopData = $specialOccasionsContent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocassionDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <?php if($ocassionDescription->language_id == $language_id): ?> 


                                    <li>
                                      <a href="<?php echo e(url("$lang/special-occassion/$ocassionDescription->event_id/$ocassionDescription->name")); ?>">
                                        <img src="<?php echo e(asset('frontend/assets/img/icon/icon-wedding.png')); ?>" alt="">
                                        <?php echo e($ocassionDescription->name); ?>

                                      </a>
                                    </li>
                                  <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                      </ul>
                  </li>

                

                  <li><a href="<?php echo e(url("$lang/careers_list")); ?>"><?php echo e(trans('lazena.careers')); ?></a></li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo e(trans('lazena.contact_us')); ?></a>
                      <ul class="dropdown-menu">
                          <li>
                            <a href="<?php echo e(url("$lang/contact")); ?>">
                              <img src="<?php echo e(asset('frontend/assets/img/icon/icon-contact.png')); ?>" alt="">
                              <?php echo e(trans('lazena.contact_info')); ?>

                            </a>
                          </li>
                         
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </div>   
    </nav>
		<!-- ====== End Navgition ======  -->