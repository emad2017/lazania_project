<?php $__env->startSection('page_title' , trans('lazena.shopping_cart')); ?>


<?php $__env->startSection('styles'); ?>
  
    <style>
      
          .active{
            color: #f8408 ; 
          }


          input.input-number::-webkit-outer-spin-button,
          input.input-number::-webkit-inner-spin-button {
              /* display: none; <- Crashes Chrome on hover */
              -webkit-appearance: none;
              margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
          }


           input.input-number::-moz-outer-spin-button,
          input.input-number::-moz-inner-spin-button {
              /* display: none; <- Crashes Chrome on hover */
              -moz-appearance: none;
              margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
          }


    </style>

<?php $__env->stopSection(); ?> 

  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


   ?>


<?php $__env->startSection('pages_content'); ?>
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="<?php echo e(url("/$lang")); ?>"><?php echo e(trans('lazena.home')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="<?php echo e(url("/$lang/customer/dashboard")); ?>"><?php echo e(trans('lazena.customer')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li> <?php echo e(trans('lazena.shopping_cart')); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->



    <!-- start shopping cart structure  --> 
        
      
    <section class="shopping-cart section-padding">
      <div class="container">
        <div class="row">
          <!-- Title -->
          <div class="col-sm-15 section-title">
            <h3><?php echo e(trans('lazena.shopping_cart')); ?></h3>
            <button class="button button-hot hvr-shutter-out-horizontal pull-right" onclick="window.location.href='<?php echo e(url("$lang/categories")); ?>'"><?php echo e(trans('lazena.continue_shopping')); ?></button>
          </div>

         
          <!-- side bar -->
          <div class="col-sm-12 col-md-4 col-lg-3 sidebar mt-30 mb-30" id="main_cart_box">
            <div class="cart-totals">
              <div class="cart-title">
                <h3>Cart Totals</h3>
              </div>

              <div class="item">
                <h4><?php echo e(trans('lazena.sub_total')); ?></h4>
                <span class="sub_total"><?php echo e(trans('lazena.s_r')); ?> 0.00</span>
              </div>

              <div class="item">
                <h4><?php echo e(trans('lazena.delivery')); ?> </h4>
                <span class="delivery"><?php echo e(trans('lazena.s_r')); ?> 0.00</span>
              </div>

              <div class="item">
                <h4><?php echo e(trans('lazena.total')); ?></h4>
                <span class="total"> </span>
              </div>

              <div class="bttn">
                <button id="checkout_btn" class="button button-hot hvr-shutter-out-horizontal" onclick="window.location.href='<?php echo e(url("$lang/customer/checkout")); ?>'"><?php echo e(trans('lazena.proceed_checkout')); ?></button>
                  
              </div>

            </div>
          </div>
          <!-- ./ side bar -->

          <!-- Content Shopping -->
          <div class="col-sm-12 col-md-8 col-lg-9 mt-30 mb-30">
            <div class="shopping-content">
              <ul id="products_holder_ul" >
                <li>
                  <div class="qty"><?php echo e(trans('lazena.qty')); ?></div>
                  <span><?php echo e(trans('lazena.item')); ?></span>
                   
                  <span class="price"><?php echo e(trans('lazena.price')); ?></span>
                  
                </li>

 
               

                 

               
              </ul>
              
              
               

                <div id="notice_hidden"  class="col-sm-12 col-md-12 col-lg-12 mt-30 mb-30 hidden ">
                    <div class="alert alert-warning alert-dismissable text-center">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <?php echo e(trans('lazena.no_item_found')); ?>

                    </div>
                </div>


            </div>
          </div>
        </div>
      </div>
    </section>
 

    <!-- end shopping cart structure --> 
 

<?php $__env->stopSection(); ?> 

 



<?php $__env->startSection('scripts'); ?>
      
    <script> 
    </script>

    <script>
      
        // inside here i need to hide the flyer shopping cart cause it make sense -_- 
        $(document).ready(function(){

            // when dom is ready find flyer cart  
            var cartWrapper = $('.cd-cart-container');

            cartWrapper.addClass('empty');
 
        });

    </script>  

    <!-- inject products in cart dynaically to dom with javascript -_- --> 
    <script>
      
        $(document).ready(function(){


              // cache pasting elements needed -_- 
              var  products_holder = $('#products_holder_ul');

              var delivery = $('.delivery'); 
              var sub_total = $('.sub_total');
              var total     = $('.total'); 

              var quantity =  0 ; 
              var price    =  0 ; 


              var subtotal_integer = 0 ; 
              var delivery_integer = 25.00 ; 
              var total_integer = 0 ; 


              if( sessionStorage.getItem('products') !== null ){

                    

                  // that means i have injected session array called products living in my browser -_- 
                  
                  var productsArray = JSON.parse(sessionStorage.getItem('products'));

                  // after having my prodcuts array of objects i will draw them in dom -_- 
                  // i will not store this into database yet untill user confirm and proceed to checkout -_- 
                  // so am looking for the best practice of database normalization 
                  if(productsArray.length != 0 ){
 
                        productsArray.forEach(function(product){

                             $('#products_holder_ul:first-child').append(`
                              
                               <li class="item">
                                  <div class="">
                                    <span class="input-number-decrement   sub_qty" data-product-price="${product.product_price}" data-product-id="${product.product_id}" >–</span>
                                    <input id="input_quan_${product.product_id}"  data-pr-qt=${product.product_quantity} data-selected-quantity="${product.selected_quantity}"  data-pr-price=${product.product_quantity}  class="input-number input_quantity " type="number" readonly  value="${product.selected_quantity}"  ><span class="input-number-increment add_qty " data-product-price=" ${product.product_price}" data-product-id="${product.product_id}">+</span>
                                  </div>

                                  <div class="pro-item">
                                    <a href="${product.product_image}" class="fresco" ><img src="${product.product_image}" alt="${product.product_image}"></a>
                                    ${product.product_name}
                                  </div>
                                  <div class="popbtn">
                                    <i class="fa fa-times delete_product" data-product-id="${product.product_id}" data-product-price="${product.product_price}"    aria-hidden="true"></i>
                                  </div>
                                  
                                  <div class="price price_label">S.R ${product.product_price}</div>
                                </li>
                                 
                            `);

                             console.log(product.product_price);


                            // this at the begining -_- 
                            var single_quantity = product.selected_quantity; 
                          
                            var single_quantity_parsed = parseInt(single_quantity);
                            quantity = quantity + single_quantity_parsed ; 
                            price = price + single_quantity_parsed*Number(product.product_price);
                            
                            $('.price_label').text(`<?php echo e(trans("lazena.s_r")); ?>  ${product.product_price} `);
                           

                        }); // end foreach 

                              delivery.text(`<?php echo e(trans('lazena.s_r')); ?> ${parseInt("25").toFixed(2)}`); 
                              sub_total.text(`<?php echo e(trans('lazena.s_r')); ?> ${price.toFixed(2)}`); 
                              var total_price = parseInt(price) + 25 ; 
       
                              total.text(`<?php echo e(trans('lazena.s_r')); ?> ${total_price.toFixed(2)}`);

                              subtotal_integer =  price.toFixed(2)  ; 
                              delivery_integer = 25.00 ; 
                              total_integer =  total_price.toFixed(2)  ; 



                        

                             $('#main_cart_box').removeClass('hidden');
                   
                  } // end chck on array length -_- 
                  else{



                      $('#notice_hidden').removeClass('hidden');
                      $('#main_cart_box').addClass('hidden');
                      $('#total_div').remove(); 
                      $('#btn_checkout_div').remove(); 


                  }
                    

              } // end check for null 


              $('.add_qty').on('click' , function(){

                   addProduct($(this).prev());
              });


              $('.sub_qty').on('click' , function(){

                   subtractProduct($(this).next());
              });


              $('.delete_product').on('click', function(){

                 var productId =  $(this).attr('data-product-id') ; 

                 deleteProduct( $(this) ,  productId); 

              });
              



               // add new quantity
              function addProduct(input){

                    var inStock = Number(input.attr('data-pr-qt'));
                    var productPrice =  input.next().attr('data-product-price'); 
                    var productId =  input.next().attr('data-product-id'); 
                    
                    var current_value = input.attr('value') ; 
                    var new_value = Number(current_value) + 1 ; 

                    if(new_value > inStock){

                      input.attr('value' , current_value );
                      input.next().attr('disabled' , true );
                      input.next().css('background-color' , '#c8c8c8' );

                      
                    }else{

                      input.attr('value' , new_value );
                      input.prev().removeAttr('disabled');
                      input.prev().css('background-color' , '#f1f1f1' );

                      // so now i need to think -_- about incrementing prices on the fly 
                     
                      quantity = quantity + current_value ; 
                      price = price +  Number(productPrice);


                      delivery.text(`<?php echo e(trans('lazena.s_r')); ?> ${parseInt("25").toFixed(2)}`); 
                      sub_total.text(` <?php echo e(trans('lazena.s_r')); ?> ${price.toFixed(2)}`); 
                      var total_price = parseInt(price) + 25 ; 

                      total.text(` <?php echo e(trans('lazena.s_r')); ?>  ${total_price.toFixed(2)}`); 


                      var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 

                      if(parseJsonProducts != null ){

                        parseJsonProducts.forEach(function(product , index){

                            if(parseJsonProducts[index].product_id == productId ){

                              parseJsonProducts[index].selected_quantity = new_value ;  
                             

                              var arrToJsonString = JSON.stringify(parseJsonProducts) ; 

                              sessionStorage.setItem('products' , arrToJsonString ) ; 

                                  return false;
                            }
                        });

                      }





                      subtotal_integer =  price.toFixed(2)  ; 
                      delivery_integer = 25.00 ; 
                      total_integer =  total_price.toFixed(2)  ; 



                        // do like what you did in remove () 


                    }}

              // subtract quantity from product -_- 
              function subtractProduct(input){

                   var inStock = Number(input.attr('data-pr-qt'));
                   var productPrice =  input.prev().attr('data-product-price');
                   var productId =  input.prev().attr('data-product-id'); 
                   
                   var current_value = Number(input.attr('value')) ; 
                   // console.log(current_value); 
                   if(current_value ==  1 ){

                     
                      input.prev().attr('disabled' , true );
                      input.prev().css('background-color' , '#c8c8c8' );

                   }else{

                     input.next().removeAttr('disabled');
                     input.next().css('background-color' , '#f1f1f1' ); 

                     var new_value = Number(current_value) - 1 ; 
                     input.attr('value' ,  new_value   );

                      // so now i need to think -_- about incrementing prices on the fly 
                     
                    // quantity = quantity + current_value ; 
                    price = price - Number(productPrice);


                    delivery.text(`<?php echo e(trans('lazena.s_r')); ?>  ${parseInt("25").toFixed(2)}`); 
                    sub_total.text(` <?php echo e(trans('lazena.s_r')); ?>  ${price.toFixed(2)}`); 
                    var total_price = parseInt(price) + 25 ; 

                    total.text(` <?php echo e(trans('lazena.s_r')); ?> ${total_price.toFixed(2)}`); 


                    var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 

                      if(parseJsonProducts != null ){

                        parseJsonProducts.forEach(function(product , index){

                            if(parseJsonProducts[index].product_id == productId ){

                              parseJsonProducts[index].selected_quantity = new_value ;  
                             

                              var arrToJsonString = JSON.stringify(parseJsonProducts) ; 

                              sessionStorage.setItem('products' , arrToJsonString ) ; 

                                  return false;
                            }
                        });

                      }












                      subtotal_integer =  price.toFixed(2)  ; 
                        delivery_integer = 25.00 ; 
                        total_integer =  total_price.toFixed(2)  ; 


                   }
              }


              // function to delete the products with its current value  multiple product price  -_-
              function deleteProduct(input, productId){
                  
                  var product_id =   productId ; 

                  $(`a.to_be_hide_${product_id}`).removeClass('hidden');

                  $(`a.item_${product_id}`).addClass('hidden');


                  


                  // i need to delete the product from the array of objects stored in session

                  if(sessionStorage.getItem('products') != null ){

                      var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 

                        if(parseJsonProducts != 0 ){

                          parseJsonProducts.forEach(function(product , index){

                              if(parseJsonProducts[index].product_id == product_id ){

                                     
                                    parseJsonProducts.splice(index,1);


                                     // then subtract current value of quantity -_- and total of this from total price 

                                        productPrice  = Number(product.product_price) ; 

                                        product_current_value =   $(`#input_quan_${product.product_id}`).attr('value')  ; 
                                        console.log(product_current_value);
                                        // total subtracted ( or intended to be subtract -_- )
                                        to_be_subtracted =  productPrice * product_current_value ; 

                                        console.log(to_be_subtracted);

                                        // then subtract it from the grand total -_- 
                                        
                                        price = price - to_be_subtracted ; 

                                        console.log(price);

                                        delivery.text(`<?php echo e(trans('lazena.s_r')); ?>  ${Number("25").toFixed(2)}`); 
                                        sub_total.text(` <?php echo e(trans('lazena.s_r')); ?> ${price.toFixed(2)}`); 
                                        var total_price = Number(price) + 25 ; 

                                        total.text(` <?php echo e(trans('lazena.s_r')); ?> ${total_price.toFixed(2)}`);


                                          subtotal_integer =  price.toFixed(2)  ; 
                                          delivery_integer = 25.00 ; 
                                          total_integer =  total_price.toFixed(2)  ; 


                                        var arrToJsonString = JSON.stringify(parseJsonProducts) ; 

                                        sessionStorage.setItem('products' , arrToJsonString ) ; 

                                        // input.parent().parent().remove();
                                        

                                        if($('ul li.item').length == 1){

                                            

                                            $('ul#products_holder_ul').next().remove(); 
                                            // $('ul#products_holder_ul').next().remove(); 

                                            $('#notice_hidden').removeClass('hidden');

                                            $('#checkout_btn').addClass('hidden');


                                        } 
                                        
                                        input.closest('ul li.item').fadeOut( "fast" , function( ){
                                            $(this).remove();  
                                        }); 





                                       



                                        return false;
                                  }
                              });


                        }

                  }

                  

              }



              $('#checkout_btn').on('click' , function(){

                    var obj = {

                      sub_total: subtotal_integer ,
                      delivery:delivery_integer ,
                      total: total_integer 

                    }

                    var objString = JSON.stringify(obj); 


                    sessionStorage.setItem('checkout_final' , objString) ; 


                    /**
                     * i need to send all products with its selected values as an array 
                     * of objects -_- 
                     * each object will hold the selected product and the selected quantity 
                     */


                    


            });

              

        });



         

    </script>
  
<?php $__env->stopSection(); ?> 






<?php echo $__env->make('frontend.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>