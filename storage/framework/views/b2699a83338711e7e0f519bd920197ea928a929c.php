<?php $__env->startSection('page_title' , "$un_cleaned_cat_name - $un_cleaned_sub_cat_name - $un_cleaned_product_name"); ?>


<?php $__env->startSection('styles'); ?>

   <style>
        
        .item_added {
            
                display: inline-block;
                padding: 0.6em 0.9em;
                background: #f8408c ;
                border-radius: 50em;
                text-transform: lowercase;
                color: #ffffff;
                font-weight: 100;
                letter-spacing: .1em;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
                -webkit-transition: all .2s;
                transition: all .2s;

        }

    </style>

<?php $__env->stopSection(); ?> 

  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



   ?>


<?php $__env->startSection('pages_content'); ?>

      <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="<?php echo e(url("/$lang")); ?>"><?php echo e(trans('lazena.home')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="<?php echo e(url("$lang/categories")); ?>"><?php echo e(trans('lazena.categories')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
              <?php  

                  $cleaned_cat_name =  $un_cleaned_cat_name  ; 
                  $cleaned_sub_cat_name =  $un_cleaned_sub_cat_name  ; 

               ?>
              <li><a href="<?php echo e(url("$lang/$id/sub-categories")); ?>"><?php echo e($un_cleaned_cat_name); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>


              <li><a href="<?php echo e(url("$lang/$id/sub-categories/$sub_id/products")); ?>"><?php echo e($un_cleaned_sub_cat_name); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>

              <li><?php echo e($un_cleaned_product_name); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ======   ======  -->
    <div class="main-product section-padding">
      <div class="container">
        <div class="row">
          <div class="col-sm-10">
            <div class="row">

              <!-- product img -->
              <div class="product-img-box col-sm-6">
                <div class="flexslider">
                  <ul class="slides">

                    
                    <!-- we will update this is the future to handle many images  --> 
                    <!-- but for now we stick with only one image  --> 
                    <?php if(count($product_holder) > 0 ): ?>
                    
                     <?php  
                         $product_image = $product_holder->dessertitem->img1 ; 
                         $product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$product_image ; 
                         $product_rate  = $product_holder->dessertitem->rate ; 
                         $product_price  = $product_holder->dessertitem->price ;

                          $product_quantity = $product_holder->dessertitem->quantity ; 
                          $product_id = $product_holder->dessertitem->id ; 
                      ?> 
                    
                        <li data-thumb="<?php echo e(asset("uploads/dessertsitem/610_383/$product_image")); ?>">
                          <a href='<?php echo e(url("uploads/dessertsitem/original/$product_image")); ?>' class="fresco" ><img src="<?php echo e(asset("uploads/dessertsitem/610_383/$product_image")); ?>" /></a>
                        </li>

                    <?php else: ?>
                        
                        <li data-thumb="<?php echo e(asset("uploads/placeholder/placeholder.jpg")); ?>">
                          <img src="<?php echo e(asset("uploads/placeholder/placeholder.jpg")); ?>" />
                        </li> 
                    
                    <?php endif; ?>
                    
                  </ul>
                </div>
              </div>
              <!-- ./ product img -->

              <!-- product shop -->
              <div class="product-shop col-sm-6">
                <!-- Sub title -->
                <div class="sub-title">
                  <h3><?php echo e($product_holder->name); ?></h3>
                </div>
    
                <div class="ratings mb-30">
                  <div class="rat">
                          <?php if($product_rate == 1): ?>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                          <?php elseif($product_rate == 2): ?>

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          <?php elseif($product_rate == 3): ?>

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          <?php elseif($product_rate == 4): ?>

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          <?php else: ?>

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                          <?php endif; ?>
                  </div>
                </div>
                
                <div class="de mb-30">
                    <p><?php echo e($product_holder->description); ?></p>
                </div>

                <div class="product-info mb-30">
                  <h5><?php echo e(trans('lazena.price_label')); ?> </h5>
                  <div class="price-box">
                    <h4><?php echo e(trans('lazena.s_r')); ?> <?php echo e($product_price); ?></h4>
                  </div>
                </div>

                <div class="add-to-box">

                  

                 <!-- i  need to check if this product added to current logged in customer wishlist or not -_-   to not addit again  -->
                                        
                                        <?php if(Auth::guard('customer')->check()): ?>
                                            
                                          <?php  
                                              
                                              // if the user logged in i will find if that product found on his/her wishlist or not  
                                              // if found then shows maybe a thumb that this is already added to whishlist  
                                              // else let him see the button normally  -_- 
                                              
                                              $customer = Auth::guard('customer')->user() ; 

                                              $customerAddedToWishlistProduct = $customer->wishlistProduct->where('product_id' ,$product_id)->first(); 

                                               

                                              

                                           ?>   
                                          
                                          <?php if(count($customerAddedToWishlistProduct) > 0 ): ?>
                                            <a class="button button-hot hvr-shutter-out-horizontal"  ><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                            </a>
                                          <?php else: ?>

                                            <a class="button button-hot hvr-shutter-out-horizontal" href='<?php echo e(url("$lang/customer/add_product_to_whislist/$product_id")); ?>'><i class="fa fa-heart" aria-hidden="true"></i>
                                            </a>
                                          <?php endif; ?>
                                        
                                        <?php else: ?>
                                          
                                          <a class="button button-hot hvr-shutter-out-horizontal" href='<?php echo e(url("$lang/customer/add_product_to_whislist/$product_id")); ?>'><i class="fa fa-heart" aria-hidden="true"></i>
                                          </a>

                                        <?php endif; ?>

                  
                  <?php if($product_quantity > 0 ): ?>
                   <a   href="#0" product-id="<?php echo e($product_id); ?>"  product-price="<?php echo e($product_price); ?>" product-quantity="<?php echo e($product_quantity); ?>"  product-image="<?php echo e($product_image_path); ?>" product-name="<?php echo e($product_holder->name); ?>" class="cd-add-to-cart to_be_hide_<?php echo e($product_id); ?>  button button-hot hvr-shutter-out-horizontal first"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php echo e(trans('lazena.add_to_cart')); ?></a>

                  <a  class="button button-hot hvr-shutter-out-horizontal  item_added   hidden item_<?php echo e($product_id); ?>"><?php echo e(trans('lazena.item_added')); ?></a>

                   <?php else: ?>
                                
                     <a  class="button button-hot hvr-shutter-out-horizontal  item_added"><?php echo e(trans('lazena.out_of_stock')); ?></a>

                  <?php endif; ?>



                </div>

              </div>
              <!-- ./ product shop -->

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ======  ======  -->

<?php $__env->stopSection(); ?> 

 



<?php $__env->startSection('scripts'); ?>
      
      
  
<?php $__env->stopSection(); ?> 






<?php echo $__env->make('frontend.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>