<?php $__env->startSection('page_title' , trans('speacialevent.speacialevent_name')); ?>

<?php $__env->startSection('breadcrumb'); ?>


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('speacialevent.speacialevent_name')); ?></h4>
    </div>





<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">
        <div class="white-box">
            
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo e(trans('speacialevent.speacialevent_name_')); ?></th>
                        <th><?php echo e(trans('speacialevent.speacialevent_description_')); ?></th>
                  

                   
                        <th><?php echo e(trans('backend.image')); ?></th>
                        <th><?php echo e(trans('speacialevent.speacialevent_date')); ?></th>
                        <th><?php echo e(trans('speacialevent.speacialevent_status')); ?></th>



                        <th class="text-nowrap"><?php echo e(trans('backend.action')); ?></th>

                    </tr>
                    </thead>
                    <tbody>



                    <?php if(count($events)): ?>


                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                <?php     


                                    $event_image = $event->specialevent->img ; 

                                 

                                  ?>


                                <tr>
                                    <td><?php echo e($loop->iteration); ?></td>
                                    <td> <?php echo e($event->name); ?> </td>
                                    <td> <?php echo e(str_limit($event->description , 100)); ?> </td>


                                   

                                    <td> 
                                            <div class="" id="images">

                                                <div class="col-sm-2">
                                                    <a  href="<?php echo e(asset("uploads/speacialevent/$event_image")); ?>" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                        <img style="width: 70px; height:70px;border-radius:50%;" src="<?php echo e(asset("uploads/speacialevent/$event_image")); ?>" class="img-responsive fresco" />
                                                    </a>

                                                    <style>
                                                        .img-responsive
                                                        {
                                                            max-width: none !important;

                                                        }
                                                    </style>
                                                </div>
                                            </div> 
                                    </td>
 


                                

                                <td><?php echo e($event->created_at); ?></td>



                                <td>
                                    <?php if($event->status == 'not_active'): ?>
                                        <span class="label label-danger"><?php echo e(trans('backend.not_active')); ?></span>
                                    <?php else: ?>
                                        <span class="label label-success"><?php echo e(trans('backend.active')); ?></span>
                                    <?php endif; ?>

                                </td>




                                <td class="text-nowrap">






                                    <a  href="<?php echo e(route('speacialevent.edit' , $event->event_id)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.edit')); ?>">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.speacialeventـ<?php echo e($event->event_id); ?>').submit();"
                                       data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.delete')); ?>">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    <?php echo Form::open(['route'=>["speacialevent.destroy" , $event->event_id ] ,
                                    'class'=>"speacialeventـ$event->event_id" ]); ?>


                                    <?php echo method_field('DELETE'); ?>




                                    <?php echo Form::close(); ?>


                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>

                        <tr>
                            <td colspan="7" class="text-center"><?php echo e(trans('speacialevent.speacialevent_zero')); ?></td>

                        </tr>

                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>









<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>