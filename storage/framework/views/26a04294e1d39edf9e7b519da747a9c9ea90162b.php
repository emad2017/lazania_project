
        <?php 
                if(Auth::check()){

                    // user authenticated
                    $user = Auth::user();

                    $user_name = '' ;

                    $userType = $user->userType;

                    if($userType == 'doctor'){

                        $user_name = $user->doctorName ;

                    }else{

                         $user_name = $user->headPersonName ;

                    }


                    $user_id = $user->id ;
                    $user_image = $user->profileImage;

                }
         ?>


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="
                                                         <?php if($userType == 'adminstrator'): ?>
                                                            <?php if(count($user_image) ): ?>
                                                                    <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/adminstrator-avatar.png')); ?>

                                                            <?php endif; ?>
                                                        <?php elseif($userType == 'doctor'): ?>
                                                             <?php if(count($user_image) ): ?>
                                                                <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/doctor-avatar.jpg')); ?>

                                                            <?php endif; ?>
                                                        <?php elseif($userType == 'pharmacy'): ?>

                                                             <?php if(count($user_image) ): ?>
                                                                <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/pharmacy-avatar.jpg')); ?>

                                                            <?php endif; ?>

                                                        <?php elseif($userType == 'hospital'): ?>
                                                             <?php if(count($user_image) ): ?>
                                                                <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/hospital-avatar.png')); ?>

                                                            <?php endif; ?>

                                                        <?php elseif($userType == 'medicalSupplies'): ?>

                                                             <?php if(count($user_image) ): ?>
                                                                <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/medical-supply-avatar.png')); ?>

                                                            <?php endif; ?>

                                                        <?php elseif($userType == 'pharmaCompany'): ?>
                                                             <?php if(count($user_image) ): ?>
                                                                <?php echo e(asset("uploads/users/$user_image")); ?>

                                                            <?php else: ?>
                                                                <?php echo e(asset('avatars/pharma-company-avatar.jpg')); ?>

                                                            <?php endif; ?>

                                                        <?php endif; ?>

                                                            " alt="user-img" class="img-circle">
                            <span class="hide-menu"><?php echo e($user_name); ?><span class="fa arrow"></span>  </span>
                        </a>
                        <ul class="nav nav-second-level">

                            <li><a href='<?php echo e(url("user/profile/$user_id")); ?>'><i class="ti-user"></i><?php echo e(trans('backend.my_profile')); ?></a></li>

                            <li>
                                 <?php echo Form::open(['url'=>'logout' , 'method'=>'POST' , 'id'=>'logout_form']); ?>

                                <a href="javascript:{}" onclick="document.getElementById('logout_form').submit();"><i class="fa fa-power-off"></i> <?php echo e(trans('backend.logout')); ?></a>
                                 <?php echo Form::close(); ?>

                            </li>



                        </ul>
                    </li>



                    <?php 

                        $lang = LaravelLocalization::getCurrentLocale();

                     ?>
                    <li>
                        <a href="<?php echo e(url('dashboard')); ?>" class="waves-effect <?php echo e(Request::is("$lang/dashboard") ? 'active' : ''); ?> ">
                            <i class="fa fa-tachometer"></i>
                            <span class="hide-menu"> <?php echo e(trans('backend.dashboard')); ?> </span>
                        </a>

                    </li>





                    <li>

                        <a href="#" class="waves-effect  <?php echo Request::path() == "$lang/language"   ? 'active' : ''; ?>

                            <?php echo Request::path() == "$lang/language/create"   ? 'active' : ''; ?>  ">
                            <i class="fa fa-language"></i>
                            <span class="hide-menu"> <?php echo e(trans('backend.language')); ?>

                                <span class="fa arrow"></span>
                            </span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li> <a href="<?php echo e(route('language.create')); ?>"><?php echo e(trans('backend.create_language')); ?></a> </li>
                            <li> <a href="<?php echo e(route('language.index')); ?>"><?php echo e(trans('backend.show_languages')); ?></a> </li>

                        </ul>
                    </li>





                     


                     <li>

                        <a href="#" class="waves-effect  <?php echo e(Request::path() == "$lang/customers"   ? 'active' : ''); ?>

                                <?php echo e(Request::path() == "$lang/customers/create"   ? 'active' : ''); ?> ">
                                <i class="fa  fa-users"></i>
                                <span class="hide-menu"><?php echo e(trans('backend.customers')); ?>

                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            
                            <li><a href="<?php echo e(route('customers.index')); ?>"><?php echo e(trans('backend.list_customers')); ?></a></li>
                        </ul>

                     </li>


                    
                    <!-- Slider Module --> 

                     <li>

                        <a href="#" class="waves-effect  <?php echo e(Request::path() == "$lang/slider"   ? 'active' : ''); ?>

                                <?php echo e(Request::path() == "$lang/slider/create"   ? 'active' : ''); ?> ">
                                <i class="fa fa-crosshairs"></i>
                                <span class="hide-menu"><?php echo e(trans('backend.slider_module')); ?>

                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('slider.create')); ?>"><?php echo e(trans('backend.add_slide')); ?></a></li>
                            <li><a href="<?php echo e(route('slider.index')); ?>"><?php echo e(trans('backend.list_sliders')); ?></a></li>
                        </ul>

                     </li>


                      <li>

                        <a href="#" class="waves-effect  <?php echo e(Request::path() == "$lang/orders"   ? 'active' : ''); ?>

                                ">
                                <i class="fa  fa-users"></i>
                                <span class="hide-menu"><?php echo e(trans('backend.orders_module')); ?>

                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            
                            <li><a href="<?php echo e(url("$lang/orders")); ?>"><?php echo e(trans('backend.list_orders')); ?></a></li>
                        </ul>

                     </li>




                    
                    <li>

                        <a class="waves-effect   ">
                            <i class="fa fa-th-large"></i>
                            <span  class="hide-menu"><?php echo e(trans('dessertdepartment.dessert_department_name')); ?>

                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('dessertdepartment.create')); ?>"><?php echo e(trans('dessertdepartment.dessert_department_create')); ?></a></li>
                            <li><a href="<?php echo e(route('dessertdepartment.index')); ?>"><?php echo e(trans('dessertdepartment.dessert_department_show')); ?></a></li>
                        </ul>

                    </li>



                    
                    
                    
                   


                    
                    <li>

                        <a class="waves-effect " href="<?php echo e(route('careers_backend')); ?>" >
                            <i class="fa fa-pencil-square"></i>
                            <span  class="hide-menu"><?php echo e(trans('backend.careers')); ?>

                                 
                                </span>
                        </a>

                        

                    </li>

                    
                    <li>

                        <a class="waves-effect  ">
                            <i class="fa fa-thumb-tack"></i>
                            <span  class="hide-menu"><?php echo e(trans('speacialevent.speacialevent')); ?>

                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('speacialevent.create')); ?>"><?php echo e(trans('speacialevent.add_event')); ?></a></li>
                            <li><a href="<?php echo e(route('speacialevent.index')); ?>"><?php echo e(trans('speacialevent.show_event')); ?></a></li>
                        </ul>

                    </li>


                    
                 

                    
                    <li>

                        <a class="waves-effect">
                            <i class="fa fa-users"></i>
                            <span  class="hide-menu"><?php echo e(trans('dessertdepartment.team')); ?>

                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('team.create')); ?>"><?php echo e(trans('dessertdepartment.add_team')); ?></a></li>
                            <li><a href="<?php echo e(route('team.index')); ?>"><?php echo e(trans('dessertdepartment.show_team')); ?></a></li>
                        </ul>

                    </li>


                    
                   <li>
                        <a href="#" class="waves-effect  "><i   class="fa  fa-sliders"></i> <span class="hide-menu"><?php echo e(trans('about.about_sidebar')); ?><span class="fa arrow"></span> </span></a>
                                       <ul class="nav nav-second-level">
                                        <li><a href="<?php echo e(route('about')); ?>"><?php echo e(trans('about.about')); ?></a></li>
                                                   </ul>
                    </li>

                    

                    <li>

                        <a href="#" class="waves-effect  <?php echo e(Request::path() == "$lang/say"   ? 'active' : ''); ?>

                        <?php echo e(Request::path() == "$lang/say/create"   ? 'active' : ''); ?> ">
                            <i class="fa  fa-volume-up"></i>
                            <span class="hide-menu"><?php echo e(trans('say.say')); ?>

                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('say.create')); ?>"><?php echo e(trans('say.create_say')); ?></a></li>
                            <li><a href="<?php echo e(route('say.index')); ?>"><?php echo e(trans('say.show_says')); ?></a></li>
                        </ul>

                    </li>

                    <!-- newsletter Module -->
                    <li>

                        <a href="#" class="waves-effect  <?php echo e(Request::path() == "$lang/newsletter"   ? 'active' : ''); ?>

                        <?php echo e(Request::path() == "$lang/newsletter/send-email"   ? 'active' : ''); ?> ">
                            <i class="fa  fa-envelope-o"></i>
                            <span class="hide-menu"><?php echo e(trans('newsletter.newsletter')); ?>

                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo e(route('newsletter')); ?>"><?php echo e(trans('newsletter.show_all_subscriptions')); ?></a></li>
                        </ul>

                    </li>


                      
                   <li ><a href="<?php echo e(route('visitorsMessages.index')); ?>" class="waves-effect  ">
                                            <i   class="fa fa-mail-forward"></i>
                                            <span class="hide-menu"><?php echo e(trans('backend.visitors_messages')); ?></span></a>


                   </li>


              

                </ul>




            </div>
        </div>
        <!-- Left navbar-header end
