<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo e(trans('lazena.login_page')); ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
     
    <link rel="icon" type="image/png" href="<?php echo e(asset('ellelazena_login/assets/images/icons/favicon.ico')); ?>"/>
<!--===============================================================================================-->
    
    <?php echo e(Html::style('ellelazena_login/assets/vendor/bootstrap/css/bootstrap.min.css')); ?>

<!--===============================================================================================-->
    
    <?php echo e(Html::style('ellelazena_login/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')); ?>

<!--===============================================================================================-->
    
    <?php echo e(Html::style('ellelazena_login/assets/vendor/animate/animate.css')); ?>

<!--===============================================================================================-->  
    
    <?php echo e(Html::style('ellelazena_login/assets/vendor/css-hamburgers/hamburgers.min.css')); ?>

<!--===============================================================================================-->
    
    <?php echo e(Html::style('ellelazena_login/assets/vendor/select2/select2.min.css')); ?>

<!--===============================================================================================-->
    
    <?php echo e(Html::style('ellelazena_login/assets/css/util.css')); ?>

    
    <?php echo e(Html::style('ellelazena_login/assets/css/main.css')); ?>

<!--===============================================================================================-->

    <?php  

            $lang = LaravelLocalization::getCurrentLocale() ;  

     ?> 


    <?php if( $lang == 'ar'): ?>

           <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan&amp;subset=arabic,latin-ext" rel="stylesheet">

            
            <style>
                
               .login100-form-title , .input100  , .login100-form-btn , .in_arabic{

                    font-family: 'Baloo Bhaijaan', cursive;
                }
            </style>
        

    <?php endif; ?>



    <style>
        
        
        .form_error{

                
                display: block;
                position: absolute;
                border-radius: 25px;
           
                width: 100%;
                height: 100%;
                box-shadow: 0px 0px 0px 0px;
                color: rgba(184, 70, 90, 0.8);

        }



    </style>


</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="<?php echo e(asset('ellelazena_login/assets/images/img-01.png')); ?>" alt="IMG">
                </div>
                
                <?php echo Form::open(['url'=>"$lang/authenticate" , 'class'=>'login100-form validate-form' , 'method'=>'POST']); ?>

                <form class="login100-form validate-form">
                    <span class="login100-form-title">
                        <?php echo e(trans('lazena.member_login')); ?>

                    </span>


                      <?php if($errors->any()): ?>

                        <div class="alert alert-danger alert-dismissable text-center in_arabic">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong> </strong> <?php echo e($errors->first()); ?>

                        </div>

                         
                      <?php endif; ?>

                    <div class="wrap-input100 validate-input" data-validate = "<?php echo e(trans('lazena.email_required')); ?>">
                        <input class="input100 " type="text" name="email"  placeholder="<?php echo e(trans('lazena.email')); ?>">
                        <span class="focus-input100  "></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "<?php echo e(trans('lazena.password_required')); ?>">
                        <input class="input100 " type="password" name="password" placeholder="<?php echo e(trans('lazena.password')); ?>">
                        <span class="focus-input100 "></span>
                        <span class="symbol-input100 ">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            <?php echo e(trans('lazena.login')); ?>

                        </button>
                    </div>

                  

                   
                </form>
            </div>
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    
    <?php echo Html::script('ellelazena_login/assets/vendor/jquery/jquery-3.2.1.min.js'); ?>

<!--===============================================================================================-->
    
    <?php echo Html::script('ellelazena_login/assets/vendor/bootstrap/js/popper.js'); ?>

    
    <?php echo Html::script('ellelazena_login/assets/vendor/bootstrap/js/bootstrap.min.js'); ?>

<!--===============================================================================================-->
    
    <?php echo Html::script('ellelazena_login/assets/vendor/select2/select2.min.js'); ?>

<!--===============================================================================================-->
    

    <?php echo Html::script('ellelazena_login/assets/vendor/tilt/tilt.jquery.min.js'); ?>

    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    
    <?php echo Html::script('ellelazena_login/assets/js/main.js'); ?>


</body>
</html>