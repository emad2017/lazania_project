  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



   ?>


  <!-- add to cart container -->
  <div class="cd-cart-container empty">
  <a href="#0" class="cd-cart-trigger">
    <?php echo e(trans('lazena.cart')); ?>

    <ul class="count"> <!-- cart items count -->
      <li>0</li>
      <li>0</li>
    </ul> <!-- .count -->
  </a>

  <div class="cd-cart">
      <div class="wrapper">
        <header>
          <h2><?php echo e(trans('lazena.shopping_cart')); ?></h2>
          
        </header>
        
        <div class="body">
          <ul class="my_cart_ul">
            <!-- products added to the cart will be inserted here using JavaScript -->
          </ul>
        </div>

        <footer>
          <a href="<?php echo e(url("$lang/customer/shopping-cart")); ?>" class="checkout btn"><em><?php echo e(trans('lazena.checkout_btn')); ?> - <?php echo e(trans('lazena.s_r')); ?> <span>0</span></em></a>
        </footer>
      </div>
    </div> <!-- .cd-cart -->
  </div> 

  <!-- cd-cart-container -->

    <!-- ======./ Start Footer ======  -->
    <footer class="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row">
            <!-- block links -->
          <div class="col-sm-3">
              <div class="block">
                <div class="block-title">
                  <h4><?php echo e(trans('lazena.ellelazena')); ?></h4>
                </div>
  
                <ul class="links">
                  <li><a href="<?php echo e(url("$lang/about-us")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.about_us')); ?></span></a></li>
  
                  <li><a href="<?php echo e(url("$lang/contact")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.contact_us')); ?></span></a></li>


                  <li><a href="<?php echo e(url("$lang/categories")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.categories')); ?></span></a></li> 

                  <li><a href="<?php echo e(url("$lang/careers")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.careers')); ?></span></a></li>
                  
                  <?php if(Auth::guard('customer')->check()): ?>
                    
                    <li><a href="<?php echo e(url("$lang/customer/dashboard")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.my_account')); ?></span></a></li>


                    <li><a href="<?php echo e(url("$lang/customer/wishlist")); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><span><?php echo e(trans('lazena.wishlist')); ?></span></a></li>

                  <?php endif; ?>
                  
                 
  
                  
                </ul>
              </div>
            </div>
            <!-- ./ block links -->
  
            <!-- block Contact -->
            <div class="col-sm-3">
              <div class="block block-contact">
                <div class="block-title">
                  <h4><?php echo e(trans('lazena.contact_us')); ?></h4>
                </div>
  
                <div class="contact-item mb-10">
                  <div class="icon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>   
                  </div>  
                  <h5><?php echo e(trans('lazena.address')); ?></h5> 

                  <p>
                      
                      <?php $__currentLoopData = $footer->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                         <?php if($description->language_id == $language_id ): ?>
                                <?php echo $description->address; ?>

                         <?php endif; ?>

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  </p>            
                </div>
  
                <div class="contact-item mb-10">
                  <div class="icon">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                  </div>
                  <div class="info">
                    <h5><?php echo e(trans('lazena.telephone')); ?></h5> 
                    <p><?php echo e($footer->telephone1); ?></p> 
                  </div>           
                </div>
  
                <div class="contact-item mb-10">
                  <div class="icon">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                  </div>
                  <div class="info">
                    <h5> <?php echo e(trans('lazena.email')); ?></h5> 
                    <a href="#"><?php echo e($footer->email); ?></a>  
                  </div>         
                </div>
  
                <div class="contact-item">
                  <div class="icon">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                  </div>
                  <div class="info">
                    <h5><?php echo e(trans('lazena.open')); ?></h5> 
                    <p> <?php echo e(trans('lazena.from')); ?> <?php echo e($footer->timestart); ?> <?php echo e(trans('lazena.am')); ?>  <?php echo e(trans('lazena.to')); ?> <?php echo e($footer->timeend); ?> <?php echo e(trans('lazena.pm')); ?> </p>   
                  </div>           
                </div>
              </div>
            </div>
            <!-- ./ block Contact -->
  
            <!-- block links -->
            <div class="col-sm-3">
              <div class="block shop">
                <div class="block-title">
                  <h4><?php echo e(trans('lazena.shop_by')); ?></h4>
                </div>
  
                <ul class="links">

                <!-- start fetching shop by --> 

                  <?php if(count($shop_by)): ?>

                    <?php $__currentLoopData = $shop_by; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php $__currentLoopData = $cat->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($description->language_id == $language_id ): ?>
                            <?php  
                                 $clean_name = str_replace(' ', '_', $description->name) ; 
                             ?> 
                            <li><a href="<?php echo e(url("$lang/$description->department_id/sub-categories")); ?>"><i class="fa fa-check" aria-hidden="true"></i>
                              <span><?php echo e($description->name); ?></span></a></li>
                        <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                        
                  <?php endif; ?>
                <!-- end fetching shop by  --> 

                </ul>
              </div>
            </div>
            <!-- ./ block links -->
  
            <div class="col-sm-3">
              <div class="block subscribe">
                <div class="block-title">
                  <h4><?php echo e(trans('lazena.subscribe_newsletter')); ?></h4>
                </div>
  
                <p><?php echo e(trans('dessertdepartment.subscribtion_text')); ?></p>
                <h6 class="mt-50 mb-15"><?php echo e(trans('dessertdepartment.enter_email')); ?></h6>
                <?php echo Form::open(['route'=>'subscribe' , 'class'=>'subscribe_form']); ?>

                <div class="input-box">
                  <input type="text" id="subscribe_email" name="subscribe_email">
                  <button type="submit" ><span><span><?php echo e(trans('dessertdepartment.submit')); ?></span></span></button>
                </div>


                

                <?php echo Form::close(); ?>


                <div class="info_message hidden" id="info_div" style="margin-top: 70px;">
                  <p class="alert alert-info">dfdfsdfd</p>
                </div>
              </div>

            </div>
          </div> <!-- ./ row -->
        </div> <!-- ./ Container -->
      </div>
      <!-- ./ footer top -->

      <!-- footer bottom -->
      <div class="footer-bottom">
        <div class="container">
            <div class="row">

              <a href="<?php echo e(url("/$lang")); ?>" class="logo"><img src="<?php echo e(asset("frontend/assets/img/logo.png")); ?>" alt=""></a>

              <div class="social-icon">
                <a href="<?php echo e($footer->facebook); ?>" target="_blank">
                  <i class="fa fa-facebook color-face" aria-hidden="true"></i>                    
                </a>

                <a href="<?php echo e($footer->twitter); ?>" target="_blank">
                  <i class="fa fa-twitter color-tw" aria-hidden="true"></i>                    
                </a>

                <a href="<?php echo e($footer->google); ?>" target="_blank">
                  <i class="fa fa-instagram color-li" aria-hidden="true"></i>
                </a>

                
              </div>

              <div class="custom-block">
                <img src="<?php echo e(asset("frontend/assets/img/payments.png")); ?>" alt="">
              </div>

              <address>©Copyright 2017 by . All Rights Reserved.</address>
          </div> <!-- ./ row -->
        </div> <!-- ./ Container -->
      </div>
      <!-- ./ footer bottom -->
    </footer>
    <!-- ======./ Footer ======  -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <?php echo Html::script('frontend/assets/js/jquery-3.1.1.min.js'); ?>

    
    <?php echo Html::script('frontend/assets/js/Migrate.js'); ?>

    
    <?php echo Html::script('frontend/assets/js/bootstrap.min.js'); ?>


    <!-- bootsnav -->
    
    <?php echo Html::script('frontend/assets/js/bootsnav.js'); ?>


    <!-- owl carousel js -->
    
    <?php echo Html::script('frontend/assets/js/owl.carousel.min.js'); ?>


    <!-- magnific-popup -->
        
    <?php echo Html::script('frontend/assets/js/jquery.magnific-popup.min.js'); ?>


     <!-- fresco js  --> 
    <?php echo e(Html::script('frontend/assets/js/fresco/fresco.js')); ?>

    <!-- validator js -->
    
    <?php echo Html::script('frontend/assets/js/validator.js'); ?>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK7lXLHQgaGdP3IvMPi1ej0B9JHUbcqB0&callback=initMap"
    async defer></script>
   
    <?php echo Html::script('frontend/assets/js/map.js'); ?>

    <?php echo Html::script('frontend/assets/js/print.js'); ?>

   

    <!-- custom JavaScript -->
    
    
    <!-- loading overlay -->
    <?php echo Html::script('frontend/assets/js/loading_overlay/loadingoverlay.js'); ?>

    <!-- cart javascript --> 
    <?php echo Html::script('frontend/assets/cart/js/cart.js'); ?>

    
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

    <!-- flexslider --> 
 
    <?php echo Html::script('frontend/assets/js/jquery.flexslider-min.js'); ?>


     <!-- jquery matchHeight -->
    <?php echo Html::script('frontend/assets/js/jquery.matchHeight-min.js'); ?>




    <?php echo Html::script('frontend/assets/js/custom.js'); ?>



    <script>
        
          $(document).ready(function(){

              $('.subscribe_form').on('submit' , function(e){

                  e.preventDefault(); 

                  var email = $(this).find('#subscribe_email').val();  
                  var token = $(this).find('input[name="_token"]').val(); 

                  $.ajax({
                      url:$(this).attr('action') , 
                      method:$(this).attr('method'), 
                      dataType:'json' , 
                      data:{ _token:token , email:email } , 
                      beforeSend:function(){

                      }, 
                      success:function(data){

                          if(data.status == 'empty_email'){

                              $('#info_div').removeClass('hidden') 
                                
                                $('#info_div').find('p').text(data.msg);
                              
                          }


                          if(data.status == 'already_subscribed'){

                              $('#info_div').removeClass('hidden') 
                                
                                $('#info_div').find('p').text(data.msg);
                              
                          }

                          if(data.status == 'subscribtion_done'){

                              $('#info_div').removeClass('hidden') ; 
                            
                              $('#info_div').find('p').removeClass('alert-info') ; 
                              $('#info_div').find('p').addClass('alert-success') ; 
                                
                                $('#info_div').find('p').text(data.msg);
                              
                          }

                      }

                  }); 
                  return false ; 
              

              });

          });

    </script>


     <script>
      $(window).load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
        });
      });
    </script>



     <script>
        $('.half').matchHeight({ property: 'min-height' });
        $('.set').css("display","none");
        $("#change_password").on("click", function(){
            $(".set").slideToggle(100);
        });
    </script>



    

    <!-- Realtime Search Mechanism -_- Like youtube  --> 

    <script>
      $(document).ready(function(){

          $("#search_txt").keyup( function() {
            
            var search_query = $(this).val() ;


            $.ajax({

              url:'<?php echo e(url("$lang/search_result")); ?>' , 
              method:'GET' , 
              dataType:'json',
              data:{search_query:search_query}, 
              success:function(data){

                if(data.status == 'success'){

                    var products = data.products ; 
                    $('#ul_searchable').empty();
                    products.forEach(function(product , index ){

                        // inject products -_- 
                        $('#ul_searchable').append(`
                            
                             <li>
                             <a href='<?php echo e(url("$lang")); ?>/${product.desert_item_searchable.product_sub_dept_search['parent']}/sub-categories/${product.desert_item_searchable.product_sub_dept_search['id']}/products/${product.desert_item_searchable['id']}'>
                              <div class="product-search">
                                <div class="part-img">
                                  <img src="http://127.0.0.1:8000/uploads/dessertsitem/610_383/${product.desert_item_searchable['img1']}" alt="">
                                </div>

                                <div class="info">
                                  <h6>${product.name}</h6>
                                </div>
                              </div>
                              </a>
                            </li>

                        `);

                        $('li#not_found').remove(); 

                        // show our fancy ul -_- 
                        $('.lest-searchable').slideDown();

                    });


                } else{
                    $('.lest-searchable').slideDown();
                    // we need to show nothing found 
                    $('#ul_searchable').empty();
                    $('#ul_searchable').append(`
                        
                        <li id="not_found">
                          <div class="product-search text-center">
                            

                            <div class="info text-center">
                              <h6><?php echo e(trans('lazena.no_product_found')); ?></h6>
                            </div>
                          </div>
                        </li>

                    `);
                }



                if(data.status == 'empty_query'){
                    $('.lest-searchable').slideDown();
                     // we need to show nothing found 
                    $('#ul_searchable').empty();
                    $('#ul_searchable').append(`
                        
                        <li id="not_found">
                          <div class="product-search text-center">
                            

                            <div class="info text-center">
                              <h6><?php echo e(trans('lazena.no_product_found')); ?></h6>
                            </div>
                          </div>
                        </li>

                    `);

                }

              },


            }); 
            // $('.lest-searchable').slideDown();
            return false; 

             
      
            
          });


          $('.top-search input').blur(function(){
              if( !$(this).val() ) {
                    $('.lest-searchable').slideUp();
              }
          });


         

      });
    </script>



    <?php echo $__env->yieldContent('scripts'); ?>
      
 
 

  </body>
</html>
