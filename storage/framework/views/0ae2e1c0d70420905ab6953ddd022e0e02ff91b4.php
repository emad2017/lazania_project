<?php $__env->startSection('page_title' , trans('backend.slider_module')); ?>

<?php $__env->startSection('breadcrumb'); ?>


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('backend.slider_module')); ?></h4>
    </div>





<?php $__env->stopSection(); ?>

<?php  
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
 ?>



<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">
        <div class="white-box">


            <a href="<?php echo e(route('slider.create' )); ?>" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; "><?php echo e(trans('backend.add_slide')); ?></a>

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo e(trans('backend.slider_title')); ?></th>
                      

                        <th><?php echo e(trans('backend.slider_image')); ?></th>
                        <th><?php echo e(trans('backend.status')); ?></th>




                        <th class="text-nowrap"><?php echo e(trans('backend.action')); ?></th>

                    </tr>
                    </thead>
                    <tbody>



                    <?php if($sliders->count()): ?>


                        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td> <?php echo e($slide->title); ?>  </td>

                                 <td>
                             
 
                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="<?php echo e(asset("uploads/slider/$slide->image")); ?>" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                <img style="width: 70px; height:70px;border-radius:50%;" src="<?php echo e(asset("uploads/slider/$slide->image")); ?>" class="img-responsive fresco" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>
                                    </div>

                                </td>

                                
 
                                <td>
                                    <?php if($slide->status == 'not_active'): ?>
                                        <span class="label label-danger"><?php echo e(trans('backend.not_active')); ?></span>
                                    <?php else: ?>
                                        <span class="label label-success"><?php echo e(trans('backend.active')); ?></span>
                                    <?php endif; ?>

                                </td>





                                <td class="text-nowrap ">
 

                                     
                                   <a  href="<?php echo e(route('slider.edit' , $slide->id)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.edit')); ?>">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a  onclick="$('.dessertdepartment_<?php echo e($slide->id); ?>').submit();"

                                       data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.delete')); ?>">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    <?php echo Form::open(['route'=>["slider.destroy" , $slide->id ] ,
                                    'class'=>"dessertdepartment_$slide->id" ]); ?>


                                    <?php echo method_field('DELETE'); ?>




                                    <?php echo Form::close(); ?>


                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>

                        <tr>
                            <td colspan="7" class="text-center"><?php echo e(trans('backend.no_slide_found')); ?></td>

                        </tr>

                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>







<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>