<?php $__env->startSection('page_title' , trans('backend.update_create_career')); ?>

<?php $__env->startSection('breadcrumb'); ?>

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('backend.update_create_career')); ?></h4>
    </div>

<?php $__env->stopSection(); ?>

<?php 

    $lang = LaravelLocalization::getCurrentLocale();


    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;



 ?>



<?php $__env->startSection('content'); ?>
    

    <?php if(Session::has('career_update')): ?>
    <div class="alert alert-success col-md-12 text-center">
        <p><?php echo e(Session::get('career_update')); ?></p>
    </div>
    <?php endif; ?>

    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> <?php echo e(trans('backend.update_create_career')); ?> </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">

                            

                            <nav>
                                <ul>
                                    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="#<?php echo e($language->label); ?>"  ><span> &nbsp;<?php echo e(strtoupper($language->label)); ?></span></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </ul>
                            </nav>





                       <?php echo Form::model( $career  ,  ['url'=>["$lang/careers_post/$career->id"],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true ,
                       'id'=>'add_files']); ?>

                            <?php echo method_field('put'); ?>

                            <?php $__currentLoopData = $career->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="content-wrap">

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="<?php echo e($loop->iteration == 1 ? 'active' : ''); ?>" id="<?php echo e($description->language->label); ?>">



                                        <?php  
                                                $label = $description->language->label ; 
                                         ?> 
                                        <div class="form-group<?php echo e($errors->has("name_blog_$label") ? ' has-error' : ''); ?>">
                                            <label for=""><?php echo e(trans('backend.career_title')); ?></label>
                                            <div class="input-group">
                                                
                                                <?php  
                                                    $career_title = $description->name ; 
                                                 ?> 
                                                <input type="text" class="form-control" name="name_blog_<?php echo e($label); ?>"
                                                       value="<?php echo e($career_title); ?>"
                                                       id="name_<?php echo e($label); ?>" placeholder="<?php echo e(trans('backend.career_title')); ?>">
                                                <?php if($errors->has("name_blog_$label")): ?>
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li><?php echo e($errors->first("name_blog_$label")); ?></li></ul>
                                                 </span>
                                                <?php endif; ?>

                                            </div>
                                        </div>


                                        <!--  Description -->

                                        <div class="form-group<?php echo e($errors->has("description_blog_$label") ? ' has-error' : ''); ?>">
                                            <label for=""><?php echo e(trans('backend.career_description')); ?></label>
                                            <div class="input-group">

                                            
                                                
                                                <textarea name="description_blog_<?php echo e($label); ?>"  ><?php echo e($description->description); ?></textarea>



                                                <?php if($errors->has("description_blog_$label")): ?>
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li><?php echo e($errors->first("description_blog_$label")); ?></li></ul>
                                                 </span>
                                                <?php endif; ?>

                                            </div>
                                        </div>


                                    </section>


                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10"><?php echo e(trans('backend.update')); ?></button>
                                   

                                </div>
                            </div>






                            <?php echo Form::close(); ?>




                        </div>

                        <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








<?php $__env->stopSection(); ?>




<?php $__env->startSection('scripts'); ?>



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p><?php echo e(trans("department.select_images")); ?></p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"><?php echo e(trans("article.select_images")); ?></p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear"><?php echo e(trans("backend.remove")); ?></button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>