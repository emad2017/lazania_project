<?php $__env->startSection('page_title' , trans('lazena.login')); ?>


<?php $__env->startSection('styles'); ?>


<?php $__env->stopSection(); ?> 

  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



   ?>


<?php $__env->startSection('pages_content'); ?>


      <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="<?php echo e(url("/$lang")); ?>"><?php echo e(trans('lazena.home')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
             
              <li><?php echo e(trans('lazena.login')); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  --> 
      
     <section class="login section-padding">
      <div class="container">
        <div class="row">

          <!-- Title -->
          <div class="section-title">
            <h3><?php echo e(trans('lazena.login_intro')); ?></h3>
          </div>

          <div class="account-login">
            
            <?php if(Session::has('authentication_needed')): ?>
              <div class="alert alert-warning alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>  </strong> <?php echo e(Session::get('authentication_needed')); ?>

              </div>
            <?php endif; ?>


            <?php if(Session::has('wishlist_auth_needed')): ?>
              <div class="alert alert-warning alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>  </strong> <?php echo e(Session::get('wishlist_auth_needed')); ?>

              </div>
            <?php endif; ?>


            <div class="col-md-6 new-account content">
              <div class="half">
                <h4><?php echo e(trans('lazena.new_customers')); ?></h4>
                <p><?php echo e(trans('lazena.new_customers_intro')); ?></p>
                 
             
               

                <div class="bbtn">
                  <a href="<?php echo e(url("$lang/customer-register")); ?>" class="button button-hot hvr-shutter-out-horizontal mt-30"><?php echo e(trans('lazena.create_account')); ?></a>
                </div>
              </div>
            </div>

            <div class="col-md-6 registered-users content">
              <div class="half">

                
                <?php echo Form::open(['url'=>"$lang/customer-login" , 'method'=>'POST']); ?> 
                  <h4><?php echo e(trans('lazena.current_customers')); ?></h4>

                  <?php if($errors->any()): ?>

                    <div class="alert alert-danger alert-dismissable text-center">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong> </strong> <?php echo e($errors->first()); ?>

                    </div>

                     
                  <?php endif; ?>
                 
                  <p><?php echo e(trans('lazena.current_customers_intro')); ?></p>
                  
                  <label for="email"><?php echo e(trans('lazena.email_address')); ?></label>
                  <input type="text" name="email" id="email"  >
  
                  <label for="password"><?php echo e(trans('lazena.password')); ?></label>
                  <input type="password" id="password" name="password"  >
  
                  <div class="buttons-set">

                    
                    <div class="bbtn">
                      <button   class="button button-hot hvr-shutter-out-horizontal mt-30"><?php echo e(trans('lazena.login')); ?></button>
                    </div>
                  </div>
                 <?php echo Form::close(); ?>

              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

<?php $__env->stopSection(); ?> 

 



<?php $__env->startSection('scripts'); ?>
      
      
  
<?php $__env->stopSection(); ?> 






<?php echo $__env->make('frontend.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>