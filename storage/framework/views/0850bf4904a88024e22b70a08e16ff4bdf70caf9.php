<?php $__env->startSection('page_title' , trans('backend.add_slide')); ?>

<?php $__env->startSection('breadcrumb'); ?>


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo e(trans('backend.add_slide')); ?></h4> 
                    </div>

                  



<?php $__env->stopSection(); ?>  


<?php $__env->startSection('styles'); ?>
    
      <style>
    
        .form_error{
            
            
            margin-top: -25px;
            margin-bottom: 10px;
            color: #d22e2e;

        }

        .error_border{

                box-shadow: 1px 1px 5px red;
        }

        .form_error small{
            word-break: break-word ; 
        }

  </style>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
					 

                   <div class="col-md-8 col-sm-12">
                        <div class="white-box">
                            <h3 class=" m-b-0"><?php echo e(trans('backend.add_slide')); ?></h3>
                            <p class="text-muted m-b-30 font-13"> <?php echo e(trans('backend.add_slide_notice')); ?></p>
                            
                                  <?php echo Form::open(['route'=>'slider.store' , 'id'=>'lang_form_add' , 'class'=>'form-horizontal' , 'files'=>true]); ?>

                                <div class="form-group row">
                                    <label   class="col-sm-3 control-label col-form-label"><?php echo e(trans('backend.slider_title')); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo old('title'); ?>" class="form-control <?php echo $errors->has("title") ? 'error_border':''; ?>"   name="title">
                                        
                                            <?php if($errors->has("title")): ?>
                                                <span class="form_error"><small><?php echo e($errors->first("title")); ?></small></span>
                                            <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label   class="col-sm-3 control-label col-form-label"><?php echo e(trans('backend.slider_image')); ?></label>
                                    <div class="col-sm-9">
                                         <input type="file" id="input-file-now" name="file"  class="dropify" />
                                        <?php if($errors->has("file")): ?>
                                            <span class="form_error"><small><?php echo e($errors->first("file")); ?></small></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                 
                               
                           
                        </div>
                    </div>  


                    <div class="col-md-4 col-sm-12">
                        <div class="white-box">
                            <h3 class="m-b-0"><?php echo e(trans('backend.publish_options')); ?></h3>
                            <br>

                              <div class="row">
                                <label class="col-sm-6 control-label col-form-label"><?php echo e(trans('backend.status')); ?></label> 
                                <div class="col-sm-6 ">
                                     <input type="checkbox" class="js-switch" checked="true"  name="status"  data-size="small" data-color="#13dafe" /> 
                                </div>

                                
                            </div>
                            
                            <br>  
                            
                            <div class="form-group m-b-0"> 
                                <button type="submit" class="btn btn-info btn-rounded waves-effect waves-light m-t-10 btn-sm"><?php echo e(trans('backend.add_slide')); ?></button> 
                            </div>

                                 <?php echo Form::close(); ?>

                        </div>
                    </div> 

<?php $__env->stopSection(); ?> 


 
<?php $__env->startSection('scripts'); ?>

    <!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>


        <script type="text/javascript">

                $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
                var radioswitch = function() {
                    var bt = function() {
                        $(".radio-switch").on("switch-change", function() {
                                $(".radio-switch").bootstrapSwitch("toggleRadioState")
                            }),
                            $(".radio-switch").on("switch-change", function() {
                                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck")
                            }),
                            $(".radio-switch").on("switch-change", function() {
                                $(".radio-switch").bootstrapSwitch("toggleRadioStateAllowUncheck", !1)
                            })
                    };
                    return {
                        init: function() {
                            bt()
                        }
                    }
                }();
                $(document).ready(function() {
                    radioswitch.init(); 


                     $('.dropify').dropify({
                            tpl: {
                                wrap:            '<div class="dropify-wrapper"></div>',
                                loader:          '<div class="dropify-loader"></div>',
                                message:         '<div class="dropify-message"><span class="file-icon" /> <p><?php echo e(trans("backend.select_slide_image")); ?></p></div>',
                                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message"><?php echo e(trans("backend.select_slide_image")); ?></p></div></div></div>',
                                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                                clearButton:     '<button type="button" class="dropify-clear"><?php echo e(trans("backend.remove")); ?></button>',
                                errorLine:       '<p class="dropify-error"></p>',
                                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                            }
                        });


                });

    </script>


       

<?php $__env->stopSection(); ?> 


 
<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>