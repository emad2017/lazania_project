  


<?php $__env->startSection('page_title' , trans('backend.orders_module')); ?>

<?php $__env->startSection('breadcrumb'); ?>


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo e(trans('backend.orders_module')); ?></h4>
 

                    </div>





<?php $__env->stopSection(); ?>


<?php  
    
    $lang = LaravelLocalization::getCurrentLocale(); 
 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
 
    $language_id = $laguage->id ; 
 
 ?> 



<?php $__env->startSection('content'); ?>

					  <div class="col-sm-12">
                        <div class="white-box">
                            
                            <div class="table-responsive">
                                <table class="table table-hover color-table info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?php echo e(trans('backend.order_ticket')); ?></th>
                                            <th><?php echo e(trans('backend.customer_name')); ?></th>
                                            <th><?php echo e(trans('backend.order_total')); ?></th>
                                          
                                            <th><?php echo e(trans('backend.order_date')); ?></th>
                                            <th><?php echo e(trans('backend.action')); ?></th>
                                    
                                           

                                        </tr>
                                    </thead>
                                    <tbody>

                                    	<?php if($orders->count()): ?>
                                    		<?php  $counter = 1 ;  ?>
                                    		<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <?php  
                                                    $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 


                                                 ?>
		                                        <tr>
		                                            <td><?php echo e($counter); ?></td>
                                                    <td><?php echo e($order->order_ticket); ?>   </td>
                                                    <td><?php echo e($shipping_to->fname); ?> <?php echo e($shipping_to->lname); ?> </td>
                                                    <td><?php echo e($order->order_total); ?> <?php echo e(trans('lazena.s_r')); ?></td>
 
                                                    <td><?php echo e(date('Y-m-d' , strtotime($order->order_date))); ?></td>
                                                    
                                                    <td>
		                                              <a  href="<?php echo e(url("$lang/order/$order->order_ticket/null/details")); ?>" data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.order_info')); ?>"> <i class="fa fa-info text-info"></i> </a>
                                                    </td>

                                                     
		                                          
		                                        </tr>
		                                        <?php  $counter++ ;  ?>
		                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>

											<tr>
												<td colspan="6" class="text-center"><?php echo e(trans('backend.user_count_zero')); ?></td>
											</tr>

                                        <?php endif; ?>

                                    </tbody>


                                </table>
                                 <div class="text-center"><?php echo $orders->links(); ?></div>
                            </div>
                        </div>
                    </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('scripts'); ?>

    <script>

    </script>

<?php $__env->stopSection(); ?>




<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>