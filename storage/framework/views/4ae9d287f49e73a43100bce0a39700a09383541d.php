<?php $__env->startSection('page_title' ,    trans('lazena.checkout')  ); ?>


<?php $__env->startSection('styles'); ?>
  
    <style>
      
          .active{
            color: #f8408 ; 
          }


          input.input-number::-webkit-outer-spin-button,
          input.input-number::-webkit-inner-spin-button {
              /* display: none; <- Crashes Chrome on hover */
              -webkit-appearance: none;
              margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
          }


    </style>

<?php $__env->stopSection(); ?> 

  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 

       $customer_address = $customer->street . ' ' . $customer->city ; 


   ?>


<?php $__env->startSection('pages_content'); ?>
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="<?php echo e(url("/$lang")); ?>"><?php echo e(trans('lazena.home')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="<?php echo e(url("/$lang/customer/dashboard")); ?>"><?php echo e(trans('lazena.customer')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><?php echo e(trans('lazena.checkout')); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->




    <!-- ======   ======  -->
    <section class="checkout section-padding">
      <div class="container">
        <div class="row" id="main_div">
          <!-- Block Info -->
          <div class="blck-info col-sm-12 col-md-6">
            <!-- block title -->
            <div class="block-title mb-50">
              <h3><?php echo e(trans('lazena.checkout')); ?></h3>
            </div>
            <!-- ./ block title -->

            <div class="sub-title-check">
              <h4> <?php echo e(trans('lazena.name_address')); ?></h4>
            </div>

  
             <form id="order_form"> 

            <div class="col-md-6 col-sm-12 box-block">
              <label for=""><?php echo e(trans('lazena.fname')); ?> <span></span> </label>
              <input type="text" name="fname" id="fname" required value="<?php echo e($customer->fname); ?>">
            </div>

            <div class="col-md-6 col-sm-12 box-block">
              <label for=""><?php echo e(trans('lazena.lname')); ?> <span></span></label>
              <input type="text" name="lname" id="lname" required value="<?php echo e($customer->lname); ?>">
            </div>

            <div class="col-sm-12 mb-30 gender">
              <label for="name"><?php echo e(trans('lazena.gender')); ?></label>
              <div class="input-areaa">
                <input class="radio" title="Male" <?php echo $customer->gender == 'male' ? 'checked':''; ?> value="male" name="gender" checked="checked" type="radio"><?php echo e(trans('lazena.male')); ?>

                <input class="radio validate-one-required" <?php echo $customer->gender == 'female' ? 'checked':''; ?> title="Female" value="female" name="gender" type="radio"><?php echo e(trans('lazena.female')); ?>

              </div>
            </div>

            <div class="col-md-12 col-sm-12 box-block">
              <label for=""><?php echo e(trans('lazena.address')); ?><span></span></label>
              <input type="text" name="address" id="address" required value="<?php echo e($customer_address); ?>">
            </div>

            <div class="col-sm-12 country-box">
              <label for="Country"><?php echo e(trans('lazena.country')); ?><span></span></label>
              <select class="country_id" name="country" id="country">
               
                <option selected=""  value="SA"><?php echo e(trans('lazena.saudia')); ?></option>
               
              </select>
            </div>

           

            <div class="col-md-6 col-sm-12 box-block">
              <label for=""><?php echo e(trans('lazena.telephone')); ?><span>*</span></label>
              <input type="text" name="telephone" id="telephone" required value="<?php echo e($customer->telephone); ?>">
            </div>

          </div>
          <!-- ./ block info  -->

          <!-- Block sign in -->
          <div class="block-sign col-md-6">
            <!-- block title -->
            <div class="block-title mb-50">
              <h3><?php echo e(trans('lazena.info')); ?></h3>
            </div>
            <!-- ./ block title -->

            <div class="sub-title-check">
              <h4><?php echo e(trans('lazena.shipping')); ?> </h4>
            </div>

            <p><?php echo e(trans('lazena.delivery')); ?> </p>

            <input class="radio" value="1" name="" checked="checked" type="radio">
            <label for=""><?php echo e(trans('lazena.flat_rate')); ?> </label><br> ( <code><?php echo e(trans('lazena.checkout_notice')); ?> </code> )

 

            <div class="sub-title-check">
              <h4><?php echo e(trans('lazena.payment_method')); ?></h4>
            </div>

            <p><?php echo e(trans('lazena.cash_on_delivery')); ?></p>

            <div class="sub-title-check mt-20">
              <h4><?php echo e(trans('lazena.review_order')); ?></h4>
            </div>

           


            <div class="cart-totals">
               

              <div class="first">
                <h4><?php echo e(trans('lazena.sub_total')); ?></h4>
                <span class="sub_total" name='subtotal'></span>
                <input type="hidden" name='sub_total' class="subtotalInput" >
              </div>

              <div class="first">
                <h4><?php echo e(trans('lazena.delivery')); ?></h4>
                <span class="delivery" name='delivery'> </span>
                <input type="hidden" name='delivery' class="deliveryInput" >
              </div>

              <div class="first">
                <h4><?php echo e(trans('lazena.total')); ?></h4>
                <span class="total grand" > </span>
                <input type="hidden" name='total' class="grandInput" >
              </div>
              <br>
              <br>

             

            </div>
   
           
            <button  type="submit" class="button button-hot hvr-shutter-out-horizontal mb-30"><?php echo e(trans('lazena.order_now')); ?></button>

           </form>

          </div>
          <!-- ./ block sign in -->
        </div>
      </div>
    </section>
    <!-- ======  ======  -->


<?php $__env->stopSection(); ?> 

 



<?php $__env->startSection('scripts'); ?>
      
    <script> 
    </script>

    <script>
      
        // inside here i need to hide the flyer shopping cart cause it make sense -_- 
        $(document).ready(function(){

            // when dom is ready find flyer cart  
            var cartWrapper = $('.cd-cart-container');

            cartWrapper.addClass('empty');
 
        });

    </script>  


    <script>
        
        $(document).ready(function(){

              var objectParsed = JSON.parse(sessionStorage.getItem('checkout_final')); 

            
              $('.sub_total').text(objectParsed.sub_total); 
              $('.delivery').text(objectParsed.delivery); 
              $('.grand').text(objectParsed.total); 

              $('.subtotalInput').val(objectParsed.sub_total);
              $('.deliveryInput').val(objectParsed.delivery);
              
               $('.grandInput').val(objectParsed.total);

               


             $('#order_form').on('submit' , function(e){


                      
                  e.preventDefault(); 

                  var total =  $('.grandInput').val();

                  var token = $('input[name="_token"]').val(); 



                  var fname = $('#fname').val(); 
                  var lname = $('#lname').val(); 
                  var address = $('#address').val(); 
                  var country = $('#country').val(); 
                  var gender = $('input[name="gender"]').val(); 
                  var telephone = $('#telephone').val();


               

                   var products  = JSON.parse(sessionStorage.getItem('products'));

   


                   $.ajax({

                  url:'<?php echo e(url("$lang/customer/place-order")); ?>' , 
                  method:'POST',
                  dataType:'json', 
                  data:{_token:token , total:total ,  products:products , fname:fname , lname:lname , address:address , country:country , gender:gender , telephone:telephone   }, 
                  beforeSend:function(){

                    $('#main_div').LoadingOverlay("show" , {
                      image       : "",
                      fontawesome : "fa fa-spinner fa-spin"
                    });

                  }, 
                  success:function(data){

                      if(data.status == 'ordered'){

                          // empty products array 
                          // then empty checkoutfinal array  -_- 


                           if(sessionStorage.getItem('products') != null ){
          
                              var parseJsonProducts  = JSON.parse(sessionStorage.getItem('products')); 

                                    if(parseJsonProducts != 0 ){

                                          parseJsonProducts.forEach(function(product , index){
                                                    
                                                    // now that will remove every product in our array 
                                                    parseJsonProducts.splice(index , parseJsonProducts.length );
                                                    var arrToJsonString = JSON.stringify(parseJsonProducts) ;
                                                    sessionStorage.setItem('products' , arrToJsonString ) ; 
                                                  
                                                         
                                          });


                                     }

                            }



                            // i need also to empty our checkout final object  -_- 
                            sessionStorage.setItem('checkout_final' , '');

                              $('#main_div').LoadingOverlay("hide" , {
                                image       : "",
                                fontawesome : "fa fa-spinner fa-spin"
                              });



                              // then  i may think about redirect customer to his orders page with  
                              // a success message  that his order has been placed  -_- 
                              // but i need a message to tell him that order was success  
                              // i flashed my message in controller  


                                window.location.href = "<?php echo e(url("$lang/customer/my-orders")); ?>"; 




                      }

                  }


                 }); 
                 return false; 


             });
               

             


        });


    </script>
 
  
<?php $__env->stopSection(); ?> 






<?php echo $__env->make('frontend.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>