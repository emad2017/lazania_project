<?php $__env->startSection('page_title' , trans('dessertdepartment.dessert_name')); ?>

<?php $__env->startSection('breadcrumb'); ?>


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo e(trans('dessertdepartment.dessert_name')); ?></h4>
    </div>





<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <div class="col-sm-12">
        <div class="white-box">

            
             <a href="<?php echo e(route('addNewItem' , $id )); ?>" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; ">Add new desert</a>

            
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo e(trans('dessertdepartment.dessert_name')); ?></th>
                        
                        <th><?php echo e(trans('dessertdepartment.dessert_image_')); ?></th>
                        

                        
                        <th><?php echo e(trans('dessertdepartment.dessert_department_rate')); ?></th>


                        <th><?php echo e(trans('dessertdepartment.show_in_slider')); ?></th>
                        <th><?php echo e(trans('dessertdepartment.best_selling')); ?></th>
                        <th><?php echo e(trans('backend.quantity')); ?></th>
                        <th><?php echo e(trans('dessertdepartment.recommended')); ?></th>


                    
                   
                        <th><?php echo e(trans('dessertdepartment.dessert_department_status')); ?></th>



                        <th class="text-nowrap"><?php echo e(trans('backend.action')); ?></th>

                    </tr>
                    </thead>
                    <tbody>



                    <?php if($items->count()): ?>


                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dessertitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>





                                <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td>
                                    <?php $__currentLoopData = $dessertitem->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php echo e($description->name); ?> <br>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </td>





                               

                                <td>
                                    
 
                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="<?php echo e(asset("uploads/dessertsitem/original/$dessertitem->img1")); ?>" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                <img style="width: 70px; height:70px;border-radius:50%;" src="<?php echo e(asset("uploads/dessertsitem/original/$dessertitem->img1")); ?>" class="img-responsive fresco" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>
                                    </div>

                                </td>

                                    
                                   
                                   
                                 

                                    
                                    <style>
                                        .fa-star{
                                            color: gold !important;
                                        }
                                    </style>
                                    <td>  <i class="fa fa-star"></i> <span> <?php echo e($dessertitem->rate); ?> </span></td>
 

                                     <td>
                                        <?php if($dessertitem->show_in_slider == 1): ?>
                                             <span class="label label-success label-sm"><?php echo e(trans('dessertdepartment.slider')); ?></span>
                                        <?php else: ?>

                                        <?php endif; ?>
                                        
                                    </td>


                                    <td>
                                        <?php if($dessertitem->best_selling == 1): ?>
                                             <span class="label label-success label-sm"><?php echo e(trans('dessertdepartment.best_selling')); ?></span>
                                        <?php else: ?>

                                        <?php endif; ?>
                                        
                                    </td>


                                    <td>
                                         <?php echo e($dessertitem->quantity); ?>

                                        
                                    </td>



                                    <td>
                                        <?php if($dessertitem->recommended == 1): ?>
                                             <span class="label label-success label-sm"><?php echo e(trans('dessertdepartment.recommended')); ?></span>
                                        <?php else: ?>

                                        <?php endif; ?>
                                        
                                    </td>


 
                                 
                              



                                <td>
                                    <?php if($dessertitem->status == 'not_active'): ?>
                                        <span class="label label-danger"><?php echo e(trans('backend.not_active')); ?></span>
                                    <?php else: ?>
                                        <span class="label label-success"><?php echo e(trans('backend.active')); ?></span>
                                    <?php endif; ?>

                                </td>




                                <td class="text-nowrap">






                                    <a  href="<?php echo e(route('editItem' , $dessertitem->id)); ?>" data-toggle="tooltip"
                                        data-original-title="<?php echo e(trans('backend.edit')); ?>">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>





                                    <a onclick="$('.dessertitemـ<?php echo e($dessertitem->id); ?>').submit();"
                                       data-toggle="tooltip" data-original-title="<?php echo e(trans('backend.delete')); ?>">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    <?php echo Form::open(['route'=>["deleteItem" , $dessertitem->id ] ,
                                    'class'=>"dessertitemـ$dessertitem->id" ]); ?>


                                    <?php echo method_field('DELETE'); ?>




                                    <?php echo Form::close(); ?>


                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>

                        <tr>
                            <td colspan="11" class="text-center"><?php echo e(trans('dessertdepartment.dessert_items_zero')); ?></td>

                        </tr>

                    <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>









<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>