<?php $__env->startSection('page_title' , trans('backend.order_info')); ?>

<?php $__env->startSection('breadcrumb'); ?>


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo e(trans('backend.order_info')); ?></h4> 
                    </div>

          



<?php $__env->stopSection(); ?>  




<?php  
    
    $lang = LaravelLocalization::getCurrentLocale(); 

    $laguage = \App\Language::where(['label'=>$lang])->first(); 


    $language_id = $laguage->id ; 

    


 ?> 



<?php $__env->startSection('content'); ?>
                 
            <div class="col-md-12">
                        <div class="white-box printableArea">
                            <h3><b><?php echo e(trans('backend.order_info')); ?></b> <span class="pull-right"><?php echo e($order->order_ticket); ?></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    
                                     <?php  

                                        $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 

                                     ?>

                                    <div class="text-center">


                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-12">
                                                 <h3><?php echo e(trans('backend.order_customer_name')); ?></h3>
                                            <h4 class="font-bold">
                                               
                                                <?php echo e($shipping_to->fname); ?> <?php echo e($shipping_to->lname); ?>


                                            </h4>
                                            </div>


                                            <div class="col-md-6 col-sm-12">
                                               <h3><?php echo e(trans('backend.contact_info')); ?></h3>

                                                <h4 class="font-bold">   
                                                  <?php echo e($shipping_to->telephone); ?> 
                                                 </h4>

                                                 
                                            </div>
                                        </div>
                                        <address>
                                           

                                           

                                            <p class="text-muted m-l-30">
                                                <?php echo e($shipping_to->street); ?> 
                                                <br>
                                                <?php echo e($shipping_to->country); ?>

                                            </p>
                                            <p class="m-t-30">  <i class="fa fa-calendar"></i>
                                                <?php echo e(date('l jS \of F Y h:i:s A' , strtotime($order->order_date))); ?>

                                            </p>
                                           
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th><?php echo e(trans('backend.product_name')); ?></th>
                                                    <th><?php echo e(trans('backend.product_image')); ?></th>
                                                    <th class="text-right"><?php echo e(trans('backend.product_quantity')); ?></th>
                                                    <th class="text-right"><?php echo e(trans('backend.product_price')); ?></th>
                                                    <th class="text-right"><?php echo e(trans('backend.product_total')); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  
                                                     $orders = \App\OrderProduct::where('order_id' , $order->order_ticket)->get(); 
                                                 ?> 

                                                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_pivot): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                     <?php   $product =  $order_pivot->product ;    ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo e($loop->iteration); ?></td>

                                                        <td>
                                                            <?php $__currentLoopData = $product->description; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $description): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                    
                                                                  <?php if($description->language_id == $language_id): ?>
                                                                     <?php echo e($description->name); ?> 
                                                                  <?php endif; ?> 
                                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                        </td>

                                                        <td>
                                                            
                                                            <img style="height: 50px; width: 50px; border-radius: 50%;" src="<?php echo e(asset("uploads/dessertsitem/300_400/$product->img1")); ?>" alt="">

                                                             
                                                        </td>
                                                        <td class="text-right"><?php echo e($order_pivot->quantity); ?></td>
                                                        <td class="text-right">  <?php echo e($product->price); ?> </td>
                                                        <td class="text-right">  <?php echo $order_pivot->quantity * $product->price; ?> </td>
                                                    </tr>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">
                                        <p><?php echo e(trans('lazena.sub_total')); ?> : <?php echo e(trans('lazena.s_r')); ?> <?php echo $order->order_total - 5; ?> </p>
                                        <p>  <?php echo e(trans('lazena.delivery')); ?>   :  <?php echo e(trans('lazena.s_r')); ?>   5.00 </p>
                                        <hr>
                                        <h3><b><?php echo e(trans('backend.grand_total')); ?> :</b> <?php echo e(trans('lazena.s_r')); ?> <?php echo e($order->order_total); ?> </h3> </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="text-right">
                                        
                                        <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> <?php echo e(trans('backend.print')); ?></span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                 

<?php $__env->stopSection(); ?> 


 
<?php $__env->startSection('scripts'); ?>
    

     

    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>
      

<?php $__env->stopSection(); ?> 

 
<?php echo $__env->make('backend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>