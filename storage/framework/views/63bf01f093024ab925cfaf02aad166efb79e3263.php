<?php $__env->startSection('page_title' ,  trans('lazena.about_us') ); ?>


<?php $__env->startSection('styles'); ?>


<?php $__env->stopSection(); ?> 

  <?php  

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



   ?>


<?php $__env->startSection('pages_content'); ?>

  <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="<?php echo e(url("/$lang")); ?>"><?php echo e(trans('lazena.home')); ?></a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li> <?php echo e(trans('lazena.about_us')); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ======  services  ======  -->
    <section class="services section-padding">
      <div class="container">
        <div class="row">
          <!-- Title -->
          <div class="section-title">
            <h3><?php echo e($about_description->name); ?></h3>
          </div>

          <div class="col-sm-12">
            <hr class="section">
          </div>
            
        
          <?php echo html_entity_decode($about_description->description); ?>



        </div>
      </div>
    </section>
    <!-- ====== ./ services ======  -->
    

<?php $__env->stopSection(); ?> 

 



<?php $__env->startSection('scripts'); ?>
      
      
  
<?php $__env->stopSection(); ?> 






<?php echo $__env->make('frontend.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>