<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


use App\Order ; 

class NewOrder extends Notification implements ShouldQueue
{
    use Queueable;



    public $order ; 

    public $customer_name ; 



    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $order , $customer_name)
    {
        $this->order = $order ; 
        $this->customer_name = $customer_name ; 
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    // public function toDatabase($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

            'order_id'=>$this->order->id , 
            'order_ticket'=>$this->order->order_ticket , 
            'customer'=>$this->order->customer_id , 
            'order_date'=>$this->order->order_date , 
            'order_total'=>$this->order->order_total , 
            'customer_name'=>$this->customer_name 
        ];
    }
}
