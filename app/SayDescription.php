<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SayDescription extends Model
{
    //
  	protected $fillable = ['sayAbout_id' , 'language_id'] ; 

    public function about()
    {

        return $this->belongsTo('App\Say');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}
 