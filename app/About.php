<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{

    protected $fillable = ['id' , 'related_to']; 

    public function description()
    {
        return $this->hasMany('App\AboutDescription','about_id');
    }


}
