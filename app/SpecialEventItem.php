<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialEventItem extends Model
{
    //
    public function description(){


        return $this->hasMany('App\SpecialEventItemDescription' , 'item_id');

    }
    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }
    public function department()
    {


        return $this->belongsTo('App\SpecialEvent', 'event_id');

    }
}
