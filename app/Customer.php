<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\SendResetPasswordEmailToCustomer ; 

class Customer extends Authenticatable
{
    use Notifiable ; 

    public $guard  = 'customer' ; 

    protected $table = 'e_customers'; 


    protected $fillable = [
        'fname', 'lname' , 'email', 'city' , 'country' , 'street' , 'telephone' , 'gender' ,  'password'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];


    public function  wishlistProduct(){

    		return $this->hasMany('App\EcommerceWishlist' , 'customers_id'); 

    }


      //Send password reset notification
      public function sendPasswordResetNotification($token)
      {
          $this->notify(new SendResetPasswordEmailToCustomer($token));
      }



      public function orders(){

          return $this->hasMany('App\Order' , 'customer_id')->orderBy('created_at' , 'DESC'); 

      }


      
}
