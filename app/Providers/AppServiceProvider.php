<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\About;
use App\AboutDescription;

use App\DessertItem;
use App\DessertItemDescription;

use App\DessertDepartments ;
use App\DessertDepartmentDescription;

use App\SpecialEvent;
use App\SpecialEventDescription;



use View;
use DB;

use LaravelLocalization; 

 

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


    
            Schema::defaultStringLength(191);

            view::share('name', 'mahmoud');

            view::composer('*' , function ($view){


               // Footer data
               $footer = About::findOrFail(1);

               // main categories : 
               
               $shop_by = DessertDepartments::where(['status'=>'active'])->get(); 

           

               $recentdessert = DessertItem::take(4)->inRandomOrder()->get();
               $cats = DessertDepartments::take(6)->inRandomOrder()->get();



               // side bar part will be shared in some views -_- 
               
               $mainCategoriesSidebar = DessertDepartments::where(['status'=>'active'])->get(); 
               $mainCategoriesContent = [] ; 
               foreach ($mainCategoriesSidebar as $mainCategory) {
                    foreach ($mainCategory->description as $description) {
                            $mainCategoriesContent [] = $description ; 
                        }    
               }


               $specialOccasions = SpecialEvent::where(['status'=>'active'])->get(); 

               $specialOccasionsContent = []  ; 
               foreach ($specialOccasions as $occasion) {
                    foreach ($occasion->description as $description) {
                            $specialOccasionsContent [] = $description ; 
                        }    
               }


               $navigationCategories = DessertDepartments::where(['status'=>'active'])->get();
                $navigationCategoriesContent = [] ; 
               foreach ($navigationCategories as $mainCategory) {
                    foreach ($mainCategory->description as $description) {
                            $navigationCategoriesContent [] = $description ; 
                        }    
               }
            
               

                

                 

                $view-> with('footer' , $footer) ;
                $view-> with('shop_by' , $shop_by) ;
                $view->with('recents',  $recentdessert);
                $view ->with('cats',$cats);
                $view ->with('mainCategoriesContent',$mainCategoriesContent);
                $view ->with('specialOccasionsContent',$specialOccasionsContent);
                $view ->with('navigationCategoriesContent',$navigationCategoriesContent);
               
                
            });





    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
