<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DessertItemDescription extends Model
{


    protected $fillable = ['img1'];

    public function dessertitem(){


        return $this->belongsTo('App\DessertItem' , 'item_id');

    }

    /**
     * Lnguage eloquent to fetch content based on language
     * if you asking of which it will help - then it makes it more easier when
     * using with tabs --- sothat everything will be dynamic .
     * @return [type] [description]
     */
    public function language(){

        return $this->belongsTo('App\Language' , 'language_id');
    }


    public function desertItemSearchable(){

         return $this->belongsTo('App\DessertItem' , 'item_id')  ;

    }

}
