<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesertSubDepartment extends Model
{
	
	/**
	 * @return 
	 * attaching many desert to specific sub department 
	 */
	public function deserts(){
		return $this->hasMany('App\DessertItem' , 'subDeptId'); 
	}    

	public function description(){
		return $this->hasMany('App\DesertSubDepartmentDescription' , 'subDeptId'); 
	}


	public function mainCategory(){
		return $this->belongsTo('App\DessertDepartments' , 'parent'); 
	}

	public function language(){
		return $this->belongsTo('App\Language'); 
	}




	public function mainDespartmentSearchable(){
		return $this->belongsTo('App\DessertDepartments' , 'parent'); 
	}





}
