<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
 * Get user who made this department
 * @return [type] [description]
 */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }

}
