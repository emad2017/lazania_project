<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogDescription extends Model
{


    protected $fillable = ['blog_id' , 'language_id'] ;


    public function blog(){


        return $this->belongsTo('App\Blog');

    }

    /**
     * Lnguage eloquent to fetch content based on language
     * if you asking of which it will help - then it makes it more easier when
     * using with tabs --- sothat everything will be dynamic .
     * @return [type] [description]
     */
    public function language(){

        return $this->belongsTo('App\Language' , 'language_id');
    }
}
