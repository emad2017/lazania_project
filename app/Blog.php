<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{


    protected $fillable = [ 'id' ,  'addby_id'] ; 
    //
    public function description(){


        return $this->hasMany('App\BlogDescription' , 'blog_id');

    }
    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }
    public function department()
    {


        return $this->belongsTo('App\BlogDepartment', 'department_id');

    }
}
