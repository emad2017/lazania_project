<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialEvent extends Model
{
    //
    public function description(){


        return $this->hasMany('App\SpecialEventDescription' , 'event_id');

    }
    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }

}
