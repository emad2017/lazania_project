<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcommerceWishlist extends Model
{
   protected $table = "e_wishlists" ; 


   public function product(){

   		return $this->hasMany('App\DessertItem' , 'id' , 'product_id'); 

   }
   
}
