<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogDepartmentDescription extends Model
{
    public function blogdespartments(){


        return $this->belongsTo('App\BlogDepartment');

    }

    /**
     * Lnguage eloquent to fetch content based on language
     * if you asking of which it will help - then it makes it more easier when
     * using with tabs --- sothat everything will be dynamic .
     * @return [type] [description]
     */
    public function language(){

        return $this->belongsTo('App\Language' , 'language_id');
    }
}
