<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewSubDepartmentRequest extends FormRequest
{
       
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
 

         switch($this->method())
        {
           
            case 'POST':
            {
               return [

                    // ar 

                    "sub_name_ar"=>'required',
                    "sub_description_ar"=>'required', 
                 
                    


                    // en 
                    "sub_name_en"=>'required',
                    "sub_description_en"=>'required', 
                     
                    
                    
                    // for image 
                    'file'=>'required'
                ];
            }
            
            case 'PUT':
            {

                
                

                return [
                    
                     // ar 

                     "sub_name_ar"=>'required',
                     "sub_description_ar"=>'required', 
                    
                     
 
 
                     // en 
                     "sub_name_en"=>'required',
                     "sub_description_en"=>'required'
                      
                     
                     

                ];
            }
            default:break;
        }



         
      
    }


    public function messages(){

        
       

        return [

           
        ];
    }


    /**
     * simply what this function do is to get the last segment at every step  
     * why u need that  cause if you're using  FormRequest in validation logic ( which is the cleanest way )
     * you will gonna need an id to skip if some unique column value is passed  so check it  -_- 
     * Emad  El Meko
     */
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }



}
