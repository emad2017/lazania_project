<?php

namespace App\Http\Middleware;

use Closure;

use Auth ; 
use LaravelLocalization ;




use Session ; 

class RedirectifNotCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \Guard   $guard
     * @return mixed
     */
    public function handle($request, Closure $next , $guard = 'customer')
    {


       $product_segment_id = $request->segment(4) ; 

      

       $lang = LaravelLocalization::getCurrentLocale();
       // dd($request->path());

       if (!Auth::guard($guard)->check()) {
            
            
            // i need to check the coming request if 
            // it was intended to shopping cart 
            // well i will make a session variable with a message that 
            // authentication is required to procceed 
            if ($request->path() == "$lang/customer/shopping-cart"){
               
                // then i got my endpoint here and 
                // i may inject my session -_- 
                $request->session()->flash('authentication_needed' , trans('lazena.login_to_shopping_cart')); 


                // this key will be used after authentication i intended to do that -_- 
                $request->session()->put('after_authentication' , ' ');

                $request->session()->forget('incoming_wishlist'); 
                $request->session()->forget('product_id'); 

                return redirect("$lang/customer-login");
              

            }else if($request->path() == "$lang/customer/add_product_to_whislist/$product_segment_id"){

               
                
                $request->session()->flash('wishlist_auth_needed' , trans('lazena.login_to_wishlist') ); 

                 $request->session()->put('incoming_wishlist' , ""); 
                 $request->session()->put('product_id' , "$product_segment_id"); 

                
                 $request->session()->forget('after_authentication'); 
                 
                return redirect("$lang/customer-login");

            }else{

                $request->session()->forget('after_authentication'); 
                $request->session()->forget('incoming_wishlist'); 
                $request->session()->forget('product_id'); 

                $request->session()->flush();
                return redirect("$lang/customer-login");

            }
            
           
             // return redirect("$lang/customer-login");


        }
  
      
        return $next($request);

    }



    
}
