<?php

namespace App\Http\Controllers\frontend;



use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DessertDepartments ;
use App\DessertDepartmentDescription;
use App\DessertItem;
use App\DessertItemDescription;

use App\About;
use App\AboutDescription;

use App\Say;
use App\SayDescription;

use App\NewsLetter ; 


use App\Slider ; 

use App\Language ; 
use LaravelLocalization ; 

use DB;




class HomeController extends Controller
{

    
    // public accessable property available in my class 
    public $language ;

    /**
     * inject language to the constructor function to be used whole over the class 
     * @param Language $language [description]
     */
    public function __construct(Language $language)
    {
       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->value('id');

       
    }

    /**
     * This is our home controller that will hold all pieces of information
     * slider section  
     * excert - about lazania 
     * main categories section  
     * most sales products  
     * most viewed products  
     * footer section 
     */
    

    public function getHomePage(){

        
        // need to fetch desert items slider  
        // fetch the latest ones  - limit results to 4  - fetch the results  
        $slider_products  = Slider::where(['status'=>'active'])->latest()->get(); 
        // about excert after slider  : 
        $about = About::findOrFail(1) ; 
        $about_content  ; 
        foreach($about->description as $description ){
            if($description->language_id == $this->language){
                // so we have an ids matches here  -_-    
                $about_content = $description->about ; 
            }
        }
        // fetch main categoreis in lazania 
        $main_categories = DessertDepartments::where(['status'=>'active'])->get(); 

        // best selling product  ( 6 maximum )
        $best_selling_products = DessertItem::where(['status'=>'active' , 'best_selling'=>1])->latest()->limit(6)->get(); 
        
        // get best selling products content based on language 
        $best_selling_products_content = [] ; 
        foreach($best_selling_products as $product) {
            foreach ($product->description as $description) {
                if($description->language_id == $this->language){
                   
                    $best_selling_products_content [] = $description ; 
                }
            }
        }


       


        // most viewed products  
        $most_viewed_products = DessertItem::orderBy('views' , 'DESC')
                                            ->take(4)
                                            ->get(); 

        // most viewed content based language  
        $most_viewed_products_content = [] ; 
      


        foreach($most_viewed_products as $product) {
           
            foreach ($product->description as $description) {
                if($description->language_id == $this->language){
                    
                    $most_viewed_products_content [] = $description ; 
                }
            }
        }
      
         
        
        // most viewed products promoted
        // in most viewed promoted i will fetch the first result of products that has the max view number  
        // so in that mannaer it would be dynamic and pure  -_-
        
        $most_viewed_product_promoted =  DessertItem::where(['status'=>'active' , 'promoted'=>1])->orderBy('views' , 'DESC')->first();
        

        // get most viewed_promoted product content based language  : 
        $most_viewed_product_content ; 
        if(count($most_viewed_product_promoted) > 0){
        foreach ($most_viewed_product_promoted->description as $description) {
            if($description->language_id == $this->language){
                    $most_viewed_product_content  = $description ; 
            }
        }
        }else{
            $most_viewed_product_content = null ;  
        }


    
        


        // Recommended products  : fetch all prodcuts that have a recommended property equals to one  -_- 
        
        $recommended_products = DessertItem::where(['status'=>'active' , 'recommended'=>1])->get(); 
        // get recommended products content based language 
        $recommended_content = [] ; 
        foreach($recommended_products as $recommended){

            foreach ($recommended->description as $description) {
                if($description->language_id == $this->language){
                        $recommended_content [] = $description ; 
                }
            }
        }




         
        
        return view('frontend.index' , compact('slider_products' , 'about_content' , 'main_categories' , 'best_selling_products_content'  , 'most_viewed_products_content' , 'most_viewed_product_content','recommended_content')); 

    }


    public function subscribe(Request $request){

             if($request->ajax()){

                 // get the subscriber email : 
                 $email = $request->email ; 

                 // if he sent an empty value  -- return a warning message  
                 if(empty($email) || $email == null){

                    return response()->json([
                        'status'=>'empty_email' , 
                        'msg'=>trans('dessertdepartment.must_enter_email') 
                    ]);
                 }
                 // find if there is any match results  
                 $check_news_letter = NewsLetter::where(['email'=>$email])->first() ;

                 // if a result found -- means he already subscribed before  
                 if(count($check_news_letter) > 0){

                    return response()->json([
                        'status'=>'already_subscribed' , 
                        'msg'=>trans('dessertdepartment.already_subscribed') 
                    ]);
                 }
                 
                 // also we may add the in-valide email 
                 

                 // last case if it was successfull send him a congratulation message   
                 
                 $new_subscriber = new NewsLetter ; 
                 $new_subscriber->email = $email ; 

                 if($new_subscriber->save()){

                    return response()->json([
                        'status'=>'subscribtion_done' , 
                        'msg'=>trans('dessertdepartment.subscribtion_done') 
                    ]);
                 }
             }

    }


}