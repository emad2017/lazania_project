<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\Customer; 
use App\NewsLetter; 
use App\Address ; 


use App\EcommerceWishlist ; 
use App\Language ; 


use Auth ; 
use Hash ; 

use Session ; 


use LaravelLocalization ; 




class WishListController extends Controller
{


    // public accessable property available in my class 
    public $language ;

    /**
	 * Customer must be authenticated to access this area  -_- 
	 * using guards  
	 * am using custom middleware to redirect un authenticated customer 
	 * to the login area 
	 */
    public function __construct(Language $language)
    {
        $this->middleware('customer');

        $default_locale =  LaravelLocalization::getCurrentLocale() ;
        $this->language =  $language = Language::where(['label'=>$default_locale])->value('id');
    }


    /**
     * This function is responsible for fetching all products added to wish list -_- 
     * @return [type] [description]
     */
    public function index(){

        $customer = Auth::guard('customer')->user(); 

    	$wishlist_products  =  $customer->wishlistProduct ;

        // array that will hold our content -_- 
        $result = [] ; 

        foreach($wishlist_products  as $product){

            /**
             * Accessing wishlist single product wwith a relation product 
             * checkout EcommerceWishlist model to make things clear
             * 
             */
            foreach($product->product as $singleProduct){


                // then i need to access the content based on language like normal we do 
                foreach ($singleProduct->description as $description) {

                  

                    if($description->language_id == $this->language){

                        // the flush our holder array with data 
                        $result [] = $description ; 
                    }
                }
                  
            }
        }



        return view('frontend.customer.wishlist' , compact('result'));


    }


    /**
     * Function to dump product in wishlist table
     * @param Request $request [description]
     * @param [type]  $id      [description]
     */
    public function addProduct(Request $request , $id){
        


           $customer = Auth::guard('customer')->user(); 


           // i need to check if this product was added before after login -_- 
           // here comes logic ya bta3 el logic 
           
           $productWasAddedBefore = EcommerceWishlist::where(['customers_id'=>$customer->id , 'product_id'=>$id])->first(); 

           if(count($productWasAddedBefore) > 0 ){

                Session::flash('product_added_before' , trans('product_added_to_wishlist_before')); 

                return redirect()->route('whislist');

           }else{

                // else add this fucking product -_-

               $wishlist_product_record = new EcommerceWishlist ; 

               $wishlist_product_record->product_id = $id ; 
               $wishlist_product_record->customers_id = $customer->id ; 

               $wishlist_product_record->save(); 

               Session::flash('product_added_to_wishlist' , trans('lazena.product_added_to_wishlist')); 

               return redirect()->route('whislist');


           }

           

    }

    /**
     * Remove product from wish list  
     * i will go through ajax  -_- 
     * cause i love it  
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function removeWishlistProduct(Request $request){

        if($request->ajax()){

            // so get inside  -_- 
            
            $customer_id = Auth::guard('customer')->user()->id ;  
            $product_id  = $request->product_id ; 

            // get the unlucky product to remove from with list  
            $findUnLuckyProduct = EcommerceWishlist::where(['customers_id'=>$customer_id , 'product_id'=>$product_id])->first(); 

            // remove the product  
            $findUnLuckyProduct->delete(); 

            // return the response  
            return response()->json(['status'=>'product_removed']); 



        }

    }
}
