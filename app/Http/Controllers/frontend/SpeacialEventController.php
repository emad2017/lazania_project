<?php

namespace App\Http\Controllers\frontend;


use App\AboutDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SpecialEvent;
use App\SpecialEventDescription;
use App\SpecialEventItem;
use App\SpecialEventItemDescription;
use  DB;
use App\Language ; 
use LaravelLocalization ; 


class SpeacialEventController extends Controller
{

     // public accessable property available in my class 
    public $language ;

    /**
     * inject language to the constructor function to be used whole over the class 
     * @param Language $language [description]
     */
    public function __construct(Language $language)
    {
       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->value('id');

       
    }

    public function index($id , $name)
    {
        
         $occassion_content =  SpecialEventDescription::where(['event_id'=>$id , 'language_id'=>$this->language])->first(); 


         
        return view('frontend.special_occassions.index' , compact('occassion_content'));


    }

     
}