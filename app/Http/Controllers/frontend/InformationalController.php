<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\About ; 
use App\AboutDescription ; 


use App\Language ; 
use LaravelLocalization ; 



class InformationalController extends Controller
{
    

	public $language ;



    public function __construct(Language $language)
    {

       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->first();

    }


    public function about(){


    	 $about = About::find(1) ; 
         $about_description  ;
        foreach ($about->description as $description ) {
            
                   if($this->language->id == $description->language->id){
                     $about_description  =  $description  ;
                   }
             
        }




    	return view('frontend.informational.about' , compact('about_description')) ; 
    }
}
