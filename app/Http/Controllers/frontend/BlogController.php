<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogDepartment;
use App\BlogDescription;
use  DB;


class BlogController extends Controller
{


    public  function index()
    {

        $blogs = Blog::all();
        $blogdepartments = BlogDepartment::all();
        return view('frontend.blog',compact('blogs','blogdepartments'));
    }


}
