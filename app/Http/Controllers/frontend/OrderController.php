<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Notifications\NewOrder ; 

use App\Mail\OrderPlaced ; 
use App\Mail\OrderPlacedForAdmin ; 


use Illuminate\Support\Facades\Mail;
 


use App\Address;
use App\Order;
use App\OrderProduct;
use App\DessertItem;

use App\Customer ; 
use App\User ; 

use Auth ; 

use Carbon\Carbon ; 

use Session  ; 

class OrderController extends Controller
{
    
	/**
	 * Customer must be authenticated to access this area  -_- 
	 * using guards  
	 * am using custom middleware to redirect un authenticated customer 
	 * to the login area 
	 */
    public function __construct()
    {
        $this->middleware('customer');
    }


    public function getMyOrders(){


    	$customer  =  Auth::guard('customer')->user(); 
    	// fetch customer orders  
    	$orders = $customer->orders ; 
 

    	return view('frontend.customer.my_orders' , compact('orders'));

    }

	public function hackit(Request $request){


		$customer = Auth::guard('customer')->user(); 

		$customer_name = $customer->fname .' '.$customer->lname ; 

		if($request->ajax()){


				 

				// then add order  in db 
				$order = new Order ; 
				$order->order_ticket = 'ellelazena_'. str_random(6) ; 
				$order->customer_id  = $customer->id ; 
				$order->order_date   = Carbon::now() ;  
				$order->order_total  = $request->total ; 
				$order->order_status  = 'pending'  ; 
				$order->save(); 

				// then add info for the address form 
				$address = new Address ; 

				$address->customer_id = $customer->id ; 
				$address->order_id = $order->order_ticket ; 
				$address->fname = $request->fname ; 
				$address->lname = $request->lname ; 
				$address->street = $request->address ; 
				$address->country = $request->country ; 
				$address->gender = $request->gender ; 
				$address->telephone = $request->telephone ; 
				$address->save() ; 
 


				// then insert products with order  in order_product table 
				
				// products sold inshallah 
				$products = json_decode(json_encode($request->products) , true) ; 
				foreach ($products as $product) {
				 
					// loop through then go -_- 
					// will use that to update product qunatity -_- 
					$find_product = DessertItem::find($product['product_id']); 
					
					$quantity = $find_product->quantity ; 

					$sold =  $product['selected_quantity'] ; 
					$find_product->quantity = $quantity - $product['selected_quantity'];  
					// save new quantity 
					$find_product->save() ; 

					$order_records = new OrderProduct ; 
					$order_records->order_id = $order->order_ticket ; 
					$order_records->product_id = $find_product->id  ; 
					$order_records->quantity = $sold ; 
					$order_records->customer_id = $customer->id  ; 
					$order_records->save() ;  

				}


				// fire a notification  -_- 
				// i will notify adminstrator  -_- 
				$adminstrators = User::all(); 
				foreach ($adminstrators as $adminstrator) {
					
					$adminstrator->notify(new NewOrder( $order , $customer_name )); 
				
				}


				// i need to send an email to customer  
				 Mail::to($customer->email)->send(new OrderPlaced($order)); 

				 // main to elleazena
				 Mail::to('info@ellelazena.com')->send(new OrderPlacedForAdmin($order)); 
				// before the return flash a message  
				
				Session::flash('order_placed' ,  trans("lazena.order_placed")  );

				return response()->json(['status'=>'ordered']); 
 

		}

		 

	}

	/**
	 * Fetch order details  -_- 
	 * @param  Request $request [description]
	 * @param  [type]  $order_ticket  [ order id  ]
	 * @return [type]           [description]
	 */
	public function orderDetails(Request $request , $order_ticket){

			$order = Order::where(['order_ticket'=>$order_ticket])->first() ; 

			return view('frontend.customer.order_details' , compact('order')) ; 


	}



	


}
