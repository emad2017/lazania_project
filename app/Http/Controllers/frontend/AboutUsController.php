<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\About;
use  DB;


class AboutUsController extends Controller
{


    public  function index()
    {

        
        return view('frontend.about_us.index' , compact('about','teams') );
        
    }


}
