<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Blog ; 
use App\BlogDescription;

use App\Language ; 
use LaravelLocalization ; 

class CareerController extends Controller
{
    

		public function index(){

			$lang = LaravelLocalization::getCurrentLocale() ;

			$language = Language::where(['label'=>$lang])->first(); 


			$career = Blog::find(1) ;

			$career_content = [] ; 

			foreach ($career->description as $description) {
			 		
				if($description->language_id == $language->id){

					$career_content [] = $description ; 
				}

			 } 


			return view ('frontend.careers.index' , compact('career_content')) ;



		}


}
