<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use LaravelLocalization ; 
use App\Language ; 

use App\DessertItemDescription ; 

class SearchController extends Controller
{
    
    public function searchResutlt(Request $request){

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;

    	if($request->ajax()){

    		// get the query 
    		$query = $request->search_query ; 

    		if(empty($query)){

    			return response()->json(['status'=>'empty_query']); 
    		}else{

    			// search inside your model  -_- 
    			$products = DessertItemDescription::where('name','LIKE','%'.$query.'%')
    					->where('language_id', $language_id)
    					->with('desertItemSearchable')
    					->with('desertItemSearchable.productSubDeptSearch')
    					->with('desertItemSearchable.productSubDeptSearch.mainDespartmentSearchable')
    					->get();

    			if($products->count()){
 
    				return response()->json([
    											'status'=>'success' , 
    											'products'=>$products   

    										]) ; 

    			}else{

    				return response()->json(['status'=>'empty_query']);
    			}


    		}

    		 




    		return $products  ; 

    	}

    }
}
