<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Customer; 
use App\NewsLetter; 
use App\Address ; 


use Auth ; 
use Hash ; 

use Session ; 



class CustomerController extends Controller
{


	/**
	 * Customer must be authenticated to access this area  -_- 
	 * using guards  
	 * am using custom middleware to redirect un authenticated customer 
	 * to the login area 
	 */
    public function __construct()
    {
        $this->middleware('customer');
    }


    /**
     * Customer logic will go through this controller 
     * profile  - account info - addresses - wishlists 
     */

    public function dashboard(){


        // what i need to send alongside with profile  is the below information -_- : 
        
        // customer information  
        // customer subscribtiion mail list  
        // default billing addrress if found  
        
        // find customer first  : 
        $customer = Auth::guard('customer')->user(); 

        // customer information  -_- : 
        $customer_information = Customer::find($customer->id) ; 

        // customer sucbsribition mail list -_-  : 
        $customer_newsletter = NewsLetter::where(['email'=>$customer->email])->first(); 

        // customer addresses  -_- :
        $customer_addresses  = Address::where(['customer_id'=>$customer->id])->get(); 


        


    	return view('frontend.customer.dashboard' , compact('customer_information','customer_newsletter','customer_addresses')) ; 
    }


    /**
     * Get Customer account information update form 
     * @return [type]     [description]
     */
    public function accountInformation(){

        // Auth customer  : 
        $customer = Auth::guard('customer')->user(); 
        // customer information  -_- : 
        $customer_information = Customer::find($customer->id); 

        return view('frontend.customer.account_information' , compact('customer_information')); 

    }

    /**
     * Check password submitted
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function checkUpdate(Request $request){
        
        if($request->ajax()){

            $customer = Customer::find($request->customer_id) ; 
            $customer_password = $customer->password ; 

             

           if (Hash::check($request->password , $customer_password)) {
 
                // then passwods matchs 
                return response()->json([
                    'status'=>'match' 
                ]);

            }else{
                return response()->json([
                    'status'=>'mismatch'
                ]);
            }

        }

    }


    public function updateAccountInformation(Request $request , $id){



            $customer = Customer::find($id) ;

            // validate incoming request  
            /**
             * Custom messages array to avoid default laravel validation rules  
             * this in later would be replaced with translated labels  -_-
             * @var [type]
             */
            $messages = [

                'fname.required'=>'Please make sure to enter your first name' , 
                'lname.required'=>'Please make sure to enter your last name' , 
                'email.required'=>'Please make sure to enter your email address' , 
                'email.email'=>'Please make sure that you entered a correct email usally email formats are like this ( example@email.com )' , 
                'email.unique'=>'We found this email in our database records , we think you should pick another email address' , 
                'city.required'=>'Could you please tell us the city you located in' , 
                'country.required'=>'Please can you tell us the country of residence' , 
                'street.required'=>'Street really helps us deliver our products quickly',
                'telephone.required'=>'Telephone is important , please mention it' 
                 


            ];

            $this->validate($request , [

                'fname'=>'required', 
                'lname'=>'required', 
                'email'=>'required|email|unique:e_customers,email,'.$customer->id , 
                'city'=>'required', 
                'country'=>'required', 
                'street'=>'required', 
                'telephone'=>'required', 
                'password'=>'required' 

            ] , $messages);

        // update incoming request  -_- : 
        $customer->fname = $request->fname ; 
        $customer->lname = $request->lname ; 
        $customer->email = $request->email ; 
        $customer->street = $request->street ; 
        $customer->city = $request->city ;
        $customer->country = $request->country ;
        $customer->gender = $request->gender ; 
        $customer->telephone = $request->telephone ; 

        $customer->save(); 


        // after that flash a message that update went successfully 
        session()->flash('update_account_successfull' , ' Your profile information has been updated successfully ') ; 

        // then return back again  
        return back(); 

    }


    /**
     * Update password function  
     * replace old password with a new one 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function updateAccountPassword(Request $request , $id){


            $customer = Customer::find($id) ;
            // validate incoming request  
            /**
             * Custom messages array to avoid default laravel validation rules  
             * this in later would be replaced with translated labels  -_-
             * @var [type]
             */
            $messages = [

                'new_pass.required'=>'Please make sure to enter your new password' , 
                'new_pass_confirmation.required'=>'Please make sure to enter your new password confirmation' , 
                'new_pass.confirmed'=>'Ooh , new password and new password confirmation don\'t match '  
                 
            ];

            $this->validate($request , [

                'new_pass'=>'required|confirmed', 
                'new_pass_confirmation'=>'required'

            ] , $messages);

         
        $customer->password = crypt($request->new_pass , '')  ; 
        $customer->save(); 


        // after that flash a message that update went successfully 
        session()->flash('update_password_successfull' , ' Your account new password has been set successfully ') ; 

        // then return back again  
        return back(); 


    }



    /**
     * Function is responsible for getting a list of all products added to cart \
     * it will have the best of logic 
     * i will prove that i provided the best of all practices -_- 
     * so keep in touch 
     * @return [type] [a list of products added to cart]
     */
    public function getCart(){

        
        return view('frontend.customer.shopping_cart') ; 
    }
}
