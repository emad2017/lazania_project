<?php

namespace App\Http\Controllers\frontend;

use App\About;
use App\AboutDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  DB;
use LaravelLocalization ; 
use App\Language ;
use Session ;

use Carbon\Carbon ; 

class ContactUsController extends Controller
{


     // public accessable property available in my class 
    public $language ;

    /**
     * inject language to the constructor function to be used whole over the class 
     * @param Language $language [description]
     */
    public function __construct(Language $language)
    {
       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->value('id');

       
    }

    public  function contact()
    {


        // get data from database to send it to view of contacr us !
        // dd('Contact us ');
        $about = About::findOrFail(1) ; 
            
        $content = [] ; 
        
            foreach ($about->description as $description) {
                  if($description->language_id == $this->language){
                        $content [] = $description ; 
                  }
            }
       
        
       
        return view('frontend.contact_us.index',compact('content'));

    }

    public function contactmsg(Request $request)
    {
        


        $name = $request['name'];
        $email = $request['email'];
        $phone = $request['phone'];
        $msg = $request['message'];


        DB::table('contact_with_u_s')->insert(
            [
                'name' => $name,
                'email' => $email,
                'subject' => $phone,
                'msg' => $msg , 
                'created_at'=>Carbon::now()
            ]
        );

        Session::flash('contact_sent' , trans('lazena.contact_sent'));
        return back(); 





    }

}
