<?php

namespace App\Http\Controllers\frontend;



use App\AboutDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DessertDepartments ;
use App\DessertDepartmentDescription;
use App\DesertSubDepartment ;
use App\DesertSubDepartmentDescription ;

use App\DessertItem;
use App\DessertItemDescription;
use DB;
use LaravelLocalization ; 
use App\Language ; 




class CategoriesController extends Controller
{


    // public accessable property available in my class 
    public $language ;

    /**
     * inject language to the constructor function to be used whole over the class 
     * @param Language $language [description]
     */
    public function __construct(Language $language)
    {
       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->value('id');

       
    }


    public function index()
    {
       
        $categories = DessertDepartments::where(['status'=>'active'])->get(); 
        $content = [] ; 
        foreach ($categories as $category) {
            foreach ($category->description as $description) {
                  if($description->language_id == $this->language){
                        $content [] = $description ; 
                  }
            }
        }
        return view('frontend.categories.index' , compact('content')); 


    }


    /**
     * List all sub category under the main category
     * @param  [type] $id [id represents main category id]
     * @return [type]     [description]
     */
    public function indexSub(  $id ){

        $lang = LaravelLocalization::getCurrentLocale(); 
        $language = Language::where(['label'=>$lang])->first(); 
        $mainCategory = DessertDepartments::find($id) ; 
        $main_category_name_clean =  DessertDepartmentDescription::where(['language_id'=>$language->id , 'department_id'=>$id])->value('name'); 

         

        $subCategories = $mainCategory->subDepts ; 

        // replace underscores with spaces again :-D 
        
        // $main_category_name_clean =   $main_category_name;

        // dd($main_category_name);


        $sub_holder = [] ; 
        if(count($subCategories) > 0 ){

          foreach ($subCategories as $subCategory) {
             foreach ($subCategory->description as $description) {
                 if($description->language_id == $this->language){
                    $sub_holder [] = $description ; 

                 }
             }
          }

        }

        return view('frontend.categories.index_sub' , compact('sub_holder' , 'main_category_name_clean' ));

    }


    /**
     * fetch the products under sub category
     * @param  [type] $main_category_name [description]
     * @param  [type] $id                 [description]
     * @param  [type] $sub_category_name  [description]
     * @param  [type] $sub_id             [description]
     * @return [type]                     [description]
     */
    public function indexProducts(  $id  , $sub_id){

            // here we are going to fetch the products under sub category 
            // i need names to draw my bread crumbs dynamic  -_-
            
            // $un_cleaned_cat_name =  $main_category_name ; 

            // $un_cleaned_sub_cat_name = $sub_category_name ; 


            $lang = LaravelLocalization::getCurrentLocale(); 
            $language = Language::where(['label'=>$lang])->first(); 
            $mainCategory = DessertDepartments::find($id) ; 
            $un_cleaned_cat_name = DessertDepartmentDescription::where(['language_id'=>$language->id , 'department_id'=>$id])->value('name'); 

            // get sub category 
            $subCategory = DesertSubDepartment::find($sub_id) ;
             $un_cleaned_sub_cat_name = DesertSubDepartmentDescription::where(['language_id'=>$language->id , 'subDeptId'=>$sub_id])->value('name');  

          
            // fetch products  : 
            $products = $subCategory->deserts ; 

            $products_content_holder = [] ;     
            if(count($products) > 0 ){

                foreach ($products as $product) {
                        
                    foreach ($product->description as $description) {
                        if($description->language_id == $this->language){
                            $products_content_holder [] = $description ; 
                        }
                    }
                }

            }else{

                $products_content_holder = null ; 
            } 



            return view('frontend.categories.index_products' , compact('products_content_holder' , 'un_cleaned_cat_name' , 'un_cleaned_sub_cat_name' , 'id' , 'sub_id')) ; 


    }



    /**
     *  Fetch a product details alongside with all needed data for the breadcrumbs 
     *  i love dynamic relations and my self -_- @emadRashad  
     * @param  [type] $main_category_name [description]
     * @param  [type] $id                 [description]
     * @param  [type] $sub_category_name  [description]
     * @param  [type] $sub_id             [description]
     * @param  [type] $product_name       [description]
     * @param  [type] $product_id         [description]
     * @return [type]                     [description]
     */
    public function productDetails(  $id  , $sub_id  , $product_id){

        // uncleaned names  : 
        // $un_cleaned_cat_name =   $main_category_name  ; 
 
        // $un_cleaned_sub_cat_name =   $sub_category_name  ; 
        // $un_cleaned_product_name =   $product_name  ; 



            $lang = LaravelLocalization::getCurrentLocale(); 
            $language = Language::where(['label'=>$lang])->first(); 
            $mainCategory = DessertDepartments::find($id) ; 
            $un_cleaned_cat_name = DessertDepartmentDescription::where(['language_id'=>$language->id , 'department_id'=>$id])->value('name'); 

            // get sub category 
            $subCategory = DesertSubDepartment::find($sub_id) ;
            $un_cleaned_sub_cat_name = DesertSubDepartmentDescription::where(['language_id'=>$language->id , 'subDeptId'=>$sub_id])->value('name');

 
            $product = DessertItem::find($product_id) ; 
            $un_cleaned_product_name = DessertItemDescription::where(['language_id'=>$language->id , 'item_id'=>$product_id])->value('name');



        $product_holder  ; 
        if(count($product) > 0 ){

            foreach ($product->description as $description) {
                if($description->language_id == $this->language){
                    $product_holder = $description ; 
                }
            }
        }else{
            $product_holder = null ; 
        }



        // i need to increment view counter for product with one  
        
        $find_product = DessertItem::find($product_id) ; 

        $find_product->views = (int) $find_product->views + 1 ; 

        $find_product->save(); 


      

        return view('frontend.categories.product_details' , compact('product_holder','un_cleaned_cat_name' , 'un_cleaned_sub_cat_name' , 'un_cleaned_product_name' , 'id' , 'sub_id' , 'product_id')) ; 

    }



    public function getcat($id)

    {
        // 1
        $cats = DessertDepartments::all(); // cat data
        // 2-
        $randdessert = DessertItem::where(['department_id'=> $id])->get();
        // 3
        $recentdessert = DessertItem::all()->take(5);

        return view('frontend.Categories',compact('cats','randdessert','recentdessert'));


    }



}