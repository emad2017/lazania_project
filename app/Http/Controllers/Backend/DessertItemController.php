<?php

namespace App\Http\Controllers\Backend;

use App\DessertDepartments;
use App\DessertItem;
use App\DessertItemDescription;
use  App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use File ;

class DessertItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("welcome to our index");
        $dessertitems = DessertItem::all();

        return view('backend.dessertitem.index' , compact('dessertitems'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //  dd("create new dessert item");
        $languages = Language::where(['status'=>'1'])->get();
        $departmentitems = DessertDepartments::where(['status'=>'active'])->get();


        return view('backend.dessertitem.create',compact('languages','departmentitems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //  dd('store dessert item');

        if($request->hasFile('file')){



            $images = $request->file('file');

            foreach ($images as $image) {



                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/dessertsitem'), $image_rename) ;

                $pic = new DessertItem();
                $pic->img1 = $image_rename ;
                $pic->addBy_id = Auth::user()->id ;
                $pic->department_id = $request->departmentdessert;

                $pic->price = $request->price;
                $pic->rate = $request->rate;



                $pic->save() ;


            }

            $languages = Language::where('status','1')->get();
            foreach ($languages as $language) {



                $dessertDescription = new  DessertItemDescription();

                $dessertDescription-> item_id = $pic->id ;
                $dessertDescription-> language_id = $language->id ;


                $dessertDescription-> name = $request->input("name_$language->label");
                $dessertDescription-> description = $request->input("description_$language->label");


                $dessertDescription->save();
            }


        }
        else {
            $pic = new DessertItem();
            $pic->addBy_id = Auth::user()->id;
            $pic->department_id = $request->departmentdessert;
            $pic->status = $request->status == 'on' ? 'active' : 'not_active';
            $pic->price = $request->price;
            $pic->rate = $request->rate;


            $pic->save();

            $languages = Language::where('status', '1')->get();

            foreach ($languages as $language) {


                $dessertDescription = new  DessertItemDescription();

                $dessertDescription->item_id = $pic->id;
                $dessertDescription->language_id = $language->id;


                $dessertDescription->name = $request->input("name_$language->label");
                $dessertDescription->description = $request->input("description_$language->label");


                $dessertDescription->save();

            }
        }





        // flashing a success message
        session()->flash('success' , trans('dessertdepartment.dessert_dessert_added'));

        // return to a specific view
        return redirect()->route('dessertitem.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd("edit desser item ");
        $languages = Language::where(['status'=>'1'])->get();

        $desseritem = DessertItem::find($id);
        $speacialdepartments = DessertDepartments::where(['status'=>'active'])->get();




        return view('backend.dessertitem.edit',compact('desseritem','languages','speacialdepartments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd("update of Desset item");

        $dessertitem =   DessertItem::find($id) ;

        $dessertitem->status = $request->status == 'on' ? 'active' : 'not_active'  ;


        $dessertitem->addby_id =  Auth::user()->id ;
        $dessertitem->department_id = $request->dessertitem;
        $dessertitem->price = $request->price;

        $dessertitem->save() ;

        /**
         * Check To See if there any attached images with this departement .
         */
        if($request->hasFile('file')){


            $images = $request->file('file');

            foreach ($images as $image) {



                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/dessertsitem'), $image_rename) ;


                /**
                 * Saving  in in our seperated table
                 * we will fetch  by eloquent
                 */



                $dessertitem->img1 = $image_rename ;


                $dessertitem->save() ;


            }

        }


        $descriptions = $dessertitem->description ;


        foreach ($descriptions as $description) {


            $description->item_id = $dessertitem->id ;
            $description->language_id = $description->language_id ;


            $description->name = $request->input("dessert_name_".$description->language->label);
            $description->description = $request->input("dessert_description_".$description->language->label);


            // saving department content !
            $description->save();
        }





        session()->flash('success' , trans('dessertdepartment.dessert_update_msg'));

        // return to a specific view
        return redirect()->route('dessertitem.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //  dd("destroy  dessert item");
        $dessertitem = DessertItem::find($id) ;

        $dessertitem->delete() ;



        // after deleting show a flash message

        session()->flash('success' , trans('dessertdepartment.dessert_delete'));


        return redirect()->route('dessertitem.index');
    }
}
