<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order ; 
use App\Customer; 
use App\OrderProduct ; 

use App\Notification ;


use Carbon\Carbon ; 



class OrderController extends Controller
{
   


   public function index(){

   		// fetch  all orders  - paginate them on 10 
		$orders = Order::orderBy('created_at','DESC')->paginate(10) ; 

		return view('backend.order.index' , compact('orders'));


   }


   public function adminOrderDetails($order_ticket , $notification){


   		// return order details to admin -_- 
   		$order = Order::where(['order_ticket'=>$order_ticket])->first() ; 


      


         if($notification != 'null' ){


          

               $find_notification  = Notification::find($notification) ; 

               $find_notification->update(['read_at' => Carbon::now()]); 
         } 
   		


   		return view('backend.order.show' , compact('order'));

   }
	 

}
