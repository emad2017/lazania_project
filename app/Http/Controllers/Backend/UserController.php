<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User ;

use Storage ;
use File ;


class UserController extends Controller
{



    /**
     * Display Profile Info
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function profile(Request $request , $id){

        $user  = User::find($id);
        return view('backend.user.profile' , compact('user'));


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('backend.user.index' , compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $user  = new User;




        $type = $request->get('type');


        $user->propertyName = $request->user_property ;
        $user->headPersonName = $request->user_head_person ;
        $user->phoneNumber = $request->user_phone_number ;
        $user->businessRecordNumber = $request->user_business_record ;
        $user->branchName = $request->user_branch_name ;




        $user->doctorName = $request->doctorName ;
        $user->proffestionPracticeNumber = $request->proffestionPracticeNumber ;
        $user->doctorHospitalRelatedTo = $request->hospital_related_to ;
        $user->doctorSpecialization = $request->doctorSpecialization ;












        // common in three cases
        $user->password = crypt($request->user_password , '');
        $user->mobileNumber = $request->user_mobile_number ;
        $user->website = $request->user_website ;
        $user->isActive = $request->user_status == 'on' ? 'active':'not_active' ;
        $user->email = $request->user_email ;
        $user->userType = $request->type  ;


        $user->latitude = $request->latitude  ;
        $user->longitude = $request->longitude  ;




         if($request->hasFile('logo')){


                // in update we will check first if user has an image or not to be replaced


                // dd($request->file('logo'));
                $logo = $request->file('logo');
                $extension = $logo->getClientOriginalExtension();
                $logo_rename = time() . '.' .$logo->getClientOriginalExtension();



                $logo->move(public_path('uploads/users'), $logo_rename) ;


                // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));
                $user->profileImage = $logo_rename ;

        }


        $user->save();



        session()->flash('success' , trans('backend.user_message_updated'));

        // return to a specific view
        return redirect()->route('user.index');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id) ;
        return view('backend.user.show' , compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = User::find($id) ;
        return view('backend.user.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // valdiation rules gors here in future
        // dd($request->all());


        // get object
        $user = User::find($id);






        if($user->userType == 'doctor'){


                $user->doctorName = $request->doctorName ;
                $user->proffestionPracticeNumber = $request->proffestionPracticeNumber ;
                $user->doctorHospitalRelatedTo = $request->hospital_related_to ;





        }else{


            $user->propertyName = $request->user_property ;

            $user->headPersonName = $request->user_head_person ;
            // $user->userType = $request->user_type  ;
            $user->phoneNumber = $request->user_phone_number ;

            $user->businessRecordNumber = $request->user_business_record ;
            $user->branchName = $request->user_branch_name ;


            $user->longitude = $request->longitude ;
            $user->latitude = $request->latitude ;





        }













        $user->website = $request->user_website ;
        $user->mobileNumber = $request->user_mobile_number ;
        $user->email = $request->user_email ;


        $user->isActive = $request->user_status == 'on' ? 'active':'not_active' ;




        if($request->user_password != null || !empty($request->user_password)){


            $user->password = crypt($request->user_password , '');

        }


         if($request->hasFile('logo')){


                // in update we will check first if user has an image or not to be replaced


                // dd($request->file('logo'));
                $logo = $request->file('logo');
                $extension = $logo->getClientOriginalExtension();
                $logo_rename = time() . '.' .$logo->getClientOriginalExtension();



                $logo->move(public_path('uploads/users'), $logo_rename) ;


                // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));


                $user->profileImage = $logo_rename ;
        }


        $user->save();



        session()->flash('success' , trans('backend.user_message_updated'));

        // return to a specific view

        return redirect()->route('user.index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


         $user = User::find($id) ;
         // WE SHALL DELETE PHARMACEUTICAL AS WELL AS PHOTOS
         $user->delete() ;



         // after deleting show a flash message

         session()->flash('success' , trans('backend.user_deleted_successfully'));


         return redirect()->route('user.index');
    }
}
