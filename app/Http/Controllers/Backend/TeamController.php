<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User ;
use App\Team;

use Storage ;
use File ;
use Auth;


class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teamworks = Team::all();


        return View('backend.team.index', compact('teamworks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('create new team memeber');
        return view('backend.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

              $team  = new Team;


              $team->name = $request->name ;
              $team->description = $request->description ;
              $team->jobtitle = $request->jobtitle ;
              $team->addby_id = Auth::user()->id;
              $team->facebook = $request->facebook;
              $team->twitter = $request->twitter;
              $team->google = $request->google;
              $team->status = $request->status == 'on' ? 'active':'not_active' ;

               if($request->hasFile('logo')){


                      // in update we will check first if user has an image or not to be replaced


                      // dd($request->file('logo'));
                      $logo = $request->file('logo');
                      $extension = $logo->getClientOriginalExtension();
                      $logo_rename = time() . '.' .$logo->getClientOriginalExtension();



                      $logo->move(public_path('uploads/team'), $logo_rename) ;


                      // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));
                      $team->img = $logo_rename ;

              }


              $team->save();



              session()->flash('success' , trans('dessertdepartment.create_msg'));

              // return to a specific view
              return redirect()->route('team.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $team = Team::find($id) ;
     return view('backend.team.edit' , compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

                  $team = Team::find($id);


                    $team->name = $request->name ;
                    $team->description = $request->description ;
                    $team->jobtitle = $request->jobtitle ;
                    $team->addby_id = Auth::user()->id;
                    $team->facebook = $request->facebook;
                    $team->twitter = $request->twitter;
                    $team->google = $request->google;
                    $team->status = $request->status == 'on' ? 'active':'not_active' ;

                     if($request->hasFile('logo')){


                            // in update we will check first if user has an image or not to be replaced


                            // dd($request->file('logo'));
                            $logo = $request->file('logo');
                            $extension = $logo->getClientOriginalExtension();
                            $logo_rename = time() . '.' .$logo->getClientOriginalExtension();



                            $logo->move(public_path('uploads/team'), $logo_rename) ;


                            // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));
                            $team->img = $logo_rename ;

                    }




                    $team->save();



                    session()->flash('success' , trans('dessertdepartment.udpdate_msg'));

                    // return to a specific view
                    return redirect()->route('team.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd("destroy date of row ");
        $teamwork  =  Team::find($id);
        $teamwork->delete();
        // after deleting show a flash message

        session()->flash('success' , trans('dessertdepartment.delete_msg'));


        return redirect()->route('team.index');


    }
}
