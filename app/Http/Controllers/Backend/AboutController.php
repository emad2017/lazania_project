<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\About ; 
use App\AboutDescription ; 
use App\AboutPhotos ; 
use App\Language ; 
use LaravelLocalization ; 
use DB ; 
use Auth ; 
use File ; 


class AboutController extends Controller
{
    
	/**
	 * So here we will check if we have about information 
	 * then we will update it unless create a new one  
	 * cause this is a single blade 
	 * @return [type] [description]
	 */
	public function index(){

		// get authenticated  
		$user = Auth::user(); 

	    $about = About::updateOrCreate([
	    	 
	    	'related_to'=>$user->id 
	    	 
	    ]);

	    $languages = Language::where(['status'=>'1'])->get(); 

	    foreach($languages as $language){

		    $about_description = AboutDescription::updateOrCreate([

		    	'about_id'=>$about->id , 
		    	'language_id'=>$language->id


		    ]);

	    	
	    }
	    



 
	    return view('backend.about.index')->withAbout($about)->withLanguages($languages); 
    }

    public function update(Request $request , $id){


    	 
    	$about = About::find($id);


    	$about->status = $request->status == 'on' ? 'active':'not_active' ; 
    	$about->telephone1 = $request->telephone_one ; 
    	$about->tele = $request->telephone_two ;
    	$about->related_to = Auth::user()->id;


    	$about->timestart  = $request->timestart;
    	$about->timeend  = $request->timeend;

        $about->class_num  = $request->class;

        $about->staff_num  = $request->staff;
        $about->master_num  = $request->master;
        $about->kitchen_num  = $request->ketchen;

        $about->since_year   = $request->Since_year;
        $about->email        = $request->email;
        $about->facebook = $request->facebook;
        $about->twitter = $request->twitter;
        $about->google = $request->google;


    	$about->save(); 





 

				if($request->hasFile('file')){


                    /**
                     * i will not delete the old images  
                     */

                    $images = $request->file('file'); 

                    foreach ($images as $image) {
                       


                            $extension = $image->getClientOriginalExtension();
                            $image_rename = str_random(6). '.' .$image->getClientOriginalExtension(); 

                            $image->move(public_path('uploads/about'), $image_rename) ; 


                            /**
                             * Saving  in in our seperated table  
                             * we will fetch  by eloquent 
                             */

                            $pic = new About();

                            $pic->info_img = $image_rename ;


                            $pic->save() ; 


                    }  

            	}

 


            $descriptions = $about->description ; 


            foreach ($descriptions as $description) {
                
                
                $description->about_id = $about->id ; 
                $description->language_id = $description->language_id ;


                $description->name = $request->input("name_".$description->language->label);  
                $description->description = $request->input("about_description_".$description->language->label);
                $description->about = strip_tags($request->input("about_".$description->language->label));
                $description->address = ($request->input("address_".$description->language->label));


                // saving department content !
                $description->save(); 
            }


             // flashing a success message  
                session()->flash('success' , trans('about.about_info_updated')); 

                
    			return back(); 

    }




    /**
     * Fetch User About Photos  
     * as we know that each user has only one about record on database  
     * @param  [type] $related_to [description]
     * @return [type]             [description]
     */
    public function getImages($related_to){



            // get about info 
            $about = About::where(['related_to'=>$related_to])->first(); 

            // then through it we wikll fetch all related photos 
            $photos = $about->photos ; 




            return view('backend.about.about-photos' , compact('photos')); 
            






    }



    public function destroyImage($id){


        $image = AboutPhotos::find($id); 

        $image_path = public_path().'/uploads/about/'.$image->name;

        File::delete($image_path); 


        $image->delete(); 



         // flashing a success message  
        session()->flash('success' , trans('about.about_image_deleted')); 

        return back(); 





    }



    /**
     * Mke Slugs Urls from title  -- 
     * help in seo performance 
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
     public function slugify($string , $separator = '-')
    {
         

            if (is_null($string)) {
                return "";
            }

            // Remove spaces from the beginning and from the end of the string
            $string = trim($string);

            // Lower case everything
            // using mb_strtolower() function is important for non-Latin UTF-8 string | more info: http://goo.gl/QL2tzK
            $string = mb_strtolower($string, "UTF-8");;

            // Make alphanumeric (removes all other characters)
            // this makes the string safe especially when used as a part of a URL
            // this keeps latin characters and arabic charactrs as well
            $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]#u/", "", $string);

            // Remove multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);

            // Convert whitespaces and underscore to the given separator
            $string = preg_replace("/[\s_]/", $separator, $string);

            return $string;

    }



}
