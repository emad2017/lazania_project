<?php

namespace App\Http\Controllers\Backend;

use App\SpecialEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Language;
use App\SpecialEventItem ;
use App\SpecialEventItemDescription;
use File ;
use Auth;


class SpeacialEventItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('index of event item');
        $items = SpecialEventItem::all();

        return view('backend.eventitem.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages = Language::where(['status'=>'1'])->get();
        $items = SpecialEvent::where(['status'=>'active'])->get();



        return view('backend.eventitem.create',compact('languages','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('file')){



            $images = $request->file('file');

            foreach ($images as $image) {



                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/speacialevent'), $image_rename) ;

                $pic = new SpecialEventItem();
                $pic->img1 = $image_rename ;
                $pic->addBy_id = Auth::user()->id ;
                $pic->event_id = $request->itemevent;
                $pic->event_id = $request->itemevent;
                $pic->price = $request->price;
                $pic->status = $request->status == 'on' ? 'active' : 'not_active';


                $pic->save() ;


            }

            $languages = Language::where('status','1')->get();
            foreach ($languages as $language) {



                $dessertDescription = new  SpecialEventItemDescription();

                $dessertDescription-> item_id = $pic->id ;
                $dessertDescription-> language_id = $language->id ;


                $dessertDescription-> name = $request->input("eventitem_name_$language->label");
                $dessertDescription-> description = $request->input("eventitem_description_$language->label");


                $dessertDescription->save();
            }


        }
        else {
            $pic = new SpecialEventItem();
            $pic->addBy_id = Auth::user()->id;
            $pic->event_id = $request->itemevent;
            $pic->price = $request->price;

            $pic->rate  = $request->rate;

            $pic->status = $request->status == 'on' ? 'active' : 'not_active';
            $pic->save();

            $languages = Language::where('status', '1')->get();

            foreach ($languages as $language) {


                $dessertDescription = new  SpecialEventItemDescription();

                $dessertDescription->item_id = $pic->id;
                $dessertDescription->language_id = $language->id;


                $dessertDescription->name = $request->input("eventitem_name_$language->label");
                $dessertDescription->description = $request->input("eventitem_description_$language->label");


                $dessertDescription->save();

            }
        }





        // flashing a success message
        session()->flash('success' , trans('speacialevent.eventitem_added'));

        // return to a specific view
        return redirect()->route('eventitem.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $languages = Language::where(['status'=>'1'])->get();

        $eventitem= SpecialEventItem::find($id);
        $speacialdepartments = SpecialEvent::all();

        // dd($desseritem->description);



        return view('backend.eventitem.edit',compact('eventitem','languages','speacialdepartments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item =   SpecialEventItem::find($id) ;

        $item->status = $request->status == 'on' ? 'active' : 'not_active'  ;
        $item->event_id = $request->itemevent;
        $item->price = $request->price;

        $item->addby_id =  Auth::user()->id ;

        $item->save() ;

        /**
         * Check To See if there any attached images with this departement .
         */
        if($request->hasFile('file')){


            $images = $request->file('file');

            foreach ($images as $image) {



                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/speacialevent'), $image_rename) ;


                /**
                 * Saving  in in our seperated table
                 * we will fetch  by eloquent
                 */



                $item->img1 = $image_rename ;


                $item->save() ;


            }

        }


        $descriptions = $item->description ;


        foreach ($descriptions as $description) {


            $description->item_id = $item->id ;
            $description->language_id = $description->language_id ;



            $description->name = $request->input("eventitem_name_".$description->language->label);
            $description->description = $request->input("eventitem_desription_".$description->language->label);


            // saving department content !
            $description->save();
        }





        session()->flash('success' , trans('speacialevent.eventitem_updated'));

        // return to a specific view
        return redirect()->route('eventitem.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd('destroy item of event');

        $item = SpecialEventItem::find($id);
        $item->delete();


        // after deleting show a flash message

        session()->flash('success' , trans('speacialevent.eventitem_deleted'));


        return redirect()->route('eventitem.index');

    }
}
