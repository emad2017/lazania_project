<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image ; 
use File ; 
use Storage ; 
use DB ; 


class ImageHandlerController extends Controller
{
    
	/**
	 * process incoming image  
	 * written by me  : @emadRashad
	 * this super class is written by me  -_- Emad Rashad 
	 * i love programing and Bangoooooooooo ! 
	 * @param  [type] $image  [description]
	 * @param  [type] $folder [description]
	 * @return [type]         [description]
	 */
	public static function handle($image , $folder=null){

		/**
		 * so what am doing here is to handle any image upload proecess in adyanamic way  
		 * putting images on related images sizes  
		 */

		/**
		 * folder names that will live inside our uploads / folder name  /  dires 
		 * in that dynamic way we controller where is the target folder we want  
		 * best rule in programing is that please avoid to repeat your self -_- 
		 * @var string
		 */
		$original = "uploads/".$folder."/original/" ;
		$size_1681_655 = "uploads/".$folder."/1681_655"."/" ;
		$size_671_594  = "uploads/".$folder."/671_594"."/" ;
		$size_640_640  = "uploads/".$folder."/640_640"."/" ;
		$size_610_383  = "uploads/".$folder."/610_383"."/" ;
		$size_300_400  = "uploads/".$folder."/300_400"."/" ;


		// get incoming file original extension 
		$extension  = $image->getClientOriginalExtension(); 
		// prefix image renamed file 
		$image_renamed = str_random().'_'.'lazania'.'.'.$extension; 

		// think about intervention now  -_- 
		// but i will not do any thing to the original photo -_- 	
		 
		$getRealPathImage = Image::make($image->getRealPath()); 

		// that way we keep the aspect ration -_- 
		// $size_1681_655_handler->resize(1681,655,function($r){
		// 	$r->aspectRatio(); 
		// })->save($size_1681_655.$image_renamed);
		

		// for size 1681 - 655 .
		$getRealPathImage->resize(1681,655)->save(public_path().'/'.$size_1681_655.$image_renamed);
		// for size 640 - 640 . 
		$getRealPathImage->resize(671,594)->save(public_path().'/'.$size_671_594.$image_renamed);


		$getRealPathImage->resize(640,640)->save(public_path().'/'.$size_640_640.$image_renamed);
		// for size 610 - 383 . 	 
		$getRealPathImage->resize(610,383)->save(public_path().'/'.$size_610_383.$image_renamed);
		// for size 300 - 400 . 
		$getRealPathImage->resize(300,400)->save(public_path().'/'.$size_300_400.$image_renamed);

		$image->move(public_path().'/'.$original , $image_renamed);


		// then return the image renamed : 
		 
		return $image_renamed ; 

	}


}
