<?php

namespace App\Http\Controllers\Backend;

use App\DessertDepartments;
use App\DessertDepartmentDescription;
use  App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use LaravelLocalization ; 


use App\DesertSubDepartment ;
use App\DesertSubDepartmentDescription ;

// desert item it self
// this would be listed under sub department
use App\DessertItem ;
use App\DessertItemDescription ;

use App\Http\Requests\StoreNewSubDepartmentRequest ;

use App\Http\Controllers\Backend\ImageHandlerController ; 


use Auth;
use File ;

class DessertDepartmentController extends Controller
{


    public $language ;



    public function __construct(Language $language)
    {

       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->first();

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $departments = DessertDepartments::get();
        return view('backend.dessertdepartment.index' , compact('departments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::where(['status'=>'1'])->get();

        return view('backend.dessertdepartment.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $newDepartment = new DessertDepartments();
        $newDepartment->addBy_id = Auth::user()->id ;
 
        
        if($request->hasFile('icon')){

            $icon = $request->file('icon'); 

              $extension = $icon->getClientOriginalExtension();
              $icon_rename = str_random(6). '.' .$extension ;
              $icon->move(public_path('uploads/dessertdepartment/icons'), $icon_rename) ;

              $newDepartment->icon = $icon_rename ; 


        }

        if($request->hasFile('file')){

            $image = $request->file('file') ; 
            $image_name =  ImageHandlerController::handle($image,'dessertdepartment'); 
            $newDepartment->img1 = $image_name ;

        }





        $newDepartment->save() ;

             $languages = Language::where('status', '1')->get();

            foreach ($languages as $language) {

                $departmentDescription = new  DessertDepartmentDescription();
                $departmentDescription->department_id = $newDepartment->id;
                $departmentDescription->language_id = $language->id;
                $departmentDescription->name = $request->input("name_$language->label");
                $departmentDescription->description = $request->input("description_$language->label");
                $departmentDescription->save();

            }




        // flashing a success message
        session()->flash('success' , trans('dessertdepartment.dessert_department_added'));

        // return to a specific view
        return redirect()->route('dessertdepartment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $languages = Language::where(['status'=>'1'])->get();

        $departments = DessertDepartments::find($id);

        //dd($departments->description);



        return view('backend.dessertdepartment.edit',compact('departments','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $department =   DessertDepartments::find($id) ;

        $department->status = $request->status == 'on' ? 'active' : 'not_active'  ;

        $department->addBy_id =  Auth::user()->id ;


       if($request->hasFile('icon')){

              // delete old image icon 
              $old_icon  = $department->icon ; 
              $wholePath = public_path()."/uploads/dessertdepartment/icons/".$old_icon;
              File::delete($wholePath);


              $icon = $request->file('icon');
              $extension = $icon->getClientOriginalExtension();
              $icon_rename = str_random(6). '.' .$extension ;
              $icon->move(public_path('uploads/dessertdepartment/icons/'), $icon_rename) ;

              $department->icon = $icon_rename ; 



       }


       /**
        * Check To See if there any attached images with this departement .
       */
        if($request->hasFile('file')){


            // delete old image icon 
            $old_image  = $department->img1 ; 
            $path_original = public_path()."/uploads/dessertdepartment/original/".$old_image;
            $path_1681_655 = public_path()."/uploads/dessertdepartment/1681_655/".$old_image;
            $path_300_400 = public_path()."/uploads/dessertdepartment/300_400/".$old_image;
            $path_610_383 = public_path()."/uploads/dessertdepartment/610_383/".$old_image;
            $path_640_640 = public_path()."/uploads/dessertdepartment/640_640/".$old_image;
            $path_671_594 = public_path()."/uploads/dessertdepartment/671_594/".$old_image;
            
            File::delete($path_original);
            File::delete($path_1681_655);
            File::delete($path_300_400);
            File::delete($path_610_383);
            File::delete($path_640_640);
            File::delete($path_671_594);


            $image = $request->file('file') ; 
            $image_name =  ImageHandlerController::handle($image,'dessertdepartment'); 
            $department->img1 = $image_name ;

        }

        // then save  
        $department->save() ;


        // then update the description  
        
        foreach ($department->description as $description) {
          
            $description->department_id = $department->id ; 
            $description->language_id = $description->language->id ; 

            $description->name =  $request->input("dessert_department_name_".$description->language->label);
            $description->description = $request->input("dessert_department_description_".$description->language->label);

            $description->save(); 


        }

 

        session()->flash('success' , trans('dessertdepartment.dessert_department_updated'));

        // return to a specific view
        return redirect()->route('dessertdepartment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // dd('dsd');
        $department = DessertDepartments::find($id) ;

        $department->delete() ;

        // after deleting show a flash message

        session()->flash('success' , trans('dessertdepartment.dessert_department_updated'));


        return redirect()->route('dessertdepartment.index');
    }



    /**
     * Get a list of sub departments
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function getSubDepts(Request $request , $id){
            $mainDepartment  =  DessertDepartments::find($id) ;
            $childrens_cats = $mainDepartment->subDepts ;


            $childrens = [] ; 


            foreach($childrens_cats as $sub_category){



                  foreach($sub_category->description as $description){


                      if($this->language->id == $description->language_id){

                          $childrens [] = $description ; 

                      }

                  }


            }


            return view('backend.dessertdepartment.sub_departments_list' , compact('childrens')) ;
    }


    public function addNewSub($id){

         $parent = $id ;
         $languages = Language::where(['status'=>'1'])->get();
        return view('backend.dessertdepartment.add_sub_department' , compact( 'languages' ,'parent')) ;


    }


    public function postNewSub(StoreNewSubDepartmentRequest $request ){



            // then we need to write our logic here  :

            // store the new sub department

            $newSub = new  DesertSubDepartment ;

            $newSub->parent = $request->main_dept_id ;
            $newSub->status = $request->status == 'on' ? 'active' : 'not_active' ;
            $newSub->addby_id = Auth::user()->id  ;


            // file upload  :
            if($request->hasFile('file')){

                $image = $request->file('file') ; 
                $image_name =  ImageHandlerController::handle($image,'sub_departments'); 

                $newSub->image = $image_name ;


            }


            // save it mother fucker
            $newSub->save() ;


            // then insert the double edged content  ar - en  -_-

            $languages = Language::where('status','1')->get();
            foreach ($languages as $language) {

                $subDeptDescription = new  DesertSubDepartmentDescription();
                $subDeptDescription->subDeptId = $newSub->id ;
                $subDeptDescription->language_id = $language->id ;


                $subDeptDescription->name = $request->input("sub_name_".$language->label);
                $subDeptDescription->description = strip_tags($request->input("sub_description_".$language->label));


                $subDeptDescription->save();



            }




            // flashing a success message
            session()->flash('success' , trans('dessertdepartment.sub_dept_added'));

            // return to a specific view
            return redirect()->route('sub_departments' , $request->main_dept_id);




    }



    public function editSubDepartment($id){

        // get sub department
        $subDepartment = DesertSubDepartment::find($id) ;




        $languages = Language::where(['status'=>'1'])->get();

        // return the mother fucking view
        return view('backend.dessertdepartment.edit_sub_department' , compact('subDepartment' , 'languages'));

    }


    public function updateSubDepartment(StoreNewSubDepartmentRequest $request , $id){



            // validation logic is going through our custom form request class  see u there  -_-

            $subDepartment = DesertSubDepartment::find($id) ;

            $subDepartment->status = $request->status == 'on' ? 'active' : 'not_active' ;

            // file upload  :
            if($request->hasFile('file')){

                // delete old image icon 
                $old_image  = $subDepartment->image ; 
                $path_original = public_path()."/uploads/sub_departments/original/".$old_image;
                $path_1681_655 = public_path()."/uploads/sub_departments/1681_655/".$old_image;
                $path_300_400 = public_path()."/uploads/sub_departments/300_400/".$old_image;
                $path_610_383 = public_path()."/uploads/sub_departments/610_383/".$old_image;
                $path_640_640 = public_path()."/uploads/sub_departments/640_640/".$old_image;
                $path_671_594 = public_path()."/uploads/sub_departments/671_594/".$old_image;
                
                File::delete($path_original);
                File::delete($path_1681_655);
                File::delete($path_300_400);
                File::delete($path_610_383);
                File::delete($path_640_640);
                File::delete($path_671_594);


                $image = $request->file('file') ; 
                $image_name =  ImageHandlerController::handle($image,'sub_departments'); 
                $subDepartment->image = $image_name ;   


            }


            // save it mother fucker
            $subDepartment->save() ;


            $descriptions = $subDepartment->description ;


            foreach($descriptions as $description) {


                $description->subDeptId = $subDepartment->id ;
                $description->language_id = $description->language->id ;

                $description->name = $request->input("sub_name_".$description->language->label);
                $description->description = strip_tags($request->input("sub_description_".$description->language->label));

                $description->save();



            }

              // flashing a success message
            session()->flash('success' , trans('dessertdepartment.sub_dept_updated'));

            // return to a specific view
            return redirect()->route('sub_departments' , $subDepartment->parent);





    }


    public function deleteSubDepartment($id){

        $subDepartment = DesertSubDepartment::find($id) ;

        $subDepartment->delete();


       // flashing a success message
        session()->flash('success' , trans('dessertdepartment.sub_dept_deleted'));

        return back();


    }



    public function getDesertItemList(Request $request , $id){

        // id now represents the sub department id
        $items = DessertItem::where('subDeptId' , $id)->get();



        return view('backend.dessertitem.index' , compact('items' , 'id'));

    }




    public function  addNewItem($id){

      $languages = Language::where(['status'=>'1'])->get();


      return view('backend.dessertitem.create',compact('languages','id'));


    }



    public function postNewItem(Request $request){





      $pic = new DessertItem();

      $pic->addBy_id = Auth::user()->id ;
      $pic->subDeptId = $request->sub_cat_id ;

      $pic->show_in_slider = $request->show_in_slider == 'on' ? 1 : 0 ; 
      $pic->best_selling = $request->best_selling == 'on' ? 1 : 0 ; 
      $pic->quantity = $request->quantity ;  
      $pic->recommended = $request->recommended == 'on' ? 1 : 0 ; 

      $pic->price = $request->price;
      $pic->rate = $request->rate;



      if($request->hasFile('file')){

              $image = $request->file('file') ; 
              $image_name =  ImageHandlerController::handle($image,'dessertsitem'); 
              $pic->img1 = $image_name ;

      }


      $pic->save() ;


      $languages = Language::where('status','1')->get();
      foreach ($languages as $language) {



          $dessertDescription = new  DessertItemDescription();

          $dessertDescription->item_id = $pic->id ;
          $dessertDescription->language_id = $language->id ;


          $dessertDescription->name = $request->input("name_$language->label");
          $dessertDescription->description = strip_tags($request->input("description_$language->label"));


          $dessertDescription->save();
      }





      // flashing a success message
      session()->flash('success' , trans('dessertdepartment.dessert_dessert_added'));

      // return to a specific view
      return redirect()->route('items' ,  $pic->subDeptId );

    }






    public function editItem($id){

        $languages = Language::where(['status'=>'1'])->get();

        $item = DessertItem::find($id);

        $subDepts = DesertSubDepartment::where(['status'=>'active'])->get();



        return view('backend.dessertitem.edit',compact('item','languages','subDepts'));

    }



    public function updateItem(Request $request , $id){


      $dessertitem =   DessertItem::find($id) ;

      $dessertitem->status = $request->status == 'on' ? 'active' : 'not_active'  ;


      $dessertitem->addby_id  =  Auth::user()->id ;
      // this is sub cat id
      $dessertitem->subDeptId  = $request->dessertitem ;
      // show in slider
      $dessertitem->show_in_slider = $request->show_in_slider == 'on' ? 1 : 0 ; 
      // best_selling
      $dessertitem->best_selling = $request->best_selling == 'on' ? 1 : 0 ; 
      // promoted
      // $dessertitem->promoted = $request->promoted == 'on' ? 1 : 0 ; 
      // recommended
      $dessertitem->recommended = $request->recommended == 'on' ? 1 : 0 ; 
      // price
      $dessertitem->price = $request->price;

      $dessertitem->quantity = $request->quantity ;  
     

      /**
       * Check To See if there any attached images with this departement .
       */
      if($request->hasFile('file')){
          

                // delete old image icon 
                $old_image  = $dessertitem->img1 ; 
                $path_original = public_path()."/uploads/dessertsitem/original/".$old_image;
                $path_1681_655 = public_path()."/uploads/dessertsitem/1681_655/".$old_image;
                $path_300_400 = public_path()."/uploads/dessertsitem/300_400/".$old_image;
                $path_610_383 = public_path()."/uploads/dessertsitem/610_383/".$old_image;
                $path_640_640 = public_path()."/uploads/dessertsitem/640_640/".$old_image;
                $path_671_594 = public_path()."/uploads/dessertsitem/671_594/".$old_image;
                
                File::delete($path_original);
                File::delete($path_1681_655);
                File::delete($path_300_400);
                File::delete($path_610_383);
                File::delete($path_640_640);
                File::delete($path_671_594);


                $image = $request->file('file') ; 
                $image_name =  ImageHandlerController::handle($image,'dessertsitem'); 
                $dessertitem->img1 = $image_name ;   

      }

      // saving item
      $dessertitem->save() ;

      $descriptions = $dessertitem->description ;


      foreach ($descriptions as $description) {


          $description->item_id = $dessertitem->id ;
          $description->language_id = $description->language_id ;


          $description->name = $request->input("dessert_name_".$description->language->label);
          $description->description =  strip_tags($request->input("dessert_description_".$description->language->label));


          // saving department content !
          $description->save();
      }





      session()->flash('success' , trans('dessertdepartment.dessert_update_msg'));



      // return to a specific view
      return redirect()->route('items' , $dessertitem->subDeptId);



    }




    public function deleteItem($id){


        $item = DessertItem::find($id) ;

        $item->delete();

        session()->flash('success' , trans('dessertdepartment.item_deleted'));

        // return to a specific view
        return back() ;


    }


}
