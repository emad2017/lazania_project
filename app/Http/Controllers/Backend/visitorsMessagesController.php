<?php

namespace App\Http\Controllers\Backend;

use App\ContactUs;
use App\ContactWithUS;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use DB;

class visitorsMessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $visitorsMessages = DB::table('contact_with_u_s')->get();


        //dd($visitorsMessages);
        return view('backend.visitorsMessages.index',compact('visitorsMessages'));
    }

    /**
     * Show the form for Reply a new message.
     *
     * @return \Illuminate\Http\Response
     */
    public function showReply($id)
    {



    }

    /**
     * Reply the message via email.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::table('contact_with_u_s')
        ->where(['id'=> $id])
        ->delete();

        session()->flash('success' , trans('visitorsMessages.message_deleted'));


        return redirect()->route('visitorsMessages.index');
    }
}
