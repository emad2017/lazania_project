<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SpecialEvent ;
use App\SpecialEventDescription;
use App\Language;

use Storage ;
use File ;
use Auth;

use LaravelLocalization ; 



class SpecialEventController extends Controller
{


     public $language ;



    public function __construct(Language $language)
    {

       $default_locale =  LaravelLocalization::getCurrentLocale() ;
       $this->language =  $language = Language::where(['label'=>$default_locale])->first();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd('Special event index');
        $events_all = SpecialEvent::all();

        $events = [] ; 

        foreach ($events_all as $event) {
            
                foreach ($event->description as $description) {
                    
                        if($description->language_id == $this->language->id){

                                $events [] = $description ; 
                        }
                }
        }


  


        return view('backend.speacialevent.index' , compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // dd('create of speaical event');

        $languages = Language::where(['status'=>'1'])->get();

        return view('backend.speacialevent.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pic = new SpecialEvent();

        $pic->addBy_id = Auth::user()->id ;
        $pic->status = $request->status == 'on' ? 'active':'not_active' ; 

    
        if($request->hasFile('file')){

                $image = $request->file('file');
                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/speacialevent'), $image_rename) ;
                $pic->img = $image_rename ;
               
 

        }
         $pic->save() ;



            $languages = Language::where('status', '1')->get();

            foreach ($languages as $language) {

                $dessertDescription = new  SpecialEventDescription();
                $dessertDescription->event_id = $pic->id;
                $dessertDescription->language_id = $language->id;
                $dessertDescription->name = $request->input("name_$language->label");
                $dessertDescription->description = strip_tags($request->input("description_$language->label"));
                $dessertDescription->save();

            }
        




        // flashing a success message
        session()->flash('success' , trans('dessertdepartment.dessert_dessert_added'));

        // return to a specific view
        return redirect()->route('speacialevent.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // dd('edit of speacial eents');

        $languages = Language::where(['status'=>'1'])->get();

        $speacialevent = SpecialEvent::find($id);


        // dd($desseritem->description);



        return view('backend.speacialevent.edit',compact('speacialevent','languages'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd('update data of speacial event');
        $speacialevent =   SpecialEvent::find($id) ;

        $speacialevent->status = $request->status == 'on' ? 'active' : 'not_active'  ;

        $speacialevent->addby_id =  Auth::user()->id ;

        $speacialevent->save() ;

        /**
         * Check To See if there any attached images with this departement .
         */
        if($request->hasFile('file')){


            $images = $request->file('file');

            foreach ($images as $image) {



                $extension = $image->getClientOriginalExtension();
                $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();
                $image->move(public_path('uploads/speacialevent'), $image_rename) ;


                /**
                 * Saving  in in our seperated table
                 * we will fetch  by eloquent
                 */



                $speacialevent->img = $image_rename ;


                $speacialevent->save() ;


            }

        }


        $descriptions = $speacialevent->description ;


        foreach ($descriptions as $description) {


            $description->event_id = $speacialevent->id ;
            $description->language_id = $description->language_id ;


            $description->name = $request->input("speacailevent_name_".$description->language->label);
            $description->description = strip_tags($request->input("speacailevent_desription_".$description->language->label));


            // saving department content !
            $description->save();
        }





        session()->flash('success' , trans('dessertdepartment.dessert_update_msg'));

        // return to a specific view
        return redirect()->route('speacialevent.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //  dd('destory of speacial event');
        $speacialevent = SpecialEvent::find($id) ;

        $speacialevent->delete();



        // after deleting show a flash message

        session()->flash('success' , trans('dessertdepartment.dessert_delete'));


        return redirect()->route('speacialevent.index');
    }
}
