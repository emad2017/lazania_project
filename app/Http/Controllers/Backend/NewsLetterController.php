<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\NewsLetter ;


use App\Mail\SendSingleNewsletterEmail ;
use App\Mail\SendBulckNewsletterEmail ;

use App\Requests\SendNewsletterEmailRequest ;

use Illuminate\Support\Facades\Mail;


class NewsLetterController extends Controller
{
    public function index(){

    	// will fetch all subscribed users
    	// send them alonside with our view
    	// then show him option to send single email or bulck

    	$subscriptions = NewsLetter::all();
    	return view('backend.newsletter.index' , compact('subscriptions'));

    }


    public function sendEmail($id){

    	$subscriper = NewsLetter::find($id) ;

    	return view('backend.newsletter.send_email' , compact('subscriper'));

    }


    public function postSingleEmail(SendNewsletterEmailRequest $request , $id){

      // wanna validate the request

    	$subscriper = NewsLetter::find($id) ;

    	Mail::to($subscriper->email)->send(new SendSingleNewsletterEmail($request->subject , strip_tags($request->message)));


    	 session()->flash('success' , trans('newsletter.email_sent'));

        // return to a specific view
        return redirect()->route('newsletter');


    }


    public function sendBulckEmail(){

    	// i should get a bulck of subscriptions and send it alongside with our view

    	return view('backend.newsletter.send_bulk');
    }

    public function postBulck(Request $request){



    	$message = $request->msg ;
    	$subject = $request->subject ;

    	$emails = [];
    	$subscriptions = NewsLetter::all();

    	foreach ($subscriptions as $subscription) {

    			$emails [] = $subscription->email ;

    	}

    	foreach ($emails as $email) {
    	 		Mail::to($email)->send(new SendBulckNewsletterEmail($request->subject , strip_tags($request->message)));
    	 }

    	session()->flash('success' , trans('newsletter.bulck_sent'));

        // return to a specific view
        return redirect()->route('newsletter');




    }
}
