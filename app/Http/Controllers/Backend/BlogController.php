<?php

namespace App\Http\Controllers\Backend;

use App\BlogDepartment;
use App\DessertDepartments;
use App\Blog;
use App\BlogDescription;
use  App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use File ;

class BlogController extends Controller
{
     
    public function careers(){

        // get authenticated  
        $user = Auth::user(); 



        $career = Blog::updateOrCreate([
             
            'addby_id'=>$user->id 
             
        ]);

        $languages = Language::where(['status'=>'1'])->get(); 

        foreach($languages as $language){

            $career_description = BlogDescription::updateOrCreate([

                'blog_id'=>$career->id , 
                'language_id'=>$language->id


            ]);

            
        }
        

        return view('backend.blog.create' , compact('career' , 'languages')) ; 

    }


    public function careersPost(Request $request  , $id){

            $career = Blog::find($id) ; 

            $descriptions = $career->description ; 

            foreach ($descriptions as $description) {
                
                $description->blog_id = $career->id ; 
                $description->language_id = $description->language_id ;

                $description->name = $request->input("name_blog_".$description->language->label);  
                $description->description = $request->input("description_blog_".$description->language->label);


                $description->save(); 
            }



             session()->flash('career_update' , trans('backend.careers_updated')); 

             return back(); 

    }


}
