<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Slider  ; 
use App\Language  ; 
use LaravelLocalization  ; 


use Auth ; 

use File  ; 
use Image  ;

use Session ; 



class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::where(['status'=>'active'])->get(); 

        return view('backend.slider.index' , compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        $rules = [

            'title'=>'required' , 
            'file'=>'required'

        ];


        $messages  = [

            'title.required'=>trans('backend.title_required') , 
            'file.required'=>trans('backend.file_required') , 

        ]; 


        $this->validate($request , $rules , $messages) ; 




        // then after validate  -- store  
        
        $slide  = new Slider ; 


        $slide->title = $request->title ; 

        $image  = $request->file('file'); 


        $extension = $image->getClientOriginalExtension();
        $image_renamed = time() . '.' .$image->getClientOriginalExtension();



        $image->move(public_path('uploads/slider'), $image_renamed) ;


        
        $slide->image = $image_renamed ; 


        $slide->status = $request->status == 'on' ? 'active':'not_active' ; 

        $slide->save(); 

        Session::flash('success' , trans('backend.slide_added')); 



        return redirect()->route('slider.index');





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $slide = Slider::find($id) ; 
        return view('backend.slider.edit' , compact('slide')); 
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $rules = [ 'title'=>'required' ];
        $messages = [ 'title.required'=>trans('backend.title_required') ];
        $this->validate($request , $rules , $messages ); 




        // then find the fucken 
        $slide = Slider::find($id); 


        $slide->title = $request->title ; 

        if($request->hasFile('file')){


            $old_image  = $slide->image ; 
            $wholePath = public_path()."/uploads/slider/".$old_image;
            File::delete($wholePath); 

            $image  = $request->file('file'); 
            $extension = $image->getClientOriginalExtension();
            $image_renamed = time() . '.' .$image->getClientOriginalExtension();
            $image->move(public_path('uploads/slider'), $image_renamed) ;
            $slide->image = $image_renamed ;   

 

        }


        $slide->status = $request->status == 'on' ? 'active':'not_active' ; 

        $slide->save(); 


         Session::flash('success' , trans('backend.slide_updated')); 


         return redirect()->route('slider.index');





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request ,  $id)
    {
        

            $slide = Slider::find($id) ; 


            $old_image  = $slide->image ; 
            $wholePath = public_path()."/uploads/slider/".$old_image;
            File::delete($wholePath); 


            $slide->delete(); 



            Session::flash('success' , trans('backend.slide_deleted')); 



            return redirect()->route('slider.index');





    }
}
