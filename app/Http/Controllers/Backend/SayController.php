<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Say;
use App\SayDescription;
use App\Language;
use Validator;

use Auth ;
use File ;





class SayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $saies = Say::get();
        return view('backend.say.index',compact('saies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $languages = Language::where(['status'=>'1'])->get();
        return view('backend.say.create',compact('languages'));   
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $rules = array(
             // 'medical_supply_name_ar' => 'required|min:3|max:255',
             // 'medical_supply_name_en' => 'required|min:3|max:255',

        );


        // //validate
        $validation = Validator::make($request->all(), $rules);

        if ($validation->passes()) {

            $saies=new Say();
            $saies->status= $request->status == 'on' ? 'active' : 'not_active'  ;
                             if($request->hasFile('image')){


                // in update we will check first if user has an image or not to be replaced 


                // dd($request->file('logo'));
                $logo = $request->file('image'); 
                $extension = $logo->getClientOriginalExtension();
                $logo_rename = time() . '.' .$logo->getClientOriginalExtension(); 



                $logo->move(public_path('uploads/say'), $logo_rename) ; 


                // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));  
                $saies->image = $logo_rename ; 

        } 

            $saies->save();
         $languages = Language::where('status','1')->get();

            foreach($languages as $language)
            {


//dd($request->input("medical_supply_description_$language->label"));

                  $saydescription=new SayDescription();

                  $saydescription->sayAbout_id=$saies->id;

                  $saydescription->language_id = $language->id ;
                  $saydescription->name=$request->input("say_name_$language->label");
                  $saydescription->descriptions=strip_tags($request->input("say_description_$language->label"));

                 $saydescription->save();
            }
                  //dd($medicalSuppliesDescription);


            session()->flash('success' , trans('say.say_message_added'));
            return redirect()->route('say.index');


        }else{

            $errors=$validation->errors()->all();

            return redirect()->route('say.create',compact('errors'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $say = Say::find($id) ; 
        $languages = Language::where(['status'=>'1'])->get(); 


        return view('backend.say.edit' , compact('say', 'languages' )); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $say=Say::find($id);


        //validations rules
        $rules = array(
             // 'medical_supply_name_ar' => 'required|min:3|max:255',
             // 'medical_supply_name_en' => 'required|min:3|max:255',
             
             // 'medical_supply_description_ar' => 'required|min:3|max:255',
             // 'medical_supply_description_en' => 'required|min:3|max:255',


        );


        //validate
        $validation = Validator::make($request->all(), $rules);

        if ($validation->passes()) {

            $say->status=$request->status =='on'? 'active':'not_active';
            if($request->hasFile('image')){


                // in update we will check first if user has an image or not to be replaced 


                // dd($request->file('logo'));
                $logo = $request->file('image'); 
                $extension = $logo->getClientOriginalExtension();
                $logo_rename = time() . '.' .$logo->getClientOriginalExtension(); 



                $logo->move(public_path('uploads/say'), $logo_rename) ; 


                // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));  
                $say->image = $logo_rename ; 

        } 
            $say->save();




            $descriptions = $say->description ; 

            // $slug =  new Slug ; 
            foreach ($descriptions as $description) {
                
                
                $description->sayAbout_id = $id ; 
                $description->language_id = $description->language_id ;
                $description->name = $request->input("say_name_" .$description->language->label);
                $description->descriptions= strip_tags($request->input("say_description_".$description->language->label));
                  
                // dd($description->medicalSuppliesDescription );

                    $description->save();  
                 
                

            }

            session()->flash('success' , trans('say.say_updated'));
            return redirect()->route('say.index');


        }else{
            $errors=$validation->errors()->all();
            return redirect()->route('say.create',compact('errors'));

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Say::destroy($id);

        session()->flash('success' , trans('say.say_message_deleted'));
            return redirect()->route('say.index'); 
               }
}
