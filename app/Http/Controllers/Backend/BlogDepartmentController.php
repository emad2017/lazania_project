<?php
namespace App\Http\Controllers\Backend;

use App\BlogDepartment;
use App\BlogDepartmentDescription;
use App\Blog;
use App\BlogDescription;
use  App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use File ;

class BlogDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd('Blog department page');
        /*
         * Return  data of blog departments
         *
         */
        $blogdepartment = BlogDepartment::all();
        // dd($blogdepartment);
        return view('backend.blogdepartment.index',compact('blogdepartment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('We will bulid the futher');

        $languages = Language::where(['status'=>'1'])->get();

        return view('backend.blogdepartment.create',compact('languages'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('we will store data of futher');
        $department = new BlogDepartment();
        $department->addby_id = Auth::user()->id;
        $department->save();

        $languages = Language::where('status', '1')->get();

        foreach ($languages as $language) {


            $blogDescription = new  BlogDepartmentDescription();

            $blogDescription->department_id = $department->id;
            $blogDescription->language_id = $language->id;


            $blogDescription->name = $request->input("name_blog_d_$language->label");


            $blogDescription->save();
        }

        // flashing a success message
        session()->flash('success' , trans('dessertdepartment.updated_msg_d'));

        // return to a specific view
        return redirect()->route('blogdepartment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd('we will edit the wolrd');
        $languages = Language::where(['status'=>'1'])->get();

        $blog = BlogDepartment::find($id);

        // dd($blog->description);



        return view('backend.blogdepartment.edit',compact('blog','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $blog =   BlogDepartment::find($id) ;

        $blog->addby_id =  Auth::user()->id ;

        $blog->save() ;



        $descriptions = $blog->description ;



        foreach ($descriptions as $description) {


            $description->department_id = $blog->id ;
            $description->language_id = $description->language_id ;


            $description->name = $request->input("blog_".$description->language->label);

            // saving department content !
            $description->save();
        }





        session()->flash('success' , trans('dessertdepartment.update_msg_d'));

        // return to a specific view
        return redirect()->route('blogdepartment.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // dd('we will destroy world here !');
        $delete = BlogDepartment::find($id);
        $delete->delete();

        // flashing a success message
        session()->flash('success' , trans('dessertdepartment.delete_msg_d'));

        // return to a specific view
        return redirect()->route('blogdepartment.index');
    }
}
