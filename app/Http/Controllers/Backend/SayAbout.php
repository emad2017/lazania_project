<?php

namespace App\Http\Controllers\Backend;

use App\ContactUs;
use App\ContactWithUS;
use App\SayAboutUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use DB;

class SayAbout extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $says = SayAboutUs::all();

        return view('backend.sayabout.index',compact('says'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.sayabout.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $say  = new SayAboutUs();


        $say->name = $request->name ;
        $say->description = $request->description ;
        $say->img = $request->jobtitle ;

        $say->status = $request->status == 'on' ? 'active':'not_active' ;

        if($request->hasFile('logo')){


            // in update we will check first if user has an image or not to be replaced


            // dd($request->file('logo'));
            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension();
            $logo_rename = time() . '.' .$logo->getClientOriginalExtension();



            $logo->move(public_path('uploads/sayaboutus'), $logo_rename) ;


            // Storage::disk('local')->put("/users/".$logo_rename ,  File::get($logo));
            $say->img = $logo_rename ;

        }


        $say->save();



        session()->flash('success' , trans('dessertdepartment.create_msg'));

        // return to a specific view
        return redirect()->route('sayabout.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $say  =  SayAboutUs::find($id);
        $say->delete();
        // after deleting show a flash message

        session()->flash('success' , trans('dessertdepartment.delete_msg'));


        return redirect()->route('sayabout.index');
    }
}
