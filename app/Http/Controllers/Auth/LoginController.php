<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use LaravelLocalization ; 

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = "dashboard";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    /**

     * [authenticate description]

     * @param  Request $request [description]

     * @return [type]           [description]

     */

    public function authenticate(Request $request){
 

         $lang = LaravelLocalization::getCurrentLocale(); 


         $email = $request->email ; 
         $password = $request->password ;  

       

          if ( Auth::attempt(['email' => $email , 'password' => $password  ]) ){

                 Auth::user() ; 

                 return redirect()->route("dashboard"); 

          }else{


                return back()->withErrors(trans('lazena.error_login')); 
            
          }

            





    }



    public function logout(Request $request){


         
        Auth::logout(); 

        // regenrate session : 

        $request->session()->regenerate();

        // then redirect to home page  : 

        return redirect('/'); 



    }








}
