<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ResetsPasswords;

use App\CustomerPasswordReset ; 

class CustomerResetPasswordController extends Controller
{
     /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $guard  = "customer" ; 
    protected $broker = "customers"; 
   

    public function showResetForm(Request $request, $token = null){

      
        return view('frontend.customer_auth.reset_password_form')->with(
            ['token' => $token, 'email' => $request->email]); 
    }


    protected function guard()
	{
	    return Auth::guard('customer');
	}


    public function broker()
    {
        return Password::broker('customers');
    }
}
