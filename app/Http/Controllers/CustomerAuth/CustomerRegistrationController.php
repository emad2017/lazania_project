<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Customer ; 

use Auth ; 

class CustomerRegistrationController extends Controller
{
    
	 public function __construct(){
       
       // coming here must be only un authenticated customer 
       // whom intend to register an account  
       $this->middleware('guest:customer');
    
    }


    /**
     * Get registration form 
     * @return [type] [description]
     */
    public function getRegistrationForm(){

    	return view('frontend.customer_auth.register'); 

    }

    /**
     * Hanle incoming new create account request
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createAccount(Request $request){

    		/**
    		 * Custom messages array to avoid default laravel validation rules  
    		 * this in later would be replaced with translated labels  -_-
    		 * @var [type]
    		 */
    		$messages = [

    			'fname.required'=>'Please make sure to enter your first name' , 
    			'lname.required'=>'Please make sure to enter your last name' , 
    			'email.required'=>'Please make sure to enter your email address' , 
    			'email.email'=>'Please make sure that you entered a correct email usally email formats are like this ( example@email.com )' , 
    			'email.unique'=>'We found this email in our database records , we think you should pick another email address' , 
    			'city.required'=>'Could you please tell us the city you located in' , 
    			'country.required'=>'Please can you tell us the country of residence' , 
    			'street.required'=>'Street really helps us deliver our products quickly',
    			'telephone.required'=>'Telephone is important , please mention it' , 
    			'password.required'=>'Password is really important , it will be used to login to your account'


    		];

    		$this->validate($request , [

    			'fname'=>'required', 
    			'lname'=>'required', 
    			'email'=>'required|email|unique:e_customers,email', 
    			'city'=>'required', 
    			'country'=>'required', 
    			'street'=>'required', 
    			'telephone'=>'required', 
    			'password'=>'required' 

    		] , $messages);


    		// after validate we have to dump this new customer to our database  
    		$customer  = new Customer ; 
    		$customer->fname = $request->fname ; 
    		$customer->lname = $request->lname ; 
    		$customer->email = $request->email ; 
    		$customer->city = $request->city ; 
    		$customer->country = $request->country ; 
    		$customer->street = $request->street ; 
    		$customer->gender = $request->gender ; 
    		$customer->telephone = $request->telephone ; 
    		$customer->password = crypt($request->password , '')  ;


    		// save this lucky customer  
    		$customer->save() ;


    		// next logic here  -_- i must send this customer a new welcome email 
    		// telling him that registiration went in a successful way -_-  -- later b2a  
    		 

    		// then i need to authenticate this new customer with auto login  and nice welcome message also -_- 
    		
    		 Auth::guard('customer')->loginUsingId($customer->id) ; 

    		// flash a warming welcome message 
    		session()->flash('new_account' , ' you are now viewing your account dashboard - Registiration went successfully') ; 
    		
    		// then redirect him back to his profile  
    		return redirect()->route('customer_dashboard'); 
    }
}
