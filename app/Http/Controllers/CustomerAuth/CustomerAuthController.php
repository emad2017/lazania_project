<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Auth ; 
use Session ; 
use LaravelLocalization ; 

class CustomerAuthController extends Controller
{

 
    
	  public function __construct(){
      
      $this->middleware('guest:customer', ['except' => ['logout']]);
     
    }

    public function showLoginForm(Request $request){

      $lang = LaravelLocalization::getCurrentLocale() ; 
      $path = $request->path() ; 

      if($path == "$lang/customer-login"){

          if($request->session()->has('after_authentication')){

              $request->session()->forget('after_authentication'); 

              $request->session()->flash('after_authentication' , ''); 

          }elseif ($request->session()->has('incoming_wishlist')) {
              
          

              $request->session()->forget('incoming_wishlist'); 

              $request->session()->flash('incoming_wishlist' , ''); 


          }
      }

    	return view('frontend.customer_auth.login') ; 
    }


    public function postLogin(Request $request){


    	$this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        if (auth()->guard('customer')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
           $customer = auth()->guard('customer')->user();
          
           // check RedirectIfNotCustomer Middleware -_- 
           if(Session::has('after_authentication')){

              // do your redirect to custom shopping cart page  -_- 
              return redirect()->route('shopping_cart'); 


           }elseif(Session::has('incoming_wishlist')){

            
            
              return redirect()->route('addProductToWishlist' , Session::get('product_id'));  
            
           }else{

              // else here means that the after_authentication session key is removed -_- 
              return redirect()->route('customer_dashboard') ;

           }




        }else{
            return redirect()->route('customer_login')->withErrors( 'Please make sure you entered a correct credentials cause we found wrong email or password');
        }

    }


    /**
     * Log out an authenticated user  -_- 
     * @return [type] [description]
     */
    public function logout(){

        // log out Auth Customer  
        Auth::guard('customer')->logout(); 
 
        
        

        // redirect
        return redirect()->route('customer_login'); 

    }
}
