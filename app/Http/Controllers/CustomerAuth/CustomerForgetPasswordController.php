<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

//Password Broker Facade
use Illuminate\Support\Facades\Password ; 

class CustomerForgetPasswordController extends Controller
{
     

	/*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
   
   use SendsPasswordResetEmails; 
   
    protected $guard  = "customer" ; 
    protected $broker = "customers"; 
 


    public function showLinkRequestForm(){

    	return view('frontend.customer_auth.forget_password'); 
    }
 
    public function broker()
    {
         return Password::broker('customers');
    }







}
