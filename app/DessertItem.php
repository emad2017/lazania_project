<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DessertItem extends Model
{
    public function description(){


        return $this->hasMany('App\DessertItemDescription' , 'item_id');

    }
    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }
    public function department()
    {


        return $this->belongsTo('App\DessertDepartments', 'department_id');

    }

    /**
     * @return 
     * fetch the sub department regarding specific desert 
     */
    public function desertSubDept(){


        return $this->belongsTo('App\DesertSubDepartment' , 'subDeptId'); 
    }


    public function productSubDeptSearch(){
        return $this->belongsTo('App\DesertSubDepartment' , 'subDeptId')->select('id' , 'parent'); 
    }


}
