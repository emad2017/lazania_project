<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialEventItemDescription extends Model
{
    public function specialeventitem(){


        return $this->belongsTo('App\SpecialEventItem');

    }

    /**
     * Lnguage eloquent to fetch content based on language
     * if you asking of which it will help - then it makes it more easier when
     * using with tabs --- sothat everything will be dynamic .
     * @return [type] [description]
     */
    public function language(){

        return $this->belongsTo('App\Language' , 'language_id');
    }
}
