<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Say extends Model
{
    //
   	protected $fillable = ['id' , 'sayAbout_id'] ; 


    public function description()
    {
        return $this->hasMany('App\SayDescription','sayAbout_id');
    }
}
  