<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPasswordReset extends Model
{
    public $table = "customer_password_resets"; 
}
