<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesertSubDepartmentDescription extends Model
{
    
    public function subDept(){
    	return $this->belongsTo('App\DesertSubDepartment' , 'subDeptId'); 
    }


     public function language(){

        return $this->belongsTo('App\Language' , 'language_id');
    }



    
}
