<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DessertDepartments extends Model
{
    public function description(){


        return $this->hasMany('App\DessertDepartmentDescription' , 'department_id');

    }


    /**
     * @return [type]
     * retrieve all sub-departments related to main department
     */
    public function subDepts(){

        return $this->hasMany('App\DesertSubDepartment' , 'parent') ; 

    }
    

    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }

}
