<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogDepartment extends Model
{
    public function description(){


        return $this->hasMany('App\BlogDepartmentDescription' , 'department_id');

    }

    /**
     * Get user who made this department
     * @return [type] [description]
     */
    public function user(){


        return $this->belongsTo('App\User' , 'addby_id');


    }
}
