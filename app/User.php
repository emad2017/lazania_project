<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function PharmaceuticalOrder(){

        return $this->hasMany('App\PharmaceuticalOrder' , 'pharmaceuticalCompany_id');

    }

    public function medicalSuppliesOrder(){

        return $this->hasMany('App\MedicalSupplies' , 'medical_supplies_company_id');

    }

    public function sender()
    {
        return $this->belongsTo('App\Message','sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Message','receiver_id');
    }




    public function social(){


        return $this->hasMany('App\SocialMedia' , 'related_to') ; 


    }



}
