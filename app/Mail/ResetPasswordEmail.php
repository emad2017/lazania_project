<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail ; 

class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $token  ;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token ; 

        Mail::send('frontend.customer_auth.emails.reset_password_email' , ['token'=>$this->token , 'message'=>'new email'] , function($message){

            
            
        } ) ; 
    }

    // /**
    //  * Build the message.
    //  *
    //  * @return $this
    //  */
    // public function build()
    // {
    //     return $this->view('frontend.customer_auth.emails.reset_password_email')
    //                 ->with([
    //                     'subject'=>'Password Reset Email'   , 
    //                     'msg'=>'You are receiving this email because we received a password reset request for your account.' , 
    //                     'token'=>$this->token
    //                 ]);
    // }



}
