<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth ; 

class OrderPlacedForAdmin extends Mailable
{
    use Queueable, SerializesModels;

     public $order ; 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order ; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    

    public function build()
    {
        $customer_email = Auth::guard('customer')->user()->email ; 
        return  $this->from($customer_email)->view('frontend.customer_auth.emails.order_invoice_admin');
    }
}
