<ul>
    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                {{ $properties['native'] }}
            </a>
        </li>
    @endforeach
</ul>


@php $lang = LaravelLocalization::getCurrentLocale() ;  @endphp 

<a href="{{ url("$lang/login") }}">Login</a>
<a href="{{ url("$lang/register")  }}">Register</a>