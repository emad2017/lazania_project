@extends('frontend.layout')


@section('page_title' , trans('lazena.create_account'))


@section('styles')
  
  <style>
  	
  		.form_error{
			
			display: block;
		    margin-top: -25px;
		    margin-bottom: 10px;
		    color: #d22e2e;

  		}

  		.form_error small{
  			word-break: break-word ; 
  		}

  </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


        


  @endphp


@section('pages_content')

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
               
              <li>{{ trans('lazena.create_account') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

        <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
           

           

          <!-- content account -->
          <div class="col-md-8 col-md-offset-2">
            <div class="content-account">
              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.create_account') }}</h3>
              </div>

              <div class="col-md-12">
                <div class="row">

                  {!! Form::open(['url'=>"$lang/customer-register" , 'method'=>'POST' , 'class'=>'account-information box-shadow-3']) !!}
                  {{-- <form class="account-information box-shadow-3" action="index.html" method="post"> --}}
                    <!-- box title -->
                    <div class="col-sm-12 mb-30 box-title">
                      {{-- <h5>Account Information</h5> --}}
                    </div>
                    <!-- box title -->

                    <div class="col-sm-6">
                      <label for="fname">{{ trans('lazena.fname') }}</label>
                      <input type="text" value="{{ old('fname') }}" name="fname" id="fname">

                      	@if($errors->has("fname"))
                        	<span class="form_error"><small>{{ $errors->first("fname") }}</small></span>
                    	@endif
                    </div>

                    <div class="col-sm-6">
                      <label for="lname">{{ trans('lazena.lname') }}</label>
                      <input type="text" value="{{ old('lname') }}" name="lname" id="lname">

                      	@if($errors->has("lname"))
                        	<span class="form_error"><small>{{ $errors->first("lname") }}</small></span>
                    	@endif
                    </div>

                    <div class="col-sm-12">
                      <label for="email">{{ trans('lazena.email') }}</label>
                      <input type="text" value="{{ old('email') }}" name="email" id="email">

                      	@if($errors->has("email"))
                        	<span class="form_error"><small>{{ $errors->first("email") }}</small></span>
                    	@endif

                    </div>


                    <div class="col-sm-6">
                      <label for="city">{{ trans('lazena.city') }}</label>
                      <input type="text" value="{{ old('city') }}" name="city" id="city">

                      	@if($errors->has("city"))
                        	<span class="form_error"><small>{{ $errors->first("city") }}</small></span>
                    	@endif

                    </div>

                    <div class="col-sm-6">
                      <label for="country">{{ trans('lazena.country') }}</label>
                      <input type="text" value="{{ old('country') }}" name="country" id="country">

                      	@if($errors->has("country"))
                        	<span class="form_error"><small>{{ $errors->first("country") }}</small></span>
                    	@endif
                    </div>

					<div class="col-sm-12">
                      <label for="street">{{ trans('lazena.street') }}</label>
                      <input type="text" value="{{ old('street') }}" name="street" id="street">

                      	@if($errors->has("street"))
                        	<span class="form_error"><small>{{ $errors->first("street") }}</small></span>
                    	@endif
                    </div>                    

                    <div class="col-sm-6 mb-30 gender">
                      <label for="name">{{ trans('lazena.gender') }}</label>
                      <div class="input-areaa">
                        <input class="radio" title="Male" value="male" name="gender" checked="checked" type="radio">{{ trans('lazena.male') }}
                        <input class="radio validate-one-required" title="Female" value="female" name="gender" type="radio">{{ trans('lazena.female') }}
                      </div>
                    </div>
					
					<div class="col-sm-6">
                      <label for="telephone">{{ trans('lazena.telephone') }}</label>
                      <input type="text" value="{{ old('telephone') }}" name="telephone" id="telephone">

                      	@if($errors->has("telephone"))
                        	<span class="form_error"><small>{{ $errors->first("telephone") }}</small></span>
                    	@endif

                    </div>


                    <div class="col-sm-12">
                      <label for="password">{{ trans('lazena.password') }}</label>
                      <input type="text" value="{{ old('password') }}" name="password" id="password">

                      	@if($errors->has("password"))
                        	<span class="form_error"><small>{{ $errors->first("password") }}</small></span>
                    	@endif
                    </div>

                   

                    <div class="col-sm-12 buttons-set">
                     {{--  <a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back Link</a> --}}
                      <button class="button button-hot hvr-shutter-out-horizontal pull-right">{{ trans('lazena.create_account') }}</button>

                    </div>
                  {{-- </form> --}}
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
          <!-- ./ content account -->

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->

@endsection  




@section('scripts')

  

@endsection  