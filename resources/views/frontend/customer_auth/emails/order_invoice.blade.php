<!doctype html>


  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


<html dir="{!! $lang=='ar'?'rtl':'ltr' !!}">
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>

	
	


    <div class="invoice-box {!! $lang=='ar' ? 'rtl' : '' !!}">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="http://ellelazena.com/fav.png"  >
                            </td>
                            
                            <td>
                                {{ trans('backend.order_ticket') }} #: {{ $order->order_ticket }}<br>
                                 {{ trans('backend.invoice_date') }} : {{ date('Y - m - d' , strtotime($order->order_date))  }} <br>
                              
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

             @php 

              $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 

            @endphp
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                               {{ $shipping_to->street }}<br>
                              {{ $shipping_to->country }}<br>
                                 
                            </td>
                            
                            <td>
                               {{ $shipping_to->fname }} <br>
                               {{ $shipping_to->lname }}<br>
                                {{ Auth::guard('customer')->user()->email }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            
            
           
            
            <tr class="heading">
                <td>
                     {{ trans('backend.product_name') }}
                </td>
                
                <td>
                     {{ trans('backend.product_quantity') }}
                </td>

                <td>
                     {{ trans('backend.product_price') }}
                </td>

                <td>
                	 {{ trans('backend.order_total') }}
                </td>
            </tr>
            @php
             $orders = \App\OrderProduct::where('order_id' , $order->order_ticket)->get(); 
            @endphp

             @foreach($orders as $order_pivot)
	            @php  $product =  $order_pivot->product ;   @endphp
	            <tr class="item">
	                <td>
	                    @foreach($product->description as $description) 
                                    
                                  @if($description->language_id == $language_id)
                                     {{ $description->name }} 
                                  @endif 
                              @endforeach
	                </td>
	                
	                <td>
	                    {{ $order_pivot->quantity }}
	                </td>

	                <td>
	                	{{ $product->price }}
	                </td>

	                <td>
	                	 {{ trans('lazena.s_r') }} {!!  $order_pivot->quantity * $product->price   !!}
	                </td>
	            </tr>

            @endforeach
            
            
            
            
            
            <tr class="total">
                <td></td>

                <td>
                	{{ trans('lazena.sub_total') }} :  {{ trans('lazena.s_r') }} {!! $order->order_total - 5  !!}
                </td>

                <td>
                	{{ trans('backend.delivery') }} :  {{ trans('lazena.s_r') }} 5.00 
                </td>
                
                <td>
                  <strong> {{ trans('backend.grand_total') }} : {{ trans('lazena.s_r') }} {{ $order->order_total }} </strong>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>