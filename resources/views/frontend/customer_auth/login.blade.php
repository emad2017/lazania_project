@extends('frontend.layout')


@section('page_title' , trans('lazena.login'))


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')


      <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
             
              <li>{{ trans('lazena.login') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  --> 
      
     <section class="login section-padding">
      <div class="container">
        <div class="row">

          <!-- Title -->
          <div class="section-title">
            <h3>{{ trans('lazena.login_intro') }}</h3>
          </div>

          <div class="account-login">
            
            @if(Session::has('authentication_needed'))
              <div class="alert alert-warning alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>  </strong> {{ Session::get('authentication_needed') }}
              </div>
            @endif


            @if(Session::has('wishlist_auth_needed'))
              <div class="alert alert-warning alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>  </strong> {{ Session::get('wishlist_auth_needed') }}
              </div>
            @endif


            <div class="col-md-6 new-account content">
              <div class="half">
                <h4>{{ trans('lazena.new_customers') }}</h4>
                <p>{{ trans('lazena.new_customers_intro') }}</p>
                 
             
               

                <div class="bbtn">
                  <a href="{{ url("$lang/customer-register") }}" class="button button-hot hvr-shutter-out-horizontal mt-30">{{ trans('lazena.create_account') }}</a>
                </div>
              </div>
            </div>

            <div class="col-md-6 registered-users content">
              <div class="half">

                
                {!! Form::open(['url'=>"$lang/customer-login" , 'method'=>'POST']) !!} 
                  <h4>{{ trans('lazena.current_customers') }}</h4>

                  @if($errors->any())

                    <div class="alert alert-danger alert-dismissable text-center">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong> </strong> {{$errors->first()}}
                    </div>

                     
                  @endif
                 
                  <p>{{ trans('lazena.current_customers_intro') }}</p>
                  
                  <label for="email">{{ trans('lazena.email_address') }}</label>
                  <input type="text" name="email" id="email"  >
  
                  <label for="password">{{ trans('lazena.password') }}</label>
                  <input type="password" id="password" name="password"  >
  
                  <div class="buttons-set">

                    {{-- <a href="{{ url("$lang/customer-forget-password") }}">Forgot Your Password?</a> --}}
                    <div class="bbtn">
                      <button   class="button button-hot hvr-shutter-out-horizontal mt-30">{{ trans('lazena.login') }}</button>
                    </div>
                  </div>
                 {!! Form::close() !!}
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

@endsection 

 



@section('scripts')
      
      
  
@endsection 





