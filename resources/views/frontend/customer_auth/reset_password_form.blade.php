@extends('frontend.layout')


@section('page_title' , "Login")


@section('styles')

    <style>
    
      .form_error{
      
      display: block;
        margin-top: -25px;
        margin-bottom: 10px;
        color: #d22e2e;

      }

      .form_error small{
        word-break: break-word ; 
      }


      .help-block{
        color: red; 
      }

  </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')


      <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">Home</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
             
              <li>Reset Your Password</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  --> 
      
     <section class="login section-padding">
      <div class="container">
        <div class="row">

          <!-- Title -->
          <div class="section-title">
            <h3>Fill the below information</h3>
          </div>

          <div class="account-login">
            
        

            <div class="col-md-8 col-md-offset-2 registered-users content">
              <div class="half">

                 <h4>Resetting your password</h4>

                   @if (session('email_sent'))
                     <div class="alert alert-success alert-dismissable text-center">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>   <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i> </strong> {{ session('email_sent') }} 
                    </div>  
                   @endif
                
                {!! Form::open(['url'=>"$lang/reset-password" , 'method'=>'POST']) !!} 

                 
                 
                  <p>Please use this form to reset your password in a correct manner </p>
                  
                    <div class="form-group">
                      
                    <label for="email">Email Address</label>
                    <input type="text" value="{{ $email or old('email') }}" name="email" id="email"  >

                      @if ($errors->has('email'))
                        <span class="help-block">
                            <strong> <i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> &nbsp; {{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    </div>


                    <div class="form-group">
                      
                    <label for="email">Password</label>
                    <input type="text" value="" name="password" id="password"  >

                      @if ($errors->has('password'))
                        <span class="help-block">
                            <strong> <i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> &nbsp; {{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    </div>


                    <div class="form-group">
                      
                    <label for="email">Password Confirm</label>
                    <input type="text" value="" name="password_confirmation" id="password_confirmation"  >

                       

                    </div>
 
                  
  
                  <div class="buttons-set">

                    <a href="{{ url("$lang/customer-forget-password") }}">Don't have an account , Register !</a>
                    <div class="bbtn">
                      <button   class="button button-hot hvr-shutter-out-horizontal mt-30">Send password reset  email</button>
                    </div>
                  </div>
                 {!! Form::close() !!}
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

@endsection 

 



@section('scripts')
      
      
  
@endsection 





