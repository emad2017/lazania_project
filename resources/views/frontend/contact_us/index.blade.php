@extends('frontend.layout')


@section('page_title' ,  trans('lazena.contact_us') )


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')
 
     <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{  trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li> {{  trans('lazena.contact_us') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->
    
      @if(Session::has('contact_sent'))
        
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong></strong> {{ Session::get('contact_sent') }}
        </div>

      @endif



          <!-- ====== Start Contact ======  -->
    <section class="contact section-padding">
      <div class="container-fluid">
        <div class="row">
          <!-- map -->
          <div id="map"></div>

          <!-- contact info -->
          <div class="col-sm-4 contact-info">

            <h4>{{  trans('lazena.contact_info') }}</h4>
            <div class="row">
              <div class="col-sm-12 details-part">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div class="info">

                   @foreach($footer->description as $description)
                        
                         @if($description->language_id == $language_id )
                                
                                <h5>{!! $description->address !!}</h5>
                         @endif

                      @endforeach
                  
                   
                </div>
              </div>
  
              <div class="col-sm-12 details-part">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <div class="info">
                  <h5>{{ $footer->telephone1 }}</h5>
                  <h5>{{ $footer->tele }}</h5>
                 
                </div>
              </div>
  
              <div class="col-sm-12 details-part">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                <div class="info">
                  <h5>{{ trans('lazena.inquires') }} <a href="#">{{ $footer->email }}</a></h5>
                  
                </div>
              </div>
            </div>
          </div> <!-- ./ contact info -->

          <!-- contact form -->
          <div class="col-md-8">

            <div class="cont-form">
                <div class="full-form">
                    <h4 class="mb-30">{{ trans('lazena.leave_msg') }}</h4>
                    {!! Form::open(['url'=>"$lang/post_contact" , 'class'=>'form' , 'method'=>'POST' , 'id'=>'contact-form']) !!}
                    {{-- <form id="contact-form" class="form" method="post" action="contact.php"> --}}
                        <div class="controls">
                            <div class="messages"></div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="row">
                                      <div class="col-md-12 form-group">
                                        <label for="name">{{ trans('lazena.name') }}</label>
                                        <input id="form_name" type="text" name="name" required="required" data-error="{{ trans('lazena.name_required') }}">
                                              
                                          <div class="help-block with-errors"></div>
                                      </div>
                  
                                      <div class="col-md-12 form-group">
                                        <label for="email">{{ trans('lazena.email') }}</label>
                                        <input id="form_email" type="email" name="email"  required="required" data-error="{{ trans('lazena.email_required') }}">
                                        
                                        <div class="help-block with-errors"></div>
                                      </div>
      
                                      <div class="col-md-12 form-group">
                                          <label for="phone">{{ trans('lazena.phone') }}</label>
                                          <input id="subject" type="text" name="phone"  required="required" data-error="{{ trans('lazena.phone_required') }}" >
                                      </div>
                                  </div>
                                </div>
            
                                <div class="col-md-6">
                                   <div class="row">
                                      <div class="col-md-12 form-group">
                                          <label for="message">{{ trans('lazena.message') }}</label>
                                          <textarea id="form_message" name="message" class="form-control" placeholder="{{ trans('lazena.type_message') }} " rows="4" required="required" data-error="{{ trans('lazena.message_required') }}"></textarea>
                                              
                                          <div class="help-block with-errors"></div>
                                      </div>
      
                                      <div class="col-md-12 button-co">
                                          <button class="button button-hot hvr-shutter-out-horizontal">{{ trans('lazena.send_msg') }}</button>
                                      </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {!! Form::close() !!}
                </div>
            </div>
          </div> <!-- ./ contact form -->
        </div> <!-- ./ Row -->
      </div> <!-- ./ Container -->
    </section>
    <!-- ====== ./ Contact ======  -->


@endsection 

 



@section('scripts')
      
      
  
@endsection 





