        @php 

           $lang = LaravelLocalization::getCurrentLocale();
           $language = \App\Language::where(['label'=>$lang])->first();
           $language_id = $language->id ;

        @endphp

 


 <!-- ====== Navgition ======  -->
    <nav class="navbar navbar-default bootsnav navbar-mobile bootsnav">
    
      <div class="nav-header">
        <div class="container">
          <div class="col-md-6 col-md-offset-6">
              <p><span><i class="fa fa-phone-square fa-1x"></i> 0554334390</span> </p>
              <!-- lest -->
              <ul class="lest">

                @if(Auth::guard('customer')->check())
                  
                  @php 
                    $customer = Auth::guard('customer')->user() ; 
                  @endphp
                 


                   <li  class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" >  {{ trans('lazena.welcome') }} {{  $customer->fname }}</a>
                      <ul class="dropdown-menu">
                        
                        <li><a href="{{ url("$lang/customer/dashboard") }}">{{ trans('lazena.my_account') }}</a></li>
                        {{-- <li><a href="{{ url("$lang/customer/wishlist") }}">My Wishlist</a> </li> --}}
                        <li><a href="javascript:{}"  style="cursor: pointer;" onclick="document.getElementById('logout_form').submit();">{{ trans('lazena.log_out') }} </a> </li>

                        {!! Form::open(['url'=>"$lang/customer/logout" , 'id'=>'logout_form' , 'mehtod'=>'POST']) !!}
                              <!--   Logout Form Construction --> 
                        {!! Form::close() !!}
                      </ul>
                    </li>


                    <li><a href="{{ url("$lang/customer/wishlist") }}">{{ trans('lazena.wishlist') }}</a></li>

                  
                 @else
                  
                  <li>
                      <a href="{{ url("$lang/customer-login") }}">{{ trans('lazena.login') }}</a>
                  </li>

                 @endif
                <li>
                    <a href="{{ url("$lang/contact") }}">{{ trans('lazena.find_us') }}</a>
                </li>


                
              </ul>
              <!-- ./ lest -->

              <!-- lang -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >{!! $lang == 'en' ? trans('lazena.english') : trans('lazena.arabic') !!}</a>
                <ul class="dropdown-menu">
                  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">{!! $localeCode == 'en' ? trans('lazena.english') : trans('lazena.arabic') !!}</a></li>
         

                  @endforeach
                </ul>
              </li>
              <!-- ./ lang -->
          </div>
        </div>
      </div>
      
      <!-- Start Top Search -->
      <div class="top-search">
          <div class="container">
              <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input type="text" id="search_txt"   class="form-control" placeholder="{{ trans('lazena.search') }}">
                  <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
              </div>

              <div class="lest-searchable">
                <ul id="ul_searchable">



                 

                  
                </ul>
              </div>
          </div>

      </div>
      <!-- End Top Search -->
      
      <div class="container">            
          <!-- Start Atribute Navigation -->
          <div class="attr-nav">
              <ul>
                  {{-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                          <i class="fa fa-shopping-bag"></i>
                          <span class="badge">0</span>
                      </a>
                      <ul class="dropdown-menu cart-list">
                          <p>You have no items in your shopping cart. </p>
                      </ul>
                  </li> --}}
                  <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
              </ul>
          </div>
          <!-- End Atribute Navigation -->

          <!-- Start Header Navigation -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                  <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="{{ url("/$lang") }}">
                <img src="{{ asset('frontend/assets/img/logo.png') }}" class="logo" alt="">
               {{--  <img src="{{ asset('frontend/assets/img/saadeddin-logo3.png') }}" class="logo-2" alt=""> --}}
              </a>
          </div>
          <!-- End Header Navigation -->

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar-menu">
              <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                

               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >{{ trans('lazena.ellelazena') }}</a>
                    <ul class="dropdown-menu">
                         <li><a href="{{ url("$lang/about-us") }}">
                          <img src="{{ asset('frontend/assets/img/icon/icon-about.png') }}" alt="">
                          {{ trans('lazena.about_us') }}
                        </a>
                </li>
                       
                       
                    </ul>
                </li>


                  <li><a href="{{ url("$lang/categories") }}">{{ trans('lazena.categories') }}</a></li>

                  <!-- megamenu -->
                  <li class="dropdown megamenu-fw">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('lazena.products') }}</a>
                      <ul class="dropdown-menu megamenu-content" role="menu">
                          <li>
                              <div class="row">


                                @if(count($navigationCategoriesContent) > 0 )
                                @foreach($navigationCategoriesContent as $cat)
                                      @if($cat->language_id == $language_id)
                                      @php 
                                          $icon = $cat->dessertdepartments->icon ; 

                                          $clean_name = strtolower(str_replace(' ', '_', $cat->name)) ;
                                      @endphp
                                      <div class="col-menu col-md-2">
                                      
                                              <ul class="menu-col">
                                                  <li><a href="{{ url("$lang/$cat->department_id/sub-categories") }}">
                                                    <img src="{{ asset("uploads/dessertdepartment/icons/$icon") }}" alt="">
                                                    <span>{{ $cat->name }}</span></a>
                                                  </li>
                                              </ul>
                                           
                                      </div> 
                                      @endif
                                  @endforeach
                                @endif
                                   

                                 
 
                              </div><!-- end row -->
                          </li>
                      </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('lazena.services') }}</a>
                      <ul class="dropdown-menu">
                          
                          @if(count($specialOccasionsContent)>0)
                              @foreach($specialOccasionsContent as $ocassionDescription)
                                   @if($ocassionDescription->language_id == $language_id) 


                                    <li>
                                      <a href="{{ url("$lang/special-occassion/$ocassionDescription->event_id/$ocassionDescription->name") }}">
                                        <img src="{{ asset('frontend/assets/img/icon/icon-wedding.png') }}" alt="">
                                        {{ $ocassionDescription->name }}
                                      </a>
                                    </li>
                                  @endif
                            @endforeach
                          @endif
                      </ul>
                  </li>

                {{--   <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kids Corner</a>
                      <ul class="dropdown-menu">
                          <li><a href="#">
                              <img src="{{ asset('frontend/assets/img/icon/icon-print-color.png') }}" alt="">
                              Print and Color
                            </a></li>
                      </ul>
                  </li> --}}

                  <li><a href="{{ url("$lang/careers_list") }}">{{ trans('lazena.careers') }}</a></li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('lazena.contact_us') }}</a>
                      <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url("$lang/contact") }}">
                              <img src="{{ asset('frontend/assets/img/icon/icon-contact.png') }}" alt="">
                              {{ trans('lazena.contact_info') }}
                            </a>
                          </li>
                         {{--  <li><a href="#">
                              <img src="{{ asset('frontend/assets/img/icon/icon-findus.png') }}" alt="">
                              Find Us
                            </a>
                          </li> --}}
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </div>   
    </nav>
		<!-- ====== End Navgition ======  -->