        
        @php 

           $lang = LaravelLocalization::getCurrentLocale();
           $language = \App\Language::where(['label'=>$lang])->first();
           $language_id = $language->id ;

        @endphp


        <div class="col-md-3">
            <div class="side-bar">
              <!-- block category -->
              <div class="block-category">
                <!-- block title -->
                <div class="block-title">
                  <h3>{{ trans('lazena.all_categories') }}</h3>

                </div>
                <!-- ./ block title -->

                <!--  block content -->
                <div class="block-content">
                  <ul>
                    <li>
                      
                      <!-- this is the main categories -->
                      @if(count($mainCategoriesContent) > 0 )
                        @foreach($mainCategoriesContent as $description)
                            @if($description->language_id == $language_id)
                              @php 
                                $mainCategorySide = $description->dessertdepartments ; 
                                $subCategoriesDynamic = $mainCategorySide->subDepts ; 

                                $name_cleaned = strtolower(str_replace(' ', '_', $description->name)) ;
                                $main_name = $description->name ; 
                                $cat_id = $description->department_id ; 
                              @endphp 
                              <div class="title-prod">
                                <a href="{{ url("$lang/$description->department_id/sub-categories") }}">{{ $description->name }}</a>
                                <i class="fa fa-plus pull-right open-sub" aria-hidden="true"></i> 
                                 <ul class="sub-product sub-product-a {{ $description->name }}">
                                  @if(count($subCategoriesDynamic) > 0 )
                                    @foreach($subCategoriesDynamic as $subCategory)
                                      @foreach($subCategory->description as $description)
                                        @if($description->language_id == $language_id)
                                            @php 
                                                $sub_cat_name = $description->name ; 
                                                $sub_cat_name_cleaned = strtolower(str_replace(' ' , '_' , $description->name))  ; 
                                                $sub_cat_id = $description->subDeptId ; 

                                            @endphp 
                                          <li>
                                            <div class="title-prod-sub">
                                              <a href="{{ url("$lang/$cat_id/sub-categories/$sub_cat_id/products") }}">{{ $description->name }}</a>
                                               
                                            </div>
                                          </li>
                                       @endif
                                      @endforeach
                                    @endforeach
                                  @else

                                    <li>
                                      <div class="title-prod-sub">
                                        {{ trans('lazena.no_sub_category') }}
                                      </div>
                                    </li>
                                  @endif
                                </ul>  
                              </div>
                            @endif
                          @endforeach
                      @endif
                      <!-- main categories end -->

                     

                    </li>
                  </ul>
                </div>
                <!-- ./ block content -->
              </div>
              <!-- ./ block category -->
            </div>
          </div>