@extends('frontend.layout')


@section('page_title' ,  trans('lazena.about_us') )


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

  <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li> {{ trans('lazena.about_us') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ======  services  ======  -->
    <section class="services section-padding">
      <div class="container">
        <div class="row">
          <!-- Title -->
          <div class="section-title">
            <h3>{{ $about_description->name }}</h3>
          </div>

          <div class="col-sm-12">
            <hr class="section">
          </div>
            
        
          {!! html_entity_decode($about_description->description) !!}


        </div>
      </div>
    </section>
    <!-- ====== ./ services ======  -->
    

@endsection 

 



@section('scripts')
      
      
  
@endsection 





