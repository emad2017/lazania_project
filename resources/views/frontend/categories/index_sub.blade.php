@extends('frontend.layout')


@section('page_title' , "$main_category_name_clean")


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

	   <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("$lang/categories") }}">{{ trans('lazena.categories') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>

              <li>{{ $main_category_name_clean }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ====== Start Container Gallery  ======  -->
    <div class="container-content section-padding">
      <div class="container">
        <div class="row">
          <!-- side bar -->
          

            <!-- Inject sidebar any where you want  --> 
            @include('frontend.partials.sidebar_categories')

          <!-- ./ side bar -->
           
          <!-- product gallery -->
          <div class="col-md-9">
            <div class="product-gallery">
              <div class="row">
                

                @if(count($sub_holder) > 0 )
                  @foreach($sub_holder as $sub_category_description)

                    @php 
                        $sub_category_image = $sub_category_description->subDept->image ; 
                        $sub_cat_name_cleaned = strtolower( str_replace(' ', '_', $sub_category_description->name )) ; 

                        $main_cat = $sub_category_description->subDept->mainCategory ; 
                        if(count($main_cat) > 0)
                        foreach($main_cat->description as $desc){

                              if($desc->language_id == $language_id){

                                $tt =   $desc->name   ; 

                              }

                        } 
                       
                    @endphp
                    <!-- product gallery item -->
                    <div class="col-md-6 mb-30">
                      <div class="proudct-gallery-item">
                        <div class="proudct-gallery-item-box">
                          <a href="{{ url("$lang/$main_cat->id/sub-categories/$sub_category_description->subDeptId/products") }}">
                            <div class="part-img">
                              <img src="{{ asset("uploads/sub_departments/640_640/$sub_category_image") }}" alt="">
                            </div>
          
                            <div class="overlay-item">
                              <h4>{{ $sub_category_description->name }}</h4>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                    <!-- ./ product gallery item -->
                    @endforeach
                @else
                  
                   <div class="col-md-12 mb-30">
                       
                           
          
                            <div class="overlay-item">
                              <h4> {{ trans('lazena.no_result') }} </h4>
                            </div>
                           
                    </div>

                @endif

                 

                 

                 
                
              </div>
            </div>
          </div>
          <!-- ./ product gallery -->
       
        </div>
      </div>
    </div>
    <!-- ====== ./ Container Gallery  ======  -->

@endsection 

 



@section('scripts')
      
      
  
@endsection 





