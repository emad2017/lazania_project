@extends('frontend.layout')


@section('page_title' , "$un_cleaned_cat_name - $un_cleaned_sub_cat_name")


@section('styles')

  <style>
        
        .item_added {
            
                display: inline-block;
                padding: 0.6em 0.9em;
                background: #f8408c ;
                border-radius: 50em;
                text-transform: lowercase;
                color: #ffffff;
                font-weight: 100;
                letter-spacing: .1em;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
                -webkit-transition: all .2s;
                transition: all .2s;

        }

        .h4break{
            
                text-overflow: ellipsis;
                overflow: hidden;
                width: 65%;
                white-space: nowrap;
        }

    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

	     <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("$lang/categories") }}">{{ trans('lazena.categories') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
              @php 

                  $cleaned_cat_name = strtolower(str_replace(' ', '_', $un_cleaned_cat_name))  ; 

              @endphp
              <li><a href="{{ url("$lang/$id/sub-categories") }}">{{ $un_cleaned_cat_name }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
                
              <li>{{  $un_cleaned_sub_cat_name   }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ====== Start Container Gallery  ======  -->
    <div class="container-content section-padding">
      <div class="container">
        <div class="row">
          <!-- side bar -->
             <!-- Inject sidebar any where you want  --> 
            @include('frontend.partials.sidebar_categories')
          <!-- ./ side bar -->
  
          <!-- details product -->
          <div class="col-md-9">
            <div class="details-product">

             
                      @if(count($products_content_holder) > 0 )

                        @foreach($products_content_holder as $product_description)

                          
                          @php 

                            // prodcut info from main table  
                            $prodcut_image  = $product_description->dessertitem->img1 ; 
                            $product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$prodcut_image ; 
                            $product_price  = $product_description->dessertitem->price ;
                            $product_rate   = $product_description->dessertitem->rate ;

                            $product_quantity = $product_description->dessertitem->quantity ; 
                            $product_id = $product_description->dessertitem->id ; 




                          @endphp 

 

                          <!-- item details product -->
                          <div class="col-md-4">
                            <div class="item-details-product">
                              <div class="part-img">
                                 <a href="{{ url("$lang/$id/sub-categories/$sub_id/products/$product_description->item_id") }}">
                                <img src="{{ asset("uploads/dessertsitem/610_383/$prodcut_image") }}" alt="{{ $prodcut_image }}">
                              </a>
                              </div>

                              <div class="part-info text-center">
                                <div class="price">
                                  <h5> {{ trans('lazena.s_r') }} {{ $product_price }}</h5>
                                </div>
                                <div class="ratings">
                                  <div class="rat">
                                      @if($product_rate == 1)
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        @elseif($product_rate == 2)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @elseif($product_rate == 3)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @elseif($product_rate == 4)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @else

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                                        @endif
                                  </div>
                                </div>



                                
                                <a href="{{ url("$lang/$id/sub-categories/$sub_id/products/$product_description->item_id") }}"><h4 class="h4break"> {{ $product_description->name }} </h4></a>

                                <div class="hover-area">


                                 <!-- i  need to check if this product added to current logged in customer wishlist or not -_-   to not addit again  -->
                                        
                                        @if(Auth::guard('customer')->check())
                                            
                                          @php 
                                              
                                              // if the user logged in i will find if that product found on his/her wishlist or not  
                                              // if found then shows maybe a thumb that this is already added to whishlist  
                                              // else let him see the button normally  -_- 
                                              
                                              $customer = Auth::guard('customer')->user() ; 

                                              $customerAddedToWishlistProduct = $customer->wishlistProduct->where('product_id' ,$product_id)->first(); 

                                               

                                              

                                          @endphp   
                                          
                                          @if(count($customerAddedToWishlistProduct) > 0 )
                                            <a class="button button-hot hvr-shutter-out-horizontal"  ><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                            </a>
                                          @else

                                            <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                            </a>
                                          @endif
                                        
                                        @else
                                          
                                          <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                          </a>

                                        @endif

                                 {{--  <a class="button button-hot hvr-shutter-out-horizontal first" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    {{  trans('lazena.add_to_cart') }}</a> --}}

                                @if($product_quantity > 0 )
                                <a   href="#0" product-id="{{ $product_id }}"  product-price="{{ $product_price }}" product-quantity="{{ $product_quantity }}"  product-image="{{ $product_image_path }}" product-name="{{ $product_description->name  }}" class="cd-add-to-cart to_be_hide_{{ $product_id }}  button button-hot hvr-shutter-out-horizontal first"><i class="fa fa-shopping-cart" aria-hidden="true"></i> {{  trans('lazena.add_to_cart') }}</a>

                                  <a  class="button button-hot hvr-shutter-out-horizontal  item_added   hidden item_{{ $product_id }}">{{  trans('lazena.item_added') }}</a>
                                @else
                                
                                   <a  class="button button-hot hvr-shutter-out-horizontal  item_added">{{  trans('lazena.out_of_stock') }}</a>

                                @endif




                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- ./ item details product -->
                          @endforeach
                        @else

                              <div class="overlay-item">
                              <h4> {{ trans('lazena.no_result') }} </h4>
                            </div>
                        @endif

            </div>
          </div>
          <!-- ./ details-product -->
       
        </div>
      </div>
    </div>
    <!-- ====== ./ Container Gallery  ======  -->

@endsection 

 



@section('scripts')
      
      
  
@endsection 





