@extends('frontend.layout')


@section('page_title' , trans('lazena.categories'))


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

			 <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{ trans('lazena.categories') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ======  product gallery  ======  -->
    <section class="product-gallery section-padding">
      <div class="container">
        <div class="row">
 
          @if(count($content) > 0 )
          @foreach($content as $category)
      
          @php 

              $image = $category->dessertdepartments->img1 ; 
              $category_id = $category->dessertdepartments->id ; 
              $main_category = strtolower(str_replace(' ', '_', $category->name)) ;

            
              
          @endphp
          <div class="col-md-6 mb-30">
            <div class="proudct-gallery-item">
              <div class="proudct-gallery-item-box">
                <a href="{{ url("$lang/$category_id/sub-categories") }}">
                  <div class="part-img">
                    <img src="{{ asset("uploads/dessertdepartment/640_640/$image") }}" alt="">
                  </div>

                  <div class="overlay-item">
                    <h4>{{ $category->name }}</h4>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <!-- ./ product gallery item -->
          @endforeach
          @endif

        </div>
      </div>
    </section>
    <!-- ====== ./  product gallery  ======  -->

    

@endsection 

 



@section('scripts')
      
      
  
@endsection 





