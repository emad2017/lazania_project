@extends('frontend.layout')


@section('page_title' , "$un_cleaned_cat_name - $un_cleaned_sub_cat_name - $un_cleaned_product_name")


@section('styles')

   <style>
        
        .item_added {
            
                display: inline-block;
                padding: 0.6em 0.9em;
                background: #f8408c ;
                border-radius: 50em;
                text-transform: lowercase;
                color: #ffffff;
                font-weight: 100;
                letter-spacing: .1em;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
                -webkit-transition: all .2s;
                transition: all .2s;

        }

    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

      <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("$lang/categories") }}">{{ trans('lazena.categories') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
              @php 

                  $cleaned_cat_name =  $un_cleaned_cat_name  ; 
                  $cleaned_sub_cat_name =  $un_cleaned_sub_cat_name  ; 

              @endphp
              <li><a href="{{ url("$lang/$id/sub-categories") }}">{{ $un_cleaned_cat_name }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>


              <li><a href="{{ url("$lang/$id/sub-categories/$sub_id/products") }}">{{ $un_cleaned_sub_cat_name }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i></li>

              <li>{{ $un_cleaned_product_name }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ======   ======  -->
    <div class="main-product section-padding">
      <div class="container">
        <div class="row">
          <div class="col-sm-10">
            <div class="row">

              <!-- product img -->
              <div class="product-img-box col-sm-6">
                <div class="flexslider">
                  <ul class="slides">

                    
                    <!-- we will update this is the future to handle many images  --> 
                    <!-- but for now we stick with only one image  --> 
                    @if(count($product_holder) > 0 )
                    
                     @php 
                         $product_image = $product_holder->dessertitem->img1 ; 
                         $product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$product_image ; 
                         $product_rate  = $product_holder->dessertitem->rate ; 
                         $product_price  = $product_holder->dessertitem->price ;

                          $product_quantity = $product_holder->dessertitem->quantity ; 
                          $product_id = $product_holder->dessertitem->id ; 
                     @endphp 
                    
                        <li data-thumb="{{ asset("uploads/dessertsitem/610_383/$product_image") }}">
                          <a href='{{ url("uploads/dessertsitem/original/$product_image") }}' class="fresco" ><img src="{{ asset("uploads/dessertsitem/610_383/$product_image") }}" /></a>
                        </li>

                    @else
                        
                        <li data-thumb="{{ asset("uploads/placeholder/placeholder.jpg") }}">
                          <img src="{{ asset("uploads/placeholder/placeholder.jpg") }}" />
                        </li> 
                    
                    @endif
                    
                  </ul>
                </div>
              </div>
              <!-- ./ product img -->

              <!-- product shop -->
              <div class="product-shop col-sm-6">
                <!-- Sub title -->
                <div class="sub-title">
                  <h3>{{ $product_holder->name }}</h3>
                </div>
    
                <div class="ratings mb-30">
                  <div class="rat">
                          @if($product_rate == 1)
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                          @elseif($product_rate == 2)

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          @elseif($product_rate == 3)

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          @elseif($product_rate == 4)

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star" aria-hidden="true"></i></span>

                          @else

                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                              <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                          @endif
                  </div>
                </div>
                
                <div class="de mb-30">
                    <p>{{ $product_holder->description }}</p>
                </div>

                <div class="product-info mb-30">
                  <h5>{{ trans('lazena.price_label') }} </h5>
                  <div class="price-box">
                    <h4>{{ trans('lazena.s_r') }} {{ $product_price  }}</h4>
                  </div>
                </div>

                <div class="add-to-box">

                  

                 <!-- i  need to check if this product added to current logged in customer wishlist or not -_-   to not addit again  -->
                                        
                                        @if(Auth::guard('customer')->check())
                                            
                                          @php 
                                              
                                              // if the user logged in i will find if that product found on his/her wishlist or not  
                                              // if found then shows maybe a thumb that this is already added to whishlist  
                                              // else let him see the button normally  -_- 
                                              
                                              $customer = Auth::guard('customer')->user() ; 

                                              $customerAddedToWishlistProduct = $customer->wishlistProduct->where('product_id' ,$product_id)->first(); 

                                               

                                              

                                          @endphp   
                                          
                                          @if(count($customerAddedToWishlistProduct) > 0 )
                                            <a class="button button-hot hvr-shutter-out-horizontal"  ><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                            </a>
                                          @else

                                            <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                            </a>
                                          @endif
                                        
                                        @else
                                          
                                          <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                          </a>

                                        @endif

                  
                  @if($product_quantity > 0 )
                   <a   href="#0" product-id="{{ $product_id }}"  product-price="{{ $product_price }}" product-quantity="{{ $product_quantity }}"  product-image="{{ $product_image_path }}" product-name="{{ $product_holder->name  }}" class="cd-add-to-cart to_be_hide_{{ $product_id }}  button button-hot hvr-shutter-out-horizontal first"><i class="fa fa-shopping-cart" aria-hidden="true"></i> {{  trans('lazena.add_to_cart') }}</a>

                  <a  class="button button-hot hvr-shutter-out-horizontal  item_added   hidden item_{{ $product_id }}">{{  trans('lazena.item_added') }}</a>

                   @else
                                
                     <a  class="button button-hot hvr-shutter-out-horizontal  item_added">{{  trans('lazena.out_of_stock') }}</a>

                  @endif



                </div>

              </div>
              <!-- ./ product shop -->

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ======  ======  -->

@endsection 

 



@section('scripts')
      
      
  
@endsection 





