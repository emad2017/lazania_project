@extends('frontend.layout')


@section('page_title' , trans('lazena.welcome_to_ellelazena'))


@section('styles')

    <style>
        
        .item_added {
            
                display: inline-block;
                padding: 0.6em 0.9em;
                background: #f8408c ;
                /*border-radius: 50em;*/
                text-transform: lowercase;
                color: #ffffff;
                font-weight: 100;
                letter-spacing: .1em;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
                -webkit-transition: all .2s;
                transition: all .2s;

        }

        .h4break{
            
                text-overflow: ellipsis;
                overflow: hidden;
                width: 80%;
                white-space: nowrap;
        }

    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')
    

        <!-- Index Home Page Start --> 
          

            <!-- ====== Start Header ======  -->
              <header class="header">
                <div class="owl-carousel owl-theme">

                  <!-- start fetch our slider products here  --> 
                  @if(count($slider_products) > 0 )
                    @foreach($slider_products as $product)
                    
                      <div class="item">
                        <a href="#">
                          <img src='{{ asset("uploads/slider/$product->image") }}' alt="{{ $product->image }}">
                        </a>
                      </div>

                    @endforeach
                  @endif
 
                 <!-- End fetch our slider products here  --> 
                
                </div>
              </header>
              <!-- ======./ Header ======  -->

              <!-- ====== Start About ======  -->
              <section class="about section-padding">
                <div class="container">
                  <div class="row">

 
                    <!-- About Info -->
                    <div class="col-sm-6  about-info text-center">
                      <!-- Title -->
                      <div class="section-title">
                        <h3>{{ trans('lazena.about_us') }}</h3>
                      </div>
                      <div class="info">
                        <p>{{ str_limit($about_content ,350)  }}</p>

                        <div class="bbtn">
                          <a href="{{ url("$lang/about-us") }}" class="button button-hot hvr-shutter-out-horizontal mt-30">{{ trans('lazena.read_more') }}</a>
                        </div>
                      </div>
                    </div>
                    <!-- ./ About Info -->


                      <!-- About Info -->
                    <div class="col-sm-6  about-info text-center">
                      <!-- Title -->
                      <div class="section-title">
                        <h3>{{ trans('lazena.latest_tweets') }}</h3>
                      </div>
                      <div class="info">
                      

                         <a class="twitter-timeline" data-height="400" href="https://www.twitter.com/EllelaZena"> {{ trans('lazena.tweets_by') }}</a> 

                        
                      </div>
                    </div>
                    <!-- ./ About Info -->
                    

                  
                  </div>
                </div>
              </section>
              <!-- ======./ About ======  -->
           
              <!-- ====== Start Product ======  -->
              <section class="product section-padding">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="section">
                    </div>
                    <!-- Sub title -->
                    <div class="sub-title text-center">
                      <h3>{{ trans('lazena.product_range') }} </h3>
                    </div>

                    <!-- start fetch our main category --> 
                    <div class="owl-carousel owl-theme">
                    @if(count($main_categories) > 0 )
                      @foreach($main_categories as $category)

                           @foreach($category->description as $description)
                              @if($description->language_id == $language_id)
                               @php 
                                $clean_name =  $description->name  ; 
                               @endphp 
                              @endif
                            @endforeach
                                
                          <a href="{{ url("$lang/$description->department_id/sub-categories") }}">
                          <div class="item text-center">
                               <div class="part-img">
                                  <img src="{{ asset("uploads/dessertdepartment/610_383/$category->img1") }}" alt="{{ $category->img1 }}">
                                </div>

                                <div class="part-info">
                                  @foreach($category->description as $description)
                                    @if($description->language_id == $language_id)
                                    <h3>{{ $description->name }}</h3>
                                    @endif
                                  @endforeach
                                </div>
                         
                          </div>
                                </a>

                      @endforeach
                    @endif
             

                    </div>

                    <!-- end fetch our main category --> 

                  </div>
                </div>
              </section>
              <!-- ======./ Product ======  -->

              <!-- ====== Start Other Product ======  -->
              <section class="best-s-p overlay section-padding">
                <div class="container">
                  <div class="row">
                      

                      <!-- Sub title -->
                      <div class="sub-title text-center">
                        <h3>{{ trans('lazena.best_selling') }} </h3>
                      </div>
                      
                      <!-- product-carousel -->
                   
                        <div class="owl-carousel owl-theme">
                          @if(count($best_selling_products_content) > 0 )
                            @foreach($best_selling_products_content as $best_selling)

                                    @php 
                                      $product_image = $best_selling->dessertitem->img1 ; 

                                      $product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$product_image ; 
                                      $product_rate   = $best_selling->dessertitem->rate ; 
                                      $product_price  = $best_selling->dessertitem->price ; 
                                      $product_quantity = $best_selling->dessertitem->quantity ; 
                                      $product_id = $best_selling->dessertitem->id ; 

                                     




                                      // what i need here is to fetch main category for product  
                                      // and sub category under this main category 
                                      // with names cleaned and un cleaned  
                                      // to send it along side with the url -_- 
                                      
                                      // this is the sub category 
                                      $product_sub_category = $best_selling->dessertitem->desertSubDept ; 

                                      foreach ($product_sub_category->description as $description) {
                                          if($description->language_id == $language_id){

                                              $sub_cat_name    = $description->name ; 
                                              $sub_cat_cleaned =  $sub_cat_name   ; 
                                              $sub_id = $product_sub_category->id ; 
                                          }
                                      }

                                      $mainCategory  =  $product_sub_category->mainCategory ; 

                                      foreach ($mainCategory->description as $description) {
                                          if($description->language_id == $language_id){

                                              $cat_name    = $description->name ; 
                                              $cat_cleaned =  $cat_name   ; 
                                              $cat_id = $mainCategory->id ; 
                                          }
                                      }


                                      $product_name = $best_selling->name ; 
                                      $product_name_cleaned =  $best_selling->name   ; 
                                       



                                    @endphp

                            <!-- item -->
                            <div class="item-frame-box">
                              <div class="item-box">
                                <div class="part-img">
                                   <a href="{{  url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}">
                                    <img src="{{ asset("uploads/dessertsitem/610_383/$product_image") }}" alt="{{ $product_image }}">
                                   </a>
                                </div>
                                <div class="part-info">
                                  <div class="info-header text-center mb-30">

                                      


                                    <a href="{{ url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}"><h3 class="h4break">{{ $best_selling->name }}</h3></a>

                                    {{-- <h3>{{ $best_selling->name }}</h3> --}}
                                  </div>

                                  <div class="ratings mb-30">
                                      <div class="rat">
                                        @if($product_rate == 1)
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                        @elseif($product_rate == 2)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @elseif($product_rate == 3)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @elseif($product_rate == 4)

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                        @else

                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                            <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                                        @endif
                                      </div>
                                  </div>

                                  <h5>{{  trans('lazena.s_r') }} {{ $product_price }}</h5>

                                  
                                   @if($product_quantity > 0  )

                                  <a   href="#0" product-id="{{ $product_id }}"  product-price="{{ $product_price }}" product-quantity="{{ $product_quantity }}"  product-image="{{ $product_image_path }}" product-name="{{ $product_name  }}" class="button hvr-shutter-out-horizontal pull-right cd-add-to-cart to_be_hide_{{ $product_id }}">{{  trans('lazena.add_to_cart') }}</a>

                                  <a   class=" item_added  hvr-shutter-out-horizontal pull-right     hidden item_{{ $product_id }}">{{  trans('lazena.item_added') }}</a>

                                  @else
                                  
                                     <a  class="button-hot   hvr-shutter-out-horizontal  item_added">{{  trans('lazena.out_of_stock') }}</a>

                                  @endif
                                  
                                </div>
                              </div>
                            </div>
                            <!-- ./ item -->
                             @endforeach
                         @endif
                                          
                                       
              </div> <!-- ./ row -->
            </div> <!-- ./ Container -->
          </section>
        <!-- ====== ./ Other Product ======  -->

              


              <!-- new most viewed --> 


               <section class="other-product section-padding">
      <div class="container">
        <div class="row">

          <!-- Sub title -->
          <div class="sub-title col-sm-12">
            <h3>{{ trans('lazena.most_viewed') }} </h3>
          </div>

          <!-- product-carousel -->
          <div class="col-md-12">
           

           

            <!-- starting most viewed products fetching data here  --> 
                          @if(count($most_viewed_products_content) > 0  )
                         
                            @foreach($most_viewed_products_content as $most_viewed_product)
                              
                                @php 
                                      $most_viewed_product_image = $most_viewed_product->dessertitem->img1 ; 

                                      $product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$most_viewed_product_image ; 
                                      
                                      $product_rate     = $most_viewed_product->dessertitem->rate ; 
                                      $product_price    = $most_viewed_product->dessertitem->price ; 
                                      $product_quantity = $most_viewed_product->dessertitem->quantity ; 
                                      $product_id       = $most_viewed_product->dessertitem->id ; 


                                     // what i need here is to fetch main category for product  
                                      // and sub category under this main category 
                                      // with names cleaned and un cleaned  
                                      // to send it along side with the url -_- 
                                      
                                      // this is the sub category 
                                      $product_sub_category = $most_viewed_product->dessertitem->desertSubDept ; 



                                      foreach ($product_sub_category->description as $description) {
                                          if($description->language_id == $language_id){

                                              $sub_cat_name    = $description->name ; 
                                              $sub_cat_cleaned =   $sub_cat_name   ; 
                                              $sub_id = $product_sub_category->id ; 
                                          }
                                      }

                                      $mainCategory  =  $product_sub_category->mainCategory ; 

                                      foreach ($mainCategory->description as $description) {
                                          if($description->language_id == $language_id){

                                              $cat_name    = $description->name ; 
                                              $cat_cleaned = $cat_name   ; 
                                              $cat_id = $mainCategory->id ; 
                                          }
                                      }


                                      $product_name = $most_viewed_product->name ; 
                                      $product_name_cleaned = $most_viewed_product->name   ; 
                                      $product_id =  $most_viewed_product->item_id ; 

                                @endphp

                              <div class=" col-md-3 col-sm-6 mb-30">
                                <!-- item -->
                                <div class="item">
                                  <div class="part-img">
                                    <a href="{{ url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}">
                                      <img src="{{ asset("uploads/dessertsitem/300_400/$most_viewed_product_image") }}" alt="{{ $most_viewed_product_image }}">

                                    </a>
                                    <div class="over-lay-item text-center">
                                      <div class="v-middle">
                                        

                                        <!-- i  need to check if this product added to current logged in customer wishlist or not -_-   to not addit again  -->
                                        
                                        @if(Auth::guard('customer')->check())
                                            
                                          @php 
                                              
                                              // if the user logged in i will find if that product found on his/her wishlist or not  
                                              // if found then shows maybe a thumb that this is already added to whishlist  
                                              // else let him see the button normally  -_- 
                                              
                                              $customer = Auth::guard('customer')->user() ; 

                                              $customerAddedToWishlistProduct = $customer->wishlistProduct->where('product_id' ,$product_id)->first(); 

                                               

                                              

                                          @endphp   
                                          
                                          @if(count($customerAddedToWishlistProduct) > 0 )
                                            <a class="button button-hot hvr-shutter-out-horizontal"  ><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                            </a>
                                          @else

                                            <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                            </a>
                                          @endif
                                        
                                        @else
                                          
                                          <a class="button button-hot hvr-shutter-out-horizontal" href='{{ url("$lang/customer/add_product_to_whislist/$product_id") }}'><i class="fa fa-heart" aria-hidden="true"></i>
                                          </a>

                                        @endif

                                        @if($product_quantity >  0 )

                                        <a class="button button-hot hvr-shutter-out-horizontal first cd-add-to-cart  to_be_hide_{{ $product_id }}" href="#0" product-id="{{ $product_id }}"  product-price="{{ $product_price }}" product-quantity="{{ $product_quantity }}"  product-image="{{ $product_image_path }}" product-name="{{ $product_name  }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>

                                        
                                     

                                          <a   class=" button button-hot hvr-shutter-out-horizontal first item_added      hidden item_{{ $product_id }}"><i class="fa fa-check" aria-hidden="true"></i></a>


                                         @else
                                
                                           <a  class="button   hvr-shutter-out-horizontal  item_added">{{  trans('lazena.out_of_stock') }}</a>

                                        @endif


                                      </div>
                                    </div>
                                  </div>
                                  <div class="part-info text-center">
                                    <a href="{{ url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}"><h4 class="h4break">{{ $most_viewed_product->name }}</h4></a>
                                    <div class="ratings">
                                      <div class="rat">
                                            
                                            @if($product_rate == 1)
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            @elseif($product_rate == 2)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @elseif($product_rate == 3)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @elseif($product_rate == 4)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @else

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                                            @endif
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- ./ item -->

                              </div>




                            @endforeach
                          @endif
                          <!-- ending most viewed products fetching data here  -->  

           








          </div>
          <!-- ./ product-carousel -->

        </div> <!-- ./ row -->
      </div> <!-- ./ Container -->
    </section>

















              <!-- ====== Start Recommended Products ======  -->
              <section class="r-products section-padding">
                <div class="container">
                  <div class="row">
 

                    <!-- Sub title -->
                    <div class="sub-title text-center">
                      <h3>{{ trans('lazena.recomended_products') }}</h3>
                    </div>

                    <!-- product-carousel -->
                 
                        <div class="owl-carousel owl-theme">
                           
                              <!-- start fetching our recommended products here  --> 
                              @if(count($recommended_content) > 0)
                                @foreach($recommended_content as $recommended)
                                  
                                  @php 
                                      $recommended_product_image = $recommended->dessertitem->img1 ; 

                                      $recommended_product_image_path = asset("uploads/dessertsitem/610_383/").'/'.$recommended_product_image ; 

                                      $recommended_rate = $recommended->dessertitem->rate ; 
                                      $recommended_price = $recommended->dessertitem->price ; 
                                      $recommended_quantity = $recommended->dessertitem->quantity ; 

                                      $recommended_id   = $recommended->dessertitem->id ; 



                                       // what i need here is to fetch main category for product  
                                      // and sub category under this main category 
                                      // with names cleaned and un cleaned  
                                      // to send it along side with the url -_- 
                                      
                                      // this is the sub category 
                                      $product_sub_category = $recommended->dessertitem->desertSubDept ; 



                                      foreach ($product_sub_category->description as $description) {
                                          if($description->language_id == $language_id){

                                              $sub_cat_name    = $description->name ; 
                                              $sub_cat_cleaned =  $sub_cat_name  ; 
                                              $sub_id = $product_sub_category->id ; 
                                          }
                                      }

                                      $mainCategory  =  $product_sub_category->mainCategory ; 

                                      foreach ($mainCategory->description as $description) {
                                          if($description->language_id == $language_id){

                                              $cat_name    = $description->name ; 
                                              $cat_cleaned =  $cat_name  ; 
                                              $cat_id = $mainCategory->id ; 
                                          }
                                      }


                                      $product_name = $recommended->name ; 
                                      $product_name_cleaned =  $recommended->name   ; 
                                      $product_id =  $recommended->item_id ; 


                                  @endphp

                                  <!-- item -->
                                  <div class="item-frame-box">
                                  <div class="item-box">
                                    <div class="part-img">
                                      <a href="{{ url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}">
                                        <img src="{{ asset("uploads/dessertsitem/610_383/$recommended_product_image") }}" alt="{{ $recommended_product_image }}">
                                      </a>
                                    </div>
                                    <div class="part-info ">
                                      <div class="info-header text-center mb-30">
                                       <a href="{{ url("$lang/$cat_id/sub-categories/$sub_id/products/$product_id") }}"><h3 class="h4break">{{ $recommended->name }}</h3> </a>
                                     </div>
                                      <div class="ratings mb-30">
                                        <div class="rat">
                                           @if($recommended_rate == 1)
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                            @elseif($recommended_rate == 2)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @elseif($recommended_rate == 3)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @elseif($recommended_rate == 4)

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star" aria-hidden="true"></i></span>

                                            @else

                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>
                                                <span><i class="fa fa-star right" aria-hidden="true"></i></span>

                                            @endif
                                        </div>
                                      </div>
                                       <h5>{{  trans('lazena.s_r') }} {{ $recommended_price }}</h5>

                                            
                                         @if($recommended_quantity > 0 )     
                                         <a   href="#0" product-id="{{ $recommended_id }}"  product-price="{{ $recommended_price }}" product-quantity="{{ $recommended_quantity }}"  product-image="{{ $recommended_product_image_path }}" product-name="{{ $product_name  }}" class="button hvr-shutter-out-horizontal pull-right cd-add-to-cart to_be_hide_{{ $recommended_id }}">{{  trans('lazena.add_to_cart') }}</a>

                                        

                                        <a   class=" item_added  hvr-shutter-out-horizontal pull-right     hidden item_{{ $recommended_id }}">{{  trans('lazena.item_added') }}</a>

                                        @else
                                
                                           <a  class=" button-hot  hvr-shutter-out-horizontal  item_added">{{  trans('lazena.out_of_stock') }}</a>

                                        @endif
                                      
                                    </div>
                                  </div>
                                </div>
                                  <!-- ./ item -->
                                @endforeach
                              @endif

                              <!-- End fetching our recommended products here  --> 
                        </div>
                      
                      <!-- ./ product-carousel -->

                  </div> <!-- ./ Row -->
                </div> <!-- ./ Container -->
              </section>
              <!-- ======./ Recommended Products ======  -->



        <!-- Index Home Page End --> 

      

@endsection 



@section('scripts')
      
      
  
@endsection 
