@extends('frontend.layout')


@section('page_title' ,    trans('lazena.account_dashboard')  )


@section('styles')
  
    <style>
      
          .active{
            color: #f8408 ; 
          }


    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


@section('pages_content')
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("/$lang/customer/dashboard") }}">{{ trans('lazena.customer') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{ trans('lazena.account_dashboard') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
          <!-- sidebar account -->
          
            <!-- inject customer sidebar into dom  --> 
            @include('frontend.customer.partials.sidebar') 

          <!-- ./ sidebar account -->

          <!-- content account -->
          <div class="col-md-9">
            <div class="content-account">

              @if(Session::has('new_account'))
                <!-- sucessfull registration alert --> 
                <div class="alert alert-success alert-dismissable text-center">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Congratulations!</strong> {{ Session::get('new_account') }}
                </div>

              @endif


              @if(Session::has('product_added'))
                <!-- sucessfull registration alert --> 
                <div class="alert alert-success alert-dismissable text-center">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong></strong> {{ Session::get('product_added') }}
                </div>

              @endif


              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.account_dashboard') }}</h3>
              </div>

              

               


              <div class="col-md-12 mb-30">
               {{--  <div class="welcome-msg">
                  <p><strong>Hello, {{ $customer_username  }} </strong>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
                </div> --}}
              </div>

              <div class="box-account-info">
                <div class="col-md-12 block-title">
                  <h4>{{ trans('lazena.account_info') }}</h4>
                </div>
                <!-- box -->
                <div class="col-md-12 mb-30">
                  <div class="box-in half">
                    <div class="box-title"> 
                      <h5>{{ trans('lazena.contact_info') }}</h5>
                      <a href="{{ url("$lang/customer/account_information/edit") }}" class="pull-right">{{ trans('lazena.edit') }}</a>
                    </div>
  
                    <div class="box-in-content">
                        
                        <div class="row">

                            <div class="col-sm-6">
                               <label for="">{{ trans('lazena.fname') }}</label>
                              
                               <p>{{ $customer->fname }}</p>
                            </div>

                             <div class="col-sm-6">
                               <label for="">{{ trans('lazena.lname') }}</label>
                            
                               <p>{{ $customer->lname }}</p>
                            </div>

                              
                            <div class="col-sm-12">
                               <label for="">{{ trans('lazena.email') }}</label>
                              
                               <p style="word-break:break-word;">{{ $customer->email }}</p>
                            </div>

                            <div class="col-sm-6">
                               <label for="">{{ trans('lazena.telephone') }}</label>
                            
                               <p>{{ $customer->telephone }}</p>
                            </div>

                            <div class="col-sm-6">
                               <label for="">{{ trans('lazena.city') }}</label>
                            
                               <p>{{ $customer->city }}</p>
                            </div>

                            <div class="col-sm-6">
                               <label for="">{{ trans('lazena.country') }}</label>
                            
                               <p>{{ $customer->country }}</p>
                            </div>

                            <div class="col-sm-6">
                               <label for="">{{ trans('lazena.gender') }}</label>
                            
                               <p>{{ $customer->gender }}</p>
                            </div>

                            <div class="col-sm-6">
                               <label for="">Registiration Date</label>
                            
                               <p>{{ date("Y - m - d" , strtotime("$customer->created_at")) }}</p>
                            </div>


                        </div>
                      
                    </div>
                  </div>
                </div>
                <!-- ./ box -->

               {{--   
                <div class="col-md-6 mb-30">
                  <div class="box-in half">
                    <div class="box-title"> 
                      <h5>Newsletters</h5>
                      <a href="#" class="pull-right">Edit</a>
                    </div>
  
                    <div class="box-in-content">
                        <div class="row">
                          <div class="col-sm-12">
                              @if(count($customer_newsletter) > 0 )
                                  <p> You are subscribed to our newletter with the below information</p>
                                  <label for="">Email Address</label>
                                  <p>{{ $customer_newsletter->email }}</p>
                                  
                              @else
                                  <p> You are not  subscribed in our newsletter  </p>
                              @endif 
                          </div>
                        </div>
                      
                    </div>
                  </div>
                </div>
                  --}}

                
              </div>

            </div>
          </div>
          <!-- ./ content account -->

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->


@endsection 

 



@section('scripts')
      
      
  
@endsection 





