  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


        $routeName =  \Illuminate\Support\Facades\Route::getFacadeRoot()->current()->getName() ; 


  @endphp


  <!-- sidebar account -->
      <div class="col-md-3">
        <div class="sidebar-account">

          <div class="block-title">
            <h4>{{ trans('lazena.my_account') }}</h4>
          </div>

          <div class="block-content-account">
            <ul>
              <li ><a  class="{!! $routeName == 'customer_dashboard' ? 'active':'' !!}"  href="{{ url("$lang/customer/dashboard") }}"><i class="fa fa-caret-right" aria-hidden="true"></i>
                {{ trans('lazena.account_dashboard') }}</a></li>
              <li><a class="{!! $routeName == 'customer_information' ? 'active':'' !!}" href='{{ url("$lang/customer/account_information/edit") }}'><i class="fa fa-caret-right" aria-hidden="true"></i>
                {{ trans('lazena.account_info') }}</a></li>
             {{--  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>
                Address Book</a></li> --}}
              <li><a class="{!! $routeName == 'myOrders' ? 'active':'' !!}" href="{{ url("$lang/customer/my-orders") }}"><i class="fa fa-caret-right" aria-hidden="true"></i>
               {{ trans('lazena.account_orders') }}</a></li>
              <li><a href='{{ url("$lang/customer/shopping-cart") }}' class="{!! $routeName == 'shopping_cart' ? 'active':'' !!}"><i class="fa fa-caret-right" aria-hidden="true"></i>
               {{ trans('lazena.account_shopping_cart') }}</a></li>
              <li><a href="{{ url("$lang/customer/wishlist") }}" class="{!! $routeName == 'whislist' ? 'active':'' !!}"><i class="fa fa-caret-right" aria-hidden="true"></i>
                {{ trans('lazena.account_wishlist') }}</a></li>
             {{--  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>
                Newsletter Subscriptions</a></li> --}}
            </ul>
          </div>
        </div>
      </div>
  <!-- ./ sidebar account -->