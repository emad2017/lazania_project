@extends('frontend.layout')


@section('page_title' ,   trans('lazena.my_orders'))


@section('styles')
  
    <style>
      
           .h4break{

              /*text-overflow: ellipsis;     */
              word-break: break-word ;
              overflow: hidden;     
              width: 80%;    
              /*white-space: nowrap;*/


          } 





    </style>


     <style media="screen">
      table td {
        padding: 8px;
      }
    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


@section('pages_content')
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{  trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("/$lang/customer/dashboard") }}">{{  trans('lazena.customer') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li> {{ trans('lazena.my_orders') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    

    


            <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
         
             <!-- sidebar account -->
          
            <!-- inject customer sidebar into dom  --> 
            @include('frontend.customer.partials.sidebar')  

          <!-- ./ sidebar account -->
          <!-- content account -->
       {{--    <div class="col-md-9">
            
               @if(Session::has('order_placed'))
                  <div class="alert alert-success alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>    </strong> {{ Session::get('order_placed') }} 
                  </div>
                @endif


            <div class="content-account">
              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.my_orders') }}</h3>
              </div>

              <div class="col-md-12">
                <div class="row">
                  <div class="order col-sm-12 mb-30">
                    <ul>
                      <li class="head">
                        <div class="order">{{ trans('lazena.order') }}</div>
                        <div class="data">{{ trans('lazena.date') }}</div>
                        <div class="ship">{{ trans('lazena.ship_to') }}</div>
                        <div class="order-total">{{ trans('lazena.order_total') }}</div>
                        <div class="order-total">{{ trans('lazena.order_status') }}</div>
                        <div class="view-order"></div>
                      </li>
                    

                    @if(count($orders) > 0)
                      @foreach($orders as $order)     
                          
                          @php 

                              $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 
                              // dd($shipping_to->lname);

                          @endphp 
                          <li class="content">
                            <div class="order h4break"> {{ $order->order_ticket }}</div>
                            <div class="data h4break">{{ date('Y - m - d' , strtotime($order->order_date))  }}</div>
                            <div class="ship h4break">{{ $shipping_to->fname  }} &nbsp; {{ $shipping_to->lname  }}</div>
                            <div class="order-total h4break">{{  $order->order_total }} S.R</div>
                            <div class="order-total h4break">

                                   @if($order->order_status == 'pending')
                                       {{ trans('lazena.pending') }} 
                                   @elseif($order->order_status == 'accepted')
                                       {{ trans('lazena.accepted') }} 
                                   @else
                                       {{ trans('lazena.rejected') }} 
                                   @endif 
                                
                            </div>
                            <div class="view-order h4break"> <a href="{{ url("$lang/customer/my-orders/$order->order_ticket/details") }}">{{  trans('lazena.view_order') }}</a> </div>

                          </li>
                       
                      @endforeach
                           
                    @else
                 
                        <li class=" content text-center" >
                           <div class="  text-center">
                            <strong>    </strong>   {{  trans('lazena.no_orders') }}
                          </div>
                        </li>

                    @endif

                      

                    </ul>
                  </div>

                  

                </div>
              </div>
            </div>
          </div> --}}
          <!-- ./ content account -->


           <!-- content account -->
          <div class="col-md-9">

             @if(Session::has('order_placed'))
                  <div class="alert alert-success alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>    </strong> {{ Session::get('order_placed') }} 
                  </div>
                @endif

            <div class="content-account">
              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.my_orders') }}</h3>
              </div>

              <div class="col-md-12">
                <div class="row">
                  <div class="order col-sm-12 mb-30">
                    <table border="1" style="width:100%;">
                      <thead style="background-color: #f9f9f9;">
                        <tr>
                          <td>{{ trans('lazena.order') }}</td>
                          <td>{{ trans('lazena.date') }}</td>
                          <td>{{ trans('lazena.ship_to') }}</td>
                          <td>{{ trans('lazena.order_total') }}</td>
                          <td> {{ trans('lazena.order_status') }}</td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>

  
                     @if(count($orders) > 0)
                      @foreach($orders as $order)     
                          
                          @php 

                              $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 
                              // dd($shipping_to->lname);

                          @endphp 


                              <tr>
                                <td> {{ $order->order_ticket }}</td>
                                <td>{{ date('Y - m - d' , strtotime($order->order_date))  }}</td>
                                <td>{{ $shipping_to->fname  }} &nbsp; {{ $shipping_to->lname  }}</td>
                                <td>{{  $order->order_total }} {{ trans('lazena.s_r') }}</td>
                                <td>{{ trans('lazena.accepted') }}</td>
                                <td> <a href="{{ url("$lang/customer/my-orders/$order->order_ticket/details") }}" class="btn btn-success btn-sm">{{  trans('lazena.view_order') }}</a> </td>
                              </tr>


                        @endforeach


                        @else
                          
                          <tr>
                            <td class="text-center" colspan="6"> {{  trans('lazena.no_orders') }}</td>
                          </tr>

                        @endif
                        
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- ./ content account -->

     

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->



 

@endsection 

 



@section('scripts')
      
   
  
@endsection 





