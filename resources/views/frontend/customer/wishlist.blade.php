@extends('frontend.layout')


@section('page_title' , trans('lazena.wishlist'))


@section('styles')
  
    <style>
      
          .active{
            color: #f8408 ; 
          }


          .h4break{

              text-overflow: ellipsis;     
              overflow: hidden;     
              width: 65%;    
              white-space: nowrap;


          }


    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


@section('pages_content')
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("/$lang/customer/dashboard") }}">{{ trans('lazena.customer') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{ trans('lazena.wishlist') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
          <!-- sidebar account -->
          
            <!-- inject customer sidebar into dom  --> 
            @include('frontend.customer.partials.sidebar') 

          <!-- ./ sidebar account -->

            <!-- content account -->
          <div class="col-md-9">
            <div class="content-account">

                @if(Session::has('product_added_to_wishlist'))
                  <div class="alert alert-success alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>  <i class="fa fa-smile-o fa-2x" aria-hidden="true"></i> </strong>  {{ Session::get('product_added_to_wishlist') }} 
                  </div>
                @endif


                @if(Session::has('product_added_before'))
                  <div class="alert alert-info alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>   <i class="fa fa-hand-o-right fa-2x" aria-hidden="true"></i> </strong> {{ Session::get('product_added_before') }} 
                  </div>
                @endif


                <div id="product_removed" class="alert alert-info alert-dismissable text-center hidden ">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>  <i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> </strong>  {{ trans('lazena.product_removed') }}  
                </div>

              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.wishlist') }}</h3>
              </div>

              <div class="col-md-12 wishlist">
                <div class="row">
                  <div class="col-sm-12 mb-30">
                    <ul id="wishlistProductsUl">
                      <!-- Header -->
                      <li>
                        <div class="product-name">
                          <h4>{{ trans('lazena.item') }}</h4>
                        </div>

                        <div class="product-price">
                          <h4>{{ trans('lazena.price') }}</h4>
                        </div>

                        <div class="product-add">
                          <h4>{{ trans('lazena.add_to_cart') }}</h4>
                        </div>

                        <div class="product-remove">
                          <h4>{{ trans('lazena.remove') }}</h4>
                        </div>

                      </li>
                      <!-- ./ Header -->



                      <!-- this is our looping part  --> 

                      @if(count($result) > 0 )

                          @foreach($result as $productDescription)
                                
                            @php 

                                $product_id          = $productDescription->dessertitem->id ;
                                $product_image       = $productDescription->dessertitem->img1 ;
                                $product_image_path  = asset("uploads/dessertsitem/610_383/").'/'.$product_image ; 
                                $product_price       = $productDescription->dessertitem->price ;
                                $product_quantity       = $productDescription->dessertitem->quantity ;
                                $product_name       = ucfirst($productDescription->name) ;  


                            @endphp

                                <!-- Product -->
                                <li class="product-item">
                                  <div class="product-name">
                                    <a href="{{ asset("uploads/dessertsitem/original/$product_image") }}" class="fresco">
                                        <img  style="height: 50px; width: 50px; border-radius:30%; "  src="{{ asset("uploads/dessertsitem/640_640/$product_image") }}" alt="">
                                    </a>
                                    <h4 class="h4break" >{{ $product_name }}</h4>
                                  </div>

                                  <div class="product-price">
                                    <h4>{{ trans('lazena.s_r') }} {{ $product_price }}</h4>
                                  </div>

                                  <div class="product-add">
                                    <a class="button button-hot hvr-shutter-out-horizontal cd-add-to-cart to_be_hide_{{ $product_id }}" product-id="{{ $product_id }}"  product-price="{{ $product_price }}" product-quantity="{{ $product_quantity }}"  product-image="{{ $product_image_path }}" product-name="{{ $product_name  }}" >{{ trans('lazena.add_to_cart') }}</a>

                                     <a class=" button button-hot hvr-shutter-out-horizontal   hidden item_{{ $product_id }}"><i class="fa fa-check" aria-hidden="true"></i></a>

                                  </div>

                                  <div class="product-remove">
                                    <i class="fa fa-times removeProduct" data-product-id="{{ $product_id }}" aria-hidden="true"></i>
                                  </div>

                                </li>
                                <!-- ./ Product -->

                          @endforeach

                      @else
                              
                              <div  class="col-sm-12 col-md-12 col-lg-12 mt-30 mb-30  ">
                                  <div class="alert alert-warning alert-dismissable text-center">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>   <i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> </strong> {{ trans('lazena.no_item_in_wishlist') }}
                                  </div>
                              </div>
                            
                      @endif


                      <!-- end our looping part man  -_- -->
                            
                             <div id="empty_wishlist_after_delete"  class="col-sm-12 col-md-12 col-lg-12 mt-30 mb-30 hidden  ">
                                  <div class="alert alert-warning alert-dismissable text-center">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>   <i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> </strong> {{ trans('lazena.no_item_in_wishlist') }}
                                  </div>
                              </div>
                     

                    </ul>
                  </div>

                 
                </div>
              </div>
            </div>
          </div>
          <!-- ./ content account -->

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->


@endsection 

 



@section('scripts')
      
      <script>
          
          // logic to go through remove product from wishlist -_- 
          
          $(document).ready(function(){

              $('.removeProduct').on('click' , function (){

                 var element  = $(this); 
                 var product_id = $(this).attr('data-product-id');

                 var token = $('input[name="_token"]').val(); 


                 $.ajax({

                  url:'{{ url("$lang/customer/wishlist/delete") }}' , 
                  method:'POST',
                  dataType:'json', 
                  data:{_token:token , product_id:product_id}, 
                  beforeSend:function(){

                    $('ul#wishlistProductsUl').LoadingOverlay("show" , {
                      image       : "",
                      fontawesome : "fa fa-spinner fa-spin"
                    });

                  }, 
                  success:function(data){

                    if(data.status == 'product_removed'){

                        $('ul#wishlistProductsUl').LoadingOverlay("hide" , {
                          image       : "",
                          fontawesome : "fa fa-spinner fa-spin"
                        });
                        element.closest('ul li.product-item').fadeOut( "fast" , function(){
                                            $(this).remove();  
                         });

                        

                         $('#product_removed').fadeIn('fast' , function(){

                              $(this).removeClass('hidden'); 


                         });

                         setTimeout(function(){  
                           $('#product_removed').fadeOut('fast' , function(){

                                $('#product_removed').addClass('hidden');   

                           });

                         }, 2000);


                         // check to see if this is the last li of type product-item
                         if($('ul#wishlistProductsUl li.product-item').length == 1){

                              setTimeout(function(){

                                $('#empty_wishlist_after_delete').fadeIn('fast' , function(){

                                  $(this).removeClass('hidden');
                                
                                });

                              } , 2005);  
                                
                          
                         } 

                    }

                  }


                 }); 
                 return false; 

              });

          });


      </script>
  
@endsection 





