@extends('frontend.layout')


@section('page_title' , trans('lazena.order_details'))


@section('styles')
  
    <style>
      
           .h4break{

              /*text-overflow: ellipsis;     */
              word-break: break-word ;
              overflow: hidden;     
              width: 80%;    
              /*white-space: nowrap;*/


          } 


    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


@section('pages_content')
 

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("/$lang/customer/dashboard") }}">{{ trans('lazena.customer') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li><a href="{{ url("/$lang/customer/my-orders") }}">{{ trans('lazena.my_orders') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{ trans('lazena.order_details') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

    
  <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
              <!-- sidebar account -->
          
                <!-- inject customer sidebar into dom  --> 
                @include('frontend.customer.partials.sidebar')  

              <!-- ./ sidebar account -->

          <!-- content account -->
          <div class="col-md-9">
            <div class="content-account order-id">
              <div class="col-md-12 block-title mb-30">
                <h3>{{ trans('lazena.ticket') }} :  {{ $order->order_ticket }} </h3>
                <div class="link">
            
                  <a onclick="printOrder();" href="#">{{ trans('lazena.print') }}</a>
                </div>
              </div>

              <div class="col-md-12">
                <p>{{ trans('lazena.order_inf') }}</p>
              </div>

              <div class="box-account-info">
                <div class="col-md-12 block-title">
                  <h4>{{ trans('lazena.date') }} : {{ date('l jS \of F Y h:i:s A' , strtotime($order->order_date)) }}</h4>
                </div>
 

                <div class="col-md-12 p-l-0">
                  <!-- box -->
                  <div class="col-md-12 mb-30">
                    <div class="box-in half">
                      <div class="box-title">
                        <h5>{{ trans('lazena.ship_address') }}</h5>
                      </div>

                      <div class="box-in-content">
                        @php 

                          $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 

                        @endphp
                        <label for="" class="col-md-6">{{ trans('lazena.ship_to') }}</label>
                        <p> {{ $shipping_to->fname }} {{ $shipping_to->lname }} </p>

                        <label for="" class="col-md-6">{{ trans('lazena.ship_address') }}</label>
                        <p>{{ $shipping_to->street }}</p>

                        <label for="" class="col-md-6">{{ trans('lazena.country') }}</label>
                        <p>{{ $shipping_to->country }}</p> 

                        <label for="" class="col-md-6">{{ trans('lazena.gender') }}</label>
                        <p>{{ ucfirst($shipping_to->gender)  }}</p>

                        <label for="" class="col-md-6">{{ trans('lazena.telephone') }}</label>
                        <p>{{ $shipping_to->telephone }}</p>

                         
                      </div>
                    </div>
                  </div>
                  <!-- ./ box -->

                 
                </div>

                <div class="col-md-12 block-title">
                  <h4>{{ trans('lazena.items_ordered') }} </h4>
                </div>

                <div class="col-md-12 item-order">
                  <ul>
                    <li class="head">
                      <div class="product-name">{{ trans('lazena.product_name') }}</div>
                   
                      <div class="price">{{ trans('lazena.price') }} </div>
                      <div class="qty">{{ trans('lazena.qty') }} </div>
                      <div class="subtotal">{{ trans('lazena.sub_total') }}</div>
                    </li>
                  
                  @php 

                      $orders = \App\OrderProduct::where('order_id' , $order->order_ticket)->get(); 



                  @endphp 

                    @if(count($orders))
                        @foreach($orders as $order_pivot)
                          @php  $product =  $order_pivot->product ;   @endphp
                              <li class="content">
                              @foreach($product->description as $description) 
                                    
                                  @if($description->language_id == $language_id)
                                     <div class="product-name">{{ $description->name }}</div>
                                  @endif 
                              @endforeach
                                <div class="price"> {{ trans('lazena.s_r') }} {{ $product->price }} </div>
                                <div class="qty">   {{ $order_pivot->quantity }}</div>
                                <div class="subtotal"> {{ trans('lazena.s_r') }} {!!  $order_pivot->quantity * $product->price   !!}</div>
                              </li>


                      @endforeach

                  @endif

                    <li class="content-2">
                      <div class="qty"> <p>{{ trans('lazena.sub_total') }} </p>
                        <p>{{ trans('lazena.delivery') }}</p>
                        <strong> {{ trans('lazena.total') }} </strong> </div>
                      <div class="subtotal">
                        <p> {{ trans('lazena.s_r') }} {!! $order->order_total - 15  !!} </p>
                         <p> {{ trans('lazena.s_r') }} 15.00 </p>
                         <strong> {{ trans('lazena.s_r') }} {{ $order->order_total }} </strong>
                      </div>
                    </li>
                  </ul>
                </div>

               

               
              </div> <!-- box-account-info -->

              
              
            </div>
          </div>
          <!-- ./ content account -->

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->


 

@endsection 

 



@section('scripts')
      
    <script>
          
          function printOrder() {
              window.print();
          }

    </script>
  
@endsection 





