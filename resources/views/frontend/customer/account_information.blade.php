@extends('frontend.layout')


@section('page_title' , trans('lazena.update_account'))


@section('styles')
  
    <style>
      
          .active{
            color: #f8408 ; 
          }

          .form_error{
          
            display: block;
            margin-top: -25px;
            margin-bottom: 10px;
            color: #d22e2e;

          }

          .form_error small{
            word-break: break-word ; 
          }


    </style>

@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;


       // we will not come here if no authentication found  
       // so it will be stupid if we checked for login first or not  -_- 
       $customer = Auth::guard('customer')->user(); 

       // concatenate firsty name with last name  
       $customer_username = $customer->fname . ' ' . $customer->lname ; 


  @endphp


@section('pages_content')

    <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
               <li><a href="{{ url("/$lang/customer/dashboard") }}">{{ trans('lazena.customer') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{  trans('lazena.update_account') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

        <!-- ====== Start account ======  -->
    <section class="account section-padding mt-50">
      <div class="container">
        <div class="row">
           

            <!-- sidebar account -->
          
              <!-- inject customer sidebar into dom  --> 
              @include('frontend.customer.partials.sidebar') 

            <!-- ./ sidebar account -->

          <!-- content account -->
          <div class="col-md-9">
              <div class="content-account">

                 @if(Session::has('update_account_successfull'))
                <!-- sucessfull registration alert --> 
                  <div class="alert alert-success alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> </strong> {{ Session::get('update_account_successfull') }}
                  </div>

                @endif

              <div class="col-md-12 block-title mb-30">
                <h3>{{  trans('lazena.update_account_intro') }}</h3>
              </div>

              <div class="col-md-12">
                <div class="row">

                  {!! Form::model( $customer ,  ['route'=>["updateAccount" , $customer->id] , 'method'=>'POST' , 'class'=>'account-information box-shadow-3' , 'id'=>'update_account_form']) !!}
                    {!! method_field('PUT') !!}
                
                    <!-- box title -->
                    <div class="col-sm-12 mb-30 box-title">
                      {{-- <h5>Account Information</h5> --}}
                    </div>
                    <!-- box title -->

                    <div class="col-sm-6">
                      <label for="fname">{{ trans('lazena.fname') }}</label>
                      <input type="text" value="{{ $customer->fname }}" name="fname" id="fname">

                        @if($errors->has("fname"))
                          <span class="form_error"><small>{{ $errors->first("fname") }}</small></span>
                      @endif
                    </div>

                    <div class="col-sm-6">
                      <label for="lname">{{ trans('lazena.lname') }}</label>
                      <input type="text" value="{{ $customer->lname }}" name="lname" id="lname">

                        @if($errors->has("lname"))
                          <span class="form_error"><small>{{ $errors->first("lname") }}</small></span>
                      @endif
                    </div>

                    <div class="col-sm-12">
                      <label for="email">{{ trans('lazena.email') }}</label>
                      <input type="text" value="{{ $customer->email }}" name="email" id="email">

                        @if($errors->has("email"))
                          <span class="form_error"><small>{{ $errors->first("email") }}</small></span>
                      @endif

                    </div>


                    <div class="col-sm-6">
                      <label for="city">{{ trans('lazena.city') }}</label>
                      <input type="text" value="{{ $customer->city }}" name="city" id="city">

                        @if($errors->has("city"))
                          <span class="form_error"><small>{{ $errors->first("city") }}</small></span>
                      @endif

                    </div>

                    <div class="col-sm-6">
                      <label for="country">{{ trans('lazena.country') }}</label>
                      <input type="text" value="{{ $customer->country }}" name="country" id="country">

                        @if($errors->has("country"))
                          <span class="form_error"><small>{{ $errors->first("country") }}</small></span>
                      @endif
                    </div>

                    <div class="col-sm-12">
                      <label for="street">{{ trans('lazena.street') }}</label>
                      <input type="text" value="{{ $customer->street }}" name="street" id="street">

                        @if($errors->has("street"))
                          <span class="form_error"><small>{{ $errors->first("street") }}</small></span>
                      @endif
                    </div>                    

                    <div class="col-sm-6 mb-30 gender">
                      <label for="name">{{ trans('lazena.gender') }}</label>
                      <div class="input-areaa">
                        <input class="radio" title="Male" value="male" name="gender" {!! $customer->gender == 'male' ? 'checked':'' !!} type="radio">{{ trans('lazena.male') }}
                        <input class="radio validate-one-required" title="Female" value="female" name="gender" {!! $customer->gender == 'female' ? 'checked':'' !!}  type="radio">{{ trans('lazena.female') }}
                      </div>
                    </div>
          
                    <div class="col-sm-6">
                      <label for="telephone">{{ trans('lazena.telephone') }}</label>
                      <input type="text" value="{{ $customer->telephone }}" name="telephone" id="telephone">

                        @if($errors->has("telephone"))
                          <span class="form_error"><small>{{ $errors->first("telephone") }}</small></span>
                      @endif

                    </div>


                    <div class="col-sm-12">
                      <label for="password">{{ trans('lazena.password') }}</label>
                      <input type="text" name="password" value="{{ old('password') }}"  id="account_password">

                         
                          <span id="password_notice" class="form_error">
                              <i id="spinner" class="fa fa-spinner fa-spin fa fa-fw hidden"></i><span class="sr-only">Loading...</span>
                              <small></small>
                          </span>
                        
                    </div>

                   

                    <div class="col-sm-12 buttons-set">
                     {{--  <a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back Link</a> --}}
                      <button type="submit" id="submit_form" class="button button-hot hvr-shutter-out-horizontal pull-right">{{ trans('lazena.update_account') }}</button>

                    </div>
                  {{-- </form> --}}
                  {!! Form::close() !!}
                </div>
              </div><!-- end col-md-12 -->
                

                
                
                <div class="col-md-12 block-title mb-30">
                 
                  @if(Session::has('update_password_successfull'))
                
                  <div class="alert alert-success alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong></strong> {{ Session::get('update_password_successfull') }}
                  </div>

                @endif

                  <h3>{{ trans('lazena.update_password') }} </h3>
                </div>
              
        
                 
              <div class="col-md-12">
                <div class="row">

                  {!! Form::model( $customer ,  ['route'=>["updateAccountPassword" , $customer->id] , 'method'=>'POST' , 'class'=>'account-information box-shadow-3' , 'id'=>'update_account_password']) !!}
                    {!! method_field('PUT') !!}
                
                    

                    <div class="col-sm-6">
                      <label for="street">{{ trans('lazena.new_password') }}</label>
                      <input type="text" value="{{ old('new_pass') }}" name="new_pass" id="new_pass">

                        @if($errors->has("new_pass"))
                          <span class="form_error"><small>{{ $errors->first("new_pass") }}</small></span>
                      @endif
                    </div>                    

                     
          
                    <div class="col-sm-6">
                      <label for="telephone">{{ trans('lazena.new_password_confirm') }}</label>
                      <input type="text" value="{{ old('new_pass_confirmation') }}" name="new_pass_confirmation" id="new_pass_confirmation">

                        @if($errors->has("new_pass_confirmation"))
                          <span class="form_error"><small>{{ $errors->first("new_pass_confirmation") }}</small></span>
                      @endif

                    </div>


                    <div class="col-sm-6">
                      <label for="password">{{ trans('lazena.old_password') }}</label>
                      <input type="text" name="old_password" value="{{ old('old_password') }}"  id="account_old_password">
 
                        
                    </div>

                    <div class="col-sm-6">
                        
                        <span id="password_notice_password" class="form_error">
                              <i id="spinner_password" class="fa fa-spinner fa-spin fa fa-fw hidden"></i><span class="sr-only"> </span>
                              <small></small>
                          </span>



                    </div>

                   

                    <div class="col-sm-12 buttons-set">
                     {{--  <a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Back Link</a> --}}
                      <button type="submit" id="submit_form_password" class="button button-hot hvr-shutter-out-horizontal pull-right">{{ trans('lazena.update_password') }}</button>

                    </div>
                  {{-- </form> --}}
                  {!! Form::close() !!}
                </div>
              </div>

 

            </div>
          </div>
          <!-- ./ content account -->


          

        </div>
      </div>
    </section>
    <!-- ====== ./ account ======  -->

@endsection  




@section('scripts')

  <script>
      /**
       * what am doing here i will prevent  form submittion unless the customer provides a correct password 
       * if password was entered by customer i will send ajax request to my database to check if  the entered password matches the 
       * one stored in database  and catch the response  
       * else i will submit the form like normal -_-
       */
      $(document).ready(function(){

          // account update logic 
          
          $('#submit_form').on('click' , function(e){

              e.preventDefault(); 

              // console.log('form submittion interrupted') ; 
              var password = $('#account_password').val(); 
              var customer_id = "{{ Auth::guard('customer')->user()->id }}"; 
             

              if( password.length  ===  0  ){

                  //  i have make sure that password is leaved empty 
                  $('#password_notice').removeClass('hidden'); 
                  $('#password_notice small').fadeIn('slow').text('Sorry , but you have to submit your account password to be able to update your account') ; 
                    
                     
              }else{

                // else here means that there was a password submition 
                // so we are going to send an ajax request to check if the submitted password 
                // equal this current customer password  -_- 
                $.ajax({

                  url:"{{ url("$lang/customer/check_update_account") }}",
                  method:'GET' , 
                  dataType:'json' , 
                  data:{customer_id:customer_id , password:password}, 
                  beforeSend:function(){
                      $('#spinner').removeClass('hidden');
                  }, 
                  success:function(data){

                     if(data.status == "match"){

                          // do your logic  here cause password match found  -- means submit my form  
                          $( "#update_account_form" ).submit();

                     }else{

                        // handle the mismatch found  -_- 
                        $('#password_notice small').fadeIn('slow').text('Ooh , we have conflict here cause this is not the password used during login , please enter the correct password') ; 

                        // hide the spinner again  
                        $('#spinner').addClass('hidden');

                     }
                  }


                });

               

              }

          });


          // password update logic 
          
           $('#submit_form_password').on('click' , function(e){

              e.preventDefault(); 

              // console.log('form submittion interrupted') ; 
              var password = $('#account_old_password').val(); 
              var customer_id = "{{ Auth::guard('customer')->user()->id }}"; 
             

              if( password.length  ===  0  ){

                  //  i have make sure that password is leaved empty 
                  $('#password_notice_password').removeClass('hidden'); 
                  $('#password_notice_password small').fadeIn('slow').text('Sorry , but you have to submit your old account password before going with a new password') ; 

                
                   
              }else{

             
                // else here means that there was a password submition 
                // so we are going to send an ajax request to check if the submitted password 
                // equal this current customer old password  -_- 
                $.ajax({

                  url:"{{ url("$lang/customer/check_update_account") }}",
                  method:'GET' , 
                  dataType:'json' , 
                  data:{customer_id:customer_id , password:password}, 
                  beforeSend:function(){
                      $('#spinner_password').removeClass('hidden');
                  }, 
                  success:function(data){

                     if(data.status == "match"){

                          // do your logic  here cause password match found  -- means submit my form  
                          $( "#update_account_password" ).submit();

                     }else{
                        console.log('mismatch');
                        // handle the mismatch found  -_- 
                        $('#password_notice_password small').fadeIn('slow').text('Ooh , Please make sure that you entered the old password !') ; 

                        // hide the spinner again  
                        $('#spinner_password').addClass('hidden');

                     }
                  }


                });

               

              }

                  

          });

      });

  </script>

@endsection  