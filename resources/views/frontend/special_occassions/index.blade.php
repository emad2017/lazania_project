@extends('frontend.layout')


@section('page_title' , "$occassion_content->name")


@section('styles')


@endsection 

  @php 

       $lang = LaravelLocalization::getCurrentLocale();
       $language = \App\Language::where(['label'=>$lang])->first();
       $language_id = $language->id ;



  @endphp


@section('pages_content')

   <!-- ====== Start Product header======  -->
    <div class="Product-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-2">
            <ul>
              <li><a href="{{ url("/$lang") }}">{{ trans('lazena.home') }}</a><i class="fa fa-chevron-right" aria-hidden="true"></i>
              </li>
              <li>{{ trans('lazena.special_occassion') }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ====== ./ Product header  ======  -->

   

     <!-- ======  services  ======  -->
    <section class="services section-padding">
      <div class="container">
        <div class="row">
          <!-- Title -->
          <div class="section-title">
            <h3>{{ $occassion_content->name }}</h3>
          </div>

          <div class="col-sm-12">
            <hr class="section">
          </div>

          <div class="services-block">
            <p>   {!! $occassion_content->description !!}  </p>
            <br>

              @php   $image = $occassion_content->specialevent->img ;   @endphp
               
               <div class="col-md-offset-2 col-md-8">   
                  <a href="{{ asset("uploads/speacialevent/$image") }}" class="fresco text-center">
                    <img  style="max-width: 1000px;" src="{{ asset("uploads/speacialevent/$image") }}" alt="">
                  </a>
              </div>

          </div>

          

         
        </div>
      </div>
    </section>
    <!-- ====== ./ services ======  -->

    

@endsection 

 



@section('scripts')
      
      
  
@endsection 





