<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ trans('lazena.login_page') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
     
    <link rel="icon" type="image/png" href="{{ asset('ellelazena_login/assets/images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> --}}
    {{ Html::style('ellelazena_login/assets/vendor/bootstrap/css/bootstrap.min.css') }}
<!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"> --}}
    {{ Html::style('ellelazena_login/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}
<!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"> --}}
    {{ Html::style('ellelazena_login/assets/vendor/animate/animate.css') }}
<!--===============================================================================================-->  
    {{-- <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> --}}
    {{ Html::style('ellelazena_login/assets/vendor/css-hamburgers/hamburgers.min.css') }}
<!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> --}}
    {{ Html::style('ellelazena_login/assets/vendor/select2/select2.min.css') }}
<!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="css/util.css"> --}}
    {{ Html::style('ellelazena_login/assets/css/util.css') }}
    {{-- <link rel="stylesheet" type="text/css" href="css/main.css"> --}}
    {{ Html::style('ellelazena_login/assets/css/main.css') }}
<!--===============================================================================================-->

    @php 

            $lang = LaravelLocalization::getCurrentLocale() ;  

    @endphp 


    @if( $lang == 'ar')

           <link href="https://fonts.googleapis.com/css?family=Baloo+Bhaijaan&amp;subset=arabic,latin-ext" rel="stylesheet">

            
            <style>
                
               .login100-form-title , .input100  , .login100-form-btn , .in_arabic{

                    font-family: 'Baloo Bhaijaan', cursive;
                }
            </style>
        

    @endif



    <style>
        
        
        .form_error{

                
                display: block;
                position: absolute;
                border-radius: 25px;
           
                width: 100%;
                height: 100%;
                box-shadow: 0px 0px 0px 0px;
                color: rgba(184, 70, 90, 0.8);

        }



    </style>


</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="{{ asset('ellelazena_login/assets/images/img-01.png') }}" alt="IMG">
                </div>
                
                {!! Form::open(['url'=>"$lang/authenticate" , 'class'=>'login100-form validate-form' , 'method'=>'POST']) !!}
                <form class="login100-form validate-form">
                    <span class="login100-form-title">
                        {{ trans('lazena.member_login') }}
                    </span>


                      @if($errors->any())

                        <div class="alert alert-danger alert-dismissable text-center in_arabic">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong> </strong> {{$errors->first()}}
                        </div>

                         
                      @endif

                    <div class="wrap-input100 validate-input" data-validate = "{{ trans('lazena.email_required') }}">
                        <input class="input100 " type="text" name="email"  placeholder="{{ trans('lazena.email') }}">
                        <span class="focus-input100  "></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "{{ trans('lazena.password_required') }}">
                        <input class="input100 " type="password" name="password" placeholder="{{ trans('lazena.password') }}">
                        <span class="focus-input100 "></span>
                        <span class="symbol-input100 ">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            {{ trans('lazena.login') }}
                        </button>
                    </div>

                  {{--   <div class="text-center p-t-12">
                        <span class="txt1">
                            Forgot
                        </span>
                        <a class="txt2" href="#">
                            Username / Password?
                        </a>
                    </div> --}}

                   {{--  <div class="text-center p-t-136">
                        <a class="txt2" href="#">
                            Create your Account
                            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                        </a>
                    </div> --}}
                </form>
            </div>
        </div>
    </div>
    
    

    
<!--===============================================================================================-->  
    {{-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> --}}
    {!! Html::script('ellelazena_login/assets/vendor/jquery/jquery-3.2.1.min.js') !!}
<!--===============================================================================================-->
    {{-- <script src="vendor/bootstrap/js/popper.js"></script> --}}
    {!! Html::script('ellelazena_login/assets/vendor/bootstrap/js/popper.js') !!}
    {{-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> --}}
    {!! Html::script('ellelazena_login/assets/vendor/bootstrap/js/bootstrap.min.js') !!}
<!--===============================================================================================-->
    {{-- <script src="vendor/select2/select2.min.js"></script> --}}
    {!! Html::script('ellelazena_login/assets/vendor/select2/select2.min.js') !!}
<!--===============================================================================================-->
    {{-- <script src="vendor/tilt/tilt.jquery.min.js"></script> --}}

    {!! Html::script('ellelazena_login/assets/vendor/tilt/tilt.jquery.min.js') !!}
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
    {{-- <script src="js/main.js"></script> --}}
    {!! Html::script('ellelazena_login/assets/js/main.js') !!}

</body>
</html>