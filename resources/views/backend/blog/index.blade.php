@extends('backend.app')

@section('page_title' , trans('dessertdepartment.blog'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.blog') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('dessertdepartment.name_blog') }}</th>
                        <th>{{ trans('dessertdepartment.description_blog') }}</th>
                        <th>{{ trans('dessertdepartment.img_blog') }}</th>
                        <th>{{ trans('dessertdepartment.blog_dep') }}</th>


                        <th>{{ trans('dessertdepartment.dessert_department_addby') }}</th>

                        <th>{{ trans('dessertdepartment.dessert_department_status') }}</th>
                        <th>{{ trans('dessertdepartment.f') }}</th>
                        <th>{{ trans('dessertdepartment.t') }}</th>
                        <th>{{ trans('dessertdepartment.g') }}</th>



                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($blogs->count())


                        @foreach($blogs as $blog)




                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @foreach($blog->description as $description)
                                        {{ $description->name }} <br>
                                    @endforeach
                                </td>





                                <td>
                                    <span class="">
                                         @foreach($blog->description as $description)
                                            {{ $description->description }} <br>
                                        @endforeach

                                    </span>
                                </td>

                                <td>
                                    {{-- image show --}}


                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="{{ asset("uploads/blog/$blog->img") }}" data-effect="mfp-3d-unfold">
                                                <img style="width: 100px; height: 50px;" src="{{ asset("uploads/blog/$blog->img") }}" class="img-responsive" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>


                                    </div>

                                </td>

                                <td>
                                    {{-- Blog Department--}}

                                    @php
                                        //   dd($item->description);
                                    @endphp
                                    @foreach($blog->department->description as $de)

                                        <span> {{ $de->name}}</span>
                                        <br> <br>

                                    @endforeach

                                </td>



                                <td>
                                    <span class="label label-info ">{{ $blog->user->headPersonName }}</span>
                                </td>


                                <td>
                                    @if($blog->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>

                                <td> <a href="{{ $blog->facebook}}" target="_blank"> <i class="fa fa-facebook fa-lg"></i> </a> </td>
                                <td> <a href="{{ $blog->twitter}}" target="_blank"> <i class="fa fa-twitter fa-lg"></i> </a>
                                <td> <a href="{{ $blog->google}}" target="_blank"> <i class="fa fa-google fa-lg"></i> </a>


                                <td class="text-nowrap">






                                    <a  href="{{ route('blog.edit' , $blog->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.blog_{{ $blog->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    {!! Form::open(['route'=>["blog.destroy" , $blog->id ] ,
                                    'class'=>"blog_$blog->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.dessert_department_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}