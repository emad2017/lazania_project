@extends('backend.app')

@section('page_title' , trans('dessertdepartment.blog_dep'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.blog_dep') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>

                        <th>{{ trans('dessertdepartment.blog_dep') }}</th>
                        <th>{{ trans('dessertdepartment.dessert_department_addby') }}</th>
                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($blogdepartment->count())


                        @foreach($blogdepartment as $department)





                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @foreach($department->description as $description)
                                        {{ $description->name }} <br>
                                    @endforeach
                                </td>

                                <td>
                                    <span class="label label-info ">{{ $department->user->headPersonName }}</span>
                                </td>


                                <td class="text-nowrap">


                                    <a  href="{{ route('blogdepartment.edit' , $department->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.blog_{{ $department->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    {!! Form::open(['route'=>["blogdepartment.destroy" , $department->id ] ,
                                    'class'=>"blog_$department->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.dessert_department_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}