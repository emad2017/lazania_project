@extends('backend.app')

@section('page_title' , trans('dessertdepartment.dessert_name'))

@section('breadcrumb')

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.dessert_name') }}</h4>
    </div>

@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp



@section('content')


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> {{ trans('dessertdepartment.dessert_editting') }} </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">



                            <nav>
                                <ul>
                                    @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>

                                    @endforeach

                                </ul>
                            </nav>






                            {!! Form::model($item , ['route'=>['updateItem',$item->id],
                            'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'add_files']) !!}
                            {!! method_field('put') !!}


                                <div class="content-wrap">
                                    @foreach($item->description as $description)

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$language->label}}">



                                        <!--  Name -->


                                        <div class="form-group{{ $errors->has("dessert_department_name_".$language->label) ? ' has-error' : '' }}">
                                            <label for="">{{ trans('dessertdepartment.dessert_name_') }}</label>
                                            <div class="input-group">


                                                <input type="text" class="form-control" name="dessert_name_{{$description->language->label}}"
                                                       value="{{$description->name}}"

                                                       placeholder="{{ trans('dessertdepartment.dessert_department_name_') }}">


                                            </div>
                                        </div>





                                        <!--  Description -->


                                        <div class="form-group{{ $errors->has("dessert_department_description_$language->label") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('dessertdepartment.dessert_description_') }}</label>
                                            <div class="input-group">

                                                {{-- <input type="text" class="form-control" name="dessert_description_{{$description->language->label}}"
                                                       value="{{$description->description}}"

                                                       placeholder="{{ trans('dessertdepartment.dessert_department_description_') }}"> --}}


                                                <textarea name="dessert_description_{{$description->language->label}}" >{{$description->description}}</textarea>

                                            </div>
                                        </div>



                                    </section>


                                    @endforeach

                                        {{-- Price edit  --}}


                                        <div class="form-group col-md-12">
                                            <label for="">{{ trans('dessertdepartment.dessert_department_price') }}</label>

                                            <input type="text" class="form-control" name="price"
                                                   value="{{$item->price}}"
                                                   placeholder="{{ trans('dessertdepartment.dessert_department_price') }}">
                                        </div>


                                        <!--  Select item department of this item -->
                                        <div class="form-group">

                                            <label for=" "> &nbsp;&nbsp; {{ trans('dessertdepartment.related_to') }}</label>
                                            <div class="col-md-12">

                                                <select class="form-control" name="dessertitem"  >

                                                    @foreach($subDepts as $speacialdepartment)

                                                        @foreach( $speacialdepartment->description as $description)

                                                            @if($description->language_id ==  $language_id )

                                                                <option value="{{ $speacialdepartment->id}}"

                                                                        {!!
                                                                         $speacialdepartment->id == $item->subDeptId ?
                                                                         'selected' : ''
                                                                         !!}
                                                                       >
                                                                    {{$description->name}}


                                                                </option>



                                                            @endif

                                                        @endforeach



                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>

                                    {{-- image --}}

                                    <div class="form-group ">

                                        <label for=" ">  &nbsp; {{ trans('dessertdepartment.dessert_image_') }}</label>
                                        <div class="col-md-12">
                                            <div class="white-box">


                                                <input type="file" id="input-file-now" name="file"  class="dropify" />
                                            </div>
                                        </div>


                                    </div>
                                    

                                     <div class="form-group">
                                        <label for=" "> &nbsp; {{ trans('backend.show_in_slider') }} </label>

                                        <div class="col-md-12">
                                            &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" name="show_in_slider" {!! $item->show_in_slider == 1 ? 'checked':'' !!}  class="js-switch" data-color="#99d683" />
                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <label for=" "> &nbsp;  {{ trans('backend.best_selling') }}  </label>

                                        <div class="col-md-12">
                                            &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" name="best_selling" {!! $item->best_selling == 1 ? 'checked':'' !!}  class="js-switch" data-color="#99d683" />
                                        </div>

                                    </div>

                                   {{--  <div class="form-group">
                                        <label for=" "> &nbsp; Best Selling Promoted</label>

                                        <div class="col-md-12">
                                            &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" name="promoted" {!! $item->promoted == 1 ? 'checked':'' !!}  class="js-switch" data-color="#99d683" />
                                        </div>

                                    </div> --}}



                                     <div class="form-group">

                                        <label for=" "> &nbsp; {{ trans('backend.product_quantity') }}</label>

                                        <div class="col-md-12">
                                            &nbsp; &nbsp; &nbsp;
                                            <input type="number" name="quantity" value="{{ $item->quantity }}"  class="form-control"  />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                    <label for=" "> &nbsp;  {{ trans('backend.recomended_product') }}  </label>

                                    <div class="col-md-12">
                                        &nbsp; &nbsp; &nbsp;
                                        <input type="checkbox" name="recommended" {!! $item->recommended == 1 ? 'checked':'' !!}  class="js-switch" data-color="#99d683" />
                                    </div>

                                </div>





                                    {{-- Status of Department--}}
                                        <div class="form-group">
                                            <label for=" "> &nbsp; {{ trans('backend.status') }}</label>

                                            <div class="col-md-12">
                                                &nbsp; &nbsp; &nbsp;
                                                <input <?php  echo $item->status=='active' ? 'checked':''; ?> type="checkbox" name="status"  class="js-switch" data-color="#99d683" />
                                            </div>

                                        </div>


                                        {{-- End Status of Department--}}

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.update') }}</button>
                                            <button type="reset" value="Reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button>

                                        </div>
                                    </div>






                                    {!! Form::close() !!}



                                </div>

                                <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("article.select_images") }}</p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
@endsection
