@extends('backend.app')

@section('page_title' , trans('dessertdepartment.dessert_name'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.dessert_name') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">

            
             <a href="{{ route('addNewItem' , $id ) }}" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; ">Add new desert</a>

            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('dessertdepartment.dessert_name') }}</th>
                        {{-- <th>{{ trans('dessertdepartment.dessert_description_') }}</th> --}}
                        <th>{{ trans('dessertdepartment.dessert_image_') }}</th>
                        {{-- <th>{{ trans('dessertdepartment.related_to') }}</th> --}}

                        {{-- <th>{{ trans('dessertdepartment.dessert_department_price') }}</th> --}}
                        <th>{{ trans('dessertdepartment.dessert_department_rate') }}</th>


                        <th>{{ trans('dessertdepartment.show_in_slider') }}</th>
                        <th>{{ trans('dessertdepartment.best_selling') }}</th>
                        <th>{{ trans('backend.quantity') }}</th>
                        <th>{{ trans('dessertdepartment.recommended') }}</th>


                    
                   
                        <th>{{ trans('dessertdepartment.dessert_department_status') }}</th>



                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($items->count())


                        @foreach($items as $dessertitem)





                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @foreach($dessertitem->description as $description)
                                        {{ $description->name }} <br>
                                    @endforeach
                                </td>





                               {{--  <td>
                                    <span class="">
                                         @foreach($dessertitem->description as $description)
                                            {{ $description->description }} <br>
                                        @endforeach

                                    </span>
                                </td> --}}

                                <td>
                                    {{-- image show --}}
 
                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="{{ asset("uploads/dessertsitem/original/$dessertitem->img1") }}" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                <img style="width: 70px; height:70px;border-radius:50%;" src="{{ asset("uploads/dessertsitem/original/$dessertitem->img1") }}" class="img-responsive fresco" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>
                                    </div>

                                </td>

                                    {{-- show department of dessert --}}
                                   {{--  <td>


                                    @foreach($dessertitem->desertSubDept->description as $departmentname)

                                            <span> {{ $departmentname->name}}</span>
                                            <br> <br>

                                        @endforeach

                                    </td> --}}
                                   
                                 {{--    <td>

                                        <span> {{$dessertitem->price}} </span>
                                    </td> --}}

                                    {{-- Rate of dessert--}}
                                    <style>
                                        .fa-star{
                                            color: gold !important;
                                        }
                                    </style>
                                    <td>  <i class="fa fa-star"></i> <span> {{$dessertitem->rate}} </span></td>
 

                                     <td>
                                        @if($dessertitem->show_in_slider == 1)
                                             <span class="label label-success label-sm">{{ trans('dessertdepartment.slider') }}</span>
                                        @else

                                        @endif
                                        
                                    </td>


                                    <td>
                                        @if($dessertitem->best_selling == 1)
                                             <span class="label label-success label-sm">{{ trans('dessertdepartment.best_selling') }}</span>
                                        @else

                                        @endif
                                        
                                    </td>


                                    <td>
                                         {{ $dessertitem->quantity }}
                                        
                                    </td>



                                    <td>
                                        @if($dessertitem->recommended == 1)
                                             <span class="label label-success label-sm">{{ trans('dessertdepartment.recommended') }}</span>
                                        @else

                                        @endif
                                        
                                    </td>


 
                                 
                              



                                <td>
                                    @if($dessertitem->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>




                                <td class="text-nowrap">






                                    <a  href="{{ route('editItem' , $dessertitem->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>





                                    <a onclick="$('.dessertitemـ{{ $dessertitem->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    {!! Form::open(['route'=>["deleteItem" , $dessertitem->id ] ,
                                    'class'=>"dessertitemـ$dessertitem->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="11" class="text-center">{{ trans('dessertdepartment.dessert_items_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}
