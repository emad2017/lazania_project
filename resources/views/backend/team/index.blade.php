@extends('backend.app')

@section('page_title' , trans('dessertdepartment.team'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.team') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('dessertdepartment.name_team') }}</th>
                        <th>{{ trans('dessertdepartment.description_team') }}</th>
                        <th>{{ trans('dessertdepartment.img_team') }}</th>
                        <th>{{ trans('dessertdepartment.add_by') }}</th>
                        <th>{{ trans('dessertdepartment.status') }}</th>
                        <th>{{ trans('dessertdepartment.f') }}</th>
                        <th>{{ trans('dessertdepartment.t') }}</th>
                        <th>{{ trans('dessertdepartment.g') }}</th>
                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($teamworks->count())


                        @foreach($teamworks as $teamwork)







                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                  {{ $teamwork->name }}
                                </td>





                                <td>
                                    <span class="">
                                        {{ $teamwork->description }}

                                    </span>
                                </td>

                                <td>
                                    {{-- image show --}}


                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="{{ asset("uploads/team/$teamwork->img") }}" data-effect="mfp-3d-unfold">
                                                <img style="width: 100px; height: 50px;" src="{{ asset("uploads/team/$teamwork->img") }}" class="img-responsive" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>


                                    </div>

                                </td>



                                <td>
                                    <span class="label label-info ">{{ $teamwork->user->headPersonName }}</span>
                                </td>


                                <td>
                                    @if($teamwork->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>

                                <td> <a href="{{ $teamwork->facebook}}" target="_blank"> <i class="fa fa-facebook fa-lg"></i> </a> </td>
                                <td> <a href="{{ $teamwork->twitter}}" target="_blank"> <i class="fa fa-twitter fa-lg"></i> </a>
                                <td> <a href="{{ $teamwork->google}}" target="_blank"> <i class="fa fa-google fa-lg"></i> </a>


                                <td class="text-nowrap">






                                    <a  href="{{ route('team.edit' , $teamwork->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.team_{{ $teamwork->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                        {!! Form::open(['route'=>["team.destroy" , $teamwork->id ] ,
                                        'class'=>"team_$teamwork->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.team_zero') }}</td>

                        </tr>

                    @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}
