@extends('backend.app')

@section('page_title' , trans('speacialevent.eventitem_name'))

@section('breadcrumb')

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('speacialevent.eventitem_name') }}</h4>
    </div>

@endsection


@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp


@section('content')


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> {{ trans('speacialevent.eventitem_edit') }} </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">





                            {!! Form::model($team , ['route'=>['team.update',$team->id],
                            'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'add_files']) !!}
                            {!! method_field('put') !!}


                                                                        {{-- name --}}
                                                                    <div class="form-group ">

                                                                     <label class="col-md-12">{{ trans('dessertdepartment.name_team') }}</label>


                                                                        <div class="col-md-12">

                                                                                  <input type="text" placeholder="{{ trans('backend.user_head_person') }}"
                                                                                  name="name"  value="{{$team->name}}"  class="form-control form-control-line">

                                                                        </div>
                                                                    </div>

                                                                                                                                      {{-- job title --}}
                                                                                                                                      <div class="form-group ">

                                                                                                                                      <label class="col-md-12">{{ trans('dessertdepartment.title_team_') }}</label>


                                                                                                                                      <div class="col-md-12">

                                                                                                                                                <input type="text" placeholder="{{ trans('dessertdepartment.title_team_') }}"
                                                                                                                                                name="jobtitle" value="{{$team->jobtitle}}"    class="form-control form-control-line">

                                                                                                                                      </div>

                                                                                                                                      </div>

                                                                    {{-- description --}}
                                                                  <div class="form-group  ">

                                                                  <label class="col-md-12">{{ trans('dessertdepartment.description_team') }}</label>


                                                                    <div class="col-md-12">

                                                                              <input type="text" placeholder="{{ trans('dessertdepartment.description_team') }}"
                                                                              name="description"   value="{{$team->description}}"    class="form-control form-control-line">

                                                                    </div>
                                                                  </div>

                            <!--  Facebook -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.f') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="facebook"
                                           value="{{$team->facebook}}"  placeholder="{{ trans('dessertdepartment.f_link') }}">

                                </div>
                            </div>
                            <!-- End Facebook -->

                            <!--  Twitter -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.t') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="twitter"
                                           value="{{$team->twitter}}"  placeholder="{{ trans('dessertdepartment.t_link') }}">

                                </div>
                            </div>
                            <!-- End Twitter -->
                            <!--  Google -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.g') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="google"
                                           value="{{$team->google}}"  placeholder="{{ trans('dessertdepartment.g_link') }}">

                                </div>
                            </div>
                            <!-- End Google -->


                                        {{-- image --}}

                                        <div class="form-group">
                                             <label for="input-file-now" class="col-md-12">{{ trans('dessertdepartment.img_team') }}</label>
                                            <div class="col-md-12">
                                                <div class="white-box">


                                                    <input type="file" name="logo" id="input-file-now" class="dropify" /> </div>
                                            </div>
                                        </div>

                                    {{-- Status of Department--}}
                                        <div class="form-group">
                                            <label for=" "> &nbsp; {{ trans('backend.status') }}</label>

                                            <div class="col-md-12">
                                                &nbsp; &nbsp; &nbsp;
                                                <input <?php  echo $team->status=='active' ? 'checked':''; ?> type="checkbox" name="status"   class="js-switch" data-color="#99d683" />
                                            </div>

                                        </div>


                                        {{-- End Status of Department--}}

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.update') }}</button>
                                            <button type="reset" value="Reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button>

                                        </div>
                                    </div>






                                    {!! Form::close() !!}



                                </div>

                                <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("article.select_images") }}</p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
@endsection
