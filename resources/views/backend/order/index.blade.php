  
@extends('backend.app')

@section('page_title' , trans('backend.orders_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.orders_module') }}</h4>
 

                    </div>





@endsection


@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
 
    $language_id = $laguage->id ; 
 
@endphp 



@section('content')

					  <div class="col-sm-12">
                        <div class="white-box">
                            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
                            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
                            <div class="table-responsive">
                                <table class="table table-hover color-table info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('backend.order_ticket') }}</th>
                                            <th>{{ trans('backend.customer_name') }}</th>
                                            <th>{{ trans('backend.order_total') }}</th>
                                          
                                            <th>{{ trans('backend.order_date') }}</th>
                                            <th>{{ trans('backend.action') }}</th>
                                    
                                           

                                        </tr>
                                    </thead>
                                    <tbody>

                                    	@if($orders->count())
                                    		@php $counter = 1 ; @endphp
                                    		@foreach($orders as $order)

                                                @php 
                                                    $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 


                                                @endphp
		                                        <tr>
		                                            <td>{{ $counter }}</td>
                                                    <td>{{ $order->order_ticket }}   </td>
                                                    <td>{{ $shipping_to->fname }} {{ $shipping_to->lname }} </td>
                                                    <td>{{ $order->order_total }} {{ trans('lazena.s_r') }}</td>
 
                                                    <td>{{ date('Y-m-d' , strtotime($order->order_date)) }}</td>
                                                    
                                                    <td>
		                                              <a  href="{{ url("$lang/order/$order->order_ticket/null/details") }}" data-toggle="tooltip" data-original-title="{{ trans('backend.order_info') }}"> <i class="fa fa-info text-info"></i> </a>
                                                    </td>

                                                     
		                                          
		                                        </tr>
		                                        @php $counter++ ; @endphp
		                                     @endforeach
                                        @else

											<tr>
												<td colspan="6" class="text-center">{{ trans('backend.user_count_zero') }}</td>
											</tr>

                                        @endif

                                    </tbody>


                                </table>
                                 <div class="text-center">{!! $orders->links() !!}</div>
                            </div>
                        </div>
                    </div>

@endsection



@section('scripts')

    <script>

    </script>

@endsection


{{--  --}}
