@extends('backend.app')

@section('page_title' , trans('backend.order_info'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.order_info') }}</h4> 
                    </div>

          



@endsection  




@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 

    $laguage = \App\Language::where(['label'=>$lang])->first(); 


    $language_id = $laguage->id ; 

    


@endphp 



@section('content')
                 
            <div class="col-md-12">
                        <div class="white-box printableArea">
                            <h3><b>{{ trans('backend.order_info') }}</b> <span class="pull-right">{{  $order->order_ticket }}</span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    
                                     @php 

                                        $shipping_to = \App\Address::where('order_id' , $order->order_ticket)->first(); 

                                    @endphp

                                    <div class="text-center">


                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-12">
                                                 <h3>{{ trans('backend.order_customer_name') }}</h3>
                                            <h4 class="font-bold">
                                               
                                                {{ $shipping_to->fname }} {{ $shipping_to->lname }}

                                            </h4>
                                            </div>


                                            <div class="col-md-6 col-sm-12">
                                               <h3>{{ trans('backend.contact_info') }}</h3>

                                                <h4 class="font-bold">   
                                                  {{ $shipping_to->telephone }} 
                                                 </h4>

                                                 
                                            </div>
                                        </div>
                                        <address>
                                           

                                           

                                            <p class="text-muted m-l-30">
                                                {{ $shipping_to->street }} 
                                                <br>
                                                {{ $shipping_to->country }}
                                            </p>
                                            <p class="m-t-30">  <i class="fa fa-calendar"></i>
                                                {{ date('l jS \of F Y h:i:s A' , strtotime($order->order_date)) }}
                                            </p>
                                           
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>{{ trans('backend.product_name') }}</th>
                                                    <th>{{ trans('backend.product_image') }}</th>
                                                    <th class="text-right">{{ trans('backend.product_quantity') }}</th>
                                                    <th class="text-right">{{ trans('backend.product_price') }}</th>
                                                    <th class="text-right">{{ trans('backend.product_total') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                     $orders = \App\OrderProduct::where('order_id' , $order->order_ticket)->get(); 
                                                @endphp 

                                                @foreach($orders as $order_pivot)
                                                     @php  $product =  $order_pivot->product ;   @endphp
                                                    <tr>
                                                        <td class="text-center">{{ $loop->iteration }}</td>

                                                        <td>
                                                            @foreach($product->description as $description) 
                                    
                                                                  @if($description->language_id == $language_id)
                                                                     {{ $description->name }} 
                                                                  @endif 
                                                              @endforeach

                                                        </td>

                                                        <td>
                                                            
                                                            <img style="height: 50px; width: 50px; border-radius: 50%;" src="{{ asset("uploads/dessertsitem/300_400/$product->img1") }}" alt="">

                                                             
                                                        </td>
                                                        <td class="text-right">{{ $order_pivot->quantity }}</td>
                                                        <td class="text-right">  {{ $product->price }} </td>
                                                        <td class="text-right">  {!!  $order_pivot->quantity * $product->price   !!} </td>
                                                    </tr>

                                                @endforeach
                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">
                                        <p>{{ trans('lazena.sub_total') }} : {{ trans('lazena.s_r') }} {!! $order->order_total - 5  !!} </p>
                                        <p>  {{ trans('lazena.delivery') }}   :  {{ trans('lazena.s_r') }}   5.00 </p>
                                        <hr>
                                        <h3><b>{{ trans('backend.grand_total') }} :</b> {{ trans('lazena.s_r') }} {{ $order->order_total }} </h3> </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="text-right">
                                        
                                        <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> {{ trans('backend.print') }}</span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                 

@endsection 


 
@section('scripts')
    

     {{-- <script src="js/jquery.PrintArea.js" type="text/JavaScript"></script> --}}

    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>
      

@endsection 

 