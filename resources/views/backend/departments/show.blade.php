@extends('backend.app')

@section('page_title' , trans('backend.pharmaceutical_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.pharmaceutical_show') }}</h4> 
                    </div>

                  



@endsection  


@section('styles')


 
@endsection



@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 



    // dd($language_id);


@endphp 
 

@section('content')
				  
    
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <ul class="nav customtab nav-tabs" role="tablist">
                               
                                <li role="presentation" class="nav-item"><a href="#info" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">{{ trans('backend.pharmaceutical_info') }}</span></a></li>
                                
                                <li role="presentation" class="nav-item"><a href="#images" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">{{ trans('backend.pharmaceutical_Images') }}</span></a></li>
                            </ul>
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="info">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_tradeName') }}</strong>
                                            <br>
                                            
                                                @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                       <p class="text-muted"> {{ $description->tradeName }} </p>
                                                    @endif

                                                @endforeach

                                            
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_scientificName') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                
                                                 @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ $description->scientificName }}
                                                    @endif

                                                @endforeach

                                            </p>
                                        </div>
                                         
                                        <div class="col-md-3 col-xs-6"> <strong>{{ trans('backend.pharmaceutical_activeIngredient') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ $description->activeIngredient }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>

                                        <div class="col-md-3 col-xs-6"> 
                                                <strong>{{ trans('backend.pharmaceutical_description') }}</strong>
                                                <br>
                                             <p class="text-muted m-t-30">
                                                @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->description)  }}
                                                    @endif

                                                @endforeach
                                             </p>
                                        </div>

                                    </div>
                                    <hr>


                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_uses') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                 @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->uses)  }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_dosage') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                 @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->dosage)  }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>
                                        
                                        <div class="col-md-3 col-xs-6"> 
                                                <strong>{{ trans('backend.pharmaceutical_sideEffect') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                     @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->sideEffect)  }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <hr>



                                     <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_storeMedicine') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                 @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->storeMedicine)  }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>

                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.pharmaceutical_other') }}</strong>
                                            <br>
                                            <p class="text-muted">
                                                 @foreach($pharmaceutical->description as $description)
                                                    @if($description->language_id == $language_id)
                                                        {{ strip_tags($description->other)  }}
                                                    @endif

                                                @endforeach
                                            </p>
                                        </div>
                                        
                                        
                                    </div>
                                    <hr>
                                 
                                     
                                </div>

                                
                                <div class="tab-pane" id="images">
                                     
                                 <div class="col-sm-12">

                                        <div class="white-box">
                                            <h3 class="box-title m-b-0">{{ trans('backend.pharmaceutical_Images') }}</h3>
                                            <div id="image-popups" class="row">
                                                

                                                @if(count($photos))
                                                @foreach($photos as $photo)
                                                <div class="col-sm-2">
                                                    <a href="{{ asset("uploads/pharmatecual/$photo->image_name") }}" data-effect="mfp-3d-unfold"><img src="{{ asset("uploads/pharmatecual/$photo->image_name") }}" class="img-responsive" />
                                                        </a>
                                                </div>
                                                @endforeach
                                                @else
                                                    
                                                    <p class="text-center">{{ trans('backend.pharmaceutical_photos_zero') }}</p>
                                                    
                                                @endif
                                             
                                                 
                                                 
                                                 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                </div>
    
@endsection 


 
@section('scripts')

    <!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {

         $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("backend.pharmaceutical_select_images") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("backend.pharmaceutical_select_images_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });


        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>



 
 

@endsection 


 