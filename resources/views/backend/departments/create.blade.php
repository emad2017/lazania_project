@extends('backend.app')

@section('page_title' , trans('department.department_module_managment'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('department.departments') }}</h4> 
                    </div>

                  



@endsection  


@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 

    $laguage = \App\Language::where(['label'=>$lang])->first(); 


    $language_id = $laguage->id ; 

    


@endphp 
 

@section('content')
				 
     <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"></h3>
                <p class="text-muted m-b-30 font-13"> {{ trans('department.add_department') }} </p>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                       <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">
                            <nav>
                                <ul>
                                      @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>
                                   {{--  <li><a href="#section-iconbox-2" class="sticon ti-gift"><span>Deals</span></a></li> --}}
                                      @endforeach
                                    
                                </ul>
                            </nav>
                    {!! Form::open(['route'=>['department.store'],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'department_create']) !!}
                                <div class="content-wrap">
                                @foreach($languages as $language)

                                    
                                    
                                <section class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$language->label}}">

                                     <div class="form-group">
                                        <label for="">{{ trans('department.department_name') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="name_{{$language->label}}" id="name_{{$language->label}}" placeholder="{{ trans('department.department_name') }}"> 
                                        </div>
                                    </div>

                                 {{--    <div class="form-group">
                                        <label for="">{{ trans('department.department_slug') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="slug_{{$language->label}}" id="slug_{{$language->label}}" placeholder="{{ trans('department.department_slug') }}"> 
                                        </div>
                                    </div> --}}

 


                                    <div class="form-group">
                                        <label for="">{{ trans('department.department_description') }}</label>
                                        <div class="input-group">
                                            
                                        

                                             <textarea id="textarea_department_description" name="department_description_{{$language->label}}" placeholder="{{ trans('department.department_description') }}"  ></textarea>
                                        </div>
                                    </div>

 
                                    
                                </section>
                                    

                                    

                                <!-- /content -->
                            @endforeach
                        
                                </div>
                                
                               

                                 <div class="form-group">
                                        <label for=" "> &nbsp; {{ trans('department.department_status') }}</label> 
                                           
                                   <div class="col-md-12">
                                        &nbsp; &nbsp; &nbsp;  &nbsp;
                                       <input type="checkbox" name="status" checked class="js-switch" data-color="#99d683" />
                                    </div>
                                         
                                </div>

                                <div class="form-group">

                                     <label > &nbsp; {{ trans('department.department_parent') }}</label> 
                                    <div class="col-md-12">

                                        <select class="selectpicker m-b-20 m-r-10" name="parent" data-style="btn-primary btn-outline">
                                            
                                            @if(count($description_arr) > 0 )

                                                
                                                <option value="" selected="" disabled="">{{ trans('department.select_department') }}</option>
                                                @foreach($description_arr as $description)
                                                          
                                                    <!-- simple check on language --> 
                                                            <option value="{{ $description->department_id}}" data-tokens="ketchup mustard">{{ $description->name }}</option>
                                                    

                                                 @endforeach

                                            @else
                                                
                                                <option value="" selected="" disabled="">{{ trans('department.select_department') }}</option>

                                            @endif
                                             
                                                

                                             
                                            
                                        </select>
                             

                                    </div>


                                </div>


                                <div class="form-group ">
                                    
                                    <label for=" ">  &nbsp; {{ trans('department.department_photos') }}</label> 
                                    <div class="col-md-12">
                                        <div class="white-box">
                                             
                                            
                                            <input type="file" id="input-file-now" name="file[]" multiple class="dropify" /> 
                                        </div>
                                    </div>


                                </div>


                               <div class="form-group">
                                     <div class="col-md-12">
                                     
                                    <button type="submit" id="submit" class="btn btn-info btn-circle btn-lg"  data-toggle="tooltip" data-original-title="{{ trans('backend.add') }}"><i class="fa fa-check"></i> </button>

                                    <button type="reset" class="btn btn-warning btn-circle btn-lg" data-toggle="tooltip" data-original-title="{{ trans('backend.reset') }}"><i class="fa fa-times"></i> </button>
                                </div>
                                </div>

                                

                                {!! Form::close() !!}


                                
                        </div>
                        <!-- /tabs -->
                    </section>
                    </div>
                </div>
            </div>
        </div>	  

@endsection 


 
@section('scripts')

    <!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {

         $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("backend.pharmaceutical_select_images_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });


        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>




        
 

@endsection 



 

    
 


 