@extends('backend.app')

@section('page_title' , trans('department.department_module_managment'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('department.departments') }}</h4> 
                    </div>

                  



@endsection  



@section('content')
					
    <div class="col-sm-12">
    <div class="white-box">
    {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
    <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
    <div class="table-responsive">
        <table class="table   table-hover color-table info-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('department.department_name') }}</th>
                    {{-- <th>{{ trans('department.department_description') }}</th> --}}
                    <th>{{ trans('department.department_status') }}</th>
                    <th>{{ trans('department.department_added_by') }}</th>
                  
                    <th>{{ trans('department.department_date_added') }}</th>
                    
                    <th class="text-nowrap">{{ trans('backend.action') }}</th>
                 
                </tr>
            </thead>
            <tbody>

            	@if($departments->count())
            		  
            		@foreach($departments as $department)

                            
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                @foreach($department->description as $description)
                                {{ $description->name }} <br>
                                @endforeach
                            </td>
                             
                          

                           {{--   <td>
                            	@foreach($department->description as $description)
                                 {!! html_entity_decode(str_limit($description->description , 10 )) !!}    
                                @endforeach
                            </td> --}}


                             <td>
                                @if($department->status == 'not_active')
                                    <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                @else
                                    <span class="label label-success">{{ trans('backend.active') }}</span>  
                                @endif
                            </td>

                            <td>
                                 <span class="label label-success">{{ $department->user->headPersonName }}</span> 
                            </td>

                           
                            <td>{{ date('Y-m-d' , strtotime($department->created_at)) }}</td>
                            <td class="text-nowrap">


                                 {{-- <a href="#" data-toggle="tooltip" data-original-title="{{ trans('backend.show') }}"> <i class="fa fa-info-circle text-inverse m-r-10"></i> </a> --}}
                               
                               {{--  @if($department->parent_id == null )
                                <a  href="#" data-toggle="tooltip" data-original-title="{{ trans('department.show_sub_departments') }}"> <i class="fa fa-hdd-o text-inverse m-r-10"></i> </a>

                                @endif --}}


                                <a  href="{{ route('department.edit' , $department->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.pharmaceutical_edit') }}"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                            

                                 <a class="department_form" data-id="{{ $department->id }}" data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}"> <i class="fa fa-close text-danger"></i> </a>
                                 


                               {{--   <a onclick="$('.department_form_{{ $department->id }}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}"> <i class="fa fa-close text-danger"></i> </a> --}}




                                {{-- {!! Form::open(['url'=>["remove_dept"] , 'class'=>"department_form_$department->id"   ]) !!}

                                  



                                {!! Form::close() !!} --}}


                                 
                            </td>
                                
                        </tr>
                       
                     @endforeach
                @else
    				
    				<tr>
    					<td colspan="7" class="text-center">{{ trans('department.departments_count_zero') }}</td>
    				</tr>

                @endif
                
            </tbody>
        </table>
    </div>
    </div>
    </div>

@endsection 


 




@section('scripts')


     <script>
                $(document).ready(function(){



               
                $('.department_form').click(function(event) {


                 event.preventDefault(); 
                 


                 var id = $(this).attr('data-id');  
                 var token = $('input[name=_token]').val(); 
              


                swal({







                        title: "{{ trans('department.are_you_sure') }}",
                        text: "{{ trans('department.data_losted') }}",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{ trans('department.yes_delete') }}",
                        cancelButtonText: "{{ trans('department.no_delete') }}",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                        function(isConfirm){
                            if (isConfirm) {

                                
                                $.ajax({



                                     headers: {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                     },
                                     xhrFields: {
                                        withCredentials: true
                                     },
                                     cache: false, 
                                     dataType:'json', 
                                     url : "{{url('remove_dept')}}" ,
                                     method : 'DELETE',
                                     
                                     data: { _token:token , id:id },
                                     beforeSend:function(){



                                     }, 

                                     success:function(data){

                                       
                                        /**
                                         * i success so we are going to show the return back success 
                                         * delete message  
                                         */
                                        
                                        if(data.status == 'department_deleted'){


                                                /**
                                                 * that means that department doesn't have 
                                                 * sub departments  so delete will go peacfully 
                                                 */
                                                

                                                swal({

                                                    title: "{{ trans('department.department_swal_deleted_success') }}",
                                                    text: "{{ trans('department.department_swal_deleted_success_message') }}",
                                                    type: "success",
                                                    closeOnConfirm: true,



                                                });
                                                 // swal("Node deleted successfully ! ", "Made with love by love ! ", "success");
                                                setTimeout(function(){ 

                                                    window.location.href = "{{ route("department.index") }}"; 
                                                     
                                                }, 3000);


                                        }

                                        if(data.status == 'sub_found'){


                                            swal({

                                                    title: "{{ trans('department.department_swal_deleted_error') }}",
                                                    text: "{{ trans('department.department_swal_deleted_error_message') }}",
                                                    type: "warning",
                                                    closeOnCancel: true,



                                                });
                                             


                                        }



                                     }
                                 });

                                   // return false; 



                                // swal("Node deleted successfully ! ", "Made with love by diva ! ", "success");
                                // form.submit();



                            } else {
                                swal("Delete process cancelled", "Made with love by diva ! ", "error");
                            }
                    }); 

                 
                 


                });
                 
                  
            });



     </script>


     


@endsection