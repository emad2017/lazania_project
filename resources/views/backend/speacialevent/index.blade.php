@extends('backend.app')

@section('page_title' , trans('speacialevent.speacialevent_name'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('speacialevent.speacialevent_name') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('speacialevent.speacialevent_name_') }}</th>
                        <th>{{ trans('speacialevent.speacialevent_description_') }}</th>
                  

                   
                        <th>{{ trans('backend.image') }}</th>
                        <th>{{ trans('speacialevent.speacialevent_date') }}</th>
                        <th>{{ trans('speacialevent.speacialevent_status') }}</th>



                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if(count($events))


                        @foreach($events as $event)


                                @php    


                                    $event_image = $event->specialevent->img ; 

                                 

                                 @endphp


                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> {{ $event->name }} </td>
                                    <td> {{ str_limit($event->description , 100)  }} </td>


                                   

                                    <td> 
                                            <div class="" id="images">

                                                <div class="col-sm-2">
                                                    <a  href="{{ asset("uploads/speacialevent/$event_image") }}" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                        <img style="width: 70px; height:70px;border-radius:50%;" src="{{ asset("uploads/speacialevent/$event_image") }}" class="img-responsive fresco" />
                                                    </a>

                                                    <style>
                                                        .img-responsive
                                                        {
                                                            max-width: none !important;

                                                        }
                                                    </style>
                                                </div>
                                            </div> 
                                    </td>
 


                                

                                <td>{{ $event->created_at}}</td>



                                <td>
                                    @if($event->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>




                                <td class="text-nowrap">






                                    <a  href="{{ route('speacialevent.edit' , $event->event_id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.speacialeventـ{{ $event->event_id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    {!! Form::open(['route'=>["speacialevent.destroy" , $event->event_id ] ,
                                    'class'=>"speacialeventـ$event->event_id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('speacialevent.speacialevent_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}
