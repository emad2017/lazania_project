@extends('backend.app')

@section('page_title' , trans('backend.user_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.user_module') }}</h4>



												{{-- <ol class="breadcrumb">
														<li>
															<i class="fa fa-home"></i>
															<a href="{{route('dashboard')}}">Home</a>

														</li>
														@for($i = 0; $i <= count(Request::segments()); $i++)
														<li>
															<a href="#">{{Request::segment($i)}}</a>

														</li>
														@endfor
												</ol>
 --}}



                    </div>





@endsection



@section('content')

					  <div class="col-sm-12">
                        <div class="white-box">
                            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
                            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
                            <div class="table-responsive">
                                <table class="table table-hover color-table info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('backend.user_email') }}</th>
                                         {{--    <th>{{ trans('backend.user_specialization') }}</th>
                                            <th>{{ trans('backend.user_head_person') }}</th> --}}
                                            <th>{{ trans('backend.user_type') }}</th>
                                            <th>{{ trans('backend.user_added_date') }}</th>
                                            <th>{{ trans('backend.status') }}</th>
                                            <th class="text-nowrap">{{ trans('backend.action') }}</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    	@if($users->count())
                                    		@php $counter = 1 ; @endphp
                                    		@foreach($users as $user)
		                                        <tr>
		                                            <td>{{ $counter }}</td>
                                                    <td>{{ $user->email }} </td>
                                                   {{--  <td>{{ $user->Specialization }} </td>
                                                    <td>{{ $user->headPersonName }} </td> --}}
		                                            <td>

                                                        @if($user->userType == 'adminstrator')
                                                            <span class="label label-success">{{ trans('backend.adminstrator') }}</span>
                                                        @elseif($user->userType == 'doctor')
                                                            <span class="label label-warning">{{ trans('backend.doctor') }}</span>
                                                        @elseif($user->userType == 'pharmacy')
                                                            <span class="label label-danger">{{ trans('backend.pharmacy') }}</span>
                                                        @elseif($user->userType == 'hospital')
                                                            <span class="label label-info">{{ trans('backend.hospital') }}</span>
                                                        @elseif($user->userType == 'medicalSupplies')
                                                            <span class="label label-primary">{{ trans('backend.medical_supply') }}</span>
                                                        @elseif($user->userType == 'PharmaCompany')
                                                            <span class="label label-inverse">{{ trans('backend.pharma_company') }}</span>
                                                        @endif

                                                    </td>



		                                            <td>{{ date('Y-m-d' , strtotime($user->created_at)) }}</td>

                                                    <td>
                                                        @if($user->isActive == 'not_active')
                                                            <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                                        @else
                                                            <span class="label label-success">{{ trans('backend.active') }}</span>
                                                        @endif
                                                    </td>

		                                            <td class="text-nowrap">

                                                        <a href="{{ route('user.show' , $user->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.show') }}"> <i class="fa fa-info-circle text-inverse m-r-10"></i> </a>

                                                        <a href="{{ route('user.edit' , $user->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.edit') }}"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>





                                                         <a onclick="$('.user_form_{{ $user->id }}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}"> <i class="fa fa-close text-danger"></i> </a>








                                                        {!! Form::open(['route'=>["user.destroy"  , $user->id ]
																												, 'class'=>"user_form_$user->id"]) !!}



                                                            {!! method_field('DELETE') !!}



                                                        {!! Form::close() !!}


		                                            </td>
		                                        </tr>
		                                        @php $counter++ ; @endphp
		                                     @endforeach
                                        @else

											<tr>
												<td colspan="6" class="text-center">{{ trans('backend.user_count_zero') }}</td>
											</tr>

                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

@endsection



@section('scripts')

    <script>

    </script>

@endsection


{{--  --}}
