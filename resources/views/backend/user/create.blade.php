@extends('backend.app')

@section('page_title' , trans('backend.user_module'))

@section('breadcrumb')


                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.user_profile_add') }}</h4> 
                    </div>

                  



@endsection  



@section('content')
                

                    
                 <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"></h3>
                            {{-- <p class="text-muted m-b-30 font-13"> {{ trans('backend.user_profile_add') }} </p> --}}
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    
                                     <!-- Adminstrator Form --> 
                                     {!! Form::open( array('route' => array('user.store' ) , 'class'=> 'form-horizontal form-material ' , 'files'=>true , 'method'=>'POST' , 'id'=>'adminstrator_form'))  !!}


                                    
 
                                        

                                        <div class="form-group ">

                                            
                                             

                                         <label class="col-md-12">{{ trans('backend.user_head_person') }}</label>
                                                   

                                            <div class="col-md-12">
 
                                                      <input type="text" placeholder="{{ trans('backend.user_head_person') }}" name="user_head_person"   class="form-control form-control-line"> 
  
                                            </div>
                                        </div>

                                         
                                        
                                         

                                        

                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_mobile_number') }}</label>
                                            <div class="col-md-12">

                                                <input type="text" placeholder="{{ trans('backend.user_mobile_number') }}" name="user_mobile_number"   class="form-control form-control-line">  

                                            </div>
                                        </div>
                                        



                                        
                                        
                                        


                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_website') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_website') }}" name="user_website"  class="form-control form-control-line"> </div>
                                        </div>
                                        



                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_type') }}</label>
                                                 <select name="type"   class="form-control form-control-line"  >
 
                                                <option value="adminstrator" selected   >{{ trans('backend.adminstrator') }}</option>

                                                
                                               
                                            </select>
                                        </div>



                                        

                                        



                                        

                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_password') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_password') }}" name="user_password"   class="form-control form-control-line"> </div>
                                        </div>

                                            
                                    <div class="form-group">
                                         <label for="input-file-now" class="col-md-12">{{ trans('backend.user_logo') }}</label>
                                        <div class="col-md-12">
                                            <div class="white-box">
                                                
                                                 
                                                <input type="file" name="logo" id="input-file-now" class="dropify" /> </div>
                                        </div>
                                    </div>

                                        @if(Auth::user()->userType == 'adminstrator')

                                             <div class="form-group">
                                                <label for=""> &nbsp; {{ trans('backend.status') }}</label>
                                             
                                                <div class="col-md-12">
                                               
                                                     <input type="checkbox" name="user_status"   class="js-switch" data-color="#99d683" />

                                                </div>

                                             
                                        </div>
                                        @endif


                                   




                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_email') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_email') }}" name="user_email"   class="form-control form-control-line"> </div>
                                        </div>

                                         

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                
                                                <button type="submit" class="btn btn-success">{{ trans('backend.add') }}</button> 


                                                <button type="reset" class="btn btn-success">{{ trans('backend.reset') }}</button>

                                            </div>
                                        </div>

    
                                      
                                        






                                        







                                   
                                </div>
                            </div>
                        </div>
                    </div>    

@endsection 


 
@section('scripts')

         
         <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("backend.select_file") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("backend.select_file_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Delete',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>


     <script>
    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });

    </script>




    <!-- Custom Script --> 
    <script>
        
            // change fields based on type selected 
            
            $(document).ready(function(){



                    var user_type = 'adminstrator' ; 
                        
                    

                    $('#user_type').on('change' , function(){
                
                        if($(this).val() == "adminstrator"){
                            
                           user_type = 'adminstrator' ; 
                          

                           $('#adminstrator_form').css('display', 'block');

                   
                        

                        }else if($(this).val() == "other"){
                           user_type = 'other' ;
                      

                            $('#adminstrator_form').css('display', 'none'); 

                        
                         
                        }else{
                            
                            user_type = 'doctor' ;

                     

                            $('#doctor_form').css('display', 'block'); 

                         
                           
                        }
                        
                    });






            });


    </script>

@endsection 


 