@php
                if(Auth::check()){

                    // user authenticated
                    $user = Auth::user();

                    $user_name = '' ;

                    $userType = $user->userType;

                    if($userType == 'doctor'){

                        $user_name = $user->doctorName ;

                    }else{

                         $user_name = $user->headPersonName ;

                    }


                    $user_id = $user->id ;
                    $user_image = $user->profileImage;

                }
        @endphp




@php

    $lang = LaravelLocalization::getCurrentLocale();

@endphp
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="#"><b>
                            {{-- <img src="{{ asset('backend/assets/plugins/images/eliteadmin-logo.png') }}" alt="Gooooo" /> --}}
                            <img src="{{ asset('backend/assets/logo.png') }}" alt="Gooooo" />
                        </b><span class="hidden-xs">

                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <ul class="dropdown-menu mailbox animated bounceInDown">

                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>

                    

                    <!-- notifications begins -_- --> 
                    

                    <li class="dropdown dropdown_notifications"> 
                        
                        <!-- this is used to open dropdown lost vand notifier  point  --> 
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
                                <i class="fa  fa-bullhorn "></i>
                                <div class="notify">
     

                               
                                    <!-- check if there are any unread notifications  --> 
                                    @if(Auth::user()->unreadNotifications->count() > 0)
                                       
                                        <span class="heartbit"></span><span class="point"></span>
                                        
                                    @else
                                        <span class=""></span><span class=""></span>
                                    @endif
                                 
                                </div>
                        </a>
                        

                        <!-- begin wraping notifications --> 
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                           
                     
                          <li>
                               <div class="message-center">
                                
                                    <div class="drop-title">{{ trans('backend.order_notification') }}</div>
                               </div>  

                          </li>
                     

                       @if(Auth::user()->unreadNotifications->count() > 0)
                            
                        <!-- then do the loop --> 
                        @foreach(Auth::user()->unreadNotifications as $notification)
    
                            <li>   
                                @php 
                                    $notification_id = $notification->id ; 
                                    $order_ticket    = $notification->data['order_ticket'] ; 
                                @endphp
                                 <a href="{{ url("$lang/order/$order_ticket/$notification_id/details") }}">   
                                    <div class="message-center">
                                        <div class="drop-title" id="header_notification">
                                            
                                      
                                                
                                                <div class="user-img"> 
                                                    <img src="{{ asset('product/product.gif') }}" alt="product" class="img-circle">
                                                </div>
                                                
                                                <div class="mail-contnet" >  
                                                    <h5> {{ trans('backend.new_order_txt') }} </h5> 
                                                    <span class="mail-desc">{{ trans('backend.order_customer_name') }} :  

                                                    <!-- i may find customer name  --> 
                                                    {!! $notification->data['customer_name'] !!}
                                                           
                                                    </span> 

                                                      <span class="mail-desc">{{ trans('backend.total') }} :  

                                                    <!-- i may find customer name  --> 
                                                    {!! $notification->data['order_total'] !!}
                                                    &nbsp;
                                                    {{ trans('backend.s_r') }}
                                                           
                                                    </span> 
                                                   
                                                </div>

                                          
                                          
                                        </div>
     
                                        
                                    </div>
                                </a>
                            </li>

                          @endforeach

                        @else

                            <li>
                                <br>
                                <p class="text-center"><strong>{{ trans('backend.no_notifications') }}</strong></p>
                            </li>

                        @endif
 


                        </ul>
                       
                    </li>








                    <li class="dropdown">


                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa fa-language"></i>
                            {{-- <div class="notify"><span class="heartbit"></span><span class="point"></span></div> --}}
                        </a>

                            <ul class="dropdown-menu dropdown-tasks animated slideInUp">



                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>



                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                    <img width="20px"  src='{{ asset("backend/assets/$localeCode-flag.png") }}' alt="">
                                                        {{ $properties['native'] }}
                                        </a>



                                    </li>
                                @endforeach


                            </ul>

                    </li>



                   







                    <li class="dropdown">


                                                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="


                                                                {{ asset('avatars/adminstrator-avatar.png') }}


                                                       " alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ $user_name }}</b> </a>

                                    <ul class="dropdown-menu dropdown-user animated flipInY">
                                        <li><a href='{{ url("user/profile/$user->id") }}'><i class="ti-user"></i> {{ trans('backend.my_profile') }}</a></li>


                                        <li role="separator" class="divider"></li>

                                        <li>

                                               <a href="javascript:{}" onclick="document.getElementById('logout_form').submit();"><i class="fa fa-power-off">  </i> {{ trans('backend.logout') }}
                                                </a>

                                              {!! Form::open(['url'=>"$lang/logout" , 'method'=>'POST' , 'id'=>'logout_form']) !!}

                                              {!! Form::close() !!}

                                        </li>
                                    </ul>


                    </li>

                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
