
        @php
                if(Auth::check()){

                    // user authenticated
                    $user = Auth::user();

                    $user_name = '' ;

                    $userType = $user->userType;

                    if($userType == 'doctor'){

                        $user_name = $user->doctorName ;

                    }else{

                         $user_name = $user->headPersonName ;

                    }


                    $user_id = $user->id ;
                    $user_image = $user->profileImage;

                }
        @endphp


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="
                                                         @if($userType == 'adminstrator')
                                                            @if(count($user_image) )
                                                                    {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/adminstrator-avatar.png') }}
                                                            @endif
                                                        @elseif($userType == 'doctor')
                                                             @if(count($user_image) )
                                                                {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/doctor-avatar.jpg') }}
                                                            @endif
                                                        @elseif($userType == 'pharmacy')

                                                             @if(count($user_image) )
                                                                {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/pharmacy-avatar.jpg') }}
                                                            @endif

                                                        @elseif($userType == 'hospital')
                                                             @if(count($user_image) )
                                                                {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/hospital-avatar.png') }}
                                                            @endif

                                                        @elseif($userType == 'medicalSupplies')

                                                             @if(count($user_image) )
                                                                {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/medical-supply-avatar.png') }}
                                                            @endif

                                                        @elseif($userType == 'pharmaCompany')
                                                             @if(count($user_image) )
                                                                {{ asset("uploads/users/$user_image") }}
                                                            @else
                                                                {{ asset('avatars/pharma-company-avatar.jpg') }}
                                                            @endif

                                                        @endif

                                                            " alt="user-img" class="img-circle">
                            <span class="hide-menu">{{ $user_name }}<span class="fa arrow"></span>  </span>
                        </a>
                        <ul class="nav nav-second-level">

                            <li><a href='{{ url("user/profile/$user_id") }}'><i class="ti-user"></i>{{ trans('backend.my_profile') }}</a></li>

                            <li>
                                 {!! Form::open(['url'=>'logout' , 'method'=>'POST' , 'id'=>'logout_form']) !!}
                                <a href="javascript:{}" onclick="document.getElementById('logout_form').submit();"><i class="fa fa-power-off"></i> {{ trans('backend.logout') }}</a>
                                 {!! Form::close() !!}
                            </li>



                        </ul>
                    </li>



                    @php

                        $lang = LaravelLocalization::getCurrentLocale();

                    @endphp
                    <li>
                        <a href="{{ url('dashboard') }}" class="waves-effect {{ Request::is("$lang/dashboard") ? 'active' : '' }} ">
                            <i class="fa fa-tachometer"></i>
                            <span class="hide-menu"> {{ trans('backend.dashboard') }} </span>
                        </a>

                    </li>





                    <li>

                        <a href="#" class="waves-effect  {!! Request::path() == "$lang/language"   ? 'active' : '' !!}
                            {!! Request::path() == "$lang/language/create"   ? 'active' : '' !!}  ">
                            <i class="fa fa-language"></i>
                            <span class="hide-menu"> {{ trans('backend.language') }}
                                <span class="fa arrow"></span>
                            </span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ route('language.create') }}">{{ trans('backend.create_language') }}</a> </li>
                            <li> <a href="{{ route('language.index') }}">{{ trans('backend.show_languages') }}</a> </li>

                        </ul>
                    </li>





                     {{-- <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/user"   ? 'active' : ''}}
                                {{Request::path() == "$lang/user/create"   ? 'active' : ''}} ">
                                <i class="fa  fa-users"></i>
                                <span class="hide-menu">{{ trans('backend.users') }}
                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('user.create') }}">{{ trans('backend.create_user') }}</a></li>
                            <li><a href="{{ route('user.index') }}">{{ trans('backend.show_users') }}</a></li>
                        </ul>

                     </li> --}}


                     <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/customers"   ? 'active' : ''}}
                                {{Request::path() == "$lang/customers/create"   ? 'active' : ''}} ">
                                <i class="fa  fa-users"></i>
                                <span class="hide-menu">{{ trans('backend.customers') }}
                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            {{-- <li><a href="{{ route('customers.create') }}">{{ trans('backend.add_customer') }}</a></li> --}}
                            <li><a href="{{ route('customers.index') }}">{{ trans('backend.list_customers') }}</a></li>
                        </ul>

                     </li>


                    
                    <!-- Slider Module --> 

                     <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/slider"   ? 'active' : ''}}
                                {{Request::path() == "$lang/slider/create"   ? 'active' : ''}} ">
                                <i class="fa fa-crosshairs"></i>
                                <span class="hide-menu">{{ trans('backend.slider_module') }}
                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('slider.create') }}">{{ trans('backend.add_slide') }}</a></li>
                            <li><a href="{{ route('slider.index') }}">{{ trans('backend.list_sliders') }}</a></li>
                        </ul>

                     </li>


                      <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/orders"   ? 'active' : ''}}
                                ">
                                <i class="fa  fa-users"></i>
                                <span class="hide-menu">{{ trans('backend.orders_module') }}
                                    <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            {{-- <li><a href="{{ route('customers.create') }}">{{ trans('backend.add_customer') }}</a></li> --}}
                            <li><a href="{{ url("$lang/orders") }}">{{ trans('backend.list_orders') }}</a></li>
                        </ul>

                     </li>




                    {{-- Dessert Departments --}}
                    <li>

                        <a class="waves-effect   ">
                            <i class="fa fa-th-large"></i>
                            <span  class="hide-menu">{{ trans('dessertdepartment.dessert_department_name') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('dessertdepartment.create') }}">{{ trans('dessertdepartment.dessert_department_create') }}</a></li>
                            <li><a href="{{ route('dessertdepartment.index') }}">{{ trans('dessertdepartment.dessert_department_show') }}</a></li>
                        </ul>

                    </li>



                    {{-- Dessert Item --}}
                    {{-- <li>

                        <a class="waves-effect   ">
                            <i class="fa fa-shopping-basket"></i>
                            <span  class="hide-menu">{{ trans('dessertdepartment.dessert_name') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('dessertitem.create') }}">{{ trans('dessertdepartment.dessert_add') }}</a></li>
                            <li><a href="{{ route('dessertitem.index') }}">{{ trans('dessertdepartment.dessert_show') }}</a></li>
                        </ul>

                    </li> --}}
                    {{-- Blogs Department --}}
                   {{--  <li>

                        <a class="waves-effect   ">
                            <i class="fa fa-cloud"></i>
                            <span  class="hide-menu">{{ trans('dessertdepartment.blog_d') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('blogdepartment.create') }}">{{ trans('dessertdepartment.add_blog_d') }}</a></li>
                            <li><a href="{{ route('blogdepartment.index') }}">{{ trans('dessertdepartment.show_blog_d') }}</a></li>
                        </ul>

                    </li> --}}


                    {{-- Blogs --}}
                    <li>

                        <a class="waves-effect " href="{{ route('careers_backend') }}" >
                            <i class="fa fa-pencil-square"></i>
                            <span  class="hide-menu">{{ trans('backend.careers') }}
                                 
                                </span>
                        </a>

                        {{-- <ul class="nav nav-second-level">
                            <li><a href="{{ route('blog.create') }}">{{ trans('dessertdepartment.add_blog') }}</a></li>
                            <li><a href="{{ route('blog.index') }}">{{ trans('dessertdepartment.show_blog') }}</a></li>
                        </ul> --}}

                    </li>

                    {{-- Speacial Events --}}
                    <li>

                        <a class="waves-effect  ">
                            <i class="fa fa-thumb-tack"></i>
                            <span  class="hide-menu">{{ trans('speacialevent.speacialevent') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('speacialevent.create') }}">{{ trans('speacialevent.add_event') }}</a></li>
                            <li><a href="{{ route('speacialevent.index') }}">{{ trans('speacialevent.show_event') }}</a></li>
                        </ul>

                    </li>


                    {{-- Speacial Events item --}}
                 {{--    <li>

                        <a class="waves-effect   ">
                            <i class="fa fa-sitemap"></i>
                            <span  class="hide-menu">{{ trans('speacialevent.eventitem') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('eventitem.create') }}">{{ trans('speacialevent.add_eventitem') }}</a></li>
                            <li><a href="{{ route('eventitem.index') }}">{{ trans('speacialevent.show_eventitem') }}</a></li>
                        </ul>

                    </li> --}}

                    {{-- TeamWork --}}
                    <li>

                        <a class="waves-effect">
                            <i class="fa fa-users"></i>
                            <span  class="hide-menu">{{ trans('dessertdepartment.team') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('team.create') }}">{{ trans('dessertdepartment.add_team') }}</a></li>
                            <li><a href="{{ route('team.index') }}">{{ trans('dessertdepartment.show_team') }}</a></li>
                        </ul>

                    </li>


                    {{--site Settings--}}
                   <li>
                        <a href="#" class="waves-effect  "><i   class="fa  fa-sliders"></i> <span class="hide-menu">{{ trans('about.about_sidebar') }}<span class="fa arrow"></span> </span></a>
                                       <ul class="nav nav-second-level">
                                        <li><a href="{{route('about')}}">{{ trans('about.about') }}</a></li>
                                                   </ul>
                    </li>

                    {{-- Say About Us --}}

                    <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/say"   ? 'active' : ''}}
                        {{Request::path() == "$lang/say/create"   ? 'active' : ''}} ">
                            <i class="fa  fa-volume-up"></i>
                            <span class="hide-menu">{{ trans('say.say') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('say.create') }}">{{ trans('say.create_say') }}</a></li>
                            <li><a href="{{ route('say.index') }}">{{ trans('say.show_says') }}</a></li>
                        </ul>

                    </li>

                    <!-- newsletter Module -->
                    <li>

                        <a href="#" class="waves-effect  {{Request::path() == "$lang/newsletter"   ? 'active' : ''}}
                        {{Request::path() == "$lang/newsletter/send-email"   ? 'active' : ''}} ">
                            <i class="fa  fa-envelope-o"></i>
                            <span class="hide-menu">{{ trans('newsletter.newsletter') }}
                                <span class="fa arrow"></span>
                                </span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('newsletter') }}">{{ trans('newsletter.show_all_subscriptions') }}</a></li>
                        </ul>

                    </li>


                      {{--visitors messages--}}
                   <li ><a href="{{route('visitorsMessages.index')}}" class="waves-effect  ">
                                            <i   class="fa fa-mail-forward"></i>
                                            <span class="hide-menu">{{ trans('backend.visitors_messages') }}</span></a>


                   </li>


              {{--end public section--}}

                </ul>




            </div>
        </div>
        <!-- Left navbar-header end
