@extends('backend.app')

@section('page_title' , trans('backend.slider_module'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('backend.slider_module') }}</h4>
    </div>





@endsection

@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
@endphp



@section('content')

    <div class="col-sm-12">
        <div class="white-box">


            <a href="{{ route('slider.create' ) }}" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; ">{{ trans('backend.add_slide') }}</a>

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('backend.slider_title') }}</th>
                      

                        <th>{{ trans('backend.slider_image') }}</th>
                        <th>{{ trans('backend.status') }}</th>




                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($sliders->count())


                        @foreach($sliders as $slide)




                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $slide->title }}  </td>

                                 <td>
                             
 
                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="{{ asset("uploads/slider/$slide->image") }}" data-effect="mfp-3d-unfold " data-effect="mfp-zoom-out" class="fresco">
                                                <img style="width: 70px; height:70px;border-radius:50%;" src="{{ asset("uploads/slider/$slide->image") }}" class="img-responsive fresco" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>
                                    </div>

                                </td>

                                
 
                                <td>
                                    @if($slide->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>





                                <td class="text-nowrap ">
 

                                     
                                   <a  href="{{ route('slider.edit' , $slide->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a  onclick="$('.dessertdepartment_{{ $slide->id }}').submit();"

                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    {!! Form::open(['route'=>["slider.destroy" , $slide->id ] ,
                                    'class'=>"dessertdepartment_$slide->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('backend.no_slide_found') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection





{{--  --}}
