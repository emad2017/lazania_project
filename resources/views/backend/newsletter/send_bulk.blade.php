 @extends('backend.app')

@section('page_title' , trans('newsletter.newsletter'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('newsletter.newsletter') }}</h4> 
                    </div>


	 
				 


@endsection  


@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
@endphp



@section('content')

 		 
 		 	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mail_listing">
 		 	 	<div class="white-box">
                                    <h3 class="box-title">{{ trans('newsletter.compose_email') }}</h3>
									
									{!! Form::open([ 'route'=>["postBulck"] , 'method'=>'POST']) !!}


	                                   
	                                    <div class="form-group">
	                                        <input class="form-control" name="subject" placeholder="{{ trans('newsletter.subject') }}"> </div>
	                                    <div class="form-group">
	                                        <textarea class="textarea_editor form-control"  name="message" ></textarea>
	                                    </div>
	                                    
	                                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> &nbsp; {{ trans('newsletter.compose') }}</button>
	                                    <button class="btn btn-default"><i class="fa fa-times"></i> &nbsp; {{ trans('newsletter.discard') }}</button>
									
									{!! Form::close() !!}


                                </div>

                            </div>


@endsection



@section('scripts')

    

@endsection 