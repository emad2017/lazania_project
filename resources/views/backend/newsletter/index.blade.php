@extends('backend.app')

@section('page_title' , trans('newsletter.newsletter'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('newsletter.newsletter') }}</h4> 
                    </div>


	 
				 


@endsection  


@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
@endphp



@section('content')


	 <div class="col-md-12 col-sm-12">

                        <div class="white-box">
                             <a href='{{ url("newsletter/send-bulck") }}' class="btn btn-inverse btn-md btn-rounded pull-right">{{ trans('newsletter.send_bulck') }}</a>
                            <h3 class="box-title">{{ trans('newsletter.show_all_subscriptions') }}</h3>
                            <p class="text-muted">{{ trans('newsletter.newsletter_note') }} </p>


                            <div class="table-responsive">
                                <table class="table full-color-table full-inverse-table hover-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('newsletter.email') }}</th>
                                            <th>{{ trans('newsletter.subscription_date') }}</th>
                                            <th>{{ trans('backend.action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @if($subscriptions->count())
                                        
                                            @foreach($subscriptions as $subscription)
                                                <tr>
                                                     <td>{{ $loop->iteration }}</td>
                                                     <td>{{ $subscription->email }}</td>
                                                     <td>{{  date('l jS \of F Y ' , strtotime($subscription->created_at))  }}</td>
                                                     <td>
                                                        <a  href="{{ route('sendEmail' , $subscription->id) }}" data-toggle="tooltip"
                                                                data-original-title="{{ trans('newsletter.send_single_email') }}">
                                                                <i class="fa fa-envelope-o text-success"></i> 
                                                        </a>
                                                     </td>
                                                </tr>
                                            @endforeach

                                        @else
                                        <tr>
                                            <td colspan="4"><p class="text-center">{{ trans('newsletter.no_subscriptions_found') }}</p></td>
                                        </tr>
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


@endsection



@section('scripts')

    

@endsection 