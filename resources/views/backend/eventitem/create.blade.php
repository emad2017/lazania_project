@extends('backend.app')

@section('page_title' , trans('speacialevent.eventitem_name'))

@section('breadcrumb')

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('speacialevent.eventitem_name') }}</h4>
    </div>

@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();


    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;



@endphp



@section('content')


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> {{ trans('speacialevent.eventitem_create') }} </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">



                            <nav>
                                <ul>
                                    @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>
                                        {{--  <li><a href="#section-iconbox-2" class="sticon ti-gift"><span>Deals</span></a></li> --}}
                                    @endforeach

                                </ul>
                            </nav>





                       {!! Form::open(['route'=>['eventitem.store'],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true ,
                       'id'=>'add_files']) !!}

                            @foreach($languages as $language)
                            <div class="content-wrap">

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$language->label}}">



                                        <!--  Name -->

                                        <div class="form-group{{ $errors->has("eventitem_name_$language->label") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('speacialevent.eventitem_name_') }}</label>
                                            <div class="input-group">

                                                <input type="text" class="form-control" name="eventitem_name_{{ $language->label }}"
                                                       value=""
                                                       id="name_{{ $language->label }}" placeholder="{{ trans('speacialevent.eventitem_name_') }}">
                                                @if ($errors->has("eventitem_name_$language->label"))
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li>{{ $errors->first("eventitem_name_$language->label") }}</li></ul>
                                                 </span>
                                                @endif

                                            </div>
                                        </div>


                                        <!--  Description -->

                                        <div class="form-group{{ $errors->has("eventitem_description_$language->label") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('speacialevent.eventitem_description_') }}</label>
                                            <div class="input-group">

                                                <input type="text" class="form-control" name="eventitem_description_{{ $language->label }}"
                                                       value=""
                                                       id="description_{{ $language->label }}" placeholder="{{ trans('speacialevent.eventitem_description_') }}">
                                                @if ($errors->has("eventitem_description_$language->label"))
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li>{{ $errors->first("eventitem_description_$language->label") }}</li></ul>
                                                 </span>
                                                @endif
                                            </div>
                                        </div>


                                    </section>



                            @endforeach
                            <!--  Price -->

                                <div class="form-group col-md-12">
                                    <label for="">{{ trans('dessertdepartment.dessert_department_price') }}</label>

                                    <input type="text" class="form-control" name="price"
                                           value=""
                                           placeholder="{{ trans('dessertdepartment.dessert_department_price') }}">


                                </div>

                            <!--  Rate -->
                                <div class="form-group col-md-12">
                                    <label for="">{{ trans('dessertdepartment.dessert_department_rate') }}</label>
                                    <br>
                                    <select class="selectpicker m-b-20 m-r-10 col-md-3" name="rate" data-style="btn-info btn-outline"  title="{{trans('dessertdepartment.dessert_department_rate_enter')}}">
                                        <option value="1" data-content="<span class='label label-info'>One - <i class='fa fa-star'> </i></span>"></option>
                                        <option value="2" data-content="<span class='label label-info'>Two - <i class='fa fa-star'> </i><i class='fa fa-star'> </i></span>"></option>
                                        <option value="3" data-content="<span class='label label-info'>Three -  <i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i></span>"></option>
                                        <option value="4" data-content="<span class='label label-info'>Four - <i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i></span>"></option>
                                        <option value="5" data-content="<span class='label label-info'>Five - <i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i><i class='fa fa-star'> </i></span>"></option>
                                    </select>
                                </div>

                            <!--  Select item department of this item -->
                                <div class="form-group">

                                    <label for=" "> &nbsp;&nbsp;&nbsp; &nbsp; {{ trans('medicalSupplies.medical_supply_company') }}</label>
                                    <div class="col-md-12">

                                        <select class="selectpicker m-b-20 m-r-10" name="itemevent" data-style="btn-info btn-outline">



                                        @foreach($items as $item)


                                            @foreach($item->description as $description)

                                                <!-- simple check on language -->
                                                    @if($description->language_id == $language_id )
                                                        <option value="{{ $item->id}}"
                                                                data-tokens="ketchup mustard">{{ $description->name }}</option >
                                                    @endif

                                                @endforeach

                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                {{-- image --}}

                                <div class="form-group ">

                                    <label for=" ">  &nbsp; {{ trans('speacialevent.eventitem_img') }}</label>
                                    <div class="col-md-12">
                                        <div class="white-box">


                                            <input type="file" id="input-file-now" name="file[]" multiple class="dropify" />
                                        </div>
                                    </div>


                                </div>

                                {{-- Status of Department--}}
                                <div class="form-group">
                                    <label for=" "> &nbsp; {{ trans('backend.status') }}</label>

                                    <div class="col-md-12">
                                        &nbsp; &nbsp; &nbsp;
                                        <input type="checkbox" name="status" checked class="js-switch" data-color="#99d683" />
                                    </div>

                                </div>

                                {{-- End Status of Department--}}

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.add') }}</button>
                                    <button type="reset" value="Reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button>

                                </div>
                            </div>






                            {!! Form::close() !!}



                        </div>

                        <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("article.select_images") }}</p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
@endsection
