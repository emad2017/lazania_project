@extends('backend.app')

@section('page_title' , trans('speacialevent.eventitem_name'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('speacialevent.eventitem_name') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('speacialevent.eventitem_name_') }}</th>
                        <th>{{ trans('speacialevent.eventitem_description_') }}</th>
                        <th>{{ trans('speacialevent.eventitem_img') }}</th>
                        <th>{{ trans('dessertdepartment.name_blog_d') }}</th>

                        <th>{{ trans('dessertdepartment.dessert_department_price') }}</th>
                        <th>{{ trans('dessertdepartment.dessert_department_rate') }}</th>

                        <th>{{ trans('speacialevent.eventitem_addby') }}</th>
                        <th>{{ trans('speacialevent.eventitem_date') }}</th>
                        <th>{{ trans('speacialevent.eventitem_status') }}</th>



                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($items->count())


                        @foreach($items as $item)





                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @foreach($item->description as $description)
                                        {{ $description->name }} <br>
                                    @endforeach
                                </td>





                                <td>
                                    <span class="">
                                         @foreach($item->description as $description)
                                            {{ $description->description }} <br>
                                        @endforeach

                                    </span>
                                </td>


                                    {{-- image show --}}

                                    <td>



                                        <div class="col-sm-4" id="image-popups">
                                            <a class="image-popup-vertical-fit" data-effect="mfp-zoom-in" title=" @foreach($item->description as $description)
                                            {{ $description->name }} <br>
                                            @endforeach"
                                               href="{{ asset("uploads/speacialevent/$item->img1") }}">
                                                <img style="width: 200px; height: 100px;" src="{{ asset("uploads/speacialevent/$item->img1") }}" class="img-responsive" /> </a>

                                        </div>
                                        <style>
                                            .img-responsive
                                            {
                                                max-width: none !important;

                                            }
                                        </style>

                                    </td>

                                    <td>

                                        @php
                                       //   dd($item->description);
                                        @endphp
                                        @foreach($item->department->description as $de)

                                            <span> {{ $de->name}}</span>
                                            <br> <br>

                                        @endforeach

                                    </td>
                                    {{-- Price of dessert--}}
                                    <td>

                                        <span> {{$item->price}} </span>
                                    </td>

                                    {{-- Rate of dessert--}}
                                    <td>  <i class="fa fa-star"></i> <span> {{$item->rate}} </span></td>
                                    <style>
                                        .fa-star{
                                            color: gold !important;
                                        }
                                    </style>




                                    <td>
                                    <span class="label label-info ">{{ $item->user->headPersonName }}</span>
                                </td>

                                <td>{{ $item->created_at}}</td>



                                <td>
                                    @if($item->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>




                                <td class="text-nowrap">






                                    <a  href="{{ route('eventitem.edit' , $item->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a onclick="$('.eventitem_{{ $item->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                    {!! Form::open(['route'=>["eventitem.destroy" , $item->id ] ,
                                    'class'=>"eventitem_$item->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('speacialevent.eventitem_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}
