@extends('backend.app')

@section('page_title' , trans('speacialevent.eventitem_name'))

@section('breadcrumb')

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('speacialevent.eventitem_name') }}</h4>
    </div>

@endsection


@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp


@section('content')


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> {{ trans('speacialevent.eventitem_edit') }} </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">



                            <nav>
                                <ul>
                                    @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>

                                    @endforeach

                                </ul>
                            </nav>

                            {!! Form::model($eventitem , ['route'=>['eventitem.update',$eventitem->id],
                            'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'add_files']) !!}
                            {!! method_field('put') !!}


                                <div class="content-wrap">
                                    @foreach($eventitem->description as $description)

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$language->label}}">



                                        <!--  Name -->


                                        <div class="form-group{{ $errors->has("eventitem_name_".$language->label) ? ' has-error' : '' }}">
                                            <label for="">{{ trans('speacialevent.eventitem_name_') }}</label>
                                            <div class="input-group">


                                                <input type="text" class="form-control" name="eventitem_name_{{$description->language->label}}"
                                                       value="{{$description->name}}"

                                                       placeholder="{{ trans('speacialevent.eventitem_name_') }}">

                                            </div>
                                        </div>





                                        <!--  Description -->

                                        <div class="form-group{{ $errors->has("eventitem_desription_$language->label") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('speacialevent.eventitem_description_') }}</label>
                                            <div class="input-group">

                                                <input type="text" class="form-control" name="eventitem_desription_{{$description->language->label}}"
                                                       value="{{$description->description}}"

                                                       placeholder="{{ trans('speacialevent.eventitem_updated') }}">

                                            </div>
                                        </div>




                                    </section>


                                    @endforeach








                                    {{-- Price edit  --}}


                                        <div class="form-group col-md-12">
                                            <label for="">{{ trans('dessertdepartment.dessert_department_price') }}</label>

                                            <input type="text" class="form-control" name="price"
                                                   value="{{$eventitem->price}}"
                                                   placeholder="{{ trans('dessertdepartment.dessert_department_price') }}">
                                        </div>







                                        <!--  Select item department of this item -->
                                        <div class="form-group">

                                            <label for=" "> &nbsp;&nbsp;&nbsp; &nbsp; {{ trans('medicalSupplies.medical_supply_company') }}</label>
                                            <div class="col-md-12">

                                                <select class="selectpicker m-b-20 m-r-10" name="itemevent" data-style="btn-info btn-outline">

                                                    @foreach($speacialdepartments as $speacialdepartment)

                                                        @foreach( $speacialdepartment->description as $description)

                                                            @if($description->language_id ==  $language_id )

                                                                <option value="{{ $speacialdepartment->id}}"

                                                                    {!!
                                                                     $speacialdepartment->id == $eventitem->event_id ?
                                                                     'selected' : ''
                                                                     !!}
                                                                        data-tokens="ketchup mustard">
                                                                    {{$description->name}}


                                                                </option>



                                                                @endif

                                                            @endforeach



                                                        @endforeach

                                                </select>
                                            </div>
                                        </div>
















                                        {{-- image --}}

                                    <div class="form-group ">

                                        <label for=" ">  &nbsp; {{ trans('speacialevent.eventitem_img') }}</label>
                                        <div class="col-md-12">
                                            <div class="white-box">


                                                <input type="file" id="input-file-now" name="file[]" multiple class="dropify" />
                                            </div>
                                        </div>


                                    </div>

                                    {{-- Status of Department--}}
                                        <div class="form-group">
                                            <label for=" "> &nbsp; {{ trans('backend.status') }}</label>

                                            <div class="col-md-12">
                                                &nbsp; &nbsp; &nbsp;
                                                <input <?php  echo $eventitem->status=='active' ? 'checked':''; ?> type="checkbox" name="status"  class="js-switch" data-color="#99d683" />
                                            </div>

                                        </div>


                                        {{-- End Status of Department--}}

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.update') }}</button>
                                            <button type="reset" value="Reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button>

                                        </div>
                                    </div>






                                    {!! Form::close() !!}



                                </div>

                                <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("article.select_images") }}</p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
@endsection