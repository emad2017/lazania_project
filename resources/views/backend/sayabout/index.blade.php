@extends('backend.app')

@section('page_title' , trans('dessertdepartment.team'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.team') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
            <div class="table-responsive">
                <table class="table   table-hover color-table info-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('about.name') }}</th>
                        <th>{{ trans('about.msg') }}</th>
                        <th>{{ trans('about.img') }}</th>

                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($says->count())


                        @foreach($says as $say)

                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                  {{ $say->name }}
                                </td>





                                <td>
                                    <span class="">
                                        {{ $say->description }}

                                    </span>
                                </td>

                                <td>
                                    {{-- image show --}}


                                    <div class="" id="images">

                                        <div class="col-sm-2">
                                            <a  href="{{ asset("uploads/sayaboutus/$say->img") }}" data-effect="mfp-3d-unfold">
                                                <img style="width: 100px; height: 50px;" src="{{ asset("uploads/sayaboutus/$say->img") }}" class="img-responsive" />
                                            </a>

                                            <style>
                                                .img-responsive
                                                {
                                                    max-width: none !important;

                                                }
                                            </style>
                                        </div>


                                    </div>

                                </td>



                                <td class="text-nowrap">





                                    <a onclick="$('.team_{{ $say->id }}').submit();"
                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>




                                        {!! Form::open(['route'=>["sayabout.destroy" , $say->id ] ,
                                        'class'=>"team_$say->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.team_zero') }}</td>

                        </tr>

                    @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection







{{--  --}}
