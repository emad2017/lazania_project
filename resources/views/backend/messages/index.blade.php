@extends('backend.app')

@section('page_title' , trans('backend.messages'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.messages') }}</h4>
                    </div>


@endsection



@section('content')

      <div class="col-md-12">



                         <div class="white-box">


                             <!-- row -->
                             <div class="row">
                             
                                   <button type="button" class="btn btn-primary "  aria-expanded="false">{{trans('backend.send_message')}}</button>

                                   <br>
                                   <br>


                              




                                 <div class="col-lg-12 col-md-9 col-sm-12 col-xs-12 mail_listing" style="border: 0px">
                                     <div class="inbox-center">
                                         <table class="table table-hover">

                                             <tbody>
                                                 @if($messages->count()>0)
                                                 @foreach($messages as $message)
                                                 <tr class=<?php  echo $message->is_read =='not_read' ?"unread":" ";?>>

                                                    <td class="hidden-xs">

                                                    <img src="{{Request::root()}}/uploads/users/{{$message->sender->profileImage}}"  style="height:40px ;width:40px;border:1px solid #03a9f3 " class="img-circle" >

                                                     </td>
                                                     <td class="hidden-xs">{{$message->sender->propertyName}}</td>
                                                     <td class="max-texts"> <a href="{{route('messages.show',$message->id)}}" />

                                                       @if($message->sender->userType =="user")
                                                      <span class="label label-info m-r-6">{{trans('backend.user')}}</span>

                                                       @elseif($message->sender->userType =="doctor")
                                                       <span class="label label-purple m-r-10">{{trans('backend.doctor')}}</span>
                                                       @elseif($message->sender->userType =="pharmacy")
                                                       <span class="label label-brown m-r-10">{{trans('backend.pharmacy')}}</span>
                                                       @elseif($message->sender->userType =="hospital")
                                                       <span class="label label-lightBlue m-r-10">{{trans('backend.hospital')}}</span>
                                                       @elseif($message->sender->userType =="medicalSupplies")
                                                       <span class="label label-dark m-r-10">{{trans('backend.medical_supply')}}</span>
                                                        @elseif($message->sender->userType =="pharmaCompany")
                                                        <span class="label label-success m-r-10">{{trans('backend.pharma_company')}}</span>
                                                        @elseif($message->sender->userType =="adminstrator")
                                                        <span class="label label-yellow m-r-6">{{trans('backend.adminstrator')}}</span>


                                                       @endif

                                                     </td>
                                                     <td>
                                                     {{$message->message}}
                                                     </td>

                                                     <td class="text-right">{{ date('l, F jS Y' , strtotime($message->created_at)) }}</td>


                                                 </tr>

                                              @endforeach

                                              @else
                                                <tr>

                                                <td class="text-center">there is no messages</td>
                                                </tr>



                                              @endif


                                             </tbody>
                                         </table>
                                     </div>


                                 </div>
                             </div>
                             <!-- /.row -->

                         </div>
                     </div>



@endsection


