@extends('backend.app')

@section('page_title' ,trans('backend.messages') )

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.messages') }}</h4>
                    </div>


@endsection



@section('content')

<div class="chat-main-box">
                    <!-- .chat-left-panel -->
                    <div class="chat-left-aside">
                        <div class="open-panel"><i class="ti-angle-right"></i></div>
                        <div class="chat-left-inner" style="height: 167px;">
                            <div class="form-material">
                                <input class="form-control p-20" type="text" placeholder="Search Contact"> </div>
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><ul class="chatonline style-none " style="overflow: hidden; width: auto; height: 100%;">
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="active"><img src="../plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                                <li class="p-20"></li>
                            </ul><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 0px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 44.1282px;"></div><div class="slimScrollRail" style="width: 0px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                        </div>
                    </div>
                    <!-- .chat-left-panel -->
                    <!-- .chat-right-panel -->
                    <div class="chat-right-aside">
                        <div class="chat-main-header">
                            <div class="p-20 b-b">
                                <h3 class="box-title">Chat Message</h3> </div>
                        </div>
                        <div class="chat-box">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><ul class="chat-list slimscroll p-t-30" style="overflow: hidden; width: auto; height: 0px;">
                                <li>
                                    <div class="chat-image"> <img alt="male" src="../plugins/images/users/ritesh.jpg"> </div>
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <h4>Ritesh</h4>
                                            <p> Hi, Genelia how are you and my son? </p> <b>10.00 am</b> </div>
                                    </div>
                                </li>
                                <li class="odd">
                                    <div class="chat-image"> <img alt="Female" src="../plugins/images/users/genu.jpg"> </div>
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <h4>Genelia</h4>
                                            <p> Hi, How are you Ritesh!!! We both are fine sweetu. </p> <b>10.03 am</b> </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="chat-image"> <img alt="male" src="../plugins/images/users/ritesh.jpg"> </div>
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <h4>Ritesh</h4>
                                            <p> Oh great!!! just enjoy you all day and keep rocking</p> <b>10.05 am</b> </div>
                                    </div>
                                </li>
                                <li class="odd">
                                    <div class="chat-image"> <img alt="Female" src="../plugins/images/users/genu.jpg"> </div>
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <h4>Genelia</h4>
                                            <p> Your movei was superb and your acting is mindblowing </p> <b>10.07 am</b> </div>
                                    </div>
                                </li>
                            </ul><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 38px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 94.5476px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            <div class="row send-chat-box">
                                <div class="col-sm-12">
                                    <input class="form-control" placeholder="Type your message">
                                    <div class="custom-send"><a href="javacript:void(0)" class="cst-icon" data-toggle="tooltip" title="" data-original-title="Insert Emojis"><i class="ti-face-smile"></i></a> <a href="javacript:void(0)" class="cst-icon" data-toggle="tooltip" title="" data-original-title="File Attachment"><i class="fa fa-paperclip"></i></a>
                                        <button class="btn btn-danger btn-rounded" type="button">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .chat-right-panel -->
                </div>



@endsection


