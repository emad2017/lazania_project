@extends('backend.app')

@section('page_title' , trans('dessertdepartment.sub_departments'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.sub_departments') }}</h4>
    </div>





@endsection



@section('content')

    <div class="col-sm-12">
        <div class="white-box">

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('dessertdepartment.dessert_department_name_') }}</th>
                        <th>{{ trans('dessertdepartment.dessert_department_description_') }}</th>

                        <th>{{ trans('dessertdepartment.dessert_department_date') }}</th>
                        <th>{{ trans('dessertdepartment.dessert_department_status') }}</th>




                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if(count($childrens))


                        @foreach($childrens as $sun_cat)




                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    
                                        {{ $sun_cat->name }} <br>
                                 
                                </td>





                                <td>
                                    <span class="">
                                         
                                            {{ $sun_cat->description }} <br>
                                      

                                    </span>
                                </td>


                                <td>{{  date( 'l jS \of F Y h:i:s A' , strtotime($sun_cat->created_at)) }}</td>



                                <td>
                                    @if($sun_cat->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>





                                <td class="text-nowrap text-center">



                                      <a  href="{{ route('items' , $sun_cat->subDeptId) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('dessertdepartment.show_related_items') }}">
                                          <i class="fa fa-align-justify text-inverse m-r-10"></i>
                                      </a>



                                      <a  href="{{ route('addNewItem' , $sun_cat->subDeptId ) }}" data-toggle="tooltip"
                                     data-original-title="{{ trans('dessertdepartment.add_item') }}">
                                     <i class="fa fa-plus-circle text-inverse m-r-10"></i> </a>



                                        <a  href="{{ route('sub_edit' , $sun_cat->subDeptId) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                        <a  onclick="$('.dessertdepartment_{{ $sun_cat->subDeptId }}').submit();"

                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    {!! Form::open(['route'=>["deleteSubDepartment" , $sun_cat->subDeptId ] ,
                                    'class'=>"dessertdepartment_$sun_cat->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.sub_depts_count_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
