@extends('backend.app')

@section('page_title' , trans('dessertdepartment.main_departments'))

@section('breadcrumb')


    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.main_departments') }}</h4>
    </div>





@endsection

@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
@endphp



@section('content')

    <div class="col-sm-12">
        <div class="white-box">


            <a href="{{ route('dessertdepartment.create' ) }}" class="btn btn-inverse btn-sm btn-rounded pull-right" style="margin-bottom: 10px; ">Add new main category</a>

            <div class="table-responsive">
                <table class="table   table-hover color-table info-table " >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('dessertdepartment.dessert_department_name_') }}</th>
                        {{-- <th>{{ trans('dessertdepartment.dessert_department_description_') }}</th> --}}

                        <th>{{ trans('dessertdepartment.dessert_department_date') }}</th>
                        <th>{{ trans('dessertdepartment.dessert_department_status') }}</th>




                        <th class="text-nowrap">{{ trans('backend.action') }}</th>

                    </tr>
                    </thead>
                    <tbody>



                    @if($departments->count())


                        @foreach($departments as $department)




                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @foreach($department->description as $description)
                                        
                                            @if($description->language_id == $language_id )

                                            {{ $description->name }}  

                                            @endif 
                                    @endforeach
                                </td>




{{-- 
                                <td>
                                    <span class="">
                                         @foreach($department->description as $description)
                                            {{ $description->description }} <br>
                                        @endforeach

                                    </span>
                                </td> --}}


                                <td>{{  date( 'l jS \of F Y' , strtotime($department->created_at)) }}</td>



                                <td>
                                    @if($department->status == 'not_active')
                                        <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                    @else
                                        <span class="label label-success">{{ trans('backend.active') }}</span>
                                    @endif

                                </td>





                                <td class="text-nowrap ">





                                    @if(count($department->subDepts))
                                    <a  href="{{ route('sub_departments' , $department->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.show_sub_departments') }}">
                                        <i class="fa fa-align-justify text-inverse m-r-10"></i> </a>

                                    @endif

                                         <a  href="{{ route('add_new_sub' , $department->id ) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('dessertdepartment.add_sub_department') }}">
                                        <i class="fa fa-plus-circle text-inverse m-r-10"></i> </a>

                                        <a  href="{{ route('dessertdepartment.edit' , $department->id) }}" data-toggle="tooltip"
                                        data-original-title="{{ trans('backend.edit') }}">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                    <a  onclick="$('.dessertdepartment_{{ $department->id }}').submit();"

                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                        <i class="fa fa-close text-danger"></i> </a>



                                    {!! Form::open(['route'=>["dessertdepartment.destroy" , $department->id ] ,
                                    'class'=>"dessertdepartment_$department->id" ]) !!}

                                    {!! method_field('DELETE') !!}



                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    @else

                        <tr>
                            <td colspan="7" class="text-center">{{ trans('dessertdepartment.dessert_department_zero') }}</td>

                        </tr>

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection





{{--  --}}
