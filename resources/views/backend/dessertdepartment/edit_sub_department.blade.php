@extends('backend.app')

@section('page_title' , trans('dessertdepartment.edit_sub_department'))

@section('breadcrumb')

    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('dessertdepartment.edit_sub_department') }}</h4>
    </div>

@endsection



@section('content')


    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0"></h3>
            <p class="text-muted m-b-30 font-13"> {{ trans('dessertdepartment.edit_sub_department') }} </p>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">



                            <nav>
                                <ul>
                                    @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>

                                    @endforeach

                                </ul>
                            </nav>






                            {!! Form::model($subDepartment , ['route'=>['update_sub_department',$subDepartment->id],
                            'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'add_files']) !!}
                            {!! method_field('put') !!}


                                <div class="content-wrap">
                                    @foreach($subDepartment->description as $description)

                                    <section style="
                                             padding-bottom: 0px;
                                        " class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$description->language->label}}">



                                        <!--  Name -->

                                        @php $ds_lang = $description->language->label ;   @endphp
                                        <div class="form-group  {{ $errors->has("sub_name_$ds_lang") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('dessertdepartment.dessert_department_name_') }}</label>
                                            <div class="input-group">


                                                <input type="text" class="form-control" name="sub_name_{{$description->language->label}}"
                                                       value="{{$description->name}}"

                                                       placeholder="{{ trans('dessertdepartment.dessert_department_name_') }}">

                                            </div>

                                                @if ($errors->has("sub_name_".$description->language->label))
                                                    <span class="help-block ">
                                                        {{ $errors->first("sub_name_".$description->language->label) }}
                                                    </span>
                                                @endif


                                        </div>





                                        <!--  Description -->

                                        <div class="form-group  {{ $errors->has("sub_description_$ds_lang") ? ' has-error' : '' }}">
                                            <label for="">{{ trans('dessertdepartment.sub_description') }}</label>
                                            <div class="input-group">

                                        

                                                  <textarea name="sub_description_{{ $description->language->label }}"   placeholder="{{ trans('dessertdepartment.sub_description') }}"  >{{ $description->description }}</textarea>

                                            </div>

                                             @if ($errors->has("sub_description_".$description->language->label))
                                                <span class="help-block ">
                                                    {{ $errors->first("sub_description_".$description->language->label) }} 
                                                </span>
                                            @endif


                                        </div>




                                    </section>


                                    @endforeach
                               

                                    <div class="form-group ">

                                        <label for=" ">  &nbsp;   {{ trans('dessertdepartment.sub_dept_image') }}</label>
                                        <div class="col-md-12">
                                            <div class="white-box">


                                                <input type="file" id="input-file-now" name="file"  class="dropify" />
                                            </div>
                                        </div>





                                    </div>

                                    {{-- Status of Department--}}
                                        <div class="form-group">
                                            <label for=""> &nbsp; {{ trans('backend.status') }}</label>

                                            <div class="col-md-12">
                                                &nbsp; &nbsp; &nbsp;
                                                <input <?php  echo $subDepartment->status=='active' ? 'checked':''; ?> type="checkbox" name="status"  class="js-switch" data-color="#99d683" />
                                            </div>

                                        </div>


                                        {{-- End Status of Department--}}

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.update') }}</button>
                                            

                                        </div>
                                    </div>






                                    {!! Form::close() !!}



                                </div>

                                <!-- /tabs -->
                        </div>

                    </section>

                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script>

        jQuery(document).ready(function() {

            $('.dropify').dropify({
                tpl: {
                    wrap:            '<div class="dropify-wrapper"></div>',
                    loader:          '<div class="dropify-loader"></div>',
                    message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("department.select_images") }}</p></div>',
                    preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("article.select_images") }}</p></div></div></div>',
                    filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                    clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                    errorLine:       '<p class="dropify-error"></p>',
                    errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
                }
            });


            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>

    <!-- For Switch  -->
@endsection