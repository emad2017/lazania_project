@extends('backend.app')

@section('page_title' , trans('say.say'))

@section('breadcrumb')

          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('say.say') }}</h4>
                    </div>

@endsection


@section('content')


  <div class="col-sm-12">
              <div class="white-box">
                  <h3 class="box-title m-b-0"></h3>
                  <p class="text-muted m-b-30 font-13"> {{ trans('say.create_say_page') }} </p>
                  <div class="row">
                      <div class="col-sm-12 col-xs-12">
                         <section class="m-t-40">
                          <div class="sttabs tabs-style-iconbox">
 


                             <nav>
                                 <ul>
                                       @foreach($languages as $language)
         <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>
                                    {{--  <li><a href="#section-iconbox-2" class="sticon ti-gift"><span>Deals</span></a></li> --}}
                                       @endforeach

                                 </ul>
                             </nav>


                      {!! Form::open(['route'=>['say.store'],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'add_say']) !!}
                                  @foreach($languages as $language)

                                  <div class="content-wrap">



                                  <section class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$language->label}}">


                              <div class="form-group{{ $errors->has("say_name_$language->label") ? ' has-error' : '' }}">
                                          <label for="">{{ trans('say.say_name') }}</label>
                                          <div class="input-group">

                                              <input type="text" class="form-control" name="say_name_{{$language->label}}"
                                                     id="say_name_{{$language->label}}"
                                                     placeholder="{{ trans('say.say_name') }}">
                                              @if ($errors->has("say_name_$language->label"))
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li>{{ $errors->first("say_name_$language->label") }}</li></ul>
                                                 </span>
                                                 @endif
                                          </div>
                                      </div>




                                       <div class="form-group{{ $errors->has("say_description_$language->label") ? ' has-error' : '' }}">
                                          <label for="">{{ trans('say.say_description') }}</label>
                                          <div class="input-group">

                                      <textarea class="form-control" name="say_description_{{$language->label}}" placeholder="{{ trans('say.say_description') }}" >
                                   </textarea> 


                                        
                                              @if ($errors->has("say_description_$language->label"))
                                                    <span class="help-block with-errors">

                                                   <ul class="list-unstyled"><li>{{ $errors->first("say_description_$language->label") }}</li></ul>
                                                 </span>
                                                 @endif
                                          </div>
                                      </div>

     


                                  </section>

                                  </div>
                                  <!-- /content -->
                              @endforeach



                                <div class="form-group ">
                                    
                                    <label for=" ">  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

                                  {{ trans('say.say_image') }}</label>
                                    <div class="col-md-12">
                                        <div class="white-box" >
                                             
                                            
                                            <input type="file" id="input-file-now" name="image" multiple class="dropify" /> 
                                        </div>
                                    </div>


                                </div>



                             <div class="form-group">
                                          <label for=" "> &nbsp; &nbsp; &nbsp;
                                      {{ trans('say.say_status') }}</label>

                                             <div class="col-md-12">
                                              &nbsp; &nbsp; &nbsp;
                                             <input type="checkbox" name="status" checked class="js-switch" data-color="#99d683" />
                                              </div>

                                 </div>


                                 <div class="form-group">
                                       <div class="col-md-12">
                                      <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">{{ trans('backend.add') }}</button>
                                      <button type="reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button>

                                  </div>
                                  </div>



                                  {!! Form::close() !!}
                     

                          </div>
                          <!-- /tabs -->
                      </section>
                      </div>
                  </div>
              </div>
          </div>





@endsection




@section('scripts')


    <!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {

         $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("about.select_images") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("about.select_images_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });


        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>

@endsection 