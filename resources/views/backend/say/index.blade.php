@extends('backend.app')

@section('page_title' , trans('say.say'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('say.say') }}</h4> 
                    </div>

                  
@endsection  


@section('content')
					
									  <div class="col-sm-12">
                        <div class="white-box">
                            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
                            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover color-table primary-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('say.say_name') }}</th>
                                            <th>{{ trans('say.say_description') }}</th>
                                            <th>{{ trans('say.say_image') }}</th>

                                            <th>{{ trans('say.status') }}</th>
                                            <th class="text-nowrap">{{ trans('say.action') }}</th>
                                         
                                        </tr>
                                    </thead>
                                      <tbody>

                                    	@if($saies->count())
                                    		@php $counter = 1 ; @endphp 
                                    		@foreach($saies as $say)
		                                        <tr>
		                                            <td>{{ $counter }}</td>
		                                                  
                                                       <td>   @foreach($say->description as $description){{ $description->name }}</br>@endforeach </td>

                                                       <td>   @foreach($say->description as $description){{ $description->descriptions }}</br>@endforeach </td>                                                        
                           <td>

                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                            
                                            <img src="
                                 @if(count($say->image) )
                            {{ asset("uploads/say/$say->image") }}
                            @endif" class="thumb-lg img-circle" alt="No Image Uploded"></a>
                            </td>
                               <td>
                                @if($say->status == 'not_active')
                                    <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                @else
                                    <span class="label label-success">{{ trans('backend.active') }}</span>  
                                @endif
                            </td>
		                   
		                            <td class=“text-nowrap”>

                                   <a  href="{{ route('say.edit' , $say->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.edit') }}"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                     <a onclick="$('.medical_form_{{ $say->id }}').submit();"
                                      data-toggle="tooltip" data-original-title="{{ trans('say.delete') }}">
                                       <i class="fa fa-close text-danger"></i> </a>

                                   {!! Form::open(['route'=>["say.destroy" , $say->id ] , 'class'=>"medical_form_$say->id" ]) !!}

                                   {!! method_field('DELETE') !!}

                                
                                   {!! Form::close() !!}

                               </td>
		                                        </tr>
		                                        @php $counter++ ; @endphp 
		                                     @endforeach
                                        @else
											
											<tr>
												<td colspan="6" class="text-center">{{ trans('say.say_count_zero') }}</td>
											</tr>

                                        @endif
                                        
                                    </tbody>


                                </table>
                            </div>
                        </div>
                                      </div>
                    </div>


@endsection 
