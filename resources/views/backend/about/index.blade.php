@extends('backend.app')

@section('page_title' , trans('about.about_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('about.about') }}</h4> 
                    </div>


	 
				 


@endsection  


@php 
    
    $lang = LaravelLocalization::getCurrentLocale(); 
    $laguage = \App\Language::where(['label'=>$lang])->first(); 
    $language_id = $laguage->id ; 
 
@endphp



@section('content')


	
	<div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"></h3>
                <p class="text-muted m-b-30 font-13"> {{ trans('about.about_edit') }} </p>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                       <section class="m-t-40">
                        <div class="sttabs tabs-style-iconbox">
                            <nav>
                                <ul>
                                      @foreach($languages as $language)
                                        <li><a href="#{{$language->label}}"  ><span> &nbsp;{{ strtoupper($language->label) }}</span></a></li>
                                  
                                      @endforeach
                                    
                                </ul>
                            </nav>
                    {!! Form::model( $about ,  ['route'=>['about_update' , $about->id],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'article_update']) !!}
                                `
                                {!! method_field('put') !!}
                                <div class="content-wrap">
                                @foreach($about->description as $description)

                                    
                                    
                                <section class="{{$loop->iteration == 1 ? 'active' : '' }}" id="{{$description->language->label}}">


                                    {{-- Name  --}}

                                     <div class="form-group">
                                        <label for="">{{ trans('about.about_title') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="name_{{$description->language->label}}"
                                                   id="name_{{$description->language->label}}" placeholder="{{ trans('about.about_title') }}"
                                                   value="{{ $description->name }}">
                                        </div>
                                    </div>


                                    {{-- Description  --}}

                                    <div class="form-group">
                                        <label for="">{{ trans('about.about_description') }}</label>
                                        <div class="input-group">
                                            
                                        

                                             <textarea id="textarea_department_description" name="about_description_{{$description->language->label}}"
                                                       placeholder="{{ trans('about.about_description') }}"  >{{ $description->description }}</textarea>
                                        </div>
                                    </div>

                                    {{-- About website   --}}

                                    <div class="form-group">
                                        <label for="">{{ trans('about.about') }}</label>
                                        <div class="input-group">



                                             <textarea id="textarea_department_description" name="about_{{$description->language->label}}"
                                                       placeholder="{{ trans('about.') }}"  >{{ $description->about }}</textarea>
                                        </div>
                                    </div>

                                    {{-- address  --}}

                                    <div class="form-group">
                                        <label for="">{{ trans('about.address') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="address_{{$description->language->label}}"
                                                   id="name_{{$description->language->label}}" placeholder="{{ trans('about.about_title') }}"
                                                   value="{{ $description->address }}">
                                        </div>
                                    </div>

                                    
                                </section>
                                    

                                    


                            @endforeach
                        
                                </div>
                            <!--  Facebook -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.f') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="facebook"
                                           value="{{ $about->facebook }}"  placeholder="{{ trans('dessertdepartment.f_link') }}">

                                </div>
                            </div>
                            <!-- End Facebook -->

                            <!--  Twitter -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.t') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="twitter"
                                           value="{{ $about->twitter }}" placeholder="{{ trans('dessertdepartment.t_link') }}">

                                </div>
                            </div>
                            <!-- End Twitter -->
                            <!--  Google -->

                            <div class="form-group col-md-12">
                                <label for="">{{ trans('dessertdepartment.g') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="google"
                                           value="{{ $about->google }}" placeholder="{{ trans('dessertdepartment.g_link') }}">

                                </div>
                            </div>
                            <!-- End Google -->

                            {{-- Time Start => [ 10 PM  ]  --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.timestart') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="timestart"
                                           placeholder=" write it like =>  [ 10 PM or 10 AM ]" value="{{ $about->timestart }}">
                                </div>
                            </div>

                            {{-- Time End => [ 10 PM  ]  --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.timeend') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="timeend"
                                           placeholder=" write it like =>  [ 10 PM or 10 AM ]" value="{{ $about->timeend }}">
                                </div>
                            </div>

                            {{-- number of Class --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.class') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="class"
                                           placeholder="{{ trans('about.class') }}"value="{{ $about->class_num }}">
                                </div>
                            </div>

                            {{-- number of ketchen --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.ketchen') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="ketchen"
                                           placeholder="{{ trans('about.ketchen') }}"value="{{ $about->kitchen_num }}">
                                </div>
                            </div>

                            {{-- number of master --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.master') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="master"
                                           placeholder="{{ trans('about.master') }}"value="{{ $about->master_num }}">
                                </div>
                            </div>

                            {{-- number of staff --}}

                            <div class="form-group col-md-12">
                                <label for="">&nbsp;&nbsp;{{ trans('about.staff') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="staff"
                                           placeholder="{{ trans('about.staff') }}"value="{{ $about->staff_num }}">
                                </div>
                            </div>

                            {{-- phone 1   --}}

                                <div class="form-group">
                                        <label for="">&nbsp;&nbsp;{{ trans('about.telephone_one') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="telephone_one"  placeholder="{{ trans('about.telephone_one') }}" value="{{ $about->telephone1 }}"> 
                                        </div>
                                </div>


                            {{-- phone 2   --}}

                                <div class="form-group">
                                        <label for="">&nbsp;&nbsp;{{ trans('about.telephone_two') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                            <input type="text" class="form-control" name="telephone_two"  placeholder="{{ trans('about.telephone_two') }}" value="{{ $about->tele }}">
                                        </div>
                                </div>

                            {{--  Since year   --}}

                            <div class="form-group">
                                <label for="">&nbsp;&nbsp;{{ trans('about.Since') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="Since_year"
                                           placeholder="{{ trans('about.Sinces') }}" value="{{ $about->since_year }}">
                                </div>
                            </div>

                            {{--  Website Email  --}}

                            <div class="form-group">
                                <label for="">&nbsp;&nbsp;{{ trans('about.email') }}</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa  fa-h-square"></i></div>
                                    <input type="text" class="form-control" name="email"
                                           placeholder="{{ trans('about.email') }}" value="{{ $about->email }}">
                                </div>
                            </div>


                            {{-- Image of infomation  --}}

                            <div class="form-group ">

                                <label for=" ">  &nbsp;   {{ trans('about.info_img') }}</label>
                                <div class="col-md-12">
                                    <div class="white-box">


                                        <input type="file" id="input-file-now" name="file[]" multiple class="dropify" />
                                    </div>
                                </div>


                            </div>


                            {{-- status  --}}

                            <div class="form-group">
                                        <label for=" "> &nbsp; {{ trans('about.about_status') }}</label> 
                                           
                                   <div class="col-md-12">
                                        &nbsp; &nbsp; &nbsp;  &nbsp;
                                       <input type="checkbox" name="status" {!! $about->status == 'active' ? 'checked':'' !!}  class="js-switch" data-color="#99d683" />
                                    </div>
                                         
                                </div>

                                




                               <div class="form-group">
                                     <div class="col-md-12">
                                     
                                    <button type="submit" id="submit" class="btn btn-info btn-circle btn-lg"  data-toggle="tooltip" data-original-title="{{ trans('backend.update') }}"><i class="fa fa-check"></i> </button>

                                    
                                </div>
                                </div>

                                

                                {!! Form::close() !!}


                                
                        </div>
                        <!-- /tabs -->
                    </section>
                    </div>
                </div>
            </div>
        </div>




@endsection



@section('scripts')


	<!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {

         $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("about.select_images") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("about.select_images_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });


        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>

@endsection 