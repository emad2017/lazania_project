@extends('backend.app')

@section('page_title' , trans('backend.user_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.user_profile') }}</h4> 
                    </div>

                  



@endsection  


@section('content')
                 

                 <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> 
                                <img width="100%" alt="user" src="  {{ asset('avatars/bg.jpg') }}">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                            
                                            <img src="
                                                        @if($user->userType == 'adminstrator')
                                                            @if(count($user->profileImage) )
                                                                    {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/adminstrator-avatar.png') }}
                                                            @endif
                                                        @elseif($user->userType == 'doctor')
                                                             @if(count($user->profileImage) )
                                                                {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/doctor-avatar.jpg') }}
                                                            @endif
                                                        @elseif($user->userType == 'pharmacy')

                                                             @if(count($user->profileImage) )
                                                                {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/pharmacy-avatar.jpg') }}
                                                            @endif
                                                            
                                                        @elseif($user->userType == 'hospital')
                                                             @if(count($user->profileImage) )
                                                                {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/hospital-avatar.png') }}
                                                            @endif
                                                            
                                                        @elseif($user->userType == 'medicalSupplies')

                                                             @if(count($user->profileImage) )
                                                                {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/medical-supply-avatar.png') }}
                                                            @endif
                                                            
                                                        @elseif($user->userType == 'PharmaCompany')
                                                             @if(count($user->profileImage) )
                                                                {{ asset("uploads/users/$user->profileImage") }}
                                                            @else
                                                                {{ asset('avatars/pharma-company-avatar.jpg') }}
                                                            @endif
                                                            
                                                        @endif " class="thumb-lg img-circle" alt="img"></a>
                                        
                                        <h4 class="text-white">{!! $user->userType=='doctor' ? $user->doctorName  : $user->headPersonName !!} </h4>
                                        <h5 class="text-white">{{ $user->email }}</h5> </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                
                                @if($user->userType == 'doctor')

                                      
                                    <div class="col-md-6 col-sm-6 text-center">
                                        <p class="text-black">{{ trans('backend.user_mobile_number') }} &nbsp; <i class="ti-mobile text-purple"></i></p>
                                        <h5>{{ $user->mobileNumber }}</h5> 
                                    </div>
                                    <div class="col-md-6 col-sm-6 text-center">
                                        <p class="text-black"> {{ trans('backend.status') }} &nbsp; <i class=" fa fa-info-circle text-purple"></i></p>
                                        <h5>
                                            @if($user->isActive == 'not_active')
                                                <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                            @else
                                                <span class="label label-success">{{ trans('backend.active') }}</span>  
                                            @endif
                                        </h5> 
                                    </div>


                                @elseif($user->userType == 'adminstrator')

                                     <div class="col-md-12 col-sm-12 text-center">
                                        <p class="text-black">{{ trans('backend.user_mobile_number') }} &nbsp; <i class="ti-mobile text-purple"></i></p>
                                        <h5>{{ $user->mobileNumber }}</h5> 
                                    </div>


                                @else

                                     <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-black">{{ trans('backend.user_phone_number') }} &nbsp; <i class="fa fa-phone text-purple"></i> </p>
                                    <h5>{{ $user->phoneNumber }}</h5> 
                                    </div>
                                    <div class="col-md-4 col-sm-4 text-center">
                                        <p class="text-black">{{ trans('backend.user_mobile_number') }} &nbsp; <i class="ti-mobile text-purple"></i></p>
                                        <h5>{{ $user->mobileNumber }}</h5> 
                                    </div>
                                    <div class="col-md-4 col-sm-4 text-center">
                                        <p class="text-black"> {{ trans('backend.status') }} &nbsp; <i class=" fa fa-info-circle text-purple"></i></p>
                                        <h5>
                                            @if($user->isActive == 'not_active')
                                                <span class="label label-danger">{{ trans('backend.not_active') }}</span>
                                            @else
                                                <span class="label label-success">{{ trans('backend.active') }}</span>  
                                            @endif
                                        </h5> 
                                    </div>

                                @endif

                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav customtab nav-tabs" role="tablist">
                               
                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">{{ trans('backend.user_profile_info') }}</span></a></li>
                                
                                <li role="presentation" class="nav-item"><a href="#settings" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">{{ trans('backend.user_profile_setting') }}</span></a></li>
                            </ul>
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="profile">
                                    <div class="row">
                                        


                                        @if($user->userType == 'doctor')

                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.doctor_name') }}</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->doctorName }}</p>
                                        </div>


                                        @elseif($user->userType == 'adminstrator')
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_head_person') }}</strong>
                                                <br>
                                                <p class="text-muted">{{ $user->headPersonName }}</p>
                                            </div>  


                                        @else
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_property') }}</strong>
                                                <br>
                                                <p class="text-muted">{{ $user->propertyName }}</p>
                                            </div>


                                        @endif



 
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_type') }}</strong>
                                            <br>
                                            <br>
                                                        @if($user->userType == 'adminstrator')
                                                            <span class="label label-success">{{ trans('backend.adminstrator') }}</span>
                                                        @elseif($user->userType == 'doctor')
                                                            <span class="label label-warning">{{ trans('backend.doctor') }}</span>  
                                                        @elseif($user->userType == 'pharmacy')
                                                            <span class="label label-danger">{{ trans('backend.pharmacy') }}</span>  
                                                        @elseif($user->userType == 'hospital')
                                                            <span class="label label-info">{{ trans('backend.hospital') }}</span>
                                                        @elseif($user->userType == 'medicalSupplies')
                                                            <span class="label label-primary">{{ trans('backend.medical_supply') }}</span>
                                                        @elseif($user->userType == 'PharmaCompany')
                                                            <span class="label label-inverse">{{ trans('backend.pharma_company') }}</span>
                                                        @endif
                                        </div>
                                        
                                        @if($user->userType == 'doctor')
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.proffestionPracticeNumber') }}
                                                </strong>
                                                <br>
                                                <p class="text-muted">{{ $user->proffestionPracticeNumber }}</p>
                                            </div>

                                        @elseif($user->userType == 'adminstrator')
                                            
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_email') }}</strong>
                                                    <br>
                                                    <p class="text-muted">{{ $user->email }}</p>
                                              </div>

                                           

                                        @else

                                              <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_business_record') }}</strong>
                                                    <br>
                                                    <p class="text-muted">{{ $user->businessRecordNumber }}</p>
                                              </div>

                                        @endif

                                      
                                       {{--  <div class="col-md-3 col-xs-6"> <strong>{{ trans('backend.user_location') }}</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->location }}</p>
                                        </div> --}}
                                    </div>
                                    <hr>


                                    <div class="row">
                                        
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>{{ trans('backend.user_website') }}</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->website }}</p>
                                        </div>


                                         @if($user->userType == 'doctor')

                                            <div class="col-md-3 col-xs-6"> <strong>{{ trans('backend.hospital_related_to') }}</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->doctorHospitalRelatedTo }}</p>
                                        </div>

                                         @elseif($user->userType == 'adminstrator')


                                           



                                         @else

                                            <div class="col-md-3 col-xs-6"> <strong>{{ trans('backend.user_branch_name') }}</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->branchName }}</p>
                                        </div>

                                         @endif
                                        
                                        
                                    </div>
                                    <hr>
                                   
                                     
                                </div>

                                
                                <div class="tab-pane" id="settings">
                                    {!! Form::model($user, array('route' => array('user.update', $user->id) , 'class'=> 'form-horizontal form-material' , 'files'=>true , 'method'=>'POST'))  !!}
                                        {!! method_field('put') !!}
                                        
                                        <div class="form-group {!! $user->userType == 'adminstrator' ? 'hide':'' !!}">
                                            
                                            @if($user->userType == 'doctor')
                                            
                                            <label class="col-md-12">{{ trans('backend.doctor_name') }}</label>
                                            
                                            @else

                                                <label class="col-md-12">{{ trans('backend.user_property') }}</label>
                                            @endif


                                            <div class="col-md-12">

                                                
                                                @if($user->userType == 'doctor')
                                                            

                                                      <input type="text" placeholder="{{ trans('backend.doctor_name') }}" name="doctorName" value="{{ $user->doctorName }}" class="form-control form-control-line"> 

                                                @else
                                                

                                                     <input type="text" placeholder="{{ trans('backend.user_property') }}" name="user_property" value="{{ $user->propertyName }}" class="form-control form-control-line"> 

                                                @endif
                                               


                                            </div>
                                        </div>
 


                                        <div class="form-group ">

                                            
                                            @if($user->userType == 'doctor')
                                            
                                            <label class="col-md-12">{{ trans('backend.proffestionPracticeNumber') }}</label>
                                            
                                            @else

                                                 <label class="col-md-12">{{ trans('backend.user_head_person') }}</label>
                                            @endif        

                                           



                                            <div class="col-md-12">

                                                


                                                 @if($user->userType == 'doctor')
                                                            

                                                      <input type="text" placeholder="{{ trans('backend.proffestionPracticeNumber') }}" name="proffestionPracticeNumber" value="{{ $user->proffestionPracticeNumber }}" class="form-control form-control-line"> 

                                                @else
                                                

                                                      <input type="text" placeholder="{{ trans('backend.user_head_person') }}" name="user_head_person" value="{{ $user->headPersonName }}" class="form-control form-control-line"> 

                                                @endif

                                               







                                            </div>
                                        </div>

                                         
                                        
                                        <div class="form-group {!! $user->userType == 'adminstrator' ? 'hide':'' !!} ">
                                            @if($user->userType != 'doctor')
                                            <label class="col-md-12">{{ trans('backend.user_phone_number') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_phone_number') }}" name="user_phone_number" value="{{ $user->phoneNumber }}" class="form-control form-control-line"> </div>

                                                @endif
                                        </div>

                                        

                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_mobile_number') }}</label>
                                            <div class="col-md-12">

                                                <input type="text" placeholder="{{ trans('backend.user_mobile_number') }}" name="user_mobile_number" value="{{ $user->mobileNumber }}" class="form-control form-control-line">  

                                            </div>
                                        </div>
                                        



                                        <div class="form-group {!! $user->userType == 'adminstrator' ? 'hide':'' !!}">


                                                                                        
                                            @if($user->userType == 'doctor')

                                                <label class="col-md-12">{{ trans('backend.hospital_related_to') }}</label>

                                            @else
                                                
                                                 <label class="col-md-12">{{ trans('backend.user_location') }}</label>
                                                
                                            @endif


                                             
                                            @if($user->userType == 'doctor')


                                                <div class="col-md-12"> 

                                                        <input type="text" placeholder="{{ trans('backend.hospital_related_to') }}" name="hospital_related_to" value="{{ $user->doctorHospitalRelatedTo }}" class="form-control form-control-line"> 


                                                </div>

                                            @else

                                                <div class="col-md-6"> 

                                                        <input type="text" placeholder="{{ trans('backend.latitude') }}" name="latitude" value="{{ $user->latitude }}" class="form-control form-control-line"> 


                                                </div>

                                                <div class="col-md-6"> 

                                                        <input type="text" placeholder="{{ trans('backend.longitude') }}" name="longitude" value="{{ $user->longitude }}" class="form-control form-control-line"> 


                                                </div>

                                            @endif  

                                           
                                        </div>
                                        
                                        <div class="form-group {!! $user->userType == 'adminstrator' ? 'hide':'' !!}">
                                        @if($user->userType != 'doctor')
                                            <label class="col-md-12">{{ trans('backend.user_business_record') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_business_record') }}" name="user_business_record" value="{{ $user->businessRecordNumber }}" class="form-control form-control-line"> </div>
                                        @endif
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_website') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_website') }}" name="user_website" value="{{ $user->website }}" class="form-control form-control-line"> </div>
                                        </div>



                                        

                                        <div class="form-group {!! $user->userType == 'adminstrator' ? 'hide':'' !!}">
                                            @if($user->userType != 'doctor'  )
                                            <label class="col-md-12">{{ trans('backend.user_branch_name') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_branch_name') }}" name="user_branch_name" value="{{ $user->branchName }}" class="form-control form-control-line"> </div>
                                                @endif
                                        </div>

                                        

                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_password') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_password') }}" name="user_password" value="" class="form-control form-control-line"> </div>
                                        </div>

                                            
                                    <div class="form-group">
                                         <label for="input-file-now" class="col-md-12">{{ trans('backend.user_logo') }}</label>
                                        <div class="col-md-12">
                                            <div class="white-box">
                                                
                                                 
                                                <input type="file" name="logo" id="input-file-now" class="dropify" /> </div>
                                        </div>
                                    </div>

                                        @if(Auth::user()->userType == 'adminstrator')

                                             <div class="form-group">
                                                <label for=""> &nbsp; {{ trans('backend.status') }}</label>
                                             
                                                <div class="col-md-12">
                                               
                                                     <input type="checkbox" name="user_status" {!! $user->isActive == 'active' ? 'checked' : '' !!}  class="js-switch" data-color="#99d683" />

                                                </div>

                                             
                                        </div>
                                        @endif


                                   




                                        <div class="form-group">
                                            <label class="col-md-12">{{ trans('backend.user_email') }}</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="{{ trans('backend.user_email') }}" name="user_email" value="{{ $user->email }}" class="form-control form-control-line"> </div>
                                        </div>

                                         

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success">{{ trans('backend.update') }}</button>
                                            </div>
                                        </div>
                                        
                                    {!! Form::close() !!}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            

@endsection 


 
@section('scripts')

     
        
         <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify({
            tpl: {
                wrap:            '<div class="dropify-wrapper"></div>',
                loader:          '<div class="dropify-loader"></div>',
                message:         '<div class="dropify-message"><span class="file-icon" /> <p>{{ trans("backend.select_file") }}</p></div>',
                preview:         '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ trans("backend.select_file_note") }}</p></div></div></div>',
                filename:        '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
                clearButton:     '<button type="button" class="dropify-clear">{{ trans("backend.remove") }}</button>',
                errorLine:       '<p class="dropify-error"></p>',
                errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
            }
        });
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Delete',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>


     <script>
    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });

    </script>


@endsection 

 