 

@extends('backend.app')

@section('page_title' , trans('backend.customers_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.customers_module') }}</h4>
 

                    </div>





@endsection



@section('content')

					  <div class="col-sm-12">
                        <div class="white-box">
                            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
                            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
                            <div class="table-responsive">
                                <table class="table table-hover color-table info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('backend.customer_name') }}</th>
                                            <th>{{ trans('backend.customer_email') }}</th>
                                            <th>{{ trans('backend.customer_phone') }}</th>
                                          
                                            <th>{{ trans('backend.user_added_date') }}</th>
                                    
                                            {{-- <th class="text-nowrap">{{ trans('backend.action') }}</th> --}}

                                        </tr>
                                    </thead>
                                    <tbody>

                                    	@if($customers->count())
                                    		@php $counter = 1 ; @endphp
                                    		@foreach($customers as $customer)
		                                        <tr>
		                                            <td>{{ $counter }}</td>
                                                    <td>{{ $customer->fname }} {{ $customer->lname }} </td>
                                                    <td>{{ $customer->email }} </td>
                                                    <td>{{ $customer->telephone }}</td>
 
		                                            <td>{{ date('Y-m-d' , strtotime($customer->created_at)) }}</td>

                                                     
		                                          {{--   <td class="text-nowrap">

                                                        <a href="{{ route('customers.show' , $customer->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.show') }}"> <i class="fa fa-info-circle text-inverse m-r-10"></i> </a>

                                                        <a href="{{ route('customers.edit' , $customer->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.edit') }}"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
 
 
		                                            </td> --}}
		                                        </tr>
		                                        @php $counter++ ; @endphp
		                                     @endforeach
                                        @else

											<tr>
												<td colspan="6" class="text-center">{{ trans('backend.user_count_zero') }}</td>
											</tr>

                                        @endif

                                    </tbody>


                                </table>
                                 <div class="text-center">{!! $customers->links() !!}</div>
                            </div>
                        </div>
                    </div>

@endsection



@section('scripts')

    <script>

    </script>

@endsection


{{--  --}}
