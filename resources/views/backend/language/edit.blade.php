@extends('backend.app')

@section('page_title' , trans('backend.language_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.language_module') }}</h4> 
                    </div>

                  



@endsection  



@section('content')
					
				 <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"></h3>
                            <p class="text-muted m-b-30 font-13"> {{ trans('backend.editing_language') }} </p>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    {!! Form::model($language, array('route' => array('language.update', $language->id)))  !!}
                                         {!! method_field('put') !!}
                                        <div class="form-group">
                                            <label for="exampleInputuname">{{ trans('backend.language_name') }}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-user"></i></div>
                                                <input type="text" class="form-control" value="{{ $language->name }}" name="lang_name" id="lang_name" placeholder="{{ trans('backend.language_name') }}"> </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputpwd1">{{ trans('backend.language_label') }}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-lock"></i></div>
                                                

                
                                                <input type="text" class="form-control" name="lang_label" value="{{ $language->label }}" id="lang_label" placeholder="{{ trans('backend.language_label') }}"> 


                                            </div>
 

                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{ trans('backend.language_status') }}</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                               
                                               &nbsp; &nbsp; &nbsp; &nbsp;
                                               <input type="checkbox" name="lang_status" {!! $language->status == '1' ? 'checked' : '' !!}  class="js-switch" data-color="#99d683" />

                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('backend.update') }}</button>
                                       {{--  <button type="reset" class="btn btn-inverse waves-effect waves-light">{{ trans('backend.reset') }}</button> --}}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>	  

@endsection 


 
@section('scripts')

    <!-- For Switch  --> 
    <script>

        jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

        }); 

        </script>

@endsection 


 