@extends('backend.app')

@section('page_title' , trans('backend.language_module'))

@section('breadcrumb')


					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ trans('backend.language_module') }}</h4> 
                    </div>

                  



@endsection  



@section('content')
					
					  <div class="col-sm-12">
                        <div class="white-box">
                            {{-- <h3 class="box-title m-b-0">Bordered Table</h3>
                            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</p> --}}
                            <div class="table-responsive">
                                <table class="table table-hover color-table info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('backend.language_name') }}</th>
                                            <th>{{ trans('backend.language_status') }}</th>
                                            <th>{{ trans('backend.language_label') }}</th>
                                            <th>{{ trans('backend.language_added_date') }}</th>
                                            <th class="text-nowrap">{{ trans('backend.action') }}</th>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>

                                    	@if($languages->count())
                                    		@php $counter = 1 ; @endphp 
                                    		@foreach($languages as $language)
		                                        <tr>
		                                            <td>{{ $counter }}</td>
		                                            <td>{{ $language->name }}</td>
		                                             
		                                            <td>
		                                            	@if($language->status == '0')
															<span class="label label-danger">{{ trans('backend.not_active') }}</span>
		                                            	@else
															<span class="label label-success">{{ trans('backend.active') }}</span>	
		                                            	@endif
		                                            </td>
		                                            <td>{{ $language->label }}</td>
		                                            <td>{{ date('Y-m-d' , strtotime($language->created_at)) }}</td>
		                                            <td class="text-nowrap">
		                                                <a href="{{ route('language.edit' , $language->id) }}"
                                                           data-toggle="tooltip" data-original-title="{{ trans('backend.edit') }}">
                                                            <i class="fa fa-pencil text-inverse m-r-10"></i> </a>


                                                        <a onclick="$('.language_form_{{ $language->id }}').submit();"
                                                           data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                                            <i class="fa fa-close text-danger"></i> </a>


                                                        {!! Form::open(['route'=>["language.destroy" , $language->id ] ,
                                                        'class'=>"language_form_$language->id" ]) !!}

                                                        {!! method_field('DELETE') !!}


                                                        {!! Form::close() !!}
		                                            </td>
		                                        </tr>
		                                        @php $counter++ ; @endphp 
		                                     @endforeach
                                        @else
											
											<tr>
												<td colspan="6" class="text-center">{{ trans('backend.language_count_zero') }}</td>
											</tr>

                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

@endsection 


 




{{--  --}}