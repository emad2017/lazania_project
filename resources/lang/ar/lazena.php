<?php 

	return [


		// general 
		'read_more'=>'معرفة المزيد' ,  
		'add_to_cart'=>'اضافه الى السله' , 
		'item_added'=>'تم الاضافه'  , 
		's_r'=>'ر.س' , 
		'home'=>'الصفحة الرئيسية' , 
		'no_result'=>'لا يوجد اى بيانات حتى الان' , 
		'all_categories'=>'كل التصنيفات ' , 
		'no_sub_category'=>'لا يوجد اى تصنيف فرعى' , 
		'availability'=>'حاله المنتج' , 
		'in_stock'=>'فى المخزن', 

		'customer'=>'العميل' , 
		'remove'=>'حذف' , 

		// navigation 

		'search'=>'البحث' , 
		'arabic'=>'العربية' , 
		'english'=>'الانجليزية' ,
		'find_us'=>'اين تجدنا' , 
		'login'=>'تسجيل الدخول' , 
		'welcome'=>'اهلا', 
		'my_account'=>'حسابى' , 
		'log_out'=>'تسجيل الخروج' , 
		'wishlist'=>'قائمه الامنيات' , 
		'ellelazena'=>'إللازينة', 
		'about_us'=>'عن إللازينة' , 
		'categories'=>'التصنيفات' , 
		'products'=>'المنتجات' , 
		'services'=>'الخدمات'  , 
		'careers'=>'فرص العمل' , 
		'contact_us'=>'اتصل بنا' , 
		'contact_info'=>'معلومات الاتصال' , 
		'inquires'=>'الاستفسارات' , 

		// index page  
		'welcome_to_ellelazena'=>'اهلا وسهلا بكم فى إللازينه', 
		'product_range'=>'منتجات إللازينة' , 
		'latest_tweets'=>'اخر التغريدات'  , 
		'tweets_by'=>'التغريدات بواسطه اللازينة'  , 
		'best_selling'=>'اكثر المنتجات مبيعا' , 
		'most_viewed'=>'اكثر المنتجات مشاهده'  , 
		'recomended_products'=>'المنتجات الموصى بها' , 



		// contact 
		'leave_msg'=>'اترك رسالتك'  , 
		'name'=>'الاسم' , 
		'name_required'=>'برجاء ادخال الاسم' , 
		'email'=>'البريد الاليكترونى' ,  
		'email_required'=>'برجاء ادخال البريد الاليكترونى' ,
		'phone'=>'رقم الجوال' ,
		'phone_required'=>'برجاء ادخال الجوال' , 
		'message'=>'الرساله',
		'message_required'=>'برجاء ادخال محتوى الرساله' , 
		'send_msg'=>'ارسال' , 
		'type_message'=>'اكتب رسالتك', 



			// customer 
		'account_dashboard'=>'الحساب الرئيسى' , 
		'account_info'=>'معلومات الحساب' , 
		'account_orders'=>'طلباتى' , 
		'account_shopping_cart'=>'سلع التسوق' , 
		'account_wishlist'=>'قائمه المفضله' , 



		// login and register  
		'login_intro'=>'قم بتسجيل الدخول او التسجيل' ,  
		'new_customers'=>'عضويه جديده' , 
		'new_customers_intro' =>' من خلال تسجيل عضويه جديده يمكنك الدخول الى صفحه حسابك الشخصيه والتحكم فى الطلبات وتعديل البيانات ايضه' , 

		'create_account'=>'تسجيل عضويه جديده', 

		'current_customers'=>'اعضاء حاليون', 
		'current_customers_intro'=>'اذا كان لديك حساب  من قبل  , قم بتسجيل الدخول', 

		'email_address'=>'البريد الاليكترونى' , 
		'password'=>'كلمة المرور' , 



		// register  
		'fname'=>'الاسم الاول', 
		'lname'=>'الاسم الاخير', 
		'city'=>'المدينه', 
		'country'=>'البلد', 
		'street'=>'الشارع', 
		'gender'=>'النوع', 
		'telephone'=>'الجوال', 
		'male'=>'ذكر' , 
		'female'=>'انثى' , 




		'edit'=>'تعديل' , 

		'update_account'=>'تحديث البيانات' , 
		'update_account_intro'=>'تحديث معلومات حسابك الشخصى' , 


		'update_password'=>'تحديث كلمه المرور' , 

		'new_password'=>'كلمه المرور الجديده' ,  
		'new_password_confirm'=>'تاكيد كلمه المرور' ,

		'old_password'=>'كلمه المرور الحاليه ' , 



		'my_orders'=>'طلباتى'  , 

		'order'=>'الطلب'  , 
		'date'=>'التاريخ' , 
		'ship_to'=>'يشحن الى' , 
		'order_total'=>'مجمل الطلب' , 
		'order_status'=>'حاله الطلب' , 
		'no_orders'=>'عفوا , لم تقوم باضافه اى طلب حتى الان'  , 
		'view_order'=>'عرض الطلب', 




		'shopping_cart'=>'سله التسوق' , 
		'continue_shopping'=>'استمر فى التسوق', 

		'price'=>'السعر' , 
		'qty'=>'الكميه' , 
		'item'=>'المنتج', 

		'no_item_found'=>'يجب اضافه منتج واحد ع الاقل' ,

		'sub_total'=>'المجموع' , 
		'delivery'=>'التوصيل' , 
		'total'=>'المجموع الكلى',
		'proceed_checkout'=>'تاكيد الدفع' , 

		'checkout'=>'تاكيد الدفع'  , 
		'name_address'=>'الاسم والعنوان' , 
		'address'=>'العنوان بالتفصيل' , 
		'saudia'=>'المملكه العربيه السعوديه', 
		'info'=>'معلومات', 

		'shipping'=>'طرق التوصيل' , 
		'flat_rate'=>'قيمه 15.00 ريال سعودى'  ,
		'payment_method'=>'طريقه الدفع' , 
		'cash_on_delivery'=>'الدفع عند الاستلام' , 

		'review_order'=>'نظره سريعه على الطلب' , 

		'order_now'=>'اطلب الان' , 
		'order_placed'=>'تم اضافه الطلب بنجاح فى انتظار التاكيد من قبل الاداره' , 

		'product_added_to_wishlist_before'=>'تم اضافه النتاج من قبل الى قائمه الامنيات' , 
		'product_added_to_wishlist'=>'تم اضافه المنتج بنجاح الى قائمه الامنيات' , 
		'no_item_in_wishlist'=>'لا يوجد اى منتج مضاف الى قائمه الامنيات' , 

		'product_removed'=>'تم ازاله المنتج من قائمه الامنيات ' , 


		 'login_to_shopping_cart' => 'يجب عليك تسجيل الدخول لكى تقوم بالاستمرار الى سله التسوق الخاصه بك' , 
		 'login_to_wishlist' => 'يجب عليك تسجيل الدخول لكى تقوم بوضع هذا المنتج فى قائمه الامنيات' , 


		 'cart'=>'سله التسوق' , 

		 'shopping_cart'=>'قائمه التسوق' , 


		 'checkout_btn'=>'مراجعه الطلب' ,  

		 'order_details'=>'تفاصيل الطلب' , 


		 'ticket'=>'رقم الطلب'  , 
		 'print'=>'طباعه الطلب' , 

		 'order_inf'=>'معلومات الطلب' ,  

		 'ship_address'=>'عنوان الشحن' , 


		 'items_ordered'=>'الطلب بالتفصيل' , 

		 'product_name'=>'اسم المنتج' , 


		 'status'=>'الحاله' , 

		 'delete'=>'حذف' , 

		 'special_occassion'=>'المناسبات الخاصه' , 





		'open'=>'مفتوح' , 
		'from'=>'من' , 
		'to'=>'الى' , 
		'am'=>'صباحا' , 
		'pm'=>'مساءا' , 

		'shop_by'=>'تسوق بواسطه' , 

		'subscribe_newsletter'=>'اشترك فى القائمه البريديه',  


		'pending'=>'قيد الانتظار', 
		'accepted'=>'تم القبول', 
		'rejected'=>'تم الرفض' , 

		'out_of_stock'=>'غير متاح حاليا' , 


		'contact_sent'=>'تم ارسال رسالتك بنجاح , يمكنك التمتع بتفصح موقعنا' , 


		'price_label'=>'السعر' , 

		'show_order_details'=>'عرض تفاصيل الطلب' , 


		'no_product_found'=>'لم نستطيع العثور على اى منتج', 


		'accepted'=>'تم قبول الطلب', 

		'password'=>'كلمة المرور' , 

		'login_page'=>"صفحة تسجيل الدخول " , 

		'member_login'=>'منطقة دخول الادارة', 

		'password_required'=>'كلمه المرور مطلوبة' , 


		'error_login'=> 'لم نستطيع ايجاد اى حساب ', 



		'checkout_notice'=>'سوف يتم خصم مبلغ 10 ريالات للمسافات القريبة', 
		



		
		




	];



 ?>