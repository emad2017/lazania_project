<?php 


return [


	// pages titles : 
	'dashboard_module'=>'لوحة التحكم -- الصفحه الرئيسية',
	'language_module'=>'لوحة التحكم  -- اللغات'  , 
	'user_module'=>'لوحة التحكم  -- المستخدمين'  , 
	'user_profile'=>'لوحة التحكم  -- الملف الشخصى'  , 
	'user_profile_add'=>'لوحة التحكم  -- اضافه مستخدم جديد'  , 
	'date_added'=>'تاريخ الاضافه' ,

	'show_sub_departments'=>'عرض الاقسام الفرعيه' , 



	// general : 
	'action'=>'الاجراءات' , 
	'edit'=>'تعديل' , 
	'delete'=>'حذف' , 
	'active'=>'مفعل' , 
	'not_active'=>'غير مفعل', 
	'add'=>'اضافة' , 
	'reset'=>'تراجع' , 
	'update'=>'تحديث' , 
	'show'=>'عرض التفاصيل' , 
	'status'=>'الحالة', 
	'remove'=>'ازالة',
	'change'=>'تغيير', 

	'select_file'=>'احتر شعار ليتم تحميله', 
	'select_file_note'=>'يمكنك الضغط مره اخرى للتغيير واختيار شعار اخر', 



	// header info for users like image - name  etc   : 
	'my_profile'=>'الملف الشخصى' , 
	'logout'=>'تسجيل الخروج', 



	// Roles General 
	'adminstrator'=>'مدير' , 
	'doctor'=>'طبيب' , 
	'pharmacy'=>'صيدلية',
	'hospital'=>'مستشفى',
	'medical_supply'=>'شركة مستلزمات طبية',	
	'pharma_company'=>'شركة ادوية',	


	// dashboard 
	'dashboard'=>'لوحه التحكم' , 
	


	// language table lisitng titles : 
	'language'=>'اللغة', 
	'create_language'=>'اضافة لغة جديدة', 
	'show_languages'=>'عرض كل اللغات', 
	'language_name'=>'اسم اللغة' , 
	'language_status'=>'حالة اللغة' , 
	'language_label'=>'معرف اللغة' ,
	'language_added_date'=>'تاريخ الاضافة' , 
	'language_count_zero'=>'عفوا , لم يتم اضافة اى لغة حتى الان', 
	'adding_language'=>'اضافة لغة جديدة' , 
	'editing_language'=>'تعديل بيانات لغة' , 
	'language_message_added'=>'تم اضافة لغة جديدة بنجاح', 
	'language_message_updated'=>'تم تحديث بيانات اللغة بنجاح', 


	// users 
	'users'=>'المستخدمين', 
	'create_user'=>'اضافة مستخدم جديد', 
	'show_users'=>'عرض كل المستخدمين',  
	'user_count_zero'=>'عفوا , لم يتم اضافة اى مستخدم حتى الان', 
	'user_property'=>'اسم الجهه' , 
	'user_specialization'=>'التخصص', 
	'user_head_person'=>'اسم الشخص المسؤول', 
	'user_type'=>'نوع المستخدم',
	'user_added_date'=>'تاريخ الاضافة' ,
	'user_phone_number'=>'  الهاتف',
	'user_mobile_number'=>'  الجوال',
	'user_location'=>'الموقع',
	'user_business_record'=>'رقم السجل التجارى',
	'user_website'=>'الموقع الاليكترونى', 
	'user_branch_name'=>'اسم الفرع' , 
	'user_password'=>'كلمة المرور' , 
	'user_email'=>'البريد الاليكترونى', 
	'user_profile_info'=>'بيانات الحساب' , 
	'user_profile_setting'=>'اعدادات الحساب' , 
	'user_logo'=>'الشعار', 
	'user_message_added'=>'تم اضافة مستخدم جديد بنجاح', 
	'user_message_updated'=>'تم تحديث بيانات المستخدم بنجاح', 
	'select_user_type'=>'اختر نوع المستخدم',

	'other'=>'اخرى' ,



	// PharmaceuticalType 
	'pharmaceutical_type_module'=>'لوحه التحكم -- انواع الدواء',
	'pharmaceutical_type'=>'انواع الدواء',
	'pharmaceutical_type_add'=>'اضافه نوع جديد',
	'pharmaceutical_type_list'=>'عرض كل الانواع',

	'pharmaceutical_type_element'=>'نوع الدواء', 
	'pharmaceutical_message_added'=>'تم اضافة نوع دواء  جديد بنجاح', 
	'pharmaceutical_message_updated'=>'تم تحديث بيانات نوع الدواء  بنجاح', 

	'pharmaceutical_type_name'=>'النوع',
	'pharmaceutical_type_count_zero'=>'عفوا , لم يتم اضافة اى نوع دواء حتى الان', 










	// PharmaceuticalManufacture
	'pharmaceutical_manufacture_module'=>'لوحه التحكم -- الشركات المصنعه للدواء',
	'pharmaceutical_manufacture'=>'الشركات المصنعه للدواء',
	'pharmaceutical_manufacture_add'=>'اضافه شركه جديده',
	'pharmaceutical_manufacture_list'=>'عرض كل الشركات',

	'pharmaceutical_manufacture_element'=>'الشركه المصنعه', 
	'pharmaceutical_manufacture_message_added'=>'تم اضافة شركه مصنعه  جديده بنجاح', 
	'pharmaceutical_manufacture_message_updated'=>'تم تحديث بيانات شركه مصنعه  بنجاح', 

	'pharmaceutical_manufacture_name'=>'اسم الشركه',
	'pharmaceutical_manufacture_count_zero'=>'عفوا , لم يتم اضافة اى شركه مصنعه حتى الان', 






	// Pharmaceutical
	'pharmaceutical_module'=>'لوحه التحكم -- الادوية',
	'pharmaceutical'=>'الادوية',
	'pharmaceutical_add'=>'اضافه دواء جديد',
	'pharmaceutical_list'=>'عرض كل الادوية',

	'pharmaceutical_element'=>'الدواء', 
	'pharmaceutical_message_added'=>'تم اضافة دواء جديد بنجاح', 
	'pharmaceutical_message_updated'=>'تم تحديث بيانات دواء   بنجاح', 

	'pharmaceutical_name'=>'اسم الدواء',
	'pharmaceutical_scientific_name'=>'اسم العلمى',
	'pharmaceutical_active_ingredient'=>'الماده الفعاله',
	'pharmaceutical_dosage'=>'الجرعه',
	'pharmaceutical_trade_name'=>'الاسم التجارى',
	'pharmaceutical_medicine_type'=>'نوع الدواء',
	'pharmaceutical_description'=>'وصف الدواء',
	'pharmaceutical_uses'=>'دواعى الاستعمال',
	'pharmaceutical_side_effects'=>'الاعراض الجانبيه',
	'pharmaceutical_store_medicine'=>'حفظ الدواء',
	'pharmaceutical_other'=>'اخرى',
	'pharmaceutical_Images'=>'الصور',
	'pharmaceutical_info'=>'معلومات الدواء',
 
	'pharmaceutical_count_zero'=>'عفوا , لم يتم اضافة اى دواء  حتى الان', 


	'pharmaceutical_type_name'=>'نوع الدواء' , 
	'pharmaceutical_manufacture_name'=>'الشركه المصنعه' , 

	'pharmaceutical_select_images'=>'اختر الصور الخاصه بالدواء' , 
	'pharmaceutical_select_images_note'=>'يمكنك الضغط مره اخرى للتغيير واختيار صور اخرى', 

	'pharmaceutical_message_added'=>'تم اضافه نوع دواء جديد بنجاح'  , 
	'pharmaceutical_show'=>'تفاصيل الدواء', 



	'pharmaceutical_tradeName'=>'الاسم التجارى', 
	'pharmaceutical_scientificName'=>'الاسم العلمى', 
	'pharmaceutical_activeIngredient'=>'الماده الفعاله', 
	'pharmaceutical_description'=>'الوصف', 
	'pharmaceutical_uses'=>'دواعى الاستخدام', 
	'pharmaceutical_dosage'=>'الجرعه', 
	'pharmaceutical_sideEffect'=>'الاعراض الجانبيه', 
	'pharmaceutical_storeMedicine'=>'كيفيه تخزين الدواء', 
	'pharmaceutical_other'=>'معلومات اخرى', 

	'pharmaceutical_photos_zero'=>'لم يتم الحاق اى صور بهذا الدواء',
	'pharmaceutical_edit'=>'تحديث البيانات',

	'pharmaceutical_deleted_successfully'=>'تم حذف الدواء بنجاح', 


	'latitude'=>'خط العرض', 
	'longitude'=>'خط الطول', 


	/**
	 * Registration and login labels
	 */



	/**
	 * Doctor
	 */
	
	'doctor_name'=>'اسم الطبيب', 
	'proffestionPracticeNumber'=>'رقم مزاوله المهنه', 
	'hospital_related_to'=>'المستشفى المنتمى اليها', 


	'select_user_type'=>'اختر نوع المستخدم اولا', 


	'doctor_specialization'=>'تخصصات الاطباء' , 






	'user_deleted_successfully'=>'تم حذف المستخدم بنجاح',



	'footer_copyright'=>'جميع الحقوق محفوظه لشركه تطوير',



    //hospital dashboard

    'clinics'=>'العيادات',
    'add_clinic'=>'اضافه عياده',
    'show_all_clinics'=>'عرض كل العيادات' ,
    'doctors'=>'الاطباء' ,
    'add_doctor'=>'اضافه طبيب',
    'show_all_doctors'=>'عرض كل الاطباء',
    'reservations'=>'الحجوزات',
    'add_reservation'=>'اضافه حجز',
    'show_all_reservations'=>'عرض كل الحجوزات',
    'campaigns'=>'الحملات',
    'add_campaign'=>'اضافه حمله' ,
    'show_all_campaigns'=>'عرض كل الحملات' ,
    'drugs_orders'=>'طلبات الادويه' ,
    'add_drugs_order'=>'طلب ادويه' ,
    'show_all_drugs_order'=>'عرض كل طلبات الادويه' ,
    'messages'=>'الرسائل' ,
    'incoming_messages'=>'الرسائل الوارده' ,
    'send_message'=>'ارسال رساله' ,
    'setting'=>'الاعدادات' ,
    'about_hospital'=>'عن المستشفى' ,
    'social_media'=>'التواصل الاجتماعى' ,
    'visitors_messages'=>'رسائل الزوار' ,
    'medical_supplies_orders'=>'طلبات المستلزمات الطبيه' ,
    'add_medical_supplies_order'=>'طلب مستلزمات طبيه' ,
    'show_all_medical_supplies_orders'=>'عرض طلبات المستلزمات الطبيه' ,


 //medical supplies dashboard
    
    'medicalSupplies'=>'شركات المستلزمات الطبية',
    'add_medicalSupply'=>'اضافه الشركات المصنعة',
    'show_all_medicalSupplies'=>'عرض كل الشركات المصنعة' ,


    'careers'=>'فرص العمل' , 
    'update_create_career'=>'تحديث او اضافه وظائف جديده' ,  

    'career_title'=>'الاسم العام', 
    'career_description'=>'امحتوى الوظائف', 

    'careers_updated'=>'تم تحديث بيانات الوظائف بنجاح', 

    'customers_module'=>'العملاء والزبائن' , 
    'customers'=>'الزبائن' , 
    'add_customer'=>'اضافه زبون جديد', 
    'list_customers'=>'عرض كل الزبائن' , 
    'customer_name'=>'اسم العميل' , 
    'customer_email'=>'البريد الاليكترونى', 
    'customer_phone'=>'رقم الهاتف', 


    'orders_module'=>'الطلبات' , 
	'list_orders'=>'عرض كل الطلبات', 
	
	'order_ticket'=>'رقم الاوردر' , 
	'order_quantity'=>'الكميه المطلوبه' , 
	'order_total'=>'التكلفه الاجماليه' , 
	'order_date'=>'تاريخ الفاتوره', 


	'main_cat_icon'=>'الايقون الرئيسي'  , 

	'show_in_slider'=>'عرض فى السلايدر' , 
	'best_selling'=>'الاكثر مبيعا' , 
	'product_quantity'=>'كميه المنتج', 
	'recomended_product'=>'منتج موصى به', 

	'quantity'=>'الكميه' , 


	'order_notification'=>'اشعارات الطلبات' , 
	'no_notifications'=>'لا يوجد اشعارات', 
	'new_order_txt'=>'طلب جديد' , 
	'order_customer_name'=>'الى' , 
	'total'=>'المبلغ' , 

	's_r'=>'ر.س', 


	'order_info'=>'تفاصيل الطلب', 

	'invoice_date'=>'تاريخ الطلب' , 

	'product_name'=>'اسم المنتج' ,

	'product_price'=>'تكلفه القطعه' , 

	'product_total'=>'اجمالى التكلفه' , 

	'delivery'=>'التوصيل' ,  

	'grand_total'=>'الاجمالى النهائى' , 

	'print'=>'طباعه' , 

	'product_image'=>'صوره المنتج' , 


	'contact_info'=>'معلومات الاتصال', 




	'slider_module'=>'موديول السلايدر', 
	'add_slide'=>'اضافه سلايدر جديد', 
	'list_sliders'=>'عرض كل السليذز' , 
	'slider_title'=>'الاسم المميز', 


	'add_slide_notice'=>'استخدم هذا الفورم لاضافه او تعديل صوره سلايدر', 


	'select_slide_image'=>'اختر صوره السلايد', 


	'slider_image'=>'صوره السلايدر', 


	'no_slide_found'=>'عفوا , لم يتم اضافه اى سلايدر حتى الان', 


	'title_required'=>'من فضلك قم بادخال اسم للسلايدر', 
	'file_required'=>'من فضلك قم باختيار صوره السلايدر', 


	'publish_options'=>'خيارات النشر', 

	'slide_added'=>'تم اضافه سلايدر جديد بنجاح', 

	'update_slide_info'=>'تحديث بيانات السلايد', 


	'slide_updated'=>'تم تحديث بيانات السلايد بنجاح', 


	'slide_deleted'=>'تم حذف السلايد بنجاح', 


	'image'=>'الصورة', 



	

     




];



 ?>