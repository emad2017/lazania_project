<?php
/**
 * Created by PhpStorm.
 * User: mahmoud
 * Date: 10/27/17
 * Time: 3:30 PM
 */



return [
    // speacialevent
    'show_event'  => 'عرض المناسبات الخاصه',
    'add_event'  => 'اضافه مناسبه جديده',
    'speacialevent'  => 'مناسبه خاصه',

    'speacialevent_name' => 'مناسبه خاصه ',
    'speacialevent_create' => 'اضافه مناسبه جديده',
    'speacialevent_show' => 'عرض المناسبات الخاصه',
    'speacialevent_name_' => 'اسم المناسبه',

    'speacialevent_description_' => 'وصف المناسبه',
    'speacialevent_edit' => 'تعديل المناسبه',

    'speacialevent_img' => 'صوره المناسبه',
    'speacialevent_status' => 'الحاله',
    'speacialevent_addby' => 'ضيف بواسطه',
    'speacialevent_date' => 'تاريخ الانشاء',
    'speacialevent_zero' => 'لم يتم اضافه مناسبات خاصه الي الان',
    'speacialevent_added'=>'تم اضافه مناسبه جديده بنجاح' ,
    'speacialevent_updated'=>'تم تحديث بيانات المناسبه بنجاح',
    'speacialevent_deleted'=>'تم حذف مناسبه بنجاح' ,

    // eventitem
    'show_eventitem'  => 'عرض حلويات المناسبات',
    'add_eventitem'  => 'اضافه حلويات لمناسبه',
    'eventitem'  => 'حلويات مناسبه خاصه',

    'eventitem_name' => 'حلويات مناسبه خاصه',
    'eventitem_create' => 'اضافه حلوي جديده',
    'eventitem_show' => 'عرض حلويات المناسبه',
    'eventitem_name_' => 'اسم الحلوي',

    'eventitem_description_' => 'وصف الحلوي',
    'eventitem_edit' => 'تعديل الحلوي',

    'eventitem_img' => 'صوره الحلوي',
    'eventitem_status' => 'الحاله',
    'eventitem_addby' => 'اضيف بواسطه',
    'eventitem_date' => 'تاريخ الانشاء',
    'eventitem_zero' => 'لم يتم اضافه حلوي الي الان',
    'eventitem_added'=>'تم اضافه حلوي جديده لمناسبه بنجاح' ,
    'eventitem_updated'=>'تم تحديث بيانات الحلوي بنجاح',
    'eventitem_deleted'=>'تم حذف الحلوي بنجاح ' ,


];