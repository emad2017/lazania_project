<?php

return [

    // dessertdepartment

    'main_departments'=>'الاقسام الرئيسيه' ,

    'dessert_department_name' => 'أقسام الحلويات',
    'dessert_department_create' => 'اضافه قسم حلويات',
    'dessert_department_show' => 'جميع اقسام الحلويات',
    'dessert_department_edit' => 'تعديل قسم الحلويات',
    'dessert_department_name_' => 'أسم القسم',
    'dessert_department_description_' => 'وصف القسم',
    'dessert_department_img' => 'صوره القسم',
    'dessert_department_status' => 'الحاله',
    'dessert_department_addby' => 'اضيف بواسطه',
    'dessert_department_date' => 'تاريخ الانشاء',
    'dessert_department_zero' => 'لم يتم اضافه أقسام حتي الان',
    'dessert_department_added'=>'تم اضافه قسم حلويات جديد' ,
    'dessert_department_updated'=>'تم تحديث بيانات قسم الحلويات',

    'dessert_department_deleted'=>'تم حذف قسم الحلويات' ,
    'dessert_department_add' => ' اضافه قسم حلويات',
    'dessert_department_department' => 'قسم الحلويات',
    'dessert_department'=>'قسم الحلويات',

    'dessert_department_price' => 'السعر',
    'dessert_department_rate' => 'التقيم',


    'dessert_department_rate_enter' => 'أدخل تقيم الحلوي',

    // Dessert item
    'dessert_name' => 'أسم الحلوي',
    'dessert_add' => 'اضافه حلوي جديده',
    'dessert_name_' => 'أسم الحلوي',
    'dessert_description_' => 'وصف الحلوي',
    'dessert_image_' => 'صوره الحلوي',
    'dessert_show' => 'عرض جميع الحلويات',
    'dessert_create' => 'انشاء حلوي جديده',
    'dessert_delete' => 'تم حذف البيانات بنجاح',
    'dessert_update_msg' => 'تم تحديث بيانات بنجاح',
    'dessert_dessert_added' => 'تم الاضافه جديده',


    'dessert_editting' => 'تعديل بيانات الحلوي',


// blog Department

    'blog_d' => 'اقسام المدونات',
    'add_blog_d'  => 'أضافه قسم جديد',
    'show_blog_d' => 'عرض جميع اقسام المدونات',
    'name_blog_d' => 'اسم القسم',
    'name_blog_d_' => 'اسم القسم',
    'delete_msg_d' => 'تم حذف قسم المدونه بنجاح',
    'update_msg_d'=>'تم تحديث البيانات بنجاح',
    'updated_msg_d'=>'تم تحديث بيانات بنجاح',
    'added_msg_d'=>'تم اضافه قسم جديد بنجاح',
    'blog_dep'=>'قسم المدونه',


    // blog
    'blog' => 'مدونه',
    'add_blog'  => 'أضافه تدوينه جديده',
    'show_blog' => 'عرض جميع المدونات',
    'name_blog' => 'أسم المدونه',
    'name_blog_' => 'أسم المدونه',
    'description_blog' => 'وصف المدونه',
    'description_blog_' => 'وصف المدونه',
    'img_blog' => 'صوره المدونه',
    'f'        => 'فيسبوك',
    't'        => 'تويتر',
    'g'        => 'جوجل',
    'f_link'        => 'رابط الفيسبوك',
    't_link'        => 'رابط تويترط',
    'g_link'        => 'رابط جوجل',
    'delete_msg' => 'تم حذف المدونه بنجاح',
    'update_msg'=>'تم تحديث بيانات التدوينه بنجاح',
    'updated_msg'=>'تم تحديث بيانات التدوينه بنجاح',


    // team

    'team' => 'فريق العمل',
    'add_team'  => 'اضافه عضو جديد',
    'show_team' => 'عرض فريق العلم',
    'name_team' => 'اسم العضو',
    'name_team_' => 'اسم العضو',
    'description_team' => 'نبذه للعضو',
    'description_team_' => 'نبذه للعضو',
    'img_team' => 'صوره للعضو',
    'f'        => 'فيسبوك',
    't'        => 'تويتر',
    'g'        => 'جوجل',
    'f_link'        => 'رابط الفيسبوك',
    't_link'        => 'رابط تويتر',
    'g_link'        => 'رابط جوجل',
    'delete_msg' => 'تم حذف بيانات العضو بنجاح',
    'update_msg'=>'تم تحديث بيانات العضو بناج',
    'updated_msg'=>'تم تحديث بيانات العضو بناج',
    'create_msg'=>'تم انشاء عضو جديد في فريق العمل',
    'team_zero' => 'لم يتم اضافه اعضاء الي الان ',
    'title_team_' => 'الوظيفه',

    'add_by' => 'اضيف بواسطه',
    'status' => 'الحاله',


    'sub_depts_count_zero'=>'عفوا لم نستطيع العثور على اى قسم فرعى' ,
    'add_sub_dept'=>'اضافه قسم فرعى' ,

    'sub_name'=>'اسم اقسم الفرعى' ,
    'sub_departments'=>'الاقسام الفرعيه',
    'add_sub_department'=>'اضافه قسم فرعى' ,
    'sub_description'=>'محتوى القسم الفرعى' ,
    'sub_dept_image'=>'الصوره الرئيسيه للقسم' ,

    'select_main_image'=>'اضغط لاختيار الصوره الرئيسيه للقسم' ,

    'errors_found'=>'عفوا , لازال هناك بعض الاخطاء التى يجب عليك اصلاحها اولا' ,
    'sub_dept_added'=>'تم اضافه قسم فرعى جديد بنجاح' ,

    'edit_sub_department'=>'تعديل بيانات قسم فرعى' ,

    'sub_dept_updated'=>'تم تحديث بيانات القسم الفرعى بنجاح' ,
    'sub_dept_deleted'=>'تم حذف القسم الفرعى بنجاح' ,

    'show_related_items'=>'عرض كل المنتجات المتعلقه بهذا التصنيف'    ,

    'related_to'=>'منتمى الى'    ,

       'dessert_items_zero'=>'لم يتم اضافه منتجات حتى الان' ,


       'add_item'=>'اضافه منتج'  ,


       'item_deleted'=>'تم الحذف بنجاح' , 


       'show_in_slider'=>'عرض فى السلايدر' , 
       'slider'=>'سلايدر', 
       'best_selling'=>'الاكثر مبيعا' , 

        'promoted'=>'مميز' , 
       'promoted_text'=>'المنتج المميز' , 

       'recommended'=>'موصى به' , 
       'subscribe'=>'اشترك فى الضائمه البريديه' , 
       'subscribtion_text'=>'اشترك فى القائمه البريديه ليصلك كلم ماهو جديد من عروض ومنتجات لازنيا'  , 

       'enter_email'=>'ادخل بريدك الاليكترونى' , 
       'submit'=>'اشتراك' , 

       'must_enter_email'=> 'يجب ادخال البريد الاليكترونى' , 
       'already_subscribed'=>'انت بالفعل مشترك من قبل فى قائمتنا البريديه' , 
       'subscribtion_done'=>'تم الاشتراك بنجاح فى قائمتنا البريديه' , 
        







];
