<?php 


	return [

		'about_sidebar'=>'الاعداات', 
		'about'=>'من نحن', 
		'about_module'=>'التحكم فى معلومات من نحن الخاصه بالمستخدم', 
		'about_edit'=>'تحديث بيانات من نحن', 
		'about_title'=>'العنوان' , 
		'about_description'=>'المحتوى' , 
		'about_status'=>'الحاله', 
		'about_photos'=>'صور من نحن' , 
		'select_images'=>'اختر صورا لاضافتها لمحتوى من نحن'  , 
		'select_images_note'=>'يمكنك اختيار اكثر من صوره' , 
		'telephone_one'=>'رقم الهاتف الاول' , 
		'telephone_two'=>'رقم الهاتف الثانى' , 
		'about_info_updated'=>'تم تحديث معلومات من نحن بنجاح' , 
		'about_photos_zero'=>'لم يتم اضافه اى صور لصفحه من نحن' , 
		'about_delete'=>'حذف الصوره', 
		'about_image_deleted'=>'تم حذف الصوره بنجاح',




        'about_us'=>'عرض ماذا قالوا عنا',
        'about_us_add'=>'اضافه قول جديد',
        'about_us_sidebar'=>'قالوا عنا',
        'name'=>'الاسم',
        'msg'=>'الرساله',
        'img'=>'صوره الشخص',



    ];



 ?>