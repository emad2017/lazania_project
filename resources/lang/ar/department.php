<?php 

return [

	'departments'=>'الاقسام' , 
	'department_module_managment'=>'التحكم فى اقسام الموقع' , 
	'add_department'=>'اضافه قسم جديد' , 
	'list_departments'=>'عرض كل الاقسام', 
	'department_name'=>'اسم القسم', 
	'department_description'=>'المحتوى', 
	'department_slug'=>'لينك مختصر',
	'department_parent'=>'منتمى الى', 
	'department_status'=>'حاله النشر' , 
	'department_added_by'=>'اضيف بواسطه',
	'department_date_added'=>'تاريخ الاضافه' , 
	'departments_count_zero'=>'لم يتم اضافه اى اقسام حتى الان',
	'department_photos'=>'الصور', 
	'select_department'=>'اختر قسم', 
	'select_images'=>'اختر صورا لاضافتها للقسم',
	'department_added'=>'تم اضافه قسم جديد بنجاح',
	'department_edit'=>'تحديث بيانات قسم',
	'department_updated'=>'تم تحديث بيانات القسم بنجاح',
	'show_sub_departments'=>'عرض الاقسام الفرعيه' , 
	'department_deleted'=>'تم حذف القسم بنجاح' , 
	'department_swal_deleted_success'=>'تم الحذف بنجاح' , 
	'department_swal_deleted_success_message'=>'تم حذف القسم بنجاح' , 

	'department_swal_deleted_error'=>'رساله تحذير' ,
	'department_swal_deleted_error_message'=>'لا يمكن حذف هذا القسم لاحتواؤه على اقسام فرعيه اخرى' , 

	'are_you_sure'=>'هل انت متاكد من القيام بهذا ؟'  , 
	'data_losted'=>'البيانات المحذوفه لا يمكن استرجاعها مره اخرى' ,
	'yes_delete'=>'نعم , قم بالحذف' , 
	'no_delete'=>'لا تقم بالحذف ', 



];

 ?>