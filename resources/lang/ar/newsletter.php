<?php 
	
	return [

		'newsletter'=>'القائمه البريديه' , 
		'show_all_subscriptions'=>'عرض كل المشتركين' , 
		'newsletter_note'=>'نقوم بعرض كل المشركين فى القائمه البريديه هنا ' ,
		'email'=>'البريد الاليكترونى' , 
		'subscription_date'=>'تاريخ الاشتراك' , 
		'no_subscriptions_found'=>'لا يوجد اى اشتراكات حتى الان' , 
		'send_bulck'=>'ارسال بريد مجمع' , 
		'send_single_email'=>'ارسال بريد' , 
		'single_email_view'=>'ارسال بريد بشكل فردى' , 
		'compose_email'=>'ارسال بريد جديد', 
		'to'=>'الى' , 
		'subject'=>'الموضوع' , 
		'message'=>'الرساله' , 
		'compose'=>'ارسال' , 
		'discard'=>'تراجع', 
		'email_sent'=>'تم ارسال البريد الاليكترونى بنجاح' , 
		'tatweer'=>'يدعم من '  , 
		'dont_like'=>'لا تحب ان ترى هذه الرسائل' , 
		'unsubscripe'=>'الغاء الاشتراك من القائمه البريديه' , 
		'bulck_sent'=>'تم ارسال بريد مجمع لكل المشتركين فى القائمه البريديه' 


	];

 ?>