<?php 


	return [

     'say'=> 'قالوا عنا',
     'create_say'=> 'إضافة قالوا عنا',
     'show_says'=> 'عرض ما قالوا عنا',
     'say_name'=>'إسم الشخص',
     'say_description'=>'ما قيل عنا',
     'say_image'=>'صورة الشخص',
     'action'=>'العمليات',
     'status'=>'الحالة',
     'say_message_deleted'=>'تم حذف التعليق',
     'say_count_zero'=>'لم يتم إضافة تعليقات',
     'create_say_page'=>'الفورم الخاصة بماذا قالوا عنا',
     'say_status'=>'الحالة',
     'say_message_added'=>'تم إضافة ما قيل عنا',
     'edit_say'=>'تعديل ما قيل عنا',
     'say_updated'=>'تم تعديل ما قاله الشخص',
     'delete'=>'حذف',

	]; 



 ?>