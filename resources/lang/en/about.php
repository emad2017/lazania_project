<?php 


	return [


		'about_sidebar'=>'Settings',
		'about'=>'About Us', 
		'about_module'=>'Controlling about us information ', 
		'about_edit'=>'Update About Us information', 
		'about_title'=>'Title' , 
		'about_description'=>'Content' , 
		'about_status'=>'Status' , 
		'about_photos'=>'About Us Images' , 
		'select_images'=>'Select images to be added to about us information'  , 
		'select_images_note'=>'You can select more than one image' ,
		'telephone_one'=>'Phone number one' , 
		'telephone_two'=>'Phone number two' , 
		'about_info_updated'=>'About us information has been updated successfully' , 
		'about_photos_zero'=>'No photo has been attached to about us till now' , 
		'about_delete'=>'Delete Image', 
		'about_image_deleted'=>'Image Has been deleted successfully',



		'about_us'=>'Show All Say About Us',
		'about_us_add'=>'Add New Say About Us',
		'about_us_sidebar'=>'Say About Us',
        'name'=>'Name',
        'msg'=>'Message',
        'img'=>'image',


	]; 



 ?>