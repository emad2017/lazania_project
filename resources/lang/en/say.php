<?php 


	return [

     'say'=>'Say About Us',
     'create_say'=> 'Add Say About Us',
     'show_says'=> 'Show What Customer Say',
     'say_name'=>'Person Name',
     'say_description'=>'What Person Said',
     'say_image'=>'Person Image',
     'action'=>'Actions',
     'status'=>'Status',
     'say_message_deleted'=>'Say Message Deleted',
     'say_count_zero'=>'Theres No Comments Form Users',
     'create_say_page'=>'What Say About Us Form',
     'say_status'=>'Status',
     'say_message_added'=>'What Said Has Added',
     'edit_say'=>'Edit What Customer Say',
     'say_updated'=>'What Said Has Updated',
     'delete'=>'Delete',

	]; 



 ?>