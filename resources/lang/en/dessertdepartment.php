<?php

return [

    // dessertdepartment

    'main_departments'=>'Main Departments' ,

    'dessert_department_name' => 'Dessert Departments',
    'dessert_department_create' => 'Add New Dessert Departments',
    'dessert_department_show' => 'Show All Dessert Departments',
    'dessert_department_name_' => 'Department Name',

    'dessert_department_description_' => 'Department Description',
    'dessert_department_edit' => 'Edit Dessert Department',


    'dessert_department_price' => 'Price',
    'dessert_department_rate' => 'Rate',


    'dessert_department_img' => 'Department image',
    'dessert_department_status' => 'Status',
    'dessert_department_addby' => 'Add By',
    'dessert_department_date' => 'Created at',
    'dessert_department_zero' => 'Sorry , No Dessert Departments added yet',
    'dessert_department_added'=>'New Dessert Departments Has been added successfully' ,
    'dessert_department_updated'=>'Dessert Departments Has been updated successfully',
    'dessert_department_deleted'=>'Dessert Departmentshas been deleted successfully' ,


    'dessert_department_rate_enter' => 'Enter The Rate Of Dessert',

    // Dessert item
    'dessert_name' => 'Desserts',
    'dessert_add' => 'Add new Dessert',
    'dessert_name_' => 'Dessert Name',
    'dessert_description_' => 'Dessert Description',
    'dessert_image_' => 'Dessert Image',
    'dessert_show' => 'show all Desserts',
    'dessert_create' => 'All New Dessert',
    'dessert_delete' => 'Dessert been deleted successfully',
    'dessert_update_msg' => 'Dessert has been updated',
    'dessert_dessert_added' => 'new Dessert been added',


    'dessert_editting' => 'Edit Desserts',
    'dessert_department_department' => 'Dessert Despartment',


    // blog Department

    'blog_d' => 'Blogs Departments',
    'add_blog_d'  => 'Add New Blog Department',
    'show_blog_d' => 'show All Blog Department',
    'name_blog_d' => 'Department Name',
    'name_blog_d_' => 'Department Name',
    'delete_msg_d' => 'Blog  been deleted successfully',
    'update_msg_d'=>'Blog Has been updated successfully',
    'updated_msg_d'=>'Blog Has been updated successfully',
    'blog_dep'=>'Blog Department',


    // blog
    'blog' => 'Blogs',
    'add_blog'  => 'Add New Blog',
    'show_blog' => 'show All Blogs',
    'name_blog' => 'Blog Name',
    'name_blog_' => 'Blog Name',
    'description_blog' => 'Blog Description',
    'description_blog_' => 'Blog Description',
    'img_blog' => 'Blog Image',
    'f'        => 'Facebook',
    't'        => 'Twitter',
    'g'        => 'Google',
    'f_link'        => 'Facebook Link',
    't_link'        => 'Twitter Link',
    'g_link'        => 'Google Link',
    'delete_msg' => 'Blog  been deleted successfully',
    'update_msg'=>'Blog Has been updated successfully',
    'updated_msg'=>'Blog Has been updated successfully',



    // team

    'team' => 'TeamWork',
    'add_team'  => 'Add New team member',
    'show_team' => 'show All team member',
    'name_team' => 'member Name',
    'name_team_' => 'member Name',
    'title_team_' => 'Job Title',

    'description_team' => 'member Description',
    'description_team_' => 'member Description',
    'img_team' => 'member Image',
    'f'        => 'Facebook',
    't'        => 'Twitter',
    'g'        => 'Google',
    'f_link'        => 'Facebook Link',
    't_link'        => 'Twitter Link',
    'g_link'        => 'Google Link',
    'delete_msg' => 'member  been deleted successfully',
    'update_msg'=>'member Has been updated successfully',
    'updated_msg'=>'member data Has been updated successfully',
    'create_msg'=>'member data Has been added successfully',

    'team_zero' => 'Sorry , No Team Members added yet',



  'add_by' => 'Add By',
  'status' => 'Status',

  'sub_depts_count_zero'=>'Sorry we couldn\'t  find any sub departments till now ' ,

  'add_sub_dept'=>'Add new sub department' ,

  'sub_name'=>'Sub Department name' ,
  'sub_departments'=>'Sub Departments',
  'add_sub_department'=>'Add new sub department' ,


  'sub_description'=>'Sub department content' ,

  'sub_dept_image'=>'Main Sub department image' ,
  'select_main_image'=>'Click to choose main image' ,

   'errors_found'=>'Sorry , but there are still some errors that needs to be fixed' ,

   'sub_dept_added'=>'New sub department has been added successfully' ,
   'edit_sub_department'=>'Update sub department information' ,

   'sub_dept_updated'=>'Sub Department information has been updated successfully' ,

   'sub_dept_deleted'=>'Sub Department has been deleted successfully' ,

   'show_related_items'=>'Show related items' ,

   'related_to'=>'Related To' ,

   'dessert_items_zero'=>'Sorry you haven\'t added any deserts yet' ,


   'add_item'=>'Add Item'  ,
   'item_deleted'=>'Item has been deleted successfully' , 


   'show_in_slider'=>'Show in slider' , 
       'slider'=>'Slider', 
       'best_selling'=>'Best Selling' , 

       'promoted'=>'Promoted' , 
       'promoted_text'=>'Best Selling Promoted' , 

       'recommended'=>'Recommended' , 

       'subscribe'=>'Subscribe to our newsletter' , 
       'subscribtion_text'=>'Subscribe now at our newsletter to be always updated with latest offers, news, and events of Lazania Pastry'  , 

       'enter_email'=>'Enter your e-mail Address' , 
       'submit'=>'Subscribe' , 

       'must_enter_email'=> 'Please make sure to enter you email' , 
       'already_subscribed'=>'You\'re  already subscribed in our newsletter'  , 
       'subscribtion_done'=>'You have subscribed successfully to our newsletter' , 






];
