<?php 
	
	return [

		'newsletter'=>'Newsletter' , 
		'show_all_subscriptions'=>'List Subscriptions' , 
		'newsletter_note'=>'we list all Subscriptions to our newsletter here' ,

		'email'=>'Email address' , 
		'subscription_date'=>'Subscription date' , 

		'no_subscriptions_found'=>'No subscriptions have been made yet' , 

		'send_bulck'=>'Send a Bulck email' , 

		'send_single_email'=>'Send an email' , 

		'single_email_view'=>'Send a singular email' , 
		'compose_email'=>'Compose new email', 
		'to'=>'To' , 
		'subject'=>'Subject' , 
		'message'=>'Message' , 
		'compose'=>'Compose' , 
		'discard'=>'Discard', 
		'email_sent'=>'Email has been sent successfully' , 
		'tatweer'=>'Supported By'  , 

		'dont_like'=>'Don\'t like these emails ? ' , 
		'unsubscripe'=>'Unsubscripe' , 

		'bulck_sent'=>'Bulck Email has been sent to all subscripers in newsletter' 

		

	];

 ?>