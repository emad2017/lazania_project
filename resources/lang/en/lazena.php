<?php 

	return [

		// general 
		'read_more'=>'معرفة المزيد' ,  
		'add_to_cart'=>'Add to cart' , 
		'item_added'=>'added'  , 
		's_r'=>'S.R' , 
		'home'=>'Home' , 
		'no_result'=>'No result found till now' , 
		'all_categories'=>'All Categories' , 
		'no_sub_category'=>'No sub category found' , 
		'availability'=>'Availability' , 
		'in_stock'=>'In stock', 
		'customer'=>'Customer' , 

		'remove'=>'Remove' , 


		// navigation 

		'search'=>'Search' , 
		'arabic'=>'Arabic' , 
		'english'=>'English' , 
		'find_us'=>'Find us' , 
		'login'=>'Login' , 
		'welcome'=>'Welcome', 
		'my_account'=>'My Account' , 
		'log_out'=>'Logout' , 
		'wishlist'=>'Wishlist ' , 
		'ellelazena'=>'Ellelazena', 
		'about_us'=>'About us' , 
		'categories'=>'Categories' , 
		'products'=>'Products' , 
		'services'=>'Services'  , 
		'careers'=>'Careers' , 
		'contact_us'=>'Contact us' , 
		'contact_info'=>'Contact info' , 
		'inquires'=>'Inquiries' , 


		// index page  
		'welcome_to_ellelazena'=>'Welcome to Ellelazena', 
		'product_range'=>'Ellelazena Products' , 
		'latest_tweets'=>'Latest Tweets'  , 
		'tweets_by'=>'Latest Tweets By Ellelazena'  , 
		'best_selling'=>'Best Selling products' , 
		
		'most_viewed'=>'Most viewed products'  , 
		'recomended_products'=>'Recommended Products ' , 


		// contact 
		'leave_msg'=>'Leave your message'  , 

		'name'=>'Name' , 
		'name_required'=>'Name is required' , 
		'email'=>'Email' ,  
		'email_required'=>'Email is required' ,
		'phone'=>'Phone' , 
		'phone_required'=>'Phone is required' , 
		'message'=>'Message', 
		'message_required'=>'Message is required' , 
		'send_msg'=>'Send' , 
		'type_message'=>'Write your message ', 



		// customer 
		'account_dashboard'=>'Account Dashboard' , 
		'account_info'=>'Account Information' , 
		'account_orders'=>'My Orders' , 
		'account_shopping_cart'=>'Shopping Cart' , 
		'account_wishlist'=>'My Wishlist' , 



		// login and register  
		'login_intro'=>'Login Or Create An Account' ,  
		'new_customers'=>'New Customer' , 
		'new_customers_intro'=>'By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more' , 

		'create_account'=>'Create new account', 


		'current_customers'=>'Current Customer', 
		'current_customers_intro'=>'If you have an account with us, please log in', 

		'email_address'=>'Email Address' , 
		'password'=>'Password' , 



		// register  
		'fname'=>'First Name', 
		'lname'=>'Last Name', 
		'city'=>'City', 
		'country'=>'Country', 
		'street'=>'Street', 
		'gender'=>'Gender', 
		'telephone'=>'Mobile', 
		'male'=>'Male' , 
		'female'=>'Female' , 


		'edit'=>'Edit' , 

		'update_account'=>'Account Info' , 
		'update_account_intro'=>'Update your account information' , 


		'update_password'=>'Update Password' ,

		'new_password'=>'New password' ,  
		'new_password_confirm'=>'New password confirmation' ,

		'old_password'=>'Old Password' ,   


		'my_orders'=>'My Orders'  , 
		'order'=>'Order'  , 
		'date'=>'Date' , 
		'ship_to'=>'Ship To' , 
		'order_total'=>'Order Total' , 
		'order_status'=>' Order Status' , 

		 'no_orders'=>'You don\'t have any orders yet , you may head to products page and begin select your desired products '  , 

		 'view_order'=>'View Order', 




		 'shopping_cart'=>'Shopping Cart' , 
		 'continue_shopping'=>'Continue Shopping', 

		 'price'=>'Price' , 
		'qty'=>'QTY' , 
		'item'=>'Item', 

		'no_item_found'=>'You Should add at least one product' ,

		'sub_total'=>'Sub Total' , 
		'delivery'=>'Delivery' , 
		'total'=>'Grand Total',
		'proceed_checkout'=>'Proceed to checkout' , 

		'checkout'=>'Final Checkout'  ,
		'name_address'=>'Name and Address' ,
		'address'=>'Address in details' , 
		'saudia'=>'Saudia Kingdom', 
		'info'=>'Information', 
		'shipping'=>'Shipping Methods' , 
		'flat_rate'=>'Flat Rate S.R 15.00'  ,

		'payment_method'=>'Payment Method' , 
		'cash_on_delivery'=>'Cash on delivery' , 

		'review_order'=>'Review Your Order' , 

		'order_now'=>'Order Now' , 

		'order_placed'=>'Your order has been placed , waiting confirmation from adminstration and we will follow up with you through emails step by step' , 

		'product_added_to_wishlist_before'=>' Product was added before to your wishlist !' , 
		'product_added_to_wishlist'=>' Product was added to your wishlist you can take actions now to add it to your shopping cart and proceed to checkout page !' , 

		 'no_item_in_wishlist'=> ' You dont have any products listed in your wishlist ' , 

		 'product_removed'=>'The Product Has been removed from your wishlist  , but if it was added to your cart , it will remains ' , 



		 'login_to_shopping_cart' => 'Ooh , you need to login or create an account to be able to proceed to your shopping cart' , 

		 'login_to_wishlist' => ' you need to login or create an account to add this product to your wishlist' , 



		  'cart'=>'Cart' , 

		  'shopping_cart'=>'Shopping Cart' ,

	 	  'checkout_btn'=>'Checkout' , 
	 	  'order_details'=>'Order Details' , 


	 	  'ticket'=>'Ticket'  ,
	 	   'print'=>'Print order' , 

	 	   'order_inf'=>'Order Information' , 

	 	    'ship_address'=>'Shipping Address' , 

	 	    'items_ordered'=>'Items Ordered' , 
	 	    'product_name'=>'Product Name' ,


	 	      'status'=>'Status' , 


	 	      'delete'=>'Delete' , 


	 	      'special_occassion'=>'Special Occassion' , 

	 	        'open'=>'Open' , 
				'from'=>'From' , 
				'to'=>'To' , 


				'am'=>'AM' , 
				'pm'=>'PM' , 

				'shop_by'=>'Shop By' , 

				'subscribe_newsletter'=>'Subscribe Newsletter', 

				'pending'=>'Pending', 
				'accepted'=>'Accepted', 
				'rejected'=>'Rejected' , 



				'out_of_stock'=>'Out of stock' , 


				 'contact_sent'=>'Your contact information has been sent to adminstration , have agood day surfing our website' , 


				 'price_label'=>'Price' , 



				 'show_order_details'=>'Show order details' , 


				 'no_product_found'=>'Sorry , we couldn\'t found any product that matchs your query', 


				 'accepted'=>'Order Accepted', 




				 'login_page'=>"Login Page Area" , 


				 'member_login'=>'Adminstration Login', 


				 'password_required'=>'Password is required' , 


				 'error_login'=> 'Sorry , No account found !' , 


				 'checkout_notice'=>'10 S.R will be subtracted from the order on short destinations', 



		
		


				
		

		




	];



 ?>