<?php 


return [


	// pages titles : 
	'dashboard_module'=>'Dashboard -- Control Panel ',
	'language_module'=>'Dashboard  -- Languages'  , 
	'user_module'=>'Dashboard  -- Users'  , 
	'user_profile'=>'Dashboard  -- User Profile'  , 
	'user_profile_add'=>'Dashboard  -- Add New User'  , 


	'show_sub_departments'=>'Show Sub Departments' , 



	// general : 
	'action'=>'Actions' , 
	'edit'=>'Edit' , 
	'delete'=>'Delete' ,
	'active'=>'Active' , 
	'not_active'=>'Not Active', 
	'add'=>'Add' , 
	'reset'=>'Reset' , 
	'update'=>'Update' , 
	'show'=>'Show Info' ,
	'status'=>'Status',
	'remove'=>'Remove',
	'change'=>'Change', 
	'select_file'=>'ٍSelect Logo to be uploaded', 
	'select_file_note'=>'You can choose another logo by clicking again', 
	'date_added'=>'Date Added' ,


	// header info for users like image - name  etc   : 
	'my_profile'=>'My Profile' , 
	'logout'=>'Logout', 


	// Roles General 
	'adminstrator'=>'Adminstrator' , 
	'doctor'=>'Doctor' , 
	'pharmacy'=>'Pharmacy',
	'hospital'=>'Hospital',
	'medical_supply'=>'Medical Supply Company',	
	'pharma_company'=>'Pharma Company',	


	// dashboard 
	'dashboard'=>'Dashboard' , 


	// language table lisitng titles : 
	'language'=>'Language', 
	'create_language'=>'ِAdd New Language', 
	'show_languages'=>'Show All Languages', 
	'language_name'=>'Lang Name' , 
	'language_status'=>'Lang Status' , 
	'language_label'=>'Lang Label' ,
	'language_added_date'=>'Date Added' , 
	'language_count_zero'=>'Sorry , No language added yet',
	'adding_language'=>'Adding New Language' ,
	'editing_language'=>'Edit Language Info' , 
	'language_message_added'=>'New Language Has Been Added Successfully', 
	'language_message_updated'=>'Language Has Been Updated Successfully', 


	// users 
	'users'=>'Users',
	'create_user'=>'Add New User', 
	'show_users'=>'Show All Users',  
	'user_count_zero'=>'Sorry , No User has been  added yet',
	'user_property'=>'Property Name' , 
	'user_specialization'=>'Specialization', 
	'user_head_person'=>'Head Person', 
	'user_type'=>'User Type',
	'user_added_date'=>'Date Added' ,
	'user_phone_number'=>'Phone ',
	'user_mobile_number'=>'Mobile ',
	'user_location'=>'Location',
	'user_business_record'=>'Business Record Num',
	'user_website'=>'Website', 
	'user_branch_name'=>'Branch Name' ,
	'user_password'=>'Password' , 
	'user_email'=>'Email', 
	'user_logo'=>'Logo', 
	'user_profile_info'=>'Account Info' , 
	'user_profile_setting'=>'Account Settings' , 
	'user_message_added'=>'New User Has Been Added Successfully', 
	'user_message_updated'=>'User Has Been Updated Successfully', 
	'select_user_type'=>'Select user type',




	// PharmaceuticalType 
	'pharmaceutical_type_module'=>'Dashboard -- Pharmaceutical Type',
	'pharmaceutical_type'=>'Pharmaceutical Types',
	'pharmaceutical_type_module'=>'Pharmaceutical Type',
	'pharmaceutical_type_add'=>'Add New Pharmaceutical Type',
	'pharmaceutical_type_list'=>'Show All Pharmaceutical Types',

	'pharmaceutical_type_element'=>'Pharmaceutical Type',
	'pharmaceutical_message_added'=>'New Pharmaceutical Type Has Been Added Successfully', 
	'pharmaceutical_message_updated'=>'Pharmaceutical Type Has Been Updated Successfully', 
	'pharmaceutical_type_name'=>'Type',
	'pharmaceutical_type_count_zero'=>'Sorry , No Pharmaceutical Type  added yet', 









	// PharmaceuticalManufacture
	'pharmaceutical_manufacture_module'=>'Dashboard -- Pharmaceutical Manufacture',
	'pharmaceutical_manufacture'=>'Pharmaceutical Manufactures',
	'pharmaceutical_manufacture_add'=>'Add New Pharmaceutical Manufacture',
	'pharmaceutical_manufacture_list'=>'Show All Pharmaceutical Manufactures',

	'pharmaceutical_manufacture_element'=>'Pharmaceutical Manufacture', 
	'pharmaceutical_manufacture_message_added'=>'New Pharmaceutical Manufacture Has Been Added Successfully', 
	'pharmaceutical_manufacture_message_updated'=>'Pharmaceutical Manufacture Has Been Updated Successfully', 

	'pharmaceutical_manufacture_name'=>'Name',
	'pharmaceutical_manufacture_count_zero'=>'Sorry , No Pharmaceutical Manufacture  added yet', 






	// Pharmaceutical
	'pharmaceutical_module'=>'Dashboard -- Pharmaceutical',
	'pharmaceutical'=>'Pharmaceutical',
	'pharmaceutical_add'=>'Add New Pharmaceutical ',
	'pharmaceutical_list'=>'Show All Pharmaceutical',


	'pharmaceutical_element'=>'Pharmaceutical', 
	'pharmaceutical_message_added'=>'New Pharmaceutical  Has Been Added Successfully', 
	'pharmaceutical_message_updated'=>'Pharmaceutical  Has Been Updated Successfully', 

	'pharmaceutical_name'=>'Name',
	'pharmaceutical_count_zero'=>'Sorry , No Pharmaceutical   added yet', 

 
	'pharmaceutical_scientific_name'=>'Scientific Name',
	'pharmaceutical_active_ingredient'=>'Active Ingredient',
	'pharmaceutical_dosage'=>'Docage',
	'pharmaceutical_trade_name'=>'Trade Name',
	'pharmaceutical_medicine_type'=>'Medicine Type',
	'pharmaceutical_description'=>'Description',
	'pharmaceutical_uses'=>'Uses',
	'pharmaceutical_side_effects'=>'Side Effects',
	'pharmaceutical_store_medicine'=>'How To Store Medicine ',
	'pharmaceutical_other'=>'Others',
	'pharmaceutical_Images'=>'Images',
	'pharmaceutical_info'=>'Pharmaceutical Info',

	'pharmaceutical_type_name'=>'Pharmaceutical Type' , 
	'pharmaceutical_manufacture_name'=>'Pharmaceutical Manufacture' , 


	'pharmaceutical_select_images'=>'Choose Images To Upload' , 
	'pharmaceutical_select_images_note'=>'You can choose another images by clicking again', 


	'pharmaceutical_message_added'=>'New Pharmaceutical Has Been Added Successfully'  , 

	'pharmaceutical_show'=>'Pharmaceutical Info', 



	'pharmaceutical_tradeName'=>'Trade Name', 
	'pharmaceutical_scientificName'=>'Scientific Name', 
	'pharmaceutical_activeIngredient'=>'Active Ingredient', 
	'pharmaceutical_description'=>'Description', 
	'pharmaceutical_uses'=>'Uses', 
	'pharmaceutical_dosage'=>'Dosage', 
	'pharmaceutical_sideEffect'=>'Side Effect', 
	'pharmaceutical_storeMedicine'=>'How To Store ', 
	'pharmaceutical_other'=>'Other Information', 

	'pharmaceutical_photos_zero'=>'We couldn\'t find  any images related ' ,
	'pharmaceutical_edit'=>'Edit',




	'pharmaceutical_deleted_successfully'=>'Pharmaceutical Delete Successfully', 


	'latitude'=>'Latitude', 
	'longitude'=>'Longitude', 

	/**
	 * Registration and login labels
	 */
	

	/**
	 * Doctor
	 */
	
	'doctor_name'=>'Doctor Name', 
	 'proffestionPracticeNumber'=>'Proffestion Practice Number', 
	 'hospital_related_to'=>'Hospital Related To', 



	 'select_user_type'=>'Select User Type First',
	 'other'=>'Other' ,

	 'doctor_specialization'=>'Doctor Specializations' , 



	 'user_deleted_successfully'=>'User has been deleted Successfully',



	'footer_copyright'=>'All Rights Reserved For Tatweer Company' , 
	
	//hospital

    'clinics'=>'Clinics',
    'add_clinic'=>'Add Clinic',
    'show_all_clinics'=>'Show All Clinics' ,
    'doctors'=>'Doctors' ,
    'add_doctor'=>'Add Doctor',
    'show_all_doctors'=>'Show All Doctors',
    'reservations'=>'Reservations',
    'add_reservation'=>'Add Reservations',
    'show_all_reservations'=>'Show All Reservations',
    'campaigns'=>'Campaigns',
    'add_campaign'=>'Add Campaigns' ,
    'show_all_campaigns'=>'Show Campaigns' ,
    'drugs_orders'=>'Drugs Orders' ,
    'add_drugs_order'=>'Add Drug Order' ,
    'show_all_drugs_order'=>'Show all Drugs Orders' ,
    'messages'=>'Messages' ,
    'incoming_messages'=>'Incoming Messages' ,
    'send_message'=>'Send Message' ,
    'setting'=>'Setting' ,
    'about_hospital'=>'About Hospital' ,
    'social_media'=>'Social Media' ,
    'visitors_messages'=>'Visitors Messages' ,
    'medical_supplies_orders'=>'Medical Supplies Orders' ,
    'add_medical_supplies_order'=>'Add Medical Supplies Order' ,
    'show_all_medical_supplies_orders'=>'Show Medical Supplies Orders' ,

 //medical supplies dashboard
    
    'medicalSupplies'=>'Medical Supplies',
    'add_medicalSupply'=>'Add Medical Supply',
    'show_all_medicalSupplies'=>'Show All Medical Supplies' ,



        'careers'=>'Careers' , 
         'update_create_career'=>'Update or Create careers' ,  

          'career_title'=>'General Name', 
    'career_description'=>'Career Content', 

    'careers_updated'=>'Careers Page Information has been updated Successfully',

    'customers_module'=>'Customers ' , 
    'customers'=>'Customers' , 
     'add_customer'=>'Add Customer', 
    'list_customers'=>'List Customers' , 
    'customer_name'=>'Customer name' , 
    'customer_email'=>'Customer email', 

    'customer_phone'=>'Telephone', 

    'orders_module'=>'Orders' , 
	'list_orders'=>'List all orders', 

	'order_ticket'=>'Order Ticket' , 
	'order_quantity'=>'Order Quantity' , 
	'order_total'=>'Total Cost' , 
	'order_date'=>'Order Date', 


	'main_cat_icon'=>'Main Category icon'  , 

	'product_quantity'=>'Product quantity', 

	'show_in_slider'=>'Show in slider' , 
	'best_selling'=>'Best Selling Product' , 
 
	'recomended_product'=>'Recommended product', 
	'quantity'=>'Quantity' , 



	'order_notification'=>'Orders Notification' ,
	'no_notifications'=>'No new notifications' , 
	'new_order_txt'=>'New order' , 
	'order_customer_name'=>'To' , 

	'total'=>'Total' , 

	's_r'=>'S.R', 

	'order_info'=>'Order Details', 

	'invoice_date'=>'Order Date' , 

	'product_name'=>'Product' , 

	'product_price'=>'Cost per item' , 

	'product_total'=>'Total' , 



	'delivery'=>'Delivery' , 

	'grand_total'=>'Grand Total' , 

	'print'=>'Print' , 

	'product_image'=>'Product image' , 



	'contact_info'=>'Contact Information' , 


	'slider_module'=>'Slider Module', 
	'add_slide'=>'Add New Slider Image', 

	'list_sliders'=>'List all slides' , 


	'add_slide_notice'=>'Use This form to add / edit slide', 


	'slider_title'=>'Title', 

	'slider_image'=>'Slider Image', 


	'no_slide_found'=>'Sorry , no slide has been added yet', 


	'select_slide_image'=>'Choose Slider image' , 

	'title_required'=>'Please give slide a  title', 
	'file_required'=>'Please make sure to select slide image',


	'publish_options'=>'Publish options',  

	'slide_added'=>'A new slide has been added successfully', 

	'update_slide_info'=>'Update Slide information', 


	'slide_updated'=>'Slide info has been updated successfully', 


	'slide_deleted'=>'Slide Has been deleted successfully', 


	'image'=>'Image', 



   



];



 ?>