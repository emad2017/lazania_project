<?php
/**
 * Created by PhpStorm.
 * User: mahmoud
 * Date: 10/27/17
 * Time: 3:30 PM
 */



return [
   // speacialevent
     'show_event'  => 'Show Speacial Event',
     'add_event'  => 'Add Speacial Event',
     'speacialevent'  => 'Speacial Event',

    'speacialevent_name' => 'Speacial Event ',
    'speacialevent_create' => 'Add New Speacial Event',
    'speacialevent_show' => 'Show All Speacial Event',
    'speacialevent_name_' => 'Event Name',

    'speacialevent_description_' => 'Event Description',
    'speacialevent_edit' => 'Edit Speacial Event',

    'speacialevent_img' => 'Speacial Event image',
    'speacialevent_status' => 'Status',
    'speacialevent_addby' => 'Add By',
    'speacialevent_date' => 'Created at',
    'speacialevent_zero' => 'Sorry , No Speacial Event added yet',
    'speacialevent_added'=>'New Speacial Event Has been added successfully' ,
    'speacialevent_updated'=>'Speacial Event Has been updated successfully',
    'speacialevent_deleted'=>'Speacial Event has been deleted successfully' ,


    // eventitem
    'show_eventitem'  => 'Show Speacial Event item',
    'add_eventitem'  => 'Add Speacial Event item',
    'eventitem'  => 'Speacial Event item',

    'eventitem_name' => 'Speacial Event item',
    'eventitem_create' => 'Add New Speacial Event item',
    'eventitem_show' => 'Show All Speacial Event item',
    'eventitem_name_' => 'item Name',

    'eventitem_description_' => 'item Description',
    'eventitem_edit' => 'Edit Speacial Event item',

    'eventitem_img' => 'Speacial Event item image',
    'eventitem_status' => 'Status',
    'eventitem_addby' => 'Add By',
    'eventitem_date' => 'Created at',
    'eventitem_zero' => 'Sorry , No  Event item added yet',
    'eventitem_added'=>'New Speacial Event item Has been added successfully' ,
    'eventitem_updated'=>'Speacial Event item Has been updated successfully',
    'eventitem_deleted'=>'Speacial Event item has been deleted successfully' ,



];