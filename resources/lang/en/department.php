<?php 

return [

	'departments'=>'Departments' , 
	'department_module_managment'=>'Controlling Site Sections' , 
 	'add_department'=>'Add New Department' , 
	'list_departments'=>'List All Departments', 
	'department_name'=>'Department Name', 
	'department_description'=>'Content', 
	'department_slug'=>'Slug',
	'department_parent'=>'Belongs To', 
	'department_status'=>'Status',
	'department_added_by'=>'Added By',
	'department_date_added'=>'Date Added', 
	'departments_count_zero'=>'No Departments has been added yet', 
	'department_photos'=>'Photos', 
	'select_department'=>'Select Department', 
	'select_images'=>'Select images to be added to this department', 
	'department_added'=>'New Department has been added successfully', 
	'department_edit'=>'Update Department Info',
	'department_updated'=>'Department Info has been updated successfully',
	'show_sub_departments'=>'Show sub departments' , 
	'department_deleted'=>'Department has been deleted successfully' , 



	'department_swal_deleted_success'=>'Delete Went successfully ' , 
	'department_swal_deleted_success_message'=>'Department has been deleted successfully' , 

	'department_swal_deleted_error'=>'Warning Message - Take Care !' ,
	'department_swal_deleted_error_message'=>'You cant delete this department cause it contains other sub departments  ' , 

	'are_you_sure'=>'Are You Sure ?'  , 
	'data_losted'=>'Deleted Data cant be retsored again !' ,

	'yes_delete'=>'Yes , Delete it' , 
	'no_delete'=>'No , Go back', 

];

 ?>